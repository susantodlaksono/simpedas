$(function () {
	
   _dir = '';

	$.fn.getting = function(option){
      var param = $.extend({
         dir : _dir,
         first_load : false,
         order : 'clean_time',
         orderby : 'desc'
      }, option);
      
      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'widgets/dir_dms/read_dir',
         dataType : "JSON",
         data : {
            dir : param.dir,
            order : param.order,
            orderby : param.orderby
         },
         beforeSend: function (xhr) {
         	if(!param.first_load){
         		spinner(container, 'block');
         	}
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){
            var t = '';
            if(r.result){
               if(r.result.total){
                  var total = r.result.total;
                  $.each(r.result.data, function(k,v){
                  	//init metadata
                  	t += '<input id="name-'+k+'" type="hidden" value="'+k+'">';
       					t += '<input id="metaname-'+k+'" type="hidden" value="'+v.name+'">';
       					t += '<input id="metanameencode-'+k+'" type="hidden" value="'+v.url+'">';
       					t += '<input id="metaext-'+k+'" type="hidden" value="'+(v.ext_orig ? v.ext_orig : '')+'">';
       					t += '<input id="metalock-'+k+'" type="hidden" value="'+(v.lock ? v.lock : '')+'">';
       					t += '<input id="metashare-'+k+'" type="hidden" value="'+(v.share ? v.share : '')+'">';
       					if(param.dir){
       						t += '<input id="metadir-'+k+'" type="hidden" value="'+r.dir_base+'/'+param.dir+'">';
       					}else{
       						t += '<input id="metadir-'+k+'" type="hidden" value="'+r.dir_base+'">';
       					}

                  	t += '<tr>';
	          				t += '<td class="mailbox-star">';
	          				if(v.favorites){
	          					t += '&nbsp;<i class="fa fa-star text-yellow" data-toggle="tooltip" data-title="Favorites"></i>';
	          				}
	          				if(v.lock){
	          					t += '&nbsp;<i class="fa fa-lock text-danger" data-toggle="tooltip" data-title="Locked document"></i>';
	          				}
	          				if(v.share){
	          					t += '&nbsp;<i class="fa fa-share-alt text-info" data-toggle="tooltip" data-title="Shared document"></i>';
	          				}
	          				t += '</td>';
	          				t += '<td class="mailbox-name">';
	          					if(param.dir){
	          						if(v.is_file){
	          							t += '<a href="'+base_url+'main/download?name='+v.url+'&dir='+r.dir_base+'/'+param.dir+'/'+v.url+'" data-id="'+k+'" style="cursor:pointer;">';
	          						}else{
	          							t += '<a data-href="'+param.dir+'/'+v.url+'" style="cursor:pointer;" class="change-dir">';
	          						}
	          					}else{
	          						if(v.is_file){
	          							t += '<a href="'+base_url+'main/download?name='+v.name+'&dir='+r.dir_base+'/'+v.url+'" data-id="'+k+'" style="cursor:pointer;">';
	          						}else{
	          							t += '<a data-href="'+v.url+'" style="cursor:pointer;" class="change-dir">';
	          						}
	          					}
	          					if(!v.is_file){
	          						t += '<i class="fa fa-folder"></i>&nbsp;';
	          					}else{
	          						if(v.ext){
	          							t += '<i class="'+v.ext+'"></i>&nbsp;';
	          						}
	          					}
	          					t += v.name
	          					t += '</a>';
	          				t += '</td>';
	          				t += '<td class="mailbox-attachment">';
	          					t += '<span class="dropdown dropdown-'+k+'">';
	       				 			t += '<button class="btn dropdown-toggle btn-default btn-xs show-options " data-toggle="dropdown">';
	       				 				t += '<i class="fa fa-ellipsis-h"></i>';
	       				 			t += '</button>';
	       				 			t += '<ul class="dropdown-menu" style="left: -138px;border: 1px solid #cccccc;">';
	       				 				if(v.is_file){
	       				 					t += '<li>';
       				 							t += '<a data-id="'+k+'" class="assign-request dropdown-options-dms" data-toggle="modal" data-target="#modal-assign" href="">';
       				 								t += '<i class="fa fa-share"></i>Share to request';
       				 							t += '</a>';
       				 						t += '</li>';
       				 						t += '<li class="divider"></li>';
	       				 				}
	       				 				if(!v.favorites){
       				 						t += '<li>';
       				 							t += '<a data-st="3" data-id="'+k+'" class="assign-flag dropdown-options-dms" href="">';
       				 								t += '<i class="fa fa-star"></i>Add to favorit';
       				 							t += '</a>';
       				 						t += '</li>';		       				 			
    				 						}else{
    				 							t += '<li>';
    				 								t += '<a data-st="3" data-id="'+v.favorites+'" class="remove-flag text-danger dropdown-options-dms" href="">';
    				 									t += '<i class="fa fa-star"></i><span class="text-danger">Remove favorites</span>';
    				 								t += '</a>';
    				 							t += '</li>';
    				 						}
	       				 				if(v.is_file){
	       				 					if(!v.lock){
	       				 						t += '<li class="divider"></li>';
	       				 						t += '<li>';
	       				 							t += '<a data-st="1" data-id="'+k+'" class="assign-flag dropdown-options-dms" href="">';
	       				 								t += '<i class="fa fa-lock"></i>Lock';
	       				 							t += '</a>';
	       				 						t += '</li>';
	       				 					}else{
	       				 						t += '<li class="divider"></li>';
	       				 						t += '<li>';
	       				 							t += '<a data-st="1" data-id="'+v.lock+'" class="remove-flag text-danger dropdown-options-dms" href="">';
	       				 								t += '<i class="fa fa-lock"></i><span class="text-danger">Remove lock</span>';
	       				 							t += '</a>';
	       				 						t += '</li>';
	       				 					}

	       				 					if(!v.share){
	       				 						t += '<li class="divider"></li>';
	       				 						t += '<li>';
	       				 							t += '<a data-st="2" data-id="'+k+'" class="assign-flag dropdown-options-dms" href="">';
	       				 								t += '<i class="fa fa-share-alt"></i>Share';
	       				 							t += '</a>';
       				 							t += '</li>';
       				 						}else{
       				 							t += '<li class="divider"></li>';
       				 							t += '<li>';
       				 								t += '<a data-st="2" data-id="'+v.share+'" class="remove-flag text-danger dropdown-options-dms" href="">';
       				 									t += '<i class="fa fa-share-alt"></i><span class="text-danger">Remove share</span>';
       				 								t += '</a>';
       				 							t += '</li>';
       				 						}

       				 						t += '<li class="divider"></li>';
       				 						t += '<li>';
	       				 						t += '<a data-id="'+k+'" class="delete-file text-danger dropdown-options-dms" href="">';
	       				 							t += '<i class="fa fa-trash"></i>Delete File';
	       				 						t += '</a>';
       				 						t += '</li>';
       				 					}else{
       				 						t += '<li class="divider"></li>';
       				 						t += '<li>';
	       				 						t += '<a data-id="'+k+'" class="delete-folder text-danger dropdown-options-dms" href="">';
	       				 							t += '<i class="fa fa-trash"></i>Delete Folder';
	       				 						t += '</a>';
       				 						t += '</li>';
       				 					}
	       				 			t += '</ul>';
       				 			t += '</span>';
	       				 	t += '</td>';
	                    	t += '<td class="mailbox-date">'+v.time+'</td>';
	                    	t += '<td class="mailbox-size">'+v.size+'</td>';
                    	t += '</tr>';
                  });
               }else{
                  t += '<tr><td class="text-center text-muted" colspan="7">No Result</td></tr>';
               }
            }
            $(container_wg).find('.sect-data').html(t);
            $(container_wg).find('.sect-nav-render').html('');
            $(container_wg).find('.sect-nav-render').html(r.nav);
            // $('.request-rating').rating({
            // 	displayOnly: true
            // });
         },
         complete : function(r){
      		spinner(container, 'unblock');
      	}
      });
   }

   // $(this).on('click', ''+container_wg+' .add-more-folder',function(e){
   //    e.preventDefault();
   //    var total = $("#modal-upload-folder .modal-body > .form-group").length;
   //    if(total < 5){
   //       t = '';
   //       t += '<div class="form-group form-group-added">';
   //       t += '<input type="text" class="form-control input-sm" name="folder_name[]" placeholder="Nama folder..." required="">';
   //       t += '</div>';
   //       $('#modal-upload-folder .modal-body').prepend(t);
   //    }else{
   //       alert('Maksimal 5 folder setiap submit');
   //    }
   // });

   $(this).on('submit', '#form-upload-folder', function(e){
      var form = $(this);
      $(this).ajaxSubmit({
         url : site_url + 'widgets/dir_dms/upload_folder',
         type : "POST",
         data : {
            dir : _dir
         },
         dataType : "JSON",
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
            loading_form(form, 'hide', '<i class="fa fa-upload"></i> Upload');
         },
         beforeSend: function (xhr) {
            loading_form(form, 'show', loadingbutton);
         },
         success: function(r) {
            session_checked(r._session, r._maintenance);
            if(r.success){                
               $(this).getting();
               $("#modal-upload-folder").modal("toggle");
               form.resetForm();
            }else{
               toastr.error(r.msg);
            }
            loading_form(form, 'hide', '<i class="fa fa-upload"></i> Upload');
         },
      });
      e.preventDefault();
   });

   $(this).on('click', ''+container_wg+' .delete-folder',function(e){
		e.preventDefault();
		id = $(this).attr('data-id');
		name = $('#metanameencode-'+id+'').val();
		dir = $('#metadir-'+id+'').val();
	 	ajaxManager.addReq({
         type : "POST",
         url : site_url + 'widgets/dir_dms/delete_folder',
         dataType : "JSON",
         data : {
            name : name,
            dir : dir,
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){
         	session_checked(r._session, r._maintenance);
            if(r.success){
               $(this).getting();
               toastr.success(r.msg);
            }else{
            	toastr.error(r.msg);
            }
      	}
   	});
	});

   $(this).on('click', ''+container_wg+' .change-dir',function(e){
      e.preventDefault();
      _dir = $(this).attr('data-href');
      $(this).getting();
   });

   $(this).on('click', ''+container_wg+' .delete-file',function(e){
		e.preventDefault();
		id = $(this).attr('data-id');
		name = $('#metanameencode-'+id+'').val();
		dir = $('#metadir-'+id+'').val();
	 	ajaxManager.addReq({
         type : "POST",
         url : site_url + 'widgets/dir_dms/delete_file',
         dataType : "JSON",
         data : {
            name : name,
            dir : dir
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){
         	session_checked(r._session, r._maintenance);
            if(r.success){
               $(this).getting();
               toastr.success(r.msg);
            }else{
            	toastr.error(r.msg);
            }
      	}
   	});
	});

   $(this).on('click', ''+container_wg+' .assign-flag',function(e){
		e.preventDefault();
		id = $(this).attr('data-id');
		stat = $(this).attr('data-st');
		name = $('#metaname-'+id+'').val();
		dir = $('#metadir-'+id+'').val();
		ext = $('#metaext-'+id+'').val();
	 	ajaxManager.addReq({
         type : "POST",
         url : site_url + 'widgets/dir_dms/assign_flag',
         dataType : "JSON",
         data : {
            name : name,
            dir : dir,
            status : stat,
            ext : ext
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){
         	session_checked(r._session, r._maintenance);
            if(r.success){
               $(this).getting();
               $('.dropdown-'+id+'').removeClass('open');
            }else{
            	toastr.error(r.msg);
            }
      	}
   	});
	});

	$(this).on('click', ''+container_wg+' .remove-flag',function(e){
		e.preventDefault();
		id = $(this).attr('data-id');
		status = $(this).attr('data-st');
		if(id != ''){
		 	ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'widgets/dir_dms/remove_flag',
	         dataType : "JSON",
	         data : {
	            id : id,
	            status : status
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	         },
	         success : function(r){
	         	session_checked(r._session, r._maintenance);
	            if(r.success){
	               $(this).getting();
	            }else{
	            	toastr.error(r.msg);
	            }
         	}
      	});
   	}else{
   		alert('ID Not Found');
   	}
	});

   $(this).on('click', ''+container_wg+' .breadcrumb-action-home', function(e){
   	Widget.Loader('dir_dms', {
			stop_loader : false
		}, 'cont-widget-dms');
   });
   
   $(this).on('click', ''+container_wg+' .breadcrumb-action',function(e){
         e.preventDefault();
         _dir = $(this).attr('data-href');
         $(this).getting();
      });

   $(this).on('click', ''+container_wg+' .change_order', function(e){
      e.preventDefault();
      $('.change_order').html('<i class="fa fa-sort"></i>');
      $(this).find('i').remove();
      var sent = $(this).data('order');
      var by = $(this).attr('data-by');
      if(by === 'asc'){ 
         $(this).attr('data-by', 'desc');
         $(this).html('<i class="fa fa-sort-asc"></i>');
      }
      else{ 
         $(this).attr('data-by', 'asc');
         $(this).html(' <i class="fa fa-sort-desc"></i>');
      }
     $(this).getting({order:sent,orderby:by});
   });

   $(this).getting({
   	first_load : true
   });

});