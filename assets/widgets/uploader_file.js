
function ui_add_log(message, color){
	var d = new Date();
  	var dateString = (('0' + d.getHours())).slice(-2) + ':' +
 	(('0' + d.getMinutes())).slice(-2) + ':' +
 	(('0' + d.getSeconds())).slice(-2);

  	color = (typeof color === 'undefined' ? 'muted' : color);

  	var template = $('#debug-template').text();
  	template = template.replace('%%date%%', dateString);
  	template = template.replace('%%message%%', message);
  	template = template.replace('%%color%%', color);
  
  	$('#debug').find('li.empty').fadeOut();
  	$('#debug').prepend(template);
}

function ui_multi_add_file(id, file){
	var template = $('#files-template').text();
  	template = template.replace('%%filename%%', file.name);

  	template = $(template);
  	template.prop('id', 'uploaderFile' + id);
  	template.data('file-id', id);

  	$('#files').find('li.empty').fadeOut();
  	$('#files').prepend(template);
}

function ui_multi_update_file_status(id, status, message){
	$('#uploaderFile' + id).find('span').html(message).prop('class', 'label label-' + status);
}

function ui_multi_update_file_progress(id, percent, color, active){
	color = (typeof color === 'undefined' ? false : color);
  	active = (typeof active === 'undefined' ? true : active);

  	var bar = $('#uploaderFile' + id).find('div.progress-bar');

  	bar.width(percent + '%').attr('aria-valuenow', percent);
  	bar.toggleClass('progress-bar-striped progress-bar-animated', active);

  	if (percent === 0){
    	bar.html('');
  	} else {
 		bar.html(percent + '%');
  	}

  	if (color !== false){
 		bar.removeClass('bg-success bg-info bg-warning bg-danger');
    	bar.addClass('bg-' + color);
  	}
}

$(function () {

	$(container_wg+' .drag-and-drop-zone').dmUploader({ //
		url: site_url + 'widgets/dir_dms/upload_file',
		extFilter: ["jpg", "jpeg", "png", "gif", "pdf", "docx", "xls", "xlsx", "pptx", "ppt", "zip", "rar", "csv", "txt"],
		extraData: function() {
		   return {
		     "dir": _dir
		   };
		},
    	onDragEnter: function(){
      	this.addClass('active');
    	},
    	onDragLeave: function(){
      	this.removeClass('active');
    	},
    	onNewFile: function(id, file){
	      ui_multi_add_file(id, file);
    	},
    	onBeforeUpload: function(id){
	      ui_multi_update_file_status(id, 'uploading', 'Uploading...');
	      ui_multi_update_file_progress(id, 0, '', true);
    	},
    	onUploadCanceled: function(id) {
	      ui_multi_update_file_status(id, 'warning', 'Canceled by User');
	      ui_multi_update_file_progress(id, 0, 'warning', false);
    	},
    	onUploadProgress: function(id, percent){
	      ui_multi_update_file_progress(id, percent);
    	},
    	onUploadSuccess: function(id, data){
      	ui_multi_update_file_status(id, 'success', 'Upload Complete');
	      ui_multi_update_file_progress(id, 100, 'success', false);
    	},
    	onComplete: function(){
    		$(this).getting();
    	},
    	onUploadError: function(id, xhr, status, message){
	    	alert(message);
	      // ui_multi_update_file_status(id, 'danger', message);
	      // ui_multi_update_file_progress(id, 0, 'danger', false);  
    	},
     	onFileSizeError: function(file){
      	alert('Ukuran file \'' + file.name + '\' melebihi batas limit');
    	},
    	onFileExtError:function(file){
    		alert('Type pada File \'' + file.name + '\' tidak sesuai');
    	}
  	});
});