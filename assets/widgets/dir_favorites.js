$(function () {

	_dir_fav = '';

	$.fn.getting_dir = function(option){
      var param = $.extend({
         dir : _dir_fav,
         order : 'clean_time',
         first_load : false,
         orderby : 'desc'
      }, option);
      
      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'widgets/dir_favorites/read_dir',
         dataType : "JSON",
         data : {
            dir : param.dir,
            order : param.order,
            orderby : param.orderby
         },
         beforeSend: function (xhr) {
         	if(!param.first_load){
         		spinner(container, 'block');
         	}
         	$('#'+widget_name+'_'+unixid).find('.sect-nav-render').removeClass('hidden');
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){
            var t = '';
            if(r.result){
               if(r.result.total){
                  var total = r.result.total;
                  $.each(r.result.data, function(k,v){
                  	//init metadata
                  	t += '<input id="name-'+k+'" type="hidden" value="'+k+'">';
       					t += '<input id="metaname-'+k+'" type="hidden" value="'+v.name+'">';
       					t += '<input id="metanameencode-'+k+'" type="hidden" value="'+v.url+'">';
       					t += '<input id="metaext-'+k+'" type="hidden" value="'+(v.ext_orig ? v.ext_orig : '')+'">';
       					t += '<input id="metalock-'+k+'" type="hidden" value="'+(v.lock ? v.lock : '')+'">';
       					t += '<input id="metashare-'+k+'" type="hidden" value="'+(v.share ? v.share : '')+'">';
       					if(param.dir){
       						t += '<input id="metadir-'+k+'" type="hidden" value="'+r.dir_base+'/'+param.dir+'">';
       					}else{
       						t += '<input id="metadir-'+k+'" type="hidden" value="'+r.dir_base+'">';
       					}

                  	t += '<tr>';
	          				t += '<td class="mailbox-star">';
	          				if(v.favorites){
	          					t += '&nbsp;<i class="fa fa-star text-yellow" data-toggle="tooltip" data-title="Favorites"></i>';
	          				}
	          				if(v.lock){
	          					t += '&nbsp;<i class="fa fa-lock text-danger" data-toggle="tooltip" data-title="Locked document"></i>';
	          				}
	          				if(v.share){
	          					t += '&nbsp;<i class="fa fa-share-alt text-info" data-toggle="tooltip" data-title="Shared document"></i>';
	          				}
	          				t += '</td>';
	          				t += '<td class="mailbox-name">';
	          					if(param.dir){
	          						if(v.is_file){
	          							t += '<a href="'+base_url+'main/download?name='+v.url+'&dir='+r.dir_base+'/'+param.dir+'/'+v.url+'" data-id="'+k+'" style="cursor:pointer;">';
	          						}else{
	          							t += '<a data-href="'+param.dir+'/'+v.url+'" style="cursor:pointer;" class="change-dir-widget">';
	          						}
	          					}else{
	          						if(v.is_file){
	          							t += '<a href="'+base_url+'main/download?name='+v.name+'&dir='+r.dir_base+'/'+v.url+'" data-id="'+k+'" style="cursor:pointer;">';
	          						}else{
	          							t += '<a data-href="'+v.url+'" style="cursor:pointer;" class="change-dir-widget">';
	          						}
	          					}
	          					if(!v.is_file){
	          						t += '<i class="fa fa-folder"></i>&nbsp;';
	          					}else{
	          						if(v.ext){
	          							t += '<i class="'+v.ext+'"></i>&nbsp;';
	          						}
	          					}
	          					t += v.name
	          					t += '</a>';
	          				t += '</td>';
	          				t += '<td class="mailbox-attachment">';
	          					if(v.favorites){
	          						t += '<span class="dropdown dropdown-'+k+'">';
		       				 			t += '<button class="btn dropdown-toggle btn-default btn-xs show-options " data-toggle="dropdown">';
		       				 				t += '<i class="fa fa-ellipsis-h"></i>';
		       				 			t += '</button>';
		       				 			t += '<ul class="dropdown-menu" style="left: -138px;border: 1px solid #cccccc;">';
	    				 						t += '<li>';
	 				 								t += '<a data-st="3" data-id="'+v.favorites+'" class="remove-flag text-danger dropdown-options-dms" href="">';
	 				 									t += '<i class="fa fa-star"></i><span class="text-danger">Remove favorites</span>';
	 				 								t += '</a>';
	 				 							t += '</li>';	       				 			
		       				 			t += '</ul>';
	       				 			t += '</span>';
	          					}
	       				 	t += '</td>';
	                    	t += '<td class="mailbox-date">'+v.time+'</td>';
	                    	t += '<td class="mailbox-size">'+v.size+'</td>';
                    	t += '</tr>';
                  });
               }else{
                  t += '<tr><td class="text-center text-muted" colspan="7">No Result</td></tr>';
               }
            }

            $('#'+widget_name+'_'+unixid).find('.sect-total').html(''+number_format(total)+' Rows Data');
            $('#'+widget_name+'_'+unixid).find('.sect-data').html(t);
            $('#'+widget_name+'_'+unixid).find('.sect-nav-render').html('');
            $('#'+widget_name+'_'+unixid).find('.sect-nav-render').html(r.nav);
         },
         complete: function(){
         	spinner(container, 'unblock');
         }
      });
   }


	$.fn.get_favorites = function(option){
      var param = $.extend({
         dir : _dir_fav,
         first_load : false,
         order : 'clean_time',
         orderby : 'desc'
      }, option);
      
      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'widgets/dir_favorites/get_favorites',
         dataType : "JSON",
         data : {
            dir : param.dir,
            order : param.order,
            orderby : param.orderby
         },
         beforeSend: function (xhr) {
         	$('#'+widget_name+'_'+unixid).find('.sect-nav-render').addClass('hidden');
         	if(!param.first_load){
         		spinner(container, 'block');
         	}
          	// $('#'+widget_name+'_'+unixid).find('.sect-data').html('<tr><td colspan="10" class="text-center">Please wait...</td></tr>');
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){
         	var t = '';
         	if(r.total > 0){
         		$.each(r.result, function(k,v){
         			t += '<tr>';
          				t += '<td class="mailbox-star"><i class="fa fa-star text-yellow" data-toggle="tooltip" data-title="Favorites"></i></td>';
          				t += '<td class="mailbox-name">';
          				if(v.is_file){
    							t += '<a href="'+base_url+'main/download?name='+v.encode_file_name+'&dir='+v.encode_full_dir+'" data-id="'+k+'" style="cursor:pointer;">';
    						}else{
    							t += '<a data-href="'+v.encode_file_name+'" style="cursor:pointer;" class="change-dir-widget">';
    						}
    						if(!v.is_file){
       						t += '<i class="fa fa-folder"></i>&nbsp;';
       					}else{
       						if(v.file_extension){
       							t += '<i class="'+v.file_extension+'"></i>&nbsp;';
       						}
       					}
    						t += v.file_name;
    						t += '</a>';
       					t += '</td>';
       					t  += '<td class="mailbox-attachment">';
          					t += '<span class="dropdown dropdown-'+k+'">';
       				 			t += '<button class="btn dropdown-toggle btn-default btn-xs show-options " data-toggle="dropdown">';
       				 				t += '<i class="fa fa-ellipsis-h"></i>';
       				 			t += '</button>';
       				 			t += '<ul class="dropdown-menu" style="left: -138px;border: 1px solid #cccccc;">';
       				 				t += '<li>';
			 								t += '<a data-st="3" data-id="'+v.id+'" class="remove-flag text-danger dropdown-options-dms" href="">';
			 									t += '<i class="fa fa-star"></i><span class="text-danger">Remove favorites</span>';
			 								t += '</a>';
			 							t += '</li>';
       				 			t += '</ul>';
    				 			t += '</span>';
       				 	t += '</td>';
       				 	t += '<td class="mailbox-date">'+v.created_date+'</td>';
       				 	t += '<td class="mailbox-size">'+v.size+'</td>';
       				t += '</tr>';
      			});
      		}else{
      			t += '<tr><td class="text-center text-muted" colspan="7">No Result</td></tr>';
      		}
      		$('#'+widget_name+'_'+unixid).find('.sect-total').html(''+number_format(r.total)+' Rows Data');
            $('#'+widget_name+'_'+unixid).find('.sect-data').html(t);
      	},
      	complete : function(r){
      		spinner(container, 'unblock');         	
      	}
   	});
	}

	$(this).on('click', '#'+widget_name+'_'+unixid+' .change-dir-widget',function(e){
		e.preventDefault();
		_dir_fav = $(this).attr('data-href');
		$('#'+widget_name+'_'+unixid+' .co').removeClass('change_order');
		$('#'+widget_name+'_'+unixid+' .co').addClass('change_order_dms');
		$(this).getting_dir();
	});

	$(this).on('click', '#'+widget_name+'_'+unixid+' .breadcrumb-action-favorites',function(e){
		e.preventDefault();
		_dir_fav = $(this).attr('data-href');
		$(this).getting_dir();
	});

	$(this).on('click', '#'+widget_name+'_'+unixid+' .breadcrumb-action-favorites-home',function(e){
		e.preventDefault();
		Widget.Loader('dir_favorites', {
			stop_loader : false
		}, 'cont-widget-dms');
	});

	$(this).on('click', '#'+widget_name+'_'+unixid+' .change_order', function(e){
      e.preventDefault();
      $('.change_order').html('<i class="fa fa-sort"></i>');
      $(this).find('i').remove();
      var sent = $(this).data('order');
      var by = $(this).attr('data-by');
      if(by === 'asc'){ 
         $(this).attr('data-by', 'desc');
         $(this).html('<i class="fa fa-sort-asc"></i>');
      }
      else{ 
         $(this).attr('data-by', 'asc');
         $(this).html(' <i class="fa fa-sort-desc"></i>');
      }
     $(this).get_favorites({order:sent,orderby:by});
   });

	$(this).on('click', '#'+widget_name+'_'+unixid+' .change_order_dms', function(e){
      e.preventDefault();
      $('.change_order_dms').html('<i class="fa fa-sort"></i>');
      $(this).find('i').remove();
      var sent = $(this).data('order');
      var by = $(this).attr('data-by');
      if(by === 'asc'){ 
         $(this).attr('data-by', 'desc');
         $(this).html('<i class="fa fa-sort-asc"></i>');
      }
      else{ 
         $(this).attr('data-by', 'asc');
         $(this).html(' <i class="fa fa-sort-desc"></i>');
      }
     $(this).getting_dir({order:sent,orderby:by});
   });

	$(this).get_favorites({
		first_load : true
	});

});