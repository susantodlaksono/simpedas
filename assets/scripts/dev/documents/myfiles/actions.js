$(function () {

	$(this).on('click', '#'+_uniqid+' .assign-request',function(e){
      e.preventDefault();
		$('#keywordreq').val('');
      $('#modal-assign-files').modal('show');
      $(this).request();
   });

   $(this).on('click', '#'+_uniqid+' .share-doc',function(e){
      e.preventDefault();
		$('#keywordopd').val('');
      $('#modal-share-opd').modal('show');
      $(this).opd();
   });

   $(this).on('click', '#'+_uniqid+' .detail-assign-request',function(e){
      e.preventDefault();
      $('#modal-detail-assign').modal('show');
      var id = $(this).data('id');
      _name_selected = $('#metaname-'+id+'').val();
      _dir_selected = $('#metadir-'+id+'').val();
      $(this).detail_assign();
   });

   $(this).on('click', '#'+_uniqid+' .detail-share',function(e){
      e.preventDefault();
      $('#modal-detail-share').modal('show');
      var id = $(this).data('id');
      _name_selected = $('#metaname-'+id+'').val();
      _dir_selected = $('#metadir-'+id+'').val();
      $(this).detail_share();
   });

   $(this).on('change', '#'+_uniqid+' .keywordreq',function(e){
   	__offsetreq = 0;
   	_pagereq = 1;
   	$(this).request();
   });

   $(this).on('change', '#'+_uniqid+' .keywordopd',function(e){
   	__offsetopd = 0;
   	_pageopd = 1;
   	$(this).opd();
   });

	$(this).on('click', '#'+_uniqid+' .breadcrumb-action',function(e){
      e.preventDefault();
      _dir = $(this).attr('data-href');
      $(this).dir();
   });

	$(this).on('click', '#'+_uniqid+' .breadcrumb-action-home', function(e){
   	e.preventDefault();
      _dir = '';
      $(this).dir();
   });

   $(this).on('click', '#'+_uniqid+' .choose-search', function(e){
      e.preventDefault();
      _dir = $(this).attr('href');
      $('#modal-search-files').modal('hide');
      $(this).dir();
   });

   $(this).on('click', '#'+_uniqid+' .rename',function(e){
      e.preventDefault();
      var id = $(this).data('id');
      var fname = $(this).data('fname');
      name = $('#metaname-'+id+'').val();
      dir = $('#metadir-'+id+'').val();
      ext = $('#metaext-'+id+'').val();
      fav = $('#metafav-'+id+'').val();
      $('#'+_uniqid+' .form-rename').find('input[name="dir"]').val(dir);
      $('#'+_uniqid+' .form-rename').find('input[name="name"]').val(name);
      $('#'+_uniqid+' .form-rename').find('input[name="fav"]').val(fav);
      $('#'+_uniqid+' .form-rename').find('input[name="ext"]').val(ext);
      $('#modal-rename').find('.modal-title-rename').html(fname);
      $('#modal-rename').modal('show');
   });
   
   $(this).on('click', '#'+_uniqid+' .option_choose',function(e){
   	arrfiles = [];
   	fav_id = [];
     	if(this.checked){
         $('.option_check').each(function(){
           	var id = $(this).val();
	   	 	name = $('#metaname-'+id+'').val();
				dir = $('#metadir-'+id+'').val();
				ext = $('#metaext-'+id+'').val();
				ternary = $('#metaternary-'+id+'').val();
				fav = $('#metafav-'+id+'').val();

				this.checked = true; 

				arrfiles.push({
	            id: id, 
	            name: name, 
	            dir:  dir,
	            ternary:  ternary,
	            ext:  (ext ? ext : 'folder')
        		});
        		if(fav !== ''){
	     			fav_id.push({
	            	id: fav 
	         	});
	     		}
         });
         $('.row-options-selected').removeClass('hidden');
     	}else{
     		$('.row-options-selected').addClass('hidden');
         $('.option_check').each(function(){
            this.checked = false; 
         });
     	}
 	});

 	$(this).on('click', '#'+_uniqid+' .option_check',function(e){
   	var id = $(this).val();
	 	name = $('#metaname-'+id+'').val();
		dir = $('#metadir-'+id+'').val();
		ext = $('#metaext-'+id+'').val();
		ternary = $('#metaternary-'+id+'').val();
		fav = $('#metafav-'+id+'').val();

		if($(this).is(':checked')){
			if($('.row-options-selected').hasClass('hidden')){
				$('.row-options-selected').removeClass('hidden');
			}
	  		arrfiles.push({
            id: id, 
            name: name,
            dir:  dir,
            ternary:  ternary,
            ext:  (ext ? ext : 'folder')
     		});

     		if(fav !== ''){
     			fav_id.push({
            	id: fav 
         	});
     		}

    	} else {
			arrfiles = arrfiles.filter(function(elem) {  
			  return elem.id !== id; 
			}); 
			fav_id = fav_id.filter(function(elem) {  
			  return elem.id !== id; 
			}); 
    	}
    	if(arrfiles.length == 0){
    		$('.row-options-selected').addClass('hidden');
    	}
   });

 	$(this).on('click', '#'+_uniqid+' .assign-flag-bulk',function(e){
		e.preventDefault();
		stat = $(this).attr('data-st');
	 	ajaxManager.addReq({
         type : "POST",
         url : site_url + 'documents/myfiles/bulk_flag',
         dataType : "JSON",
         data : {
            files : arrfiles,
            status : stat
         },
       	beforeSend: function (xhr) {
      		spinner('#cont-widget-dms', 'block');
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){
         	session_checked(r._session, r._maintenance);
            if(r.success){
               $(this).dir();
            }else{
            	toastr.error(r.msg);
            }
      	},
      	complete: function(){
      		$(this).bulk_option_reset();
      		spinner('#cont-widget-dms', 'unblock');
      	}
   	});
	});

	$(this).on('click', '#'+_uniqid+' .remove-flag-bulk',function(e){
		e.preventDefault();
		var conf = confirm('Yakin ingin menghapus?');
		if(conf){
			ajaxManager.addReq({
	         type : "POST",
	         url : site_url + 'documents/myfiles/bulk_remove',
	         dataType : "JSON",
	         data : {
	            files : arrfiles,
	            fav : fav_id
	         },
	         beforeSend: function (xhr) {
	      		spinner('#cont-widget-dms', 'block');
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	         },
	         success : function(r){
	         	$(this).bulk_option_reset();
	         	session_checked(r._session, r._maintenance);
	            if(r.success){
	               $(this).dir();
	            }else{
	            	toastr.error(r.msg);
	            }
	      	},
	      	complete: function(){
	      		$(this).bulk_option_reset();
	      		spinner('#cont-widget-dms', 'unblock');
	      	}
	   	});
		}else{
			return false;
		}
	});

	$(this).on('click', '#'+_uniqid+' .download-bulk',function(e){
		e.preventDefault();
		stat = $(this).attr('data-st');
	 	ajaxManager.addReq({
         type : "POST",
         url : site_url + 'documents/myfiles/zipped_bulk',
         dataType : "JSON",
         data : {
            files : arrfiles,
            filename : _dir_active_name
         },
         beforeSend: function (xhr) {
      		spinner('#cont-widget-dms', 'block');
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success: function(r){
         	session_checked(r._session, r._maintenance);
         	window.location.href = 'documents/myfiles/download_bulk?name='+r.title+'&dir=download/'+r.filename+'.zip';
      	},
      	complete: function(){
      		$(this).bulk_option_reset();
      		spinner('#cont-widget-dms', 'unblock');
      	}
   	});
	});

});