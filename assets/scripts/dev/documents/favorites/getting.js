$(function () {

	_dir = '';
	_dir_active_name = '';
	_except_ternary = true;
	_act_fldr_name = '';
	_order = 'clean_time';
	_orderby = 'desc';

	_offsetreq = 0;
	_offsetsopd = 0;
	_pagereq = 1;
	_pageopd = 1;

	_name_selected = '';
	_dir_selected = '';

	$.fn.getting = function(option){
      var param = $.extend({
         first_load : _first_load,
         order : _order,
         orderby : _orderby,
      }, option);
      
      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'documents/favorites/getting',
         dataType : "JSON",
         data : {
            dir : param.dir,
            order : param.order,
            orderby : param.orderby
         },
         beforeSend: function (xhr) {
            $(this).reset_breadcrumb();
         	$(this).bulk_option_reset();
         	if(!param.first_load){
         		spinner('#cont-widget-dms', 'block');
         	}
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){
   		 	var t = '';
            if(r.result.length > 0){
            	$('.option_choose').removeClass('hidden');
               $.each(r.result, function(k,v){
               	t += '<input id="metaname-'+k+'" type="hidden" value="'+v.name+'">';
    					t += '<input id="metaext-'+k+'" type="hidden" value="'+(v.ext_orig ? v.ext_orig : '')+'">';
    					t += '<input id="metashare-'+k+'" type="hidden" value="'+(v.share ? v.share : '')+'">';
    					t += '<input id="metafav-'+k+'" type="hidden" value="'+v.id+'">';
    					if(v.file_ternary){
    						t += '<input id="metaternary-'+k+'" type="hidden" value="'+v.file_ternary+'">';
    					}else{
    						t += '<input id="metaternary-'+k+'" type="hidden" value="'+v.name+'">';
    					}
    					t += '<input id="metadir-'+k+'" type="hidden" value="'+v.file_dir+'">';

    					t += '<tr>';
               		t += '<td><input type="checkbox" class="option_check" value="'+k+'"></td>';
               		t += '<td class="mailbox-name">';
       						if(v.is_file){
	    							t += '<a href="'+base_url+'main/download?name='+v.name+'&dir='+v.base_dir+'">';
	    						}else{
	    							if(v.file_ternary){
	    								t += '<a data-href="'+v.file_ternary+'" style="cursor:pointer;" class="change-dir" data-active-fldr="'+v.name+'">';
	    							}else{
	    								t += '<a data-href="'+v.name+'" style="cursor:pointer;" class="change-dir" data-active-fldr="">';
	    							}
	    						}

	       					t += '<div style="height:100%;width:100%">';
	       					if(!v.is_file){
	       						t += '<i class="fa fa-folder"></i>&nbsp;';
	       					}else{
	       						if(v.ext){
	       							t += '<i class="'+v.ext+'" style="font-size:19px"></i>&nbsp;';
	       						}
	       					}
	       					t += v.label
	       					t += '</div>';
	       					t += '</a>';
	       				t += '</td>';

	       				t += '<td class="mailbox-star">';
          				// if(v.fav){
          				// 	t += '&nbsp;<i class="fa fa-star text-yellow" data-toggle="tooltip" data-title="Favorites"></i>';
          				// }
          				// if(v.share){
          				// 	t += '&nbsp;<i class="fa fa-share-alt text-info" data-toggle="tooltip" data-title="'+v.share+'"></i>';
          				// }
          				// if(v.assign){
          				// 	t += '&nbsp;<i class="fa fa-clipboard" data-toggle="tooltip" data-title="'+v.assign+'"></i>';
          				// }
          				t += '</td>';

          				t += '<td class="mailbox-attachment">';
          					t += '<span class="dropdown dropdown-'+k+'">';
       				 			t += '<a href="" class="dropdown-toggle" data-toggle="dropdown" style="color:#333">';
       				 				t += '<i class="fa fa-ellipsis-h"></i>';
       				 			t += '</a>';
       				 			t += '<ul class="dropdown-menu dropdown-menu-act" style="left: -131px;border: 1px solid #cccccc;top:8px;">';
    				 					if(v.assign){
		       				 			t += '<li>';
												t += '<a class="detail-assign-request dropdown-options-dms" data-id="'+k+'" href="">';
													t += 'Detail assign request';
												t += '</a>';
											t += '</li>';
										}
										if(v.share){
		       				 			t += '<li>';
												t += '<a class="detail-share dropdown-options-dms" data-id="'+k+'" href="">';
													t += 'Detail share';
												t += '</a>';
											t += '</li>';
										}
										if(v.is_file){
											t += '<li>';
												t += '<a class="dropdown-options-dms" href="'+base_url+'main/download?name='+v.name+'&dir='+v.base_dir+'">';
													t += 'Download';
												t += '</a>';
											t += '</li>';
										}else{
											t += '<li>';
												t += '<a class="dropdown-options-dms" href="'+base_url+'main/zipped?name='+v.name+'&dir='+v.base_dir+'">';
													t += 'Download';
												t += '</a>';
											t += '</li>';
										}
       				 			t += '</ul>';
    				 			t += '</span>';
 				 			t += '</td>';
 				 			t += '<td class="mailbox-date">'+v.time+'</td>';
                    	t += '<td class="mailbox-size">'+v.size+'</td>';
                    	if(v.share){
                    		t += '<td class="mailbox-date text-center" style="background-color:#58b5b9;color:#fff;cursor:pointer;" title="'+v.share+'"><i class="fa fa-check"></i></td>';
                 		}else{
                 			t += '<td class="mailbox-date text-center"></td>';
                 		}
                 		if(v.assign){
                    		t += '<td class="mailbox-date text-center" style="background-color:#58b958;color:#fff;cursor:pointer;" title="'+v.assign+'"><i class="fa fa-check"></i></td>';
                 		}else{
                 			t += '<td class="mailbox-date text-center"></td>';
                 		}

    					t += '</tr>';
            	});
         	}else{
         		$('.option_choose').addClass('hidden');
         	}

         	$('#'+_uniqid).find('.sect-data').html(t);
      	},
      	complete : function(r){
      		spinner('#cont-widget-dms', 'unblock');
      		_first_load = false;
   		}
   	});
	}

	$.fn.dir = function(option){
      var param = $.extend({
         dir : _dir,
         first_load : _first_load,
         order : _order,
         orderby : _orderby,
      }, option);
      
      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'documents/myfiles/dir',
         dataType : "JSON",
         data : {
            dir : param.dir,
            except_ternary : _except_ternary,
            act_fldr_name : _act_fldr_name,
            order : param.order,
            orderby : param.orderby
         },
         beforeSend: function (xhr) {
         	$(this).bulk_option_reset();
         	if(!param.first_load){
         		spinner('#cont-widget-dms', 'block');
         	}
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){
   		 	var t = '';
            _dir_active_name = r.active_dir_name;

            if(r.result.data.length > 0){
            	$('.option_choose').removeClass('hidden');
               var total = r.result.total;
               $.each(r.result.data, function(k,v){
    					t += '<input id="metaname-'+k+'" type="hidden" value="'+v.name+'">';
    					t += '<input id="metaext-'+k+'" type="hidden" value="'+(v.ext_orig ? v.ext_orig : '')+'">';
    					t += '<input id="metashare-'+k+'" type="hidden" value="'+(v.share ? v.share : '')+'">';
    					t += '<input id="metafav-'+k+'" type="hidden" value="'+(v.fav ? v.fav : '')+'">';
    					if(param.dir){
    						t += '<input id="metaternary-'+k+'" type="hidden" value="'+param.dir+'/'+v.name+'">';
    					}else{
    						t += '<input id="metaternary-'+k+'" type="hidden" value="'+v.name+'">';
    					}
    					if(param.dir){
    						t += '<input id="metadir-'+k+'" type="hidden" value="'+r.dir_base+'/'+param.dir+'">';
    					}else{
    						t += '<input id="metadir-'+k+'" type="hidden" value="'+r.dir_base+'">';
    					}

    					t += '<tr>';
               		t += '<td><input type="checkbox" class="option_check" value="'+k+'"></td>';
               		t += '<td class="mailbox-name">';
	       					if(param.dir){
	       						if(v.is_file){
	       							t += '<a href="'+base_url+'main/download?name='+v.name+'&dir='+r.dir_base+'/'+param.dir+'/'+v.name+'" data-id="'+k+'">';
	       						}else{
	       							t += '<a data-href="'+param.dir+'/'+v.name+'" class="change-dir" data-active-fldr="">';
	       						}
	       					}else{
	       						if(v.is_file){
	       							t += '<a href="'+base_url+'main/download?name='+v.name+'&dir='+r.dir_base+'/'+v.name+'" data-id="'+k+'">';
	       						}else{
	       							t += '<a data-href="'+v.name+'" class="change-dir" data-active-fldr="">';
	       						}
	       					}
	       					t += '<div style="height:100%;width:100%">';
	       					if(!v.is_file){
	       						t += '<i class="fa fa-folder"></i>&nbsp;';
	       					}else{
	       						if(v.ext){
	       							t += '<i class="'+v.ext+'" style="font-size:19px"></i>&nbsp;';
	       						}
	       					}
	       					t += v.label
	       					if(v.fav){
	       						t += '&nbsp;<i class="fa fa-star text-yellow" data-toggle="tooltip" data-title="Favorites"></i>';
       						}
	       					t += '</div>';
	       					t += '</a>';
	       				t += '</td>';

	       				t += '<td class="mailbox-star">';
          				// if(v.fav){
          				// 	t += '&nbsp;<i class="fa fa-star text-yellow" data-toggle="tooltip" data-title="Favorites"></i>';
          				// }
          				// if(v.share){
          				// 	t += '&nbsp;<i class="fa fa-share-alt text-info" data-toggle="tooltip" data-title="'+v.share+'"></i>';
          				// }
          				// if(v.assign){
          				// 	t += '&nbsp;<i class="fa fa-clipboard" data-toggle="tooltip" data-title="'+v.assign+'"></i>';
          				// }
          				t += '</td>';

          				t += '<td class="mailbox-attachment">';
          					t += '<span class="dropdown dropdown-'+k+'">';
       				 			t += '<a href="" class="dropdown-toggle" data-toggle="dropdown" style="color:#333">';
       				 				t += '<i class="fa fa-ellipsis-h"></i>';
       				 			t += '</a>';
       				 			t += '<ul class="dropdown-menu dropdown-menu-act" style="left: -131px;border: 1px solid #cccccc;top:8px;">';
    				 					if(v.assign){
		       				 			t += '<li>';
												t += '<a class="detail-assign-request dropdown-options-dms" data-id="'+k+'" href="">';
													t += 'Detail assign request';
												t += '</a>';
											t += '</li>';
										}
										if(v.share){
		       				 			t += '<li>';
												t += '<a class="detail-share dropdown-options-dms" data-id="'+k+'" href="">';
													t += 'Detail share';
												t += '</a>';
											t += '</li>';
										}
										if(v.is_file){
											t += '<li>';
												t += '<a class="rename dropdown-options-dms" data-id="'+k+'" href="" >';
													t += 'Rename';
												t += '</a>';
											t += '</li>';
										}
										if(param.dir){
											if(v.is_file){
												t += '<li>';
													t += '<a class="dropdown-options-dms" href="'+base_url+'main/download?name='+v.name+'&dir='+r.dir_base+'/'+param.dir+'/'+v.name+'">';
														t += 'Download';
													t += '</a>';
												t += '</li>';
											}else{
												t += '<li>';
													t += '<a class="dropdown-options-dms" href="'+base_url+'main/zipped?name='+v.name+'&dir='+r.dir_base+'/'+param.dir+'/'+v.name+'">';
														t += 'Download';
													t += '</a>';
												t += '</li>';
											}
										}else{
											if(v.is_file){
			       							t += '<a href="'+base_url+'main/download?name='+v.name+'&dir='+r.dir_base+'/'+v.name+'" data-id="'+k+'">';
			       						}else{
			       							t += '<li>';
													t += '<a class="dropdown-options-dms" href="'+base_url+'main/zipped?name='+v.name+'&dir='+r.dir_base+param.dir+'/'+v.name+'">';
														t += 'Download';
													t += '</a>';
												t += '</li>';
			       						}
										}
										
       				 			t += '</ul>';
    				 			t += '</span>';
 				 			t += '</td>';
 				 			t += '<td class="mailbox-date">'+v.time+'</td>';
                    	t += '<td class="mailbox-size">'+v.size+'</td>';
                    	if(v.share){
                    		t += '<td class="mailbox-date text-center" style="background-color:#58b5b9;color:#fff;cursor:pointer;" title="'+v.share+'"><i class="fa fa-check"></i></td>';
                 		}else{
                 			t += '<td class="mailbox-date text-center"></td>';
                 		}
                 		if(v.assign){
                    		t += '<td class="mailbox-date text-center" style="background-color:#58b958;color:#fff;cursor:pointer;" title="'+v.assign+'"><i class="fa fa-check"></i></td>';
                 		}else{
                 			t += '<td class="mailbox-date text-center"></td>';
                 		}

    					t += '</tr>';
            	});
         	}else{
         		$('.option_choose').addClass('hidden');
         	}

         	$('#'+_uniqid).find('.sect-data').html(t);
            $('#'+_uniqid).find('.sect-nav-render').html('');
            $('#'+_uniqid).find('.sect-nav-render').html(r.nav);
      	},
      	complete : function(r){
      		spinner('#cont-widget-dms', 'unblock');
      		_first_load = false;
   		}
   	});
	}

	$.fn.request = function(option){
      var param = $.extend({
         keyword : $('#keywordreq').val(),
         offset : _offsetreq, 
         currentPage : _pagereq,
         order : 'a.id', 
         orderby : 'desc'
      }, option);
      
      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'documents/myfiles/request',
         dataType : "JSON",
         data : {
            offset : param.offset,
            keyword : param.keyword,
            order : param.order,
            orderby : param.orderby
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){
            var t = '';
         	if(r.total > 0){
               var total = r.total;
               $.each(r.result, function(k,v){
               	t += '<tr>';
               		t += '<td><input type="checkbox" class="reqid" value="'+v.id+'" name="reqid[]"></td>';
               		t += '<td>';
               			t += v.requested_date+'<br>'+v.requested_time;
               		t += '</td>';
               		t += '<td>'+v.title+'</td>';
               		t += '<td>'+v.start_date+'</td>';
               		t += '<td>'+v.end_date+'</td>';
               		t += '<td>';
               			t += v.limit_date;
               			t += '<br><span style="font-size:10px;font-weight:bold">'+v.days_left+' Days Left</span>';
               		t += '</td>';
               		t += '<td>'+v.status_label+'</td>';
               		if(v.total_files){
               			t += '<td>';
               			t += '<button type="button" class="btn btn-default btn-xs stream-file" data-toggle="popover" data-trigger="focus" data-placement="left" data-content="" data-html="true" data-id="'+v.id+'">'+v.total_files+' Lampiran</button>'
               			t += '</td>';
               		}else{
               			t += '<td></td>';
               		}               		
               	t += '</tr>';
               });
            }else{
         		t += '<tr><td class="text-center text-muted" colspan="8">Data tidak ditemukan</td></tr>';
            }

            $('#'+_uniqid).find('.sect-total-request-files').html(''+number_format(total)+' Rows Data');
            $('#'+_uniqid).find('.sect-data-request-files').html(t);

            $('#'+_uniqid+' .sect-pagination-request-files').pagination({
		         items: r.total,
		         itemsOnPage: 10,
		         edges: 0,
		         hrefTextPrefix: '',
		         displayedPages: 1,
		         currentPage : param.currentPage,
		         prevText : '&laquo;',
		         nextText : '&raquo;',
		         dropdown: true,
		         onPageClick : function(n,e){
		            e.preventDefault();
		            _offsetreq = (n-1) * 10;
		            _pagereq = n;
		            $(this).request();
		         }
		      });
         }
      });
   }

   $.fn.opd = function(option){
      var param = $.extend({
         keyword : $('#keywordopd').val(),
         offset : _offsetsopd, 
         currentPage : _pageopd,
         order : 'a.id', 
         orderby : 'desc'
      }, option);
      
      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'documents/myfiles/opd',
         dataType : "JSON",
         data : {
            offset : param.offset,
            keyword : param.keyword,
            order : param.order,
            orderby : param.orderby
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){
            var t = '';
         	if(r.total > 0){
               var total = r.total;
               $.each(r.result, function(k,v){
               	t += '<tr>';
               		t += '<td><input type="checkbox" class="opdid" value="'+v.id+'" name="opdid[]"></td>';
               		t += '<td>'+v.name+'</td>';
               		t += '<td>'+(v.phone ? v.phone : '')+'</td>';
               		t += '<td>'+(v.description ? v.description : '')+'</td>';
               	t += '</tr>';
               });
            }else{
         		t += '<tr><td class="text-center text-muted" colspan="8">Data tidak ditemukan</td></tr>';
            }

            $('#'+_uniqid).find('.sect-total-share-opd').html(''+number_format(total)+' Rows Data');
            $('#'+_uniqid).find('.sect-data-share-opd').html(t);

            $('#'+_uniqid+' .sect-pagination-share-opd').pagination({
		         items: r.total,
		         itemsOnPage: 10,
		         edges: 0,
		         hrefTextPrefix: '',
		         displayedPages: 1,
		         currentPage : param.currentPage,
		         prevText : '&laquo;',
		         nextText : '&raquo;',
		         dropdown: true,
		         onPageClick : function(n,e){
		            e.preventDefault();
		            _offsetsopd = (n-1) * 10;
		            _pagereq = n;
		            $(this).opd();
		         }
		      });
         }
      });
   }

   $.fn.detail_assign = function(option){
      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'documents/myfiles/detail_assign',
         dataType : "JSON",
         data : {
         	filename : _name_selected,
         	dir : _dir_selected,
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){
            var t = '';
         	if(r.result){
               $.each(r.result, function(k,v){
               	t += '<tr>';
               		t += '<td><input type="checkbox" class="assignid" value="'+v.id+'" name="assignid[]"></td>';
               		t += '<td>';
               			t += v.requested_date+'<br>'+v.requested_time;
               		t += '</td>';
               		t += '<td>'+v.title+'</td>';
               		t += '<td>'+v.start_date+'</td>';
               		t += '<td>'+v.end_date+'</td>';
               		t += '<td>';
               			t += v.limit_date;
               			t += '<br><span style="font-size:10px;font-weight:bold">'+v.days_left+' Days Left</span>';
               		t += '</td>';
               		t += '<td>'+v.status_label+'</td>';
               	t += '</tr>';
               });
            }

            $('#'+_uniqid+'').find('.sect-data-assign-detail').html(t);
         }
      });
   }

   $.fn.detail_share = function(option){
      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'documents/myfiles/detail_share',
         dataType : "JSON",
         data : {
         	filename : _name_selected,
         	dir : _dir_selected,
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){
            var t = '';
         	if(r.result){
               $.each(r.result, function(k,v){
               	t += '<tr>';
               		t += '<td><input type="checkbox" class="shareid" value="'+v.id+'" name="shareid[]"></td>';
               		t += '<td>'+v.name+'</td>';
               		t += '<td>'+(v.phone ? v.phone : '')+'</td>';
               		t += '<td>'+(v.description ? v.description : '')+'</td>';
               	t += '</tr>';
               });
            }

            $('#'+_uniqid+'').find('.sect-data-share-detail').html(t);
         }
      });
   }

   $.fn.search_keyword = function(option){
      var param = $.extend({
         keyword : null
      }, option);
      
      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'documents/favorites/search_keyword',
         dataType : "JSON",
         data : {
            keyword : param.keyword
         },
         beforeSend: function (xhr) {
         	$(this).bulk_option_reset();
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){
         	var t = '';
         	t += '<h4>Hasil Pencarian  : <span style="font-size:14px;font-weight:normal"><i>'+param.keyword+'</i></span></h4>';
         	if(r.result.length > 0){
         		t += '<ul class="nav nav-pills nav-stacked">';
         		$.each(r.result, function(k,v){
         			t += '<li class="" >';
         				t += '<a href="'+(v.imp_final ? v.imp_final : '')+'" class="choose-search" style="font-size:10px;">';
         					t += '<h6 style="margin:0;color:#333">';
         						t += '<i class="'+v.ext+'" style="font-size:19px"></i>&nbsp;';
         						t += ''+v.filename+'';
         					t += '</h6>';
         					t += '<span style="margin-left:19px"><i>'+(v.imp_final_clean ? v.imp_final_clean : '')+'</i></span>';
      					t += '</a>';
   					t += '</li>';
      			});
    				t += '</ul>';
         	}
   		 	$('#'+_uniqid).find('.sect-data-search').html(t);
      	}
   	});
	}

   $(this).on('change', '#'+_uniqid+' .search_file', function(e){
      e.preventDefault();
      var keyword = $(this).val();
      $(this).search_keyword({
      	keyword : keyword
      });
   });

   $(this).on('click', '#'+_uniqid+' .stream-file', function () {
   	var trigger = $(this);
  		if (trigger.data('visit') != 1) {
         trigger.data('visit', 1);
	      ajaxManager.addReq({
	    		url : site_url + 'documents/myfiles/stream_files',
	       	type: 'GET',
	       	dataType: 'json',
	       	data: {
	        		id: trigger.data('id'),
	       	},
	       	beforeSend: function () {
	        		trigger.attr('data-content', '<div class="text-center"><i class="fa fa-spinner fa-spin"></i></div>');
	           	trigger.popover('show');
	       	},
	       	error: function () {
    				trigger.data('visit', 0);
	           	trigger.attr('data-content', '<div class="text-center">Error</div>');
	           	trigger.popover('show');
	       	},
	       	success: function (r) {
	           	var t = '';
	        		if(r.result) {
	           		$.each(r.result, function(k,v){
	           			t += '<div style="font-size:11px;margin-bottom:5px;"><i class="'+v.ext+'" style="font-size:19px"></i> '+v.full_path+'</div>';
	        			});
	     			}
	           trigger.attr('data-content', t);
	           trigger.popover('show');
	       	}
	   	});
   	}else{
   		trigger.popover('show');
   	}
 	});

	$(this).getting();

});