$(function () {

	$(this).on('submit', '#'+_uniqid+' .form-assign-files', function(e){
      var form = $(this);
      var data = [];

      $('.reqid').each(function(){
         if(this.checked){
            data.push($(this).val());
         }
      });
      if(data.length > 0){
         $(this).ajaxSubmit({
            url : site_url + 'documents/myfiles/assign',
            type : "POST",
            data : {
               files : arrfiles
            },
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', 'Lampirkan');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function(r) {
               session_checked(r._session, r._maintenance);
               if(r.success){
                  $('#modal-assign-files').modal('hide');
                  $(this).bulk_option_reset();
                  if(_dir !== ''){
                  	$(this).dir();
                  }else{
                  	$(this).getting();
                  }
                  toastr.success(r.msg);
               }else{
                  toastr.error(r.msg);
               }
            },
            complete: function(){
               loading_form(form, 'hide', 'Lampirkan');
            }
         });
      }else{
         alert('tidak ada request yang ditandai');
      }
      e.preventDefault();
   });

   $(this).on('submit', '#'+_uniqid+' .form-share-doc', function(e){
      var form = $(this);
      var data = [];

      $('.opdid').each(function(){
         if(this.checked){
            data.push($(this).val());
         }
      });
      if(data.length > 0){
         $(this).ajaxSubmit({
            url : site_url + 'documents/myfiles/share',
            type : "POST",
            data : {
               files : arrfiles
            },
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', 'Share');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function(r) {
               session_checked(r._session, r._maintenance);
               if(r.success){
                  $('#modal-share-doc').modal('hide');
                  $(this).bulk_option_reset();
                  if(_dir !== ''){
                     $(this).dir();
                  }else{
                     $(this).getting();
                  }
                  toastr.success(r.msg);
               }else{
                  toastr.error(r.msg);
               }
            },
            complete: function(){
               loading_form(form, 'hide', 'Share');
            }
         });
      }else{
         alert('tidak ada request yang ditandai');
      }
      e.preventDefault();
   });

   $(this).on('submit', '#'+_uniqid+' .form-detail-assign', function(e){
      var form = $(this);
      var data = [];

      $('.assignid').each(function(){
         if(this.checked){
            data.push($(this).val());
         }
      });
      if(data.length > 0){
         $(this).ajaxSubmit({
            url : site_url + 'documents/myfiles/delete_assign',
            type : "POST",
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', 'Hapus file dari request yang ditandai');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function(r) {
               session_checked(r._session, r._maintenance);
               if(r.success){
                  $('#modal-detail-assign').modal('hide');
                  if(_dir !== ''){
                     $(this).dir();
                  }else{
                     $(this).getting();
                  }
                  toastr.success(r.msg);
               }else{
                  toastr.error(r.msg);
               }
            },
            complete: function(){
               loading_form(form, 'hide', 'Hapus file dari request yang ditandai');
            }
         });
      }else{
         alert('tidak ada request yang ditandai');
      }
      e.preventDefault();
   });

   $(this).on('submit', '#'+_uniqid+' .form-detail-share', function(e){
      var form = $(this);
      var data = [];

      $('.shareid').each(function(){
         if(this.checked){
            data.push($(this).val());
         }
      });
      if(data.length > 0){
         $(this).ajaxSubmit({
            url : site_url + 'documents/myfiles/delete_share',
            type : "POST",
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', 'Hapus file dari opd yang ditandai');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function(r) {
               session_checked(r._session, r._maintenance);
               if(r.success){
                  $('#modal-detail-share').modal('hide');
                  if(_dir !== ''){
                     $(this).dir();
                  }else{
                     $(this).getting();
                  }
                  toastr.success(r.msg);
               }else{
                  toastr.error(r.msg);
               }
            },
            complete: function(){
               loading_form(form, 'hide', 'Hapus file dari opd yang ditandai');
            }
         });
      }else{
         alert('tidak ada opd yang ditandai');
      }
      e.preventDefault();
   });

});