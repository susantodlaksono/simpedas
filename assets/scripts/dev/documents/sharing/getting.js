$(function () {

	_dir = '';
	_dir_active_name = '';
	_except_ternary = true;
	_act_fldr_name = '';
	_order = 'clean_time';
	_orderby = 'desc';

	_offsetreq = 0;
	_offsetsopd = 0;
	_pagereq = 1;
	_pageopd = 1;

	_name_selected = '';
	_dir_selected = '';

	$.fn.getting = function(option){
      var param = $.extend({
         first_load : _first_load,
         order : _order,
         orderby : _orderby,
      }, option);
      
      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'documents/sharing/getting',
         dataType : "JSON",
         data : {
            dir : param.dir,
            order : param.order,
            orderby : param.orderby
         },
         beforeSend: function (xhr) {
         	$(this).bulk_option_reset();
            $(this).reset_breadcrumb();
            $('.co[data-order="opd_name"]').removeClass('hidden');
            $('.co[data-order="shared_name"]').removeClass('hidden');
         	if(!param.first_load){
         		spinner('#cont-widget-dms', 'block');
         	}
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){
   		 	var t = '';
            if(r.result.length > 0){
            	$('.option_choose').removeClass('hidden');
               $.each(r.result, function(k,v){
    					t += '<tr>';
               		t += '<td><input type="checkbox" class="option_check" value="'+k+'"></td>';
               		t += '<td class="mailbox-name">';
       						if(v.is_file){
	    							t += '<a href="'+base_url+'main/download?name='+v.name+'&dir='+v.base_dir+'">';
	    						}else{
	    							if(v.file_ternary){
	    								t += '<a data-href="'+v.file_ternary+'" style="cursor:pointer;" class="change-dir" data-active-fldr="'+v.name+'">';
	    							}else{
	    								t += '<a data-href="'+v.name+'" style="cursor:pointer;" class="change-dir" data-active-fldr="">';
	    							}
	    						}

	       					t += '<div style="height:100%;width:100%">';
	       					if(!v.is_file){
	       						t += '<i class="fa fa-folder"></i>&nbsp;';
	       					}else{
	       						if(v.ext){
	       							t += '<i class="'+v.ext+'" style="font-size:19px"></i>&nbsp;';
	       						}
	       					}
	       					t += v.label
	       					t += '</div>';
	       					t += '</a>';
	       				t += '</td>';

          				t += '<td class="mailbox-attachment">';
          					t += '<span class="dropdown dropdown-'+k+'">';
       				 			t += '<a href="" class="dropdown-toggle" data-toggle="dropdown" style="color:#333">';
       				 				t += '<i class="fa fa-ellipsis-h"></i>';
       				 			t += '</a>';
       				 			t += '<ul class="dropdown-menu dropdown-menu-act" style="left: -131px;border: 1px solid #cccccc;top:8px;">';
										if(v.is_file){
											t += '<li>';
												t += '<a class="dropdown-options-dms" href="'+base_url+'main/download?name='+v.name+'&dir='+v.base_dir+'">';
													t += 'Download';
												t += '</a>';
											t += '</li>';
										}else{
											t += '<li>';
												t += '<a class="dropdown-options-dms" href="'+base_url+'main/zipped?name='+v.name+'&dir='+v.base_dir+'">';
													t += 'Download';
												t += '</a>';
											t += '</li>';
										}
       				 			t += '</ul>';
    				 			t += '</span>';
 				 			t += '</td>';
 				 			t += '<td class="mailbox-date">'+v.time+'</td>';
                    	t += '<td class="mailbox-size">'+v.size+'</td>';
                     t += '<td class="mailbox-size">'+(v.shared_name ? v.shared_name : '')+'</td>';
                     t += '<td class="mailbox-size">'+(v.opd_name ? v.opd_name : '')+'</td>';
    					t += '</tr>';
            	});
         	}else{
         		$('.option_choose').addClass('hidden');
         	}

         	$('#'+_uniqid).find('.sect-data').html(t);
      	},
      	complete : function(r){
      		spinner('#cont-widget-dms', 'unblock');
      		_first_load = false;
   		}
   	});
	}

	$.fn.dir = function(option){
      var param = $.extend({
         dir : _dir,
         first_load : _first_load,
         order : _order,
         orderby : _orderby,
      }, option);
      
      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'documents/sharing/dir',
         dataType : "JSON",
         data : {
            dir : param.dir,
            except_ternary : _except_ternary,
            act_fldr_name : _act_fldr_name,
            order : param.order,
            orderby : param.orderby
         },
         beforeSend: function (xhr) {
            $('.co[data-order="opd_name"]').addClass('hidden');
            $('.co[data-order="shared_name"]').addClass('hidden');
         	$(this).bulk_option_reset();
         	if(!param.first_load){
         		spinner('#cont-widget-dms', 'block');
         	}
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){
   		 	var t = '';
            _dir_active_name = r.active_dir_name;

            if(r.result.data.length > 0){
            	$('.option_choose').removeClass('hidden');
               var total = r.result.total;
               $.each(r.result.data, function(k,v){
    					t += '<input id="metaname-'+k+'" type="hidden" value="'+v.name+'">';
    					t += '<input id="metaext-'+k+'" type="hidden" value="'+(v.ext_orig ? v.ext_orig : '')+'">';
    					t += '<input id="metashare-'+k+'" type="hidden" value="'+(v.share ? v.share : '')+'">';
    					t += '<input id="metafav-'+k+'" type="hidden" value="'+(v.fav ? v.fav : '')+'">';
    					if(param.dir){
    						t += '<input id="metaternary-'+k+'" type="hidden" value="'+param.dir+'/'+v.name+'">';
    					}else{
    						t += '<input id="metaternary-'+k+'" type="hidden" value="'+v.name+'">';
    					}
    					if(param.dir){
    						t += '<input id="metadir-'+k+'" type="hidden" value="'+r.dir_base+'/'+param.dir+'">';
    					}else{
    						t += '<input id="metadir-'+k+'" type="hidden" value="'+r.dir_base+'">';
    					}

    					t += '<tr>';
               		t += '<td><input type="checkbox" class="option_check" value="'+k+'"></td>';
               		t += '<td class="mailbox-name">';
	       					if(param.dir){
	       						if(v.is_file){
	       							t += '<a href="'+base_url+'main/download?name='+v.name+'&dir='+r.dir_base+'/'+param.dir+'/'+v.name+'" data-id="'+k+'">';
	       						}else{
	       							t += '<a data-href="'+param.dir+'/'+v.name+'" class="change-dir" data-active-fldr="">';
	       						}
	       					}else{
	       						if(v.is_file){
	       							t += '<a href="'+base_url+'main/download?name='+v.name+'&dir='+r.dir_base+'/'+v.name+'" data-id="'+k+'">';
	       						}else{
	       							t += '<a data-href="'+v.name+'" class="change-dir" data-active-fldr="">';
	       						}
	       					}
	       					t += '<div style="height:100%;width:100%">';
	       					if(!v.is_file){
	       						t += '<i class="fa fa-folder"></i>&nbsp;';
	       					}else{
	       						if(v.ext){
	       							t += '<i class="'+v.ext+'" style="font-size:19px"></i>&nbsp;';
	       						}
	       					}
	       					t += v.label
	       					if(v.fav){
	       						t += '&nbsp;<i class="fa fa-star text-yellow" data-toggle="tooltip" data-title="Favorites"></i>';
       						}
	       					t += '</div>';
	       					t += '</a>';
	       				t += '</td>';

          				t += '<td class="mailbox-attachment">';
          					t += '<span class="dropdown dropdown-'+k+'">';
       				 			t += '<a href="" class="dropdown-toggle" data-toggle="dropdown" style="color:#333">';
       				 				t += '<i class="fa fa-ellipsis-h"></i>';
       				 			t += '</a>';
       				 			t += '<ul class="dropdown-menu dropdown-menu-act" style="left: -131px;border: 1px solid #cccccc;top:8px;">';
										if(param.dir){
											if(v.is_file){
												t += '<li>';
													t += '<a class="dropdown-options-dms" href="'+base_url+'main/download?name='+v.name+'&dir='+r.dir_base+'/'+param.dir+'/'+v.name+'">';
														t += 'Download';
													t += '</a>';
												t += '</li>';
											}else{
												t += '<li>';
													t += '<a class="dropdown-options-dms" href="'+base_url+'main/zipped?name='+v.name+'&dir='+r.dir_base+'/'+param.dir+'/'+v.name+'">';
														t += 'Download';
													t += '</a>';
												t += '</li>';
											}
										}else{
											if(v.is_file){
			       							t += '<a href="'+base_url+'main/download?name='+v.name+'&dir='+r.dir_base+'/'+v.name+'" data-id="'+k+'">';
			       						}else{
			       							t += '<li>';
													t += '<a class="dropdown-options-dms" href="'+base_url+'main/zipped?name='+v.name+'&dir='+r.dir_base+param.dir+'/'+v.name+'">';
														t += 'Download';
													t += '</a>';
												t += '</li>';
			       						}
										}
										
       				 			t += '</ul>';
    				 			t += '</span>';
 				 			t += '</td>';
 				 			t += '<td class="mailbox-date">'+v.time+'</td>';
                    	t += '<td class="mailbox-size">'+v.size+'</td>';
                     t += '<td class="mailbox-size">'+(v.shared_name ? v.shared_name : '')+'</td>';
                     t += '<td class="mailbox-size">'+(v.opd_name ? v.opd_name : '')+'</td>';
    					t += '</tr>';
            	});
         	}else{
         		$('.option_choose').addClass('hidden');
         	}

         	$('#'+_uniqid).find('.sect-data').html(t);
            $('#'+_uniqid).find('.sect-nav-render').html('');
            $('#'+_uniqid).find('.sect-nav-render').html(r.nav);
      	},
      	complete : function(r){
      		spinner('#cont-widget-dms', 'unblock');
      		_first_load = false;
   		}
   	});
	}

   $(this).on('click', '#'+_uniqid+' .co', function(e){
      e.preventDefault();
      $('.co').html('<i class="fa fa-sort"></i>');
      $(this).find('i').remove();
      var sent = $(this).data('order');
      var by = $(this).attr('data-by');
      if(by === 'asc'){ 
         $(this).attr('data-by', 'desc');
         $(this).html('<i class="fa fa-sort-asc"></i>');
      }
      else{ 
         $(this).attr('data-by', 'asc');
         $(this).html(' <i class="fa fa-sort-desc"></i>');
      }
      _order = sent;
      _orderby = by;
      if(_dir == '' ){
         $(this).getting();
      }else{
         $(this).dir();
      }
   });

	$(this).getting();

});