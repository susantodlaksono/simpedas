<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * @author SUSANTO DWI LAKSONO
 */


class Mailing{

	public function __construct() {
      $this->_ci = & get_instance();
      $cf = parse_ini_file(CONFIG_FILE, true);
      $this->_ci->load->library('email');
   }

   public function opd_mapping($id, $status = array()){
   	$this->_ci->db->select('a.group_id, b.email');
   	$this->_ci->db->join('users as b', 'a.user_id = b.id','left');
   	$this->_ci->db->where('a.opd_id', $id);
   	$this->_ci->db->where_in('a.group_id', $status);
   	$result = $this->_ci->db->get('mapping_opd as a')->result_array();
   	if($result){
   		foreach ($result as $v) {
				$cc[] = $v['email'];
   		}
   		return implode(',', $cc);
   	}
   }

   public function kadis_detail($opdid){
   	$this->_ci->db->select('b.first_name, b.email');
   	$this->_ci->db->join('users as b', 'a.user_id = b.id','left');
   	$this->_ci->db->where('a.opd_id', $opdid);
   	$this->_ci->db->where('a.group_id', 3);
   	return $this->_ci->db->get('mapping_opd as a')->row_array();
   }

   public function request_data($id){
   	$request = $this->_ci->db->get_where('request', array('id' => $id))->row_array();
   	$cc = $this->opd_mapping($request['opd_id'], array(4,5));
   	$kadis_detail = $this->kadis_detail($request['opd_id']);
   	$t = '';
   	$t .= 'Kepada Bapak/ Ibu '.$kadis_detail['first_name'].' ysh,<br>';
		$t .= 'Bersama ini, kami mengajukan permohonan <b>'.$request['title'].'</b> periode <b>'.date('d M Y', strtotime($request['start_date'])).' sd '.date('d M Y', strtotime($request['end_date'])).'</b> <br>';
		$t .= 'sesuai dengan format terlampir dan diharapkan paling lambat tanggal <b>'.date('d M Y', strtotime($request['limit_date'])).'</b><br>';
		$t .= 'data tersebut sudah disampaikan pada Dinas Komunikasi dan Informatika Kota<br>';
		$t .= 'Bandung melalui aplikasi Sistem Informasi Manajemen Pedasi yang dapat diakses pada halaman pedasi.xyz.<br><br>';
		$t .= 'Demikian, atas perhatian dan kerjasamanya kami haturkan terima kasih.';

   	$this->_ci->email->set_mailtype('html');
   	$this->_ci->email->from('sc.dwilaksono@gmail.com', 'Simpedas');
   	$this->_ci->email->subject('[Diskominfo] INFO | Permohonan Data | Eksternal');
		$this->_ci->email->to('susantodlaksono@gmail.com'); //to

		$this->_ci->email->to($kadis_detail['email']); //cc
   	// if($opd['kasie']){
   	// 	$this->_ci->email->cc(implode(',', $opd['kasie'])); //cc
   	// }
   	if($cc){
	      $this->_ci->email->cc($cc); //cc
   	}
   	$this->_ci->email->message($t);
   	$send = $this->_ci->email->send();

      if($send){
         return array(
         	'status' => TRUE
      	);
      }else{
      	return array(
         	'status' => FALSE,
         	'msg' => $this->_ci->email->print_debugger()
      	);
      }
   }

   public function after_kompilasi_data($idreq, $idkasie){
   	$request = $this->_ci->db->get_where('request', array('id' => $idreq))->row_array();
   	$kasie = $this->_ci->db->where('id', $idkasie)->get('users')->row_array();
   	$kadis_detail = $this->kadis_detail($request['opd_id']);

   	$t = '';
   	$t .= 'Kepada Bapak/ Ibu '.$kasie['first_name'].' ysh,<br>';
		$t .= 'Berdasarkan permohonan <b>'.$request['title'].'</b> yang disampaikan pada tanggal <b>'.date('d M Y', strtotime($request['disposisi_kasie_created'])).'</b><br>	';
		$t .= 'Saya sudah melakukan kompilasi terhadap pemohonan data tersebut.<br>Mohon kiranya kepada Bapak/ Ibu untuk melakukan pemeriksaan hasil kompilasi data yang sudah saya kerjakan.<br>';
		$t .= 'Demikian, atas perhatian dan kerjasamanya kami haturkan terima kasih.';

   	$this->_ci->email->set_mailtype('html');
   	$this->_ci->email->from('sc.dwilaksono@gmail.com', 'Simpedas');
   	$this->_ci->email->subject('[Dinas Pengirim] SUBMIT | Lapor Hasil Kompilasi Data | Eksternal');
		$this->_ci->email->to('susantodlaksono@gmail.com'); //to

		$this->_ci->email->to($kadis_detail['email']); //cc
   	if($kasie['email']){
	      $this->_ci->email->cc($kasie['email']); //cc
   	}
   	$this->_ci->email->message($t);
   	$send = $this->_ci->email->send();

      if($send){
         return array(
         	'status' => TRUE
      	);
      }else{
      	return array(
         	'status' => FALSE,
         	'msg' => $this->_ci->email->print_debugger()
      	);
      }
   }

   public function after_validasi_kasie($idreq){
      $request = $this->_ci->db->get_where('request', array('id' => $idreq))->row_array();
      $requestor = $this->_ci->db->where('id', $request['requested_by'])->get('users')->row_array();
      $kadis_detail = $this->kadis_detail($request['opd_id']);

      $t = '';
      $t .= 'Kepada Dinas Komunikasi dan Informatika,<br>';
      $t .= 'Berdasarkan permohonan <b>'.$request['title'].'</b> yang disampaikan pada tanggal <b>'.date('d M Y', strtotime($request['requested_date'])).'</b>,<br>';
      $t .= 'Kami sudah melakukan kompilasi terhadap pemohonan data tersebut. Mohon kiranya kepada Bapak/ Ibu untuk menerima hasil kompilasi data yang sudah kami kerjakan.<br>';
      $t .= 'Adapun hasil kompilasi data tersebut kami lampirkan pada aplikasi Sistem Informasi Manajemen Pedasi.<br>';
      $t .= 'Demikian, atas perhatian dan kerjasamanya kami haturkan terima kasih.';

      $this->_ci->email->set_mailtype('html');
      $this->_ci->email->from('sc.dwilaksono@gmail.com', 'Simpedas');
      $this->_ci->email->subject('[Dinas Pengirim] INFO | Menyampaikan Hasil Permohonan Data | Eksternal ');
      $this->_ci->email->to($requestor['email']);
      $this->_ci->email->cc($kadis_detail['email']);
      $this->_ci->email->message($t);
      $send = $this->_ci->email->send();

      if($send){
         return array(
            'status' => TRUE
         );
      }else{
         return array(
            'status' => FALSE,
            'msg' => $this->_ci->email->print_debugger()
         );
      }
   }

   public function register_opd($params){
      $pedasi = $this->_ci->db->where('id', $params['pedasi'])->get('users')->row_array();
      $kadis = $this->_ci->db->where('id', $params['kadis'])->get('users')->row_array();
      foreach ($params['kasie'] as $v) {
         $ks = $this->_ci->db->where('id', $v)->get('users')->row_array();
         $kasie[] = $ks['email'];
      }
      $t = '';
      $t .= 'Kepada Bapak/ Ibu <b>'.$params['name'].'</b> ysh,<br>';
      $t .= 'Berdasarkan permohonan pembuatan akun yang disampaikan oleh Kepala Perangkat Daerah '.$params['name'].'.<br>';
      $t .= 'Kami bermaksud untuk menginformasikan bahwa akun anda sudah aktif dan dapat diakses pada aplikasi Sistem Informasi Manajemen Pedasi yang dapat diakses pada halaman pedasi.xyz dengan autentikasi sebagai berikut:<br>';
      $t .= 'Username : <b>'.$pedasi['username'].'</b><br>';
      $t .= 'Password : <b>'.($pedasi['password_show'] ? $pedasi['password_show'] : '').'</b><br>';
      $t .= 'Demikian, atas perhatian dan kerjasamanya kami haturkan terima kasih.';

      $this->_ci->email->set_mailtype('html');
      $this->_ci->email->from('sc.dwilaksono@gmail.com', 'Simpedas');
      $this->_ci->email->subject('[Diskominfo] INFO | Pendaftaran Pedasi | Eksternal');
      $this->_ci->email->to($pedasi['email']);
      $this->_ci->email->cc($kadis['email'].','.implode(',', $kasie));
      $this->_ci->email->message($t);
      $send = $this->_ci->email->send();

      if($send){
         return array(
            'status' => TRUE,
            'pedasi' => $pedasi['email'],
            'kadis' => $kadis['email'],
            'kasie' => $kasie,
         );
      }else{
         return array(
            'status' => FALSE,
            'msg' => $this->_ci->email->print_debugger()
         );
      }
   }

}