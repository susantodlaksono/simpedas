<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * @author SUSANTO DWI LAKSONO
 */


class Dir_extraction{

   public function __construct() {
      $this->_ci = & get_instance();
      $this->ext_multimedia = array('mp4', 'mp3', 'zip', 'rar', 'jpg', 'png', 'gif', 'jpeg');
      $this->ext_office = array('pdf', 'docx', 'xls', 'xlsx', 'ppt', 'doc', 'csv', 'txt');
   }

   public function count_dir_ternary_path($path){
      if(file_exists($path)){
         $iterator = new RecursiveDirectoryIterator($path);
         $count_byte = 0;
         $i = 0;
         $ii = 0;
         $multimedia = 0;
         $office = 0;
         $other = 0;
         $files = array();
         foreach (new RecursiveIteratorIterator($iterator) as $filename => $cur) {
            if(is_file($filename)){
               $filesize = $cur->getSize();
               $count_byte += $filesize;
               $i++;
               $files[] = $filename;
               $ext = pathinfo($filename, PATHINFO_EXTENSION);
               if(in_array($ext, $this->ext_multimedia)){
                  $multimedia += 1;
               }
               else if(in_array($ext, $this->ext_office)){
                  $office += 1;
               }else{
                  $other += 1;
               }
               $ii++;
            }
         }
         $count_byte = number_format($count_byte);
         return array(
            'count_files' => $i,
            'count_multimedia' => $multimedia,
            'count_office' => $office,
            'count_other' => $other,
            'total_size' => $count_byte,
            'files' => $files
         );
      }else{
         return array(
            'count_files' => 0,
            'count_multimedia' => 0,
            'count_office' => 0,
            'count_other' => 0,
            'total_size' => 0,
            'files' => 0
         );
      }
	}

   public function in_array_r($needle, $haystack, $strict = false) {
      foreach ($haystack as $item) {
         if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
            return $item;
         }
      }
      return FALSE;
   }
}