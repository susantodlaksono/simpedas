<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * @author SUSANTO DWI LAKSONO
 */


class Global_mapping{

   public function __construct() {
      $this->ci = & get_instance();
   }

   public function picture_path($path, $filename, $type){
      if($filename){
         if($type == 'thumb'){
         	$exp = explode('.', $filename);
         	$fixfilename = $exp[0];
         	$fixext = $exp[1];
            if (file_exists($path.'thumbnail/'.$fixfilename.'_thumb.'.$fixext)) {
               return $path.'thumbnail/'.$fixfilename.'_thumb.'.$fixext;
            }else{
            	if (file_exists($path.$filename)) {
	               return $path.$filename;
	            }else{
	               return 'none.png';
	            }
            }
         }
         if($type == 'original'){
            if (file_exists($path.$filename)) {
               return $path.$filename;
            }else{
               return 'none.png';
            }
         }
      }else{
         return 'none.png';
      }
   }

   public function mapping_icon($ext, $withclass = FALSE){
      $upper = strtoupper($ext);
      switch ($upper) {
   		case '.DOCX': return 'fa fa-file-word-o '.($withclass ? 'text-blue' : '').''; break;
   		case '.DOC': return 'fa fa-file-word-o '.($withclass ? 'text-blue' : '').''; break;
   		case '.XLS': return 'fa fa-file-excel-o '.($withclass ? 'text-green' : '').''; break;
   		case '.XLSX': return 'fa fa-file-excel-o '.($withclass ? 'text-green' : '').''; break;
   		case '.CSV': return 'fa fa-file-text-o '.($withclass ? 'text-muted' : '').''; break;
   		case '.PDF': return 'fa fa-file-pdf-o '.($withclass ? 'text-red' : '').''; break;
   		case '.PPT': return 'fa fa-file-powerpoint-o '.($withclass ? 'text-yellow' : '').''; break;
         case '.PPTX': return 'fa fa-file-powerpoint-o '.($withclass ? 'text-yellow' : '').''; break;
   		case '.ZIP': return 'fa fa-file-zip-o '.($withclass ? 'text-muted' : '').''; break;
   		case '.RAR': return 'fa fa-file-zip-o '.($withclass ? 'text-muted' : '').''; break;
   		case '.TXT': return 'fa fa-file-text-o '.($withclass ? 'text-muted' : '').''; break;
   		case '.JPG': return 'fa fa-file-image-o '.($withclass ? 'text-muted' : '').''; break;
   		case '.PNG': return 'fa fa-file-image-o '.($withclass ? 'text-muted' : '').''; break;
   		case '.JPEG': return 'fa fa-file-image-o '.($withclass ? 'text-muted' : '').''; break;
   		case '.GIF': return 'fa fa-file-image-o '.($withclass ? 'text-muted' : '').''; break;
   		default : return 'fa fa-question '.($withclass ? 'text-muted' : '').''; break;
   	}
   }

   // public function sort_by($field, $array, $direction = 'asc'){
   //    usort($array, create_function('$a, $b', '
   //      $a = $a["' . $field . '"];
   //      $b = $b["' . $field . '"];

   //      if ($a == $b) return 0;

   //      $direction = strtolower(trim($direction));

   //      return ($a ' . ($direction == 'desc' ? '>' : '<') .' $b) ? -1 : 1;
   //    '));

   //    return true;
   // }

   public function column_sort($unsorted, $column, $direction) { 
      $sorted = $unsorted; 
      for ($i=0; $i < sizeof($sorted)-1; $i++) { 
         for ($j=0; $j<sizeof($sorted)-1-$i; $j++) 
           if ($sorted[$j][$column] > $sorted[$j+1][$column]) { 
             $tmp = $sorted[$j]; 
             $sorted[$j] = $sorted[$j+1]; 
             $sorted[$j+1] = $tmp; 
         } 
       } 
      return $sorted; 
   }

   public function request_status($status, $class){
      switch ($status) {
         case '1': return '<span class="label '.$class.'label-success">Kompilasi Data</span>'; break;
         case '2': return '<span class="label '.$class.'label-primary">Validasi Kasie</span>'; break;
         case '3': return '<span class="label '.$class.'label-info">Diserahkan</span>'; break;
         default : return '<span class="label '.$class.'label-default">Proses Disposisi</span>'; break;
      }
   }

   public function request_status_pengolahan($status, $class){
      switch ($status) {
         case '1': return '<span class="label '.$class.'label-default">Diterima</span>'; break;
         case '2': return '<span class="label '.$class.'label-warning">Pengolahan Data</span>'; break;
         case '3': return '<span class="label '.$class.'label-info">Diunggah ke Open Data</span>'; break;
         default : return ''; break;
      }
   } 

   public function get_attachments_request($id){
      $this->ci->db->where('req_id', $id);
      $result = $this->ci->db->get('request_attach')->result_array();
      if($result){
         foreach ($result as $key => $value) {
            $result[$key]['icon'] = $this->mapping_icon_request($value['file_extension']);
         }
         return $result;
      }else{
         return FALSE;
      }
   }

   public function mapping_icon_request($ext){
      $upper = strtoupper($ext);
      switch ($upper) {
         case '.DOCX': return 'fa fa-file-word-o'; break;
         case '.DOC': return 'fa fa-file-word-o'; break;
         case '.XLS': return 'fa fa-file-excel-o'; break;
         case '.XLSX': return 'fa fa-file-excel-o'; break;
         case '.CSV': return 'fa fa-file-excel-o'; break;
         case '.PDF': return 'fa fa-file-pdf-o'; break;
         case '.PPT': return 'fa fa-file-powerpoint-o'; break;
         case '.PPTX': return 'fa fa-file-powerpoint-o'; break;
         case '.ZIP': return 'fa fa-file-zip-o'; break;
         case '.RAR': return 'fa fa-file-zip-o'; break;
         case '.TXT': return 'fa fa-file-text-o'; break;
         case '.JPG': return 'fa fa-file-image-o'; break;
         case '.PNG': return 'fa fa-file-image-o'; break;
         case '.JPEG': return 'fa fa-file-image-o'; break;
         case '.GIF': return 'fa fa-file-image-o'; break;
         default : return 'fa fa-file'; break;
      }
   }

   public function format_size_units($bytes){
      if ($bytes >= 1073741824){
         $bytes = number_format($bytes / 1073741824, 2) . ' GB';
      }
      elseif ($bytes >= 1048576){
         $bytes = number_format($bytes / 1048576, 2) . ' MB';
      }
      elseif ($bytes >= 1000){
         $bytes = number_format($bytes / 1000, 2) . ' KB';
      }
      elseif ($bytes > 1){
         $bytes = $bytes . ' bytes';
      }
      elseif ($bytes == 1){
         $bytes = $bytes . ' byte';
      }
      else{
         $bytes = '0 bytes';
      }
      return $bytes;
   }

   public function format_filesize($B, $D=2){
       $S = 'kMGTPEZY';
       $F = floor((strlen($B) - 1) / 3);
       return sprintf("%.{$D}f", $B/pow(1024, $F)).' '.@$S[$F-1].'B';
   }

   public function formatBytes($bytes, $precision = 2) { 
       $units = array('B', 'KB', 'MB', 'GB', 'TB'); 

       $bytes = max($bytes, 0); 
       $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
       $pow = min($pow, count($units) - 1); 

       // Uncomment one of the following alternatives
       // $bytes /= pow(1024, $pow);
       // $bytes /= (1 << (10 * $pow)); 

       return round($bytes, $precision) . ' ' . $units[$pow]; 
   } 
}