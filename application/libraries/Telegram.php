<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * @author SUSANTO DWI LAKSONO
 */


class Telegram{

	protected $url;
	protected $chatid;
	private $curl_obj;

   public function __construct() {
      $this->_ci = & get_instance();
      $cf = parse_ini_file(CONFIG_FILE, true);

      $this->chatid = $cf['application']['chat_id'];
      $this->url = 'https://api.telegram.org/bot'.$cf['application']['token'].'/';

      if(!function_exists('curl_init')){
      	echo 'ERROR: Install CURL module for php';
         exit();
     	}
     	$this->init();
   }

   public function message($body){
   	$params = array
		(
			'chat_id' => $this->chatid,
			'text' => $body,
			'parse_mode' => 'HTML',
			'disable_web_page_preview' => null,
			'reply_to_message_id' => null,
			'reply_markup' => null
		);
		// $this->url .= http_build_query($params);
		return $this->request($this->url.'sendMessage', 'POST', $params);
   }

 	public function request($url, $method = 'GET', $params = array(), $opts = array())
    {
        $method = trim(strtoupper($method));

        // default opts
        $opts[CURLOPT_FOLLOWLOCATION] = true;
        $opts[CURLOPT_RETURNTRANSFER] = 1;
        //$opts[CURLOPT_FRESH_CONNECT] = true;

        if($method==='GET')
	{
		$url .= "?".$params;
		$params = http_build_query($params);
	}
        elseif($method==='POST')
        {
            $opts[CURLOPT_POST] = 1;
            $opts[CURLOPT_POSTFIELDS] = $params;
        }

        $opts[CURLOPT_URL] = $url;

		curl_setopt_array($this->curl_obj, $opts);

        $content = curl_exec($this->curl_obj);
        $http = curl_getinfo($this->curl_obj, CURLINFO_HTTP_CODE);
        $error = curl_error($this->curl_obj);
        return json_decode($content, TRUE);
        // return array(
        //     'data' => $content,
        //     'http' => $http,
        //     'error' => $error
        // );
    }

   public function init(){
  		$this->curl_obj = curl_init();
 	}

 	public function close(){
     	if(gettype($this->curl_obj) === 'resource')
         curl_close($this->curl_obj);
 	}

 	public function __destruct(){
     	$this->close();
 	}

   // public function send($body){
   // 	try {
   // 		$data = array(
	  //        'chat_id' => $this->chatid,
	  //        'text' => $body,
	  //        'parse_mode' => 'HTML'
	  //     );

	  //     $options = array(
	  //        'http' => array(
	  //           'method' =>'POST',
	  //           'header'=> "Content-Type:application/x-www-form-urlencoded\r\n",
	  //           'content'=>http_build_query($data)
	  //        )
	  //     );
	  //     $context = stream_context_create($options);	      
	  //     $result = file_get_contents($this->url, false, $context);
	  //     return $result;
   // 	} catch (Exception $e) {
   // 		return $e->getMessage();
   // 	}
   // }

}