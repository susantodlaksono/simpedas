<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Minify</title>
   <script src="<?php echo base_url() ?>assets/neon/js/jquery-1.11.3.min.js"></script>
</head>
<body>
   <h4>Global JS</h4>
   <table border="1">
      <form method="post" action="<?php echo site_url('minify_extractor/index') ?>">
      <tr>
         <td><input type="text" name="directory" placeholder="Path JS Here.."> <input type="submit" value="Search Path"></td>
      </tr>
      </form>
      <?php
      foreach ($globaldevjs as $v) {
         echo '<tr>';
         echo '<td>'.$v['dev'].'</td>';
         echo '<td><button type="button" class="set-minify" data-dir="'.$v['dev'].'">Minify</button></td>';
         echo '</td>';
      }
      ?>
   </table>
</body>
</html>

<script type="text/javascript">
var site_url = '<?php echo site_url() ?>';

$(function() {
   $('.set-minify').click(function (e) {
      e.preventDefault();
      var dir = $(this).data('dir');
      $.ajax({
         url: site_url+'minify_extractor/push',
         type: "POST",
         dataType: "json",
         data: {
            dir : dir
         },
         success: function (response) {
            if (typeof callback == "function") {
               callback(response);
            }
            alert(response.from+' To '+response.to);
         }
      });
   });
});

</script>