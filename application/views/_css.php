<?php
$_css_file_widget = isset($_csswidget) && $_csswidget ? $_csswidget : FALSE;

if ($_css_file_widget) {
   if (!is_array($_css_file_widget)) {
      $_css_file_widget = array((string) $_css_file_widget);
   }
   foreach ($_css_file_widget as $v) {
      $_url = $v;
      if (!preg_match('/^http/', $_url)) {
         $_url = base_url($_url);
      }
      ?>
      <link type="text/css" rel="stylesheet" href="<?php echo $_url; ?>" />
   <?php
   }
}