
<!DOCTYPE html>
<html>
   <head>
      <title>Simpedas | Login</title>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <link href="<?php echo base_url('cloudbdg.png') ?>" rel="shortcut icon"/>
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/<?php echo $template ?>/bootstrap/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/<?php echo $template ?>/plugins/fontawesome/css/font-awesome.min.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/<?php echo $template ?>/dist/css/AdminLTE.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/<?php echo $template ?>/plugins/snackbar/css/snackbar.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/<?php echo $template ?>/plugins/snackbar/css/material.css">
      <script src="<?php echo base_url() ?>assets/<?php echo $template ?>/plugins/jquery-1.11.3.min.js"></script>
   </head>
   <body class="login-page" style="background-color: #3c8dbc">
      <div class="login-box" style="">
         <div class="login-box-body" style="border-radius: 10px;">
            <div class="row">
               <div class="col-md-12">
                  <img class="img-responsive pad" style="margin:0 auto;"src="<?php echo base_url('cloudbdg.png') ?>" alt="Photo" width="150" height="150">
               </div>
               <div class="col-md-12">
                  <h6 class="text-center" style="color:#34b0dc;margin-top: 0px;margin-bottom:20px;"><b>Document Management System</b></h6>
               </div>
            </div>        
            <form id="form-verify">
               <input type="hidden" name="redirect" value="<?php echo $this->input->get('redirect') ?>">
               <div class="form-group has-feedback">
                  <input type="text" name="username" class="form-control" placeholder="Username" required="required">
                  <span class="glyphicon glyphicon-user form-control-feedback"></span>
               </div>
               <div class="form-group has-feedback">
                  <input type="password" name="password" class="form-control" placeholder="Password" required="required">
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
               </div>
               <div class="row">
                  <div class="col-xs-12">
                     <p class="text-center"><button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-sign-in"></i> Masuk</button></p>
                  </div><!-- /.col -->
               </div>
            </form>
         </div>
         <p class="text-center" style="margin-top:10px;color:white;">© 2021-2022 Simpedas</p>
      </div>
      
      <script src="<?php echo base_url() ?>assets/include/jquery.form.min.js"></script>  
      <script src="<?php echo base_url() ?>assets/<?php echo $template ?>/bootstrap/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/<?php echo $template ?>/plugins/snackbar/js/snackbar.min.js"></script>
      <script src="<?php echo base_url() ?>assets/scripts/<?php echo $envjsglobal ?>/setup.js?<?php echo time() ?>"></script>
      <script src="<?php echo base_url() ?>assets/scripts/<?php echo $envjsglobal ?>/login.js?<?php echo time() ?>>"></script>
      <script type="text/javascript">
         site_url = '<?php echo site_url() ?>';
         _csrf_hash = '<?php echo $this->security->get_csrf_hash(); ?>';
      </script>
   </body>
</html>
