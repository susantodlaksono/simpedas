<header class="main-header">
	<a href="<?php echo base_url() ?>" class="logo">
		<span class="logo-mini"><img src="<?php echo base_url('cloudbdg.png') ?>" width="20"></span>
		<span class="logo-lg text-bold" style="font-size: 25px;"><img src="<?php echo base_url('cloudbdg.png') ?>" width="100"></span>
	</a>
	<nav class="navbar navbar-static-top" role="navigation">
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
		<span class="sr-only">Toggle navigation</span>
		</a>
		<div class="navbar-custom-menu text-center">
	   <ul class="nav navbar-nav">
	    	<li class="dropdown notifications-menu">
	         <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
	           <i class="fa fa-bell-o"></i>
	           <span class="label label-warning">0</span>
	         </a>
	         <ul class="dropdown-menu">
	        		<li class="header">You have 10 notifications</li>
	           	<li>
	             	<ul class="menu">
	            		<li>
	                 		<a href="#">
	                 			<i class="fa fa-users text-aqua"></i> 5 new members joined today
	                 		</a>
	            		</li>
	               	<li>
	                 		<a href="#">
	                    		<i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the page and may cause design problems
	                 		</a>
	               	</li>
	             	</ul>
	           	</li>
	           	<li class="footer"><a href="#">View all</a></li>
	      	</ul>
	    	</li>
	    	<li class="dropdown user user-menu">
	        	<a href="#" class="dropdown-toggle" data-toggle="dropdown">
	            <span class="hidden-xs"><i class="fa fa-user"></i> <?php echo $user_profile['username'] ?></span>
	            <i class="fa fa-angle-down"></i>
	        	</a>
	        	<ul class="dropdown-menu ">
	            <li class="user-header" style="">
	             	<img src="<?php echo base_url().$user_profile['avatar'] ?>" class="img-circle" alt="User Image" />
	             	<p style="font-size: 14px;font-weight: bold">
	             		<?php echo $user_profile['first_name'] ?>
	             		<small style="font-weight: bold;font-size: 11px;font-style: italic;">
	             			<!-- <i class="fa fa-envelope"></i> <?php echo $user_profile['email'] ?><br> -->
	             			<i class="fa fa-group"></i>&nbsp;<?php echo $user_group_name['name'] ?>
             				<br>
	             			<?php
	             			if($user_opd){
	             				echo '&nbsp;&nbsp;<i class="fa fa-building"></i>&nbsp;'.$user_opd.'';
	             			}
	             			?>
	             		</small>		
             		</p>
	            </li>
	            <li class="user-footer">
	             	<div class="pull-left">
	              		<a href="<?php echo site_url('profile') ?>" class="btn btn-default btn-flat"><i class="fa fa-user"></i>  Profile</a>
	             	</div>
	             	<div class="pull-right">
	              		<a href="<?php echo site_url('logout-page-autentication') ?> " class="btn btn-default btn-flat"> <i class="fa fa-sign-out"></i>Sign out</a>
	             	</div>
	            </li>
	        	</ul>
	    	</li>
		</ul>
		</div>
	</nav>
</header>