<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="description" content="Task Management" />
		<meta name="author" content="eBdesk Teknologi" />
		<link rel="icon" href="<?php echo base_url('cloudbdg.png') ?>">
		<?php
	   	if (uri_string() == 'profile'){
	   		echo '<title>Simpedas | Profile</title>';
	   	}else{
	      	echo '<title>Simpedas | '.$_menu['name'].'</title>';
	   	}
		?>
		<?php
   		$this->load->view($_apps['template'].'/base/css');
   		$this->load->view($_apps['template'].'/base/_css');
		?>
		<link rel="stylesheet" href="<?php echo $_path_template ?>/custom.css">
		<script src="<?php echo base_url() ?>assets/<?php echo $_apps['template'] ?>/plugins/jquery-1.11.3.min.js"></script>
		<style type="text/css">
			.label-status{
				font-size: 12px;
    			text-transform: uppercase;
			}
			.label-status-table{
				font-size: 11px;
    			text-transform: uppercase;
			}
			.rating-lg .caption{
				display: none;
			}
			.rating-sm .caption{
				display: none;
			}
			.rating-lg{
				font-size: 0px;
			}
			.rating-sm{
				font-size: 0px;
			}
			.description-text{
				font-size: 11px;
    			font-weight: bold;
			}
			.dropdown-options-dms{
				font-size: 12px;
			}
			.pagination-sm > li > a, .pagination-sm > li > span{
			    padding: 2px 7px;
				font-size: 12px;
			}
		</style>
	</head>
	<body class="skin-black-light sidebar-mini pace-done">

  	<div class="wrapper">
  		<?php echo $this->load->view($_apps['template'].'/base/header'); ?>

		<aside class="main-sidebar">
    		<section class="sidebar" style="height : auto">
        		<?php echo Modules::run('modmenu/left_sidebar_menu'); ?>
     		</section>
  		</aside>

  		<div class="content-wrapper">
	 		<section class="content-header">
        		<h1>
        			<i class="<?php echo $_menu['icon'] ?>"></i> <b><?php echo $_menu['name'] ?></b>
        		</h1>
     		</section>
     		<section class="content">
	  			<?php
		      if (!defined('BASEPATH')) 
		         exit('No direct script access allowed');
		         if ($content) {
	            	$this->load->view($content);
		         }  
		      ?>
	      </section>
		</div>
		<footer class="main-footer text-center" style="padding:5px;">
         <span class="text-bold text-muted" style="font-size: 13px;">Copyright &copy; 2021-2022 Simpedas</span> 
     	</footer>
	</div>

 	<?php
		$this->load->view($_apps['template'].'/base/js');
		$this->load->view($_apps['template'].'/base/_js');
	?>

		<script type="text/javascript">
	  		var site_url = '<?php echo site_url(); ?>';
	  		var base_url = '<?php echo base_url(); ?>';
	  		var id_role = '<?php echo $user_group_name['id']; ?>';
	  		_csrf_hash = '<?php echo $this->security->get_csrf_hash() ?>';
	  		var loadingbutton = '<i class="fa fa-spinner fa-spin"></i> 	Please Wait';

	  		$(function (){

	  			// $('body').tooltip({ selector: '[data-toggle="tooltip"]' });

	  			// ajaxManager.addReq({
	     //        type : "GET",
	     //        url : site_url + 'notification/notification/result',
	     //        dataType : "JSON",
	     //        error: function (jqXHR, status, errorThrown) {
	     //           error_handle(jqXHR, status, errorThrown);
	     //        },
	     //        success: function(r){
	     //        	$('.panel-notif-task').find('.panel-body').html(r.task);
	     //        	$('.panel-notif-approval').find('.panel-body').html(r.approval);
	     //        	$('.panel-notif-attendance').find('.panel-body').html(r.attendance);
	     //        	if(r.task){
	     //        		$('.panel-notif-task').find('.panel-footer').removeClass('hidden');
	     //        	}else{
	     //        		$('.panel-notif-task').find('.panel-footer').addClass('hidden');
	     //        	}
	     //        	if(r.approval){
	     //        		$('.panel-notif-approval').find('.panel-footer').removeClass('hidden');
	     //        	}else{
	     //        		$('.panel-notif-approval').find('.panel-footer').addClass('hidden');
	     //        	}
	     //        	if(r.attendance){
	     //        		$('.panel-notif-attendance').find('.panel-footer').removeClass('hidden');
	     //        	}else{
	     //        		$('.panel-notif-attendance').find('.panel-footer').addClass('hidden');
	     //        	}
	     //        },
	     //        complete: function(){
	     //        	_ttltask = $('.panel-notif-task').find(".panel-body > div").length;
	     //        	_ttlappr = $('.panel-notif-approval').find(".panel-body > div").length;
	     //        	_ttlattend = $('.panel-notif-attendance').find(".panel-body > div").length;
	     //        	_totalnotif = (_ttltask + _ttlappr + _ttlattend);
	     //        	if(_totalnotif > 0){
	     //        		$('.badge-notif').html(_totalnotif);
	     //        	}else{
	     //        		$('.badge-notif').remove();
	     //        	}
	     //        }
	     //     }); 

	  			// $(this).on('click', '.show-notif',function(e){
	  			// 	$('#modal-notif').modal('show');
  				// });

  				// $(this).on('click', '.btn-read-notif',function(e){
  				// 	var trgt = $(this).data('trgt');
	  			// 	ajaxManager.addReq({
		    //         type : "GET",
		    //         url : site_url + 'notification/notification/mark_read',
		    //         dataType : "JSON",
		    //         data : {
		    //         	target : trgt
		    //         },
		    //         error: function (jqXHR, status, errorThrown) {
		    //            error_handle(jqXHR, status, errorThrown);
		    //         },
		    //         success: function(r){
		    //         	if(r.success){
		    //         		if(trgt == 1){
			   //          		$('.panel-notif-task').find('.panel-body').html('');
			   //          		$('.panel-notif-task').find('.panel-footer').addClass('hidden');
			   //          		_ttltask = 0;
			   //          	}
			   //          	if(trgt == 2){
			   //          		$('.panel-notif-approval').find('.panel-body').html('');
			   //          		$('.panel-notif-approval').find('.panel-footer').addClass('hidden');
			   //          		_ttlappr = 0;
			   //          	}
		    //         		if(trgt == 3){
			   //          		$('.panel-notif-attendance').find('.panel-body').html('');
			   //          		$('.panel-notif-attendance').find('.panel-footer').addClass('hidden');
			   //          		_ttlattend = 0;
			   //          	}
			   //          	_totalnotif = (_ttltask + _ttlappr + _ttlattend);
			   //          	if(_totalnotif > 0){
			   //          		$('.badge-notif').html(_totalnotif);
			   //          	}else{
			   //          		$('.badge-notif').remove();
			   //          	}
			   //             toastr.success(r.msg);
			   //          }else{
			   //             toastr.error(r.msg);
			   //          }
	     //        	},
		    //         complete: function(){
		    //         }
		    //      }); 
  				// });
	  		});

		</script>
	</body>
</html>