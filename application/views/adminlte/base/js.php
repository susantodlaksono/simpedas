<script type="text/javascript" src="<?php echo base_url() ?>assets/<?php echo $_apps['template'] ?>/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/<?php echo $_apps['template'] ?>/dist/js/app.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/<?php echo $_apps['template'] ?>/plugins/moment/js/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/<?php echo $_apps['template'] ?>/plugins/datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/<?php echo $_apps['template'] ?>/plugins/daterangepicker/js/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/<?php echo $_apps['template'] ?>/plugins/snackbar/js/snackbar.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/<?php echo $_apps['template'] ?>/plugins/form/jquery.form.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/<?php echo $_apps['template'] ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/<?php echo $_apps['template'] ?>/plugins/pace/pace.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url() ?>assets/<?php echo $_apps['template'] ?>/plugins/toastr.js"></script> -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/<?php echo $_apps['template'] ?>/plugins/toastr.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/<?php echo $_apps['template'] ?>/plugins/simplepagination/jquery.simplePagination.js"></script>
<script src="<?php echo $_path_scripts ?>/setup.js?<?php echo time() ?>"></script>
<script src="<?php echo $_path_scripts ?>/widget.js?<?php echo time() ?>"></script>