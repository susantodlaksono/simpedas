<?php
$_js_file_widget = isset($_jswidget) && $_jswidget ? $_jswidget : FALSE;

if ($_js_file_widget) {
    if (!is_array($_js_file_widget)) {
        $_js_file_widget = array((string) $_js_file_widget);
    }
    foreach ($_js_file_widget as $v) {
        $_urlwidget = $v;
        if (!preg_match('/^http/', $_urlwidget)) {
            $_urlwidget = base_url($_urlwidget);
        }
        ?>
        <script type="text/javascript" src="<?php echo $_urlwidget; ?>?version=<?php echo time() ?>"></script>
        <?php
    }
}  
