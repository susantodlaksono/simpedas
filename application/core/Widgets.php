<?php

/**
 * Description of Widgets
 *
 * @author SUSANTO DWILAKSONO
 */
class Widgets extends MY_Controller {

    private $_params;
    
    public function __construct() {
        parent::__construct();
    }

    private function _css() {
        $_css = isset($this->_params['_csswidget']) && $this->_params['_csswidget'] ? $this->_params['_csswidget'] : FALSE;
        if ($_css) {
            if (!is_array($_css)) {
                $_css = array((string) $_css);
            }
            foreach ($_css as $url) {
                $css[] = preg_match('/^http/', $url) ? $url : base_url($url);
            }
        }
        return isset($css) ? $css : FALSE;
    }

    private function _js() {
        $_js = isset($this->_params['_jswidget']) && $this->_params['_jswidget'] ? $this->_params['_jswidget'] : FALSE;
        if ($_js) {
            if (!is_array($_js)) {
                $_js = array((string) $_js);
            }
            foreach ($_js as $url) {
                $js[] = preg_match('/^http/', $url) ? $url : base_url($url);
            }
        }
        return isset($js) ? $js : FALSE;
    }

    public function render($params = array()) {
        $this->_params = $params;
        $this->_params['module'] = uri_string();
        $this->_params['unixid'] = uniqid();
        $this->_params['widget_name'] = str_replace('/', '_', uri_string());
        $data = array(
            '_css_file_widget' => $this->_css(),
            // '_js_file_widget' => $this->_js(),
            'container' => $this->input->get('container'),
            'html' => $this->load->view('widget', $this->_params, TRUE)
        );
        header('Content-Type: application/json');
        echo json_encode($data);
   }
}
