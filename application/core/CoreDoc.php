<?php
if (!defined('BASEPATH'))
   exit('No direct script access allowed');

/**
 * Description of Widgets
 *
 * @author SUSANTO DWILAKSONO
 */
class CoreDoc extends MY_Controller {

 	public function __construct() {
     	parent::__construct();
     	if(in_array(1, $this->_role_id)){
         $this->render_page($data, 'superadmin', 'modular');
      }else if(in_array(2, $this->_role_id)){
         $this->_defaultdir = 'dms/apd/'.$this->_user->id;
      }else if(in_array(3, $this->_role_id)){
         $this->_defaultdir = 'dms/kadis/'.$this->_user->id;
      }else if(in_array(5, $this->_role_id)){
         $this->_defaultdir = 'dms/pedasi/'.$this->_user->id;
      }else if(in_array(4, $this->_role_id)){
         $this->_defaultdir = 'dms/kasie/'.$this->_user->id;
      }	
 	}

   public function dir_create(){
      if (!file_exists($this->_defaultdir)) {
         mkdir($this->_defaultdir);
         chmod($this->_defaultdir, 0777);
      }
   }

   public function reader($path){
      if($path){
         $dir = $this->_defaultdir.'/'.$path;
      }else{
         $dir = $this->_defaultdir;
      }

      $scan = scandir(urldecode($dir));
      unset($scan[array_search('.', $scan, true)]);
      unset($scan[array_search('..', $scan, true)]);

      if (count($scan) < 1)
         return;

      return array(
         'scan' => $scan,
         'dir' => $dir
      );
   }

   public function delete_folder_files($dir, $dirnoencode){
      $files = array_diff(scandir($dir), array('.','..'));
      foreach ($files as $file) {
         if(is_dir("$dir/$file")){
            $this->delete_folder_files("$dir/$file", "$dirnoencode/$file");
         }else{
            $this->delete_full_dir_tags($dirnoencode.'/'.urlencode($file), $this->_user->id);
            $this->delete_request_files($dirnoencode.'/'.urlencode($file), $this->_user->id);
            $this->delete_full_dir_shared($dirnoencode.'/'.urlencode($file), $this->_user->id);
            unlink("$dir/$file");

         }
      }
      $this->delete_full_dir_tags($dirnoencode, $this->_user->id);
      $this->delete_request_files($dirnoencode, $this->_user->id);
      $this->delete_full_dir_shared($dirnoencode, $this->_user->id);
      return rmdir($dir);
   }

   public function search_dir_subdirectory($keyword){
      $this->load->library('global_mapping');
      $recursive = new RecursiveDirectoryIterator($this->_defaultdir);
      $i = 0;
      $result = array();

      if($recursive){
         foreach(new RecursiveIteratorIterator($recursive) as $file){
            if (!((stripos($file, $keyword)) === false) || empty($keyword)){
               $dir = preg_replace("#/#", "/", $file);
               if(is_file($dir)){
                  $explode = explode('/', $dir);
                  $exp_limit = explode('/', $dir, 4);
                  $num = end($explode);
                  $ext = pathinfo($dir, PATHINFO_EXTENSION);
                  $explode_exp_limit = explode('/', $exp_limit[3]);
                  foreach (array_keys($explode_exp_limit,  $num, true) as $key) {
                     unset($explode_exp_limit[$key]);
                  }
                  $implode_final = implode('/', $explode_exp_limit);

                  $result[$i]['directory'] = $dir;
                  $result[$i]['filename'] = $num;
                  if($exp_limit[3] == $num){
                     $result[$i]['path'] = null;
                  }else{
                     $result[$i]['path'] = $exp_limit[3];
                  }
                  if($implode_final){
                     $result[$i]['imp_final'] = urlencode($implode_final);
                     $result[$i]['imp_final_clean'] = $implode_final;
                  }else{
                     $result[$i]['imp_final'] = null;
                     $result[$i]['imp_final_clean'] = null;
                  }
                  $result[$i]['ext'] = $this->global_mapping->mapping_icon('.'.$ext.'', TRUE);
                  $i++;
               }
            }
         }
      }
      return $result;
   }

   public function privileges_folder_ternary($dir, $name){
      $exploded = explode('/', $dir);
      $key = array_search($name, $exploded);
      $offset = ($key + 1);
      $implode = implode('/', $exploded);
      $finalexp = explode('/', $implode, $offset);
      $numend = end($finalexp);
      foreach (array_keys($finalexp,  $numend, true) as $k) {
          unset($finalexp[$k]);
      }
      return $finalexp;
   }
   
   public function delete_full_dir_tags($dir, $userid){
      $this->db->where('full_dir', $dir);
      $this->db->where('created_by', $userid);
      $this->db->delete('document_tags');
   }

   public function delete_full_dir_shared($dir, $userid){
      $this->db->where('full_dir', $dir);
      $this->db->where('created_by', $userid);
      $this->db->delete('document_shared');
   }

   public function delete_request_files($dir, $userid){
      $this->db->where('full_path', $dir);
      $this->db->where('created_by', $userid);
      $this->db->delete('request_result_files');
   }
}