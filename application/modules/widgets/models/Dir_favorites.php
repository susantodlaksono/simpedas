<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Dir_favorites extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function getting($mode, $params, $userid){
      $this->db->where('created_by', $userid);
      $this->db->where('type_dms', 3);
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('document_tags')->result_array();
         case 'count':
            return $this->db->get('document_tags')->num_rows();
      }
   }

}