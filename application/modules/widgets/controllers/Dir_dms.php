<?php

/**
 * Description of Users
 *
 * @author SUSANTO DWILAKSONO
 */
class Dir_dms extends Widgets {

   public function __construct() {
      parent::__construct();
      $this->load->library('global_mapping');
      $this->load->library('date_extraction');
      $this->load->library('dir_extraction');
      $this->_path = 'dms/pedasi/'.$this->_user->id;
      $this->_uniqid = uniqid();
   }

   public function index() {
   	$data = array(
         'container' => $this->_get['container'],
         '_jswidget' => 'assets/widgets/uploader_file.js',
         'view' => 'dir_dms'
      );
      $this->render($data);
	}

   public function read_dir(){
      $response['result'] = $this->list_folder_files($this->_get);
      $response['dir_base'] = $this->_path;
      $response['dir_clean'] = urldecode($this->_get['dir']);

      $nav = '';
      $nav .= '<li><a data-href="" class="breadcrumb-action-home" style="cursor:pointer;"><i class="fa fa-home"></i></a></li>';
      $uri_string = explode('/', urldecode($this->_get['dir']));
      $uri = array();
      foreach (array_filter($uri_string) as $rs) {
         $uri[] = urlencode($rs);
         $nav .= '<li><a data-href="'.implode('/', $uri).'" class="breadcrumb-action">'.$rs.'</a></li>';
      }
      $response['nav'] = $nav;
      $this->json_result($response);
   }

   public function list_folder_files($params){
      if($params['dir']){
         $dir = $this->_path.'/'.urldecode($params['dir']);
      }else{
         $dir = $this->_path;
      }

      $ffs = scandir($dir);

      unset($ffs[array_search('.', $ffs, true)]);
      unset($ffs[array_search('..', $ffs, true)]);

      // prevent empty ordered elements
      if (count($ffs) < 1)
         return;

      $i = 0;
      $data = array();
      
      $lock_dms = $this->get_document_tags(1);
      $share_dms = $this->get_document_tags(2);
      $favorite_dms = $this->get_document_tags(3);

      foreach($ffs as $ff){
         $time = date('d M Y H:i:s', filectime($dir.'/'.$ff));
         $data[$i]['name'] = $ff;
         if(is_file($dir.'/'.$ff)){
            $ext = pathinfo($dir.'/'.$ff, PATHINFO_EXTENSION);
            $data[$i]['ext_orig'] = $ext;
            $data[$i]['ext'] = $this->global_mapping->mapping_icon_request('.'.$ext.'');
         }else{
            $data[$i]['ext_orig'] = NULL;
            $data[$i]['ext'] = NULL;
         }
         $data[$i]['base_dir'] = $dir;
         $data[$i]['url'] = urlencode($ff);
         $data[$i]['is_file'] = is_file($dir.'/'.$ff);
         $data[$i]['time'] = $this->date_extraction->time_ago($time);
         $data[$i]['clean_time'] = $time;
         $data[$i]['size'] = $this->global_mapping->format_size_units(filesize($dir.'/'.$ff));
         $data[$i]['size_bytes'] = filesize($dir.'/'.$ff);

         if($lock_dms){
            $cond_lock = $this->dir_extraction->in_array_r($dir.'/'.$ff, $lock_dms);
            if($cond_lock){
               $data[$i]['lock'] = $cond_lock['id'];
            }else{
               $data[$i]['lock'] = FALSE;
            }
         }

         if($share_dms){
            $cond_share = $this->dir_extraction->in_array_r($dir.'/'.$ff, $share_dms);
            if($cond_share){
               $data[$i]['share'] = $cond_share['id'];
            }else{
               $data[$i]['share'] = FALSE;
            }
         }

         if($favorite_dms){
            $cond_favorite = $this->dir_extraction->in_array_r($dir.'/'.$ff, $favorite_dms);
            if($cond_favorite){
               $data[$i]['favorites'] = $cond_favorite['id'];
            }else{
               $data[$i]['favorites'] = FALSE;
            }
         }

         $i++;
      }
      if($data){
         if($params['orderby'] == 'desc'){
            usort($data, function($b, $a) use ($params) {
                return $a[$params['order']] <=> $b[$params['order']];
            });
         }
         if($params['orderby'] == 'asc'){
            usort($data, function($a, $b) use ($params) {
                return $a[$params['order']] <=> $b[$params['order']];
            });
         }
      }
      return array(
         'data' => $data,
         'params' => $params['order'],
         'total' => count($data)
      );
   }

   public function upload_file(){
      try {
         if (
            !isset($_FILES['file']['error']) ||
            is_array($_FILES['file']['error'])
         ) {
            throw new RuntimeException('Invalid parameters.');
         }
         switch ($_FILES['file']['error']) {
            case UPLOAD_ERR_OK:
            break;
            case UPLOAD_ERR_NO_FILE:
               throw new RuntimeException('No file sent.');
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
               throw new RuntimeException('Exceeded filesize limit.');
            default:
               throw new RuntimeException('Unknown errors.');
         }

         $config['upload_path']  = $this->_post['dir'] ? $this->_path.'/'.urldecode($this->_post['dir']) : $this->_path;
         $config['allowed_types'] = '*';
         $config['overwrite'] = TRUE;
         $config['remove_spaces'] = FALSE; 


         $this->load->library('upload', $config);
         if($this->upload->do_upload('file')){
            $uploadresult = $this->upload->data();
            $permission = $uploadresult['full_path'];
            chmod($permission, 0777);
            
            $response['path'] = $config['upload_path'];
            $response['status'] = 'ok';
         }else{
            $response['status'] = 'error';
            $response['path'] = $config['upload_path'];
            $response['msg'] = $this->upload->display_errors();
         }
      }catch (RuntimeException $e) {
         http_response_code(400);
         $response['status'] = 'error';
         $response['message'] = $e->getMessage();
      }
      $this->json_result($response);
   }

   public function upload_folder(){
      $response['msg'] = 'Some error occured';
      $done_upload = 0;
      $fail_upload = 0;
      $process = FALSE;

      if(isset($this->_post['folder_name']) && count($this->_post['folder_name']) > 0){
         foreach ($this->_post['folder_name'] as $v) {
            if($this->_post['dir']){
               $upload_path = $this->_path.'/'.urldecode($this->_post['dir']).'/'.$v;
            }else{
               $upload_path = $this->_path.'/'.$v;
            }
            if (!file_exists($upload_path)) {
               mkdir($upload_path);
               chmod($upload_path, 0777);
               $done_upload += 1;
            }else{
               $fail_upload += 1;
            }
         }
         $process = TRUE;
      }
      if($process) {
         $response['msg'] = $done_upload.' Folder berhasil dibuat '.$fail_upload.' folder gagal di upload';
         $response['success'] = $process;
      }else{
         $response['msg'] = 'Gagal mengupload folder';
      }
      $this->json_result($response);
   }

   public function assign_flag(){
      $response['success'] = FALSE;
      $response['msg'] = 'Function failed';

      $this->db->trans_start();
      $tmp['file_name'] = $this->_post['name'] ? $this->_post['name'] : NULL;
      $tmp['file_dir'] = $this->_post['dir'] ? urldecode($this->_post['dir']) : NULL;
      if($this->_post['dir'] && $this->_post['name']){
         $tmp['full_dir'] = urldecode($this->_post['dir']).'/'.urldecode($this->_post['name']);
      }
      $tmp['file_extension'] = $this->_post['ext'] ? $this->_post['ext'] : NULL;
      $tmp['type_dms'] = $this->_post['status'];
      $tmp['created_by'] = $this->_user->id;
      $tmp['created_at'] = date('Y-m-d H:i:s');
      $this->db->insert('document_tags', $tmp);
      $this->db->trans_complete();

      if($this->db->trans_status()){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         if($this->_post['status'] == 1){
            $response['msg'] = 'File berhasil di lock';
         }
         if($this->_post['status'] == 2){
            $response['msg'] = 'File berhasil di share';
         }
         if($this->_post['status'] == 3){
            $response['msg'] = 'Favorite berhasil di tambahkan';
         }
      }else{
         $this->db->trans_rollback();
      }
      $this->json_result($response);
   }

   public function delete_folder(){
      $response['success'] = FALSE;
      $response['msg'] = 'Function failed';

      if($this->_post['dir'] && $this->_post['name']){
         $path = urldecode($this->_post['dir']).'/'.urldecode($this->_post['name']);
         
         $this->db->trans_start();
         $this->delete_flag($path);
         $this->delete_folder_files($path);
         $this->db->trans_complete();

         if($this->db->trans_status()){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'folder berhasil dihapus';
         }else{
            $this->db->trans_rollback();
         }
      }
      $this->json_result($response);
   }

   public function delete_flag($path){
      $this->db->where('created_by', $this->_user->id);
      $this->db->where('full_dir', $path);
      $this->db->delete('document_tags');
   }

   public function delete_folder_files($dir){
      $files = array_diff(scandir($dir), array('.','..'));
      foreach ($files as $file) {
         if(is_dir("$dir/$file")){
            $this->delete_folder_files("$dir/$file");
         }else{
            $full_dir_lock = $dir.'/'.$file;
            $this->db->where('created_by', $this->_user->id);
            $this->db->where('full_dir', $dir.'/'.$file);
            $this->db->delete('document_tags');
            unlink("$dir/$file");

         }
      }
      $this->db->where('created_by', $this->_user->id);
      $this->db->where('full_dir', $dir);
      $this->db->delete('document_tags');
      return rmdir($dir);
   }

   public function remove_flag(){
      $response['success'] = FALSE;
      $response['msg'] = 'Function failed';

      $this->db->trans_start();
      $prm['id'] = $this->_get['id'];
      $prm['type_dms'] = $this->_get['status'];
      $this->db->delete('document_tags', $prm);
      $this->db->trans_complete();

      if($this->db->trans_status()){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['msg'] = 'Tanda berhasil di hapus';
      }else{
         $this->db->trans_rollback();
      }
      $this->json_result($response);
   }
   

   public function delete_file(){
      $response['success'] = FALSE;
      $response['msg'] = 'Function failed';

      if($this->_post['dir'] && $this->_post['name']){
         $path = urldecode($this->_post['dir']).'/'.urldecode($this->_post['name']);
         
         $this->db->trans_start();
         $tmp['created_by'] = $this->_user->id;
         $tmp['full_dir'] = $path;
         $this->db->delete('document_tags', $tmp);
         unlink($path);
         $this->db->trans_complete();

         if($this->db->trans_status()){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'file berhasil dihapus';
         }else{
            $this->db->trans_rollback();
         }
      }
      $this->json_result($response);
   }

   public function get_document_tags($status){
      $data = array();
      $this->db->where('created_by', $this->_user->id);
      $this->db->where('type_dms', $status);
      $result = $this->db->get('document_tags')->result_array();
      if($result){
         foreach ($result as $v) {
            if($v['file_dir'] && $v['file_name']){
               $data[$v['id']]['dir'] = $v['full_dir'];
               $data[$v['id']]['id'] = $v['id'];
            }
         }
         return $data;
      }else{
         return FALSE;
      }
   }

}