<?php

/**
 * Description of Users
 *
 * @author SUSANTO DWILAKSONO
 */
class Dir_favorites extends Widgets {

   public function __construct() {
      parent::__construct();
      $this->load->library('global_mapping');
      $this->load->library('date_extraction');
      $this->load->library('dir_extraction');
      $this->_path = 'dms/pedasi/'.$this->_user->id;
   }

   public function index() {
   	$data = array(
   		'container' => $this->_get['container'],
         'view' => 'dir_favorites'
      );
      $this->render($data);
	}

	public function get_favorites(){
      $list = array();
      $params = $this->_get;
      $data = $this->getting('get', $this->_get, $this->_user->id);
      if($data){
      	$i = 0;
         foreach ($data as $v) {
         	$time = date('d M Y H:i:s', filectime($v['full_dir']));
            $is_file = is_file($v['full_dir']);

         	$list[$i]['id'] = $v['id'];
         	$list[$i]['is_file'] = $is_file;
         	$list[$i]['clean_time'] = $time;
         	$list[$i]['size'] = $this->global_mapping->format_size_units(filesize($v['full_dir']));
         	$list[$i]['size_bytes'] = filesize($v['full_dir']);
         	$list[$i]['file_name'] = $v['file_name'];
         	$list[$i]['file_dir'] = $v['file_dir'];
         	$list[$i]['full_dir'] = $v['full_dir'];
         	$list[$i]['encode_full_dir'] = urlencode($v['full_dir']);
         	$list[$i]['encode_file_name'] = urlencode($v['file_name']);
         	$list[$i]['created_date'] = $this->date_extraction->time_ago($time);
         	$list[$i]['file_extension'] = $v['file_extension'] ? $this->global_mapping->mapping_icon_request('.'.$v['file_extension'].'') : NULL;
         	$i++;
         }
         if($list){
	    		if($params['orderby'] == 'desc'){
	    			usort($list, function($b, $a) use ($params) {
					    return $a[$params['order']] <=> $b[$params['order']];
					});
	    		}
	    		if($params['orderby'] == 'asc'){
	    			usort($list, function($a, $b) use ($params) {
					    return $a[$params['order']] <=> $b[$params['order']];
					});
	    		}
	    	}
         $response['result'] = $list;
         $response['total'] = $this->getting('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

   public function read_dir(){
   	$response['result'] = $this->list_folder_files($this->_get);
   	$response['dir_base'] = $this->_path;
   	$response['dir_clean'] = urldecode($this->_get['dir']);

   	$nav = '';
   	$nav .= '<li><a data-href="" class="breadcrumb-action-favorites-home" style="cursor:pointer;"><i class="fa fa-home"></i></a></li>';
   	$uri_string = explode('/', urldecode($this->_get['dir']));
   	$uri = array();
      foreach (array_filter($uri_string) as $rs) {
       	$uri[] = urlencode($rs);
       	$nav .= '<li><a data-href="'.implode('/', $uri).'" class="breadcrumb-action-favorites">'.$rs.'</a></li>';
      }
      $response['nav'] = $nav;
   	$this->json_result($response);
   }

   public function getting($mode, $params, $userid){
      $this->db->where('created_by', $userid);
      $this->db->where('type_dms', 3);
      switch ($mode) {
         case 'get':
            return $this->db->get('document_tags')->result_array();
         case 'count':
            return $this->db->get('document_tags')->num_rows();
      }
   }

   public function list_folder_files($params){
   	if($params['dir']){
   		$dir = $this->_path.'/'.urldecode($params['dir']);
   	}else{
   		$dir = $this->_path;
   	}

    	$ffs = scandir($dir);

    	unset($ffs[array_search('.', $ffs, true)]);
    	unset($ffs[array_search('..', $ffs, true)]);

    	// prevent empty ordered elements
    	if (count($ffs) < 1)
        	return;

     	$i = 0;
     	$data = array();
     	
     	$lock_dms = $this->get_document_tags(1);
     	$share_dms = $this->get_document_tags(2);
     	$favorite_dms = $this->get_document_tags(3);

    	foreach($ffs as $ff){
	    	$time = date('d M Y H:i:s', filectime($dir.'/'.$ff));
	    	$data[$i]['name'] = $ff;
	    	if(is_file($dir.'/'.$ff)){
	    		$ext = pathinfo($dir.'/'.$ff, PATHINFO_EXTENSION);
	    		$data[$i]['ext_orig'] = $ext;
	    		$data[$i]['ext'] = $this->global_mapping->mapping_icon_request('.'.$ext.'');
	    	}else{
	    		$data[$i]['ext_orig'] = NULL;
	    		$data[$i]['ext'] = NULL;
	    	}
	    	$data[$i]['base_dir'] = $dir;
	    	$data[$i]['url'] = urlencode($ff);
	    	$data[$i]['is_file'] = is_file($dir.'/'.$ff);
	    	$data[$i]['time'] = $this->date_extraction->time_ago($time);
         $data[$i]['clean_time'] = $time;
	    	$data[$i]['size'] = $this->global_mapping->format_size_units(filesize($dir.'/'.$ff));
	    	$data[$i]['size_bytes'] = filesize($dir.'/'.$ff);

	    	if($lock_dms){
	    		$cond_lock = $this->dir_extraction->in_array_r($dir.'/'.$ff, $lock_dms);
	    		if($cond_lock){
	    			$data[$i]['lock'] = $cond_lock['id'];
	    		}else{
	    			$data[$i]['lock'] = FALSE;
	    		}
	    	}

	    	if($share_dms){
	    		$cond_share = $this->dir_extraction->in_array_r($dir.'/'.$ff, $share_dms);
	    		if($cond_share){
	    			$data[$i]['share'] = $cond_share['id'];
	    		}else{
	    			$data[$i]['share'] = FALSE;
	    		}
	    	}

	    	if($favorite_dms){
	    		$cond_favorite = $this->dir_extraction->in_array_r($dir.'/'.$ff, $favorite_dms);
	    		if($cond_favorite){
	    			$data[$i]['favorites'] = $cond_favorite['id'];
	    		}else{
	    			$data[$i]['favorites'] = FALSE;
	    		}
	    	}

	    	$i++;
    	}
    	if($data){
    		if($params['orderby'] == 'desc'){
    			usort($data, function($b, $a) use ($params) {
				    return $a[$params['order']] <=> $b[$params['order']];
				});
    		}
    		if($params['orderby'] == 'asc'){
    			usort($data, function($a, $b) use ($params) {
				    return $a[$params['order']] <=> $b[$params['order']];
				});
    		}
    	}
    	return array(
    		'data' => $data,
    		'params' => $params['order'],
    		'total' => count($data)
    	);
	}

	public function get_document_tags($status){
   	$data = array();
   	$this->db->where('created_by', $this->_user->id);
   	$this->db->where('type_dms', $status);
   	$result = $this->db->get('document_tags')->result_array();
   	if($result){
   		foreach ($result as $v) {
   			if($v['file_dir'] && $v['file_name']){
   				$data[$v['id']]['dir'] = $v['full_dir'];
   				$data[$v['id']]['id'] = $v['id'];
   			}
   		}
   		return $data;
   	}else{
   		return FALSE;
   	}
   }
}