<?php

/**
 * Description of Users
 *
 * @author SUSANTO DWILAKSONO
 */
class Load_personel extends Widgets {

   public function __construct() {
      parent::__construct();
      $this->load->library('mask_date');
   }

   public function index() {
      $users = $this->get_users($this->_get['division']);
      for($i=$this->_get['month']; $i<=$this->_get['monthend']; $i++) {
         $numpadded = sprintf("%02d", $i);
         $masking_date[$numpadded] = $this->mask_date->masking_date('monthly', $i, $this->_get['year']);
      }
      $result = $this->get_results($users, $masking_date, $this->_get);
      $data = array(
         'categories' => $masking_date,
         'result' => $result,
         'view' => 'load_personel'
      );
      $this->render($data);
   }

   public function get_users($division){
      $this->db->select('user_id, fullname');
      $this->db->where('division_id', $division);
      return $this->db->get('members')->result_array();
   }

   public function get_results($users, $categories, $params){
      foreach ($users as $v) {
         $data[$v['user_id']]['fullname'] = $v['fullname'];
         $data[$v['user_id']]['bgcolor'] = '';
         $data[$v['user_id']]['color'] = '';
         $data[$v['user_id']]['activity_all'] = $this->get_activity($v['user_id'], $params, $categories);
         // $data[$v['user_id']]['project'] = $this->get_activity($v['user_id'], $params['year'].''.$params['month'].'-'.$categories);
         $data[$v['user_id']]['project'] = $this->get_project_name($v['user_id'], $params, $categories);
      }
      return $data;
   }

   public function get_project_name($userid, $params, $categories){
      $this->db->select('b.name');
      $this->db->where('a.assigned_to', $userid);
      $this->db->join('project as b', 'a.project_id = b.id', 'left');
      $this->db->group_by('a.project_id');
      $result = $this->db->get('project_task_member as a')->result_array();
      if($result){
         foreach ($result as $v) {
            $data[] = $v['name'];
         }
         return implode('<br /><br />', $data);
      }else{
         return FALSE;
      }
   }

   public function get_project($userid, $params, $categories){
      $this->db->select('b.id, b.code, b.name');
      $this->db->where('a.assigned_to', $userid);
      $this->db->join('project as b', 'a.project_id = b.id', 'left');
      $this->db->group_by('a.project_id');
      $result = $this->db->get('project_task_member as a')->result_array();
      if($result){
         foreach ($result as $v) {
            $data[$v['id']]['code'] = $v['code'];
            $data[$v['id']]['name'] = $this->limit_text($v['name'], 10);
            $data[$v['id']]['result'] = $this->get_activity_project($v['id'], $userid, $params, $categories);
         }
         return $data;
      }else{
         return FALSE;
      }
   }

   public function get_activity($userid, $params, $categories){
      foreach ($categories as $k => $v) {
         foreach ($v as $vv) {
            $data[$k][$vv] = $this->count_by_date_member($userid, $params['year'].'-'.$k.'-'.$vv);
         }
      }
      return $data;
   }
   public function get_activity_project($projectid, $userid, $params, $categories){
      foreach ($categories as $v) {
         $data[] = $this->count_by_date_project($projectid, $userid, $params['year'].'-'.$params['month'].'-'.$v);
      }
      return $data;
   }

   public function count_by_date_member($userid, $date){
      $this->db->where('created_by', $userid);
      $this->db->where('task_id IS NOT NULL');
      $this->db->where('date_activity', $date);
      $result = $this->db->get('project_task_activity')->num_rows();
      if($result > 0){
         return '#6fd0ff';
      }else{
         return '';
      }
   }

   public function count_by_date_project($projectid, $userid, $date){
      $this->db->where('project_id', $projectid);
      $this->db->where('task_id IS NOT NULL');
      $this->db->where('created_by', $userid);
      $this->db->where('date_activity', $date);
      $result = $this->db->get('project_task_activity')->num_rows();
      if($result > 0){
         return '#f5a13a';
      }else{
         return '';
      }
   }

   public function limit_text($text, $limit) {
      if (str_word_count($text, 0) > $limit) {
         $words = str_word_count($text, 2);
         $pos   = array_keys($words);
         $text  = substr($text, 0, $pos[$limit]) . '<span data-toggle="tooltip" data-title="'.$text.'">...</span>';
      }
      return $text;
   }
}