<?php

/**
 * Description of Users
 *
 * @author SUSANTO DWILAKSONO
 */
class Summary_users extends Widgets {

   public function __construct() {
      parent::__construct();
   }

   public function index() {
      $data['_jswidget'] = array(
         'assets/scripts/'.$this->_apps['envjsglobal'].'/widgets/summary_users.js?version=' . time()
      );
      $data['view'] = 'summary_users';
      // $data = array(
      //    '_jswidget' => array(
      //       'assets/scripts/'.$this->_apps['envjsglobal'].'/widgets/summary_users.js?version=' . time()
      //    ),
      //    'view' => 'summary_users'
      // );
      $this->render($data);
   }
}