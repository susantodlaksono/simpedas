<style type="text/css">
	.breadcrumb-action-favorites{
		cursor:pointer;font-size: 13px;
	}
</style>

<div id="<?php echo $widget_name ?>_<?php echo $unixid ?>"> 
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb sect-nav-render hidden"></ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="mailbox-messages">
	    		<table class="table table-hover table-striped" style="font-size: 13px;">
	    			<thead>	
	    				<th width="60"></th>
	    				<th>Name <a class="change_order co" href="#" data-order="name" data-by="desc"><i class="fa fa-sort"></i></a></th>
	    				<th></th>
	    				<th>Time <a class="change_order co" href="#" data-order="clean_time" data-by="asc"><i class="fa fa-sort-desc"></i></a></th>
	    				<th>Size <a class="change_order co" href="#" data-order="size_bytes" data-by="desc"><i class="fa fa-sort"></i></a></th>
	 				</thead>
	    			<tbody class="sect-data"></tbody>
	 			</table>
			</div>
		</div>
	</div>
</div>

<script>
	var widget_name = '<?php echo $widget_name ?>';
	var unixid = '<?php echo $unixid ?>';
	var container = '#<?php echo $container ?>';
</script>