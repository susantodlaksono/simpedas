<style type="text/css">
	.table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td{
		padding:2px;
	}
	.popover{
    	width: 910px; /* Max Width of the popover (depending on the container!) */
	}
	.trim-table{
	    /*white-space: nowrap;
	    text-overflow: ellipsis;
	    display: inline-block;
	    overflow: hidden;
	    width: auto;*/
	}
</style>
<div class="row">
	<div class="col-md-3">
		<table class="table table-bordered">
			<thead>
				<th width="300" style="height:34px;"></th>
			</thead>
			<thead>
				<th width="300">Member Name</th>
			</thead>
			<tbody>
				<?php
				foreach ($result as $k => $v) {
					echo '<tr>';
					echo '<td style="background-color:'.$v['bgcolor'].';font-weight:bold;color:'.$v['color'].';fontsize:13px;">';
					// echo '<div id="member-'.$k.'" style="display:none;">'.($v['project'] ? $v['project'] : '').'</div>';
					echo '<span style="cursor:pointer" data-toggle="popover" data-html="true" data-trigger="hover" data-placement="right" data-content="'.($v['project'] ? $v['project'] : '').'">';
					echo $v['fullname'];
					echo '</span>';
					echo '</td>';
					echo '</tr>';
				}
				?>
			</tbody>
		</table>		
	</div>
	<div class="col-md-9">
		<table class="table table-bordered">
			<thead>
				<?php
				$i = 0;
				foreach ($categories as $k => $v) {
					$total = count($v);
					$dt = DateTime::createFromFormat('!m', $k);
					$mod = ($i % 2);
					if($mod == 0){
						$bg = '#a9f5ea';
					}else{
						$bg = '';
					}
					echo '<th class="text-center" style="background-color:'.$bg.'" colspan="'.$total.'">'.$dt->format('M').'</th>';
					$i++;
		      }
				?>
			</thead>
			<thead>
				<?php
				foreach ($categories as $k => $v) {
					// echo '<th class="text-center" rowspan="'.$total.'"></th>';
		         foreach ($v as $vv) {
		            echo '<th class="text-center">'.$vv.'</th>';
		         }
		      }
				?>
			</thead>
			<tbody>
			<?php
			foreach ($result as $k => $v) {
				echo '<tr style="height:34px;">';
				foreach ($v['activity_all'] as $k => $v) {
		         foreach ($v as $vv) {
            		echo '<td style="background-color:'.$vv.'"></td>';
		         }
		      }
		      echo '</tr>';
				// echo '<tr style="height:34px;">';
				// if($v['activity_all']){
				// 	foreach ($v['activity_all'] as $vaa) {
				// 		echo '<td style="background-color:'.$vaa.'"></td>';
				// 	}
				// }
				// if($v['project']){
				// 	foreach ($v['project'] as $vv) {
				// 		echo '<tr>';
				// 		echo '<td class="trim-table">'.$vv['name'].'</td>';
				// 		if($vv['result']){
				// 			foreach ($vv['result'] as $vvp) {
				// 				echo '<td style="background-color:'.$vvp.'"></td>';
				// 			}
				// 		}
				// 		echo '</tr>';
				// 	}
				// }
			}
			?>
			</tbody>
		</table>		
	</div>
</div>
<!-- <table class="table table-condensed">
	<thead>
		<th style="width:500px;"></th>
		<?php
		foreach ($categories as $v) {
			echo '<th class="text-center">'.$v.'</th>';
		}
		?>
	</thead>
	<tbody>
		<?php
		foreach ($result as $k => $v) {
			echo '<tr>';
			echo '<td style="background-color:'.$v['bgcolor'].';font-weight:bold;color:'.$v['color'].';fontsize:13px;width:500px;">';
			// echo '<div id="member-'.$k.'" style="display:none;">'.($v['project'] ? $v['project'] : '').'</div>';
			echo '<span style="cursor:pointer" data-toggle="popover" data-html="true" data-trigger="hover" data-placement="bottom" data-content="'.($v['project'] ? $v['project'] : '').'">';
			echo $v['fullname'];
			echo '</span>';
			echo '</td>';
			if($v['activity_all']){
				foreach ($v['activity_all'] as $vaa) {
					echo '<td style="background-color:'.$vaa.'"></td>';
				}
			}
			echo '</tr>';
			// if($v['project']){
			// 	foreach ($v['project'] as $vv) {
			// 		echo '<tr>';
			// 		echo '<td class="trim-table">'.$vv['name'].'</td>';
			// 		if($vv['result']){
			// 			foreach ($vv['result'] as $vvp) {
			// 				echo '<td style="background-color:'.$vvp.'"></td>';
			// 			}
			// 		}
			// 		echo '</tr>';
			// 	}
			// }
		}
		?>
	</tbody>
</table> -->

<script type="text/javascript">
	$('[data-toggle="popover"]').each(function(i, el){
		var $this = $(el),
			placement = attrDefault($this, 'placement', 'right'),
			trigger = attrDefault($this, 'trigger', 'click'),
			popover_class = $this.hasClass('popover-secondary') ? 'popover-secondary' : ($this.hasClass('popover-primary') ? 'popover-primary' : ($this.hasClass('popover-default') ? 'popover-default' : ''));

		$this.popover({
			placement: placement,
			trigger: trigger
		});

		$this.on('shown.bs.popover', function(ev)
		{
			var $popover = $this.next();

			$popover.addClass(popover_class);
		});
	});
</script>