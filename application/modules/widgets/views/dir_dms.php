<style type="text/css">
	.breadcrumb-action-favorites{
		cursor:pointer;font-size: 13px;
	}
</style>
<div id="<?php echo $widget_name ?>_<?php echo $unixid ?>"> 
	<div class="row">
		<div class="col-md-11">
			<ol class="breadcrumb sect-nav-render">
				<li><a data-href="" class="breadcrumb-action"><i class="fa fa-home"></i></a></li>
			</ol>
		</div>
		<div class="col-md-1">
     		<button class="btn dropdown-toggle btn-default lg show-options" data-toggle="dropdown" style="margin-top: 3px;">
     			<i class="fa fa-plus"></i>
  			</button>
       	<ul class="dropdown-menu" style="left:-105px;border:1px solid #cccccc;">
       		<li>
       			<a href="" data-toggle="modal" data-target="#modal-upload-file" style="font-size: 12px;">
       				<i class="fa fa-file"></i>&nbsp;Upload File
       			</a>
       		</li>
       		<li class="divider"></li>
       		<li>
       			<a class="btn-upload-folder" href="" data-toggle="modal" data-target="#modal-upload-folder" style="font-size: 12px;">
    					<i class="fa fa-folder"></i>&nbsp;New Folder
    				</a>
    			</li>
    		</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="mailbox-messages">
	    		<table class="table table-hover table-striped" style="font-size: 13px;">
	    			<thead>	
	    				<th width="60"></th>
	    				<th>Name <a class="change_order" href="#" data-order="name" data-by="desc"><i class="fa fa-sort"></i></a></th>
	    				<th></th>
	    				<th>Time <a class="change_order" href="#" data-order="clean_time" data-by="asc"><i class="fa fa-sort-desc"></i></a></th>
	    				<th>Size <a class="change_order" href="#" data-order="size_bytes" data-by="desc"><i class="fa fa-sort"></i></a></th>
	 				</thead>
	    			<tbody class="sect-data"></tbody>
	 			</table>
			</div>
		</div>
	</div>

	<div class="modal fade in" id="modal-upload-file">
	 	<div class="modal-dialog">
	     	<div class="modal-content" style="width: 900px;margin-left: -139px;">
         	<div class="modal-body">
         		<div class="row">
         			<div class="col-md-6 col-sm-12">
         				<div class="drag-and-drop-zone dm-uploader text-center" style="border: 0.25rem dashed #A5A5C7;padding: 5rem!important;border: 0.25rem dashed #A5A5C7;">
				            <h4 class="text-muted" style="margin-bottom: 3rem!important;margin-top: 5rem!important;">
				            	Drag &amp; drop files here
				         	</h4>
				            <div class="btn btn-primary btn-block" style="margin-bottom: 3rem!important;">
			                	<span>Open the file Browser</span>
			                	<input type="file" title='Click to add Files' />
				            </div>
			          	</div>
      				</div>
      				<div class="col-md-6 col-sm-12">
      					<div class="box box-default box-solid">
				            <div class="box-header">
				            	<h6 class="box-title" style="font-size: 15px;"><i class="fa fa-upload"></i> Status Upload</h6>
				            	<div class="box-tools">
					           		<button class="btn btn-default clear-list-upload">
					           			<i class="fa fa-trash"></i> Bersihkan List
					        			</button>
				        			</div>
				         	</div>
				         	<div class="box-body">
				         		<ul class="list-unstyled p-2 d-flex flex-column col" id="files" style="overflow:auto;height: 208px;">
				              		<li class="text-muted text-center empty" style="margin-top: 25%">Tidak ada file yang di upload.</li>
				            	</ul>
			         		</div>
			          	</div>
			        	</div>
      			</div>
      		</div>
			</div>
		</div>
	</div>

	<div class="modal fade in" id="modal-upload-folder">
	 	<div class="modal-dialog">
	     	<div class="modal-content" style="width: 350px;margin-left: 110px;">
	         <form id="form-upload-folder">                
	         	<div class="modal-body">
	         		<div class="form-group">
	         			<input type="text" class="form-control input-sm" name="folder_name[]" placeholder="Nama folder..." required="">
	      			</div>
	         		<a href="" class="add-more-folder"><h6><i class="fa fa-plus"></i> Tambah lainnya</h6></a>
	      		</div>
	      		<div class="modal-footer">
	              	<button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> Simpan</button>
	             </div>
	   		</form>
			</div>
		</div>
	</div>


	<!-- <script type="text/html" id="files-template">
	   <li class="media">
	     <div class="media-body mb-1">
	       <h6 style="margin-top: 0;margin-bottom: 5px;">
	         <strong>%%filename%%</strong> <span class="text-muted">Waiting</span>
	       </h6>
	       <div class="progress" style="margin-bottom: 0">
	         <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" 
	           role="progressbar"
	           style="width: 0%" 
	           aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
	         </div>
	       </div>
	     </div>
	   </li>
	 </script> -->

</div>

<script type="text/javascript">
	var uniq_id = '<?php echo $unixid ?>';
	var container_wg = '#<?php echo $widget_name ?>_<?php echo $unixid ?>';
	var container = '#<?php echo $container ?>';
	alert(uniq_id);
</script>