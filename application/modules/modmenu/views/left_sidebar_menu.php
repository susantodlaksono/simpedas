<?php
if($result){
   echo '<ul class="sidebar-menu">';
      foreach ($result as $first) {
         if(isset($first['secondary_menu']) && $first['secondary_menu']){
            echo '<li class="treeview">';
               echo '<a style="cursor:pointer;">';
                  echo '<i class="'.$first['icon'].'"></i>';
                  echo '<span>'.$first['name'].'</span>';
                  echo '<i class="fa fa-angle-right pull-right"></i>';
               echo '</a>';
               echo '<ul class="treeview-menu">';
                  foreach ($first['secondary_menu'] as $second) {
                     echo '<li>';
                        echo '<a href="'.site_url($second['url']).'">';
                           echo '<i class="'.$second['icon'].'"></i>&nbsp;';
                           echo '<span>'.$second['name'].'</span>';
                        echo '</a>';
                     echo '</li>';
                  }
               echo '</ul>';
            echo '</li>';
         }else{
            echo '<li class="'.(uri_string() == $first['url'] ? 'active' : '').'">';
               echo '<a href="'.site_url($first['url']).'">';
                  echo '<i class="'.$first['icon'].'"></i>&nbsp;';
                  echo '<span>'.$first['name'].'</span>';
               echo '</a>';
            echo '</li>';
         }
      }
	echo '</ul>';          
}
?>