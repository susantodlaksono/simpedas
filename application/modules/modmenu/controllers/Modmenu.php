<?php

/**
 * Description of Menus
 *
 * @author SUSANTO DWILAKSONO
 */
class Modmenu extends Widgets {

   public function __construct() {
      parent::__construct();
      $this->load->model('m_modmenu');
   }

   public function top_horizontal_menu() {
      echo $this->draw_top_menu($this->_user->id);
   }

   public function left_sidebar_menu() {
      $result = NULL;
      $group_id = $this->get_groups($this->_user->id);
      if($group_id){
         $result = $this->primary_menu_left($group_id);
      }
      $data = array(
         'result' => $result,
         'userid' => $this->_user->id
      );
      $this->load->view('left_sidebar_menu', $data);
   }

   public function get_groups($user_id){
      $this->db->where('user_id', $user_id);
      $rs = $this->db->get('users_groups');
      if($rs->num_rows() > 0){
         foreach ($rs->result_array() as $key => $value) {
            $group_id[] = $value['group_id'];
         }
         return $group_id;
      }else{
         return false;
      }
   }

   public function primary_menu_left($group_id){
      $this->db->select('a.*');
      $this->db->join('menu_groups as b', 'a.id = b.menu_id', 'left');
      $this->db->where('a.status', 1); // Active Menu
      $this->db->where('a.parent', 0);
      $this->db->where_in('b.group_id', $group_id);
      $this->db->order_by('a.sort', 'asc');
      $this->db->group_by('b.menu_id');
      $rs = $this->db->get('menu as a')->result_array();
      if($rs){
         $i = 0;
         foreach ($rs as $k => $v) {
            $data[] = $v;
            $data[$i]['secondary_menu'] = $this->secondary_menu_left($v['id'], $group_id);
            $i++;
         }
         return $data;
      }else{
         return FALSE;
      }
   }

   public function secondary_menu_top($menu_id, $group_id){
      $this->db->select('a.*');
      $this->db->join('menu_groups as b', 'a.id = b.menu_id', 'left');
      $this->db->where('a.status', 1); // Active Menu
      $this->db->where('a.parent', $menu_id);
      $this->db->where('a.position IS NULL'); // left menu
      $this->db->where_in('group_id', $group_id);
      $this->db->order_by('a.sort', 'asc');
      $this->db->group_by('b.menu_id');
      $rs = $this->db->get('menu as a')->result_array();
      if($rs){
         return $rs;
      }else{
         return FALSE;
      }
   }

   public function secondary_menu_left($menu_id, $group_id){
      $this->db->select('a.*');
      $this->db->join('menu_groups as b', 'a.id = b.menu_id', 'left');
      $this->db->where('a.status', 1); // Active Menu
      $this->db->where('a.parent', $menu_id);
      // $this->db->where('a.position', 1); // left menu
      $this->db->where_in('group_id', $group_id);
      $this->db->order_by('a.sort', 'asc');
      $this->db->group_by('b.menu_id');
      $rs = $this->db->get('menu as a')->result_array();
      if($rs){
         return $rs;
      }else{
         return FALSE;
      }
   }

}