<div class="row">
   <div class="col-md-4 border-right">
      <div class="description-block">
         <h1 class="no-margin total-request"><?php echo $total ?></h1>
         <span class="description-text">Total Request</span>
      </div>
   </div>
   <div class="col-md-4 border-right">
      <div class="description-block">
         <h1 class="no-margin proses-disposisi"><?php echo $disposisi ?></h1>
        <span class="description-text">Proses Disposisi</span>
      </div>
   </div>
   <div class="col-md-4 border-right">
      <div class="description-block">
         <h1 class="no-margin kompilasi-data"><?php echo $kompilasi ?></h1>
         <span class="description-text">Kompilasi Data</span>
      </div>
   </div>
   <div class="col-md-6 border-right">
      <div class="description-block">
         <h1 class="no-margin validasi-kasie"><?php echo $validasi ?></h1>
         <span class="description-text">Validasi Kepala Seksi</span>
      </div>
   </div>
   <div class="col-md-6 border-right">
      <div class="description-block">
         <h1 class="no-margin diserahkan"><?php echo $diserahkan ?></h1>
         <span class="description-text">Diserahkan</span>
      </div>
   </div>
</div>