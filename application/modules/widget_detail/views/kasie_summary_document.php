<div class="row">
   <div class="col-md-4 border-right">
      <div class="description-block">
         <h1 class="no-margin last-upload-document"><?php echo $result['count_files'] ?></h1>
         <span class="description-text">Last Upload Document</span>
      </div>
   </div>
   <div class="col-md-4 border-right">
      <div class="description-block">
         <h1 class="no-margin lock-document"><?php echo $lock ?></h1>
        <span class="description-text">Lock Document</span>
      </div>
   </div>
   <div class="col-md-4 border-right">
      <div class="description-block">
         <h1 class="no-margin shared-document"><?php echo $share ?></h1>
         <span class="description-text">Shared Document</span>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-md-4 border-right">
      <div class="description-block">
         <h1 class="no-margin multimedia-files"><?php echo $result['count_multimedia'] ?></h1>
         <span class="description-text">Multimedia Files</span>
      </div>
   </div>
   <div class="col-md-4 border-right">
      <div class="description-block">
         <h1 class="no-margin office-files"><?php echo $result['count_office'] ?></h1>
        <span class="description-text">Office Files</span>
      </div>
   </div>
   <div class="col-md-4 border-right">
      <div class="description-block">
         <h1 class="no-margin other-files"><?php echo $result['count_other'] ?></h1>
         <span class="description-text">Other Files</span>
      </div>
   </div>
</div>