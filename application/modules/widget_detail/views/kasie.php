<?php
$_uniqid = uniqid();
?>

<div id="<?php echo $_uniqid ?>">

   <form class="form-detail">
   <input type="hidden" name="id" value="<?php echo $id ?>">

      <div class="row">
         <div class="col-md-12">
            <table class="table table-condensed table-striped" style="font-size: 13px">
               <tr>
                  <td width="200" class="text-bold text-info" style="border-top: 0">OPD</td>
                  <td width="10" style="border-top: 0">:</td>
                  <td class="opd-name" style="border-top: 0"><?php echo $request['opd_name'] ?></td>
               </tr>
               <tr>
                  <td width="200" class="text-bold text-info">Pedasi</td>
                  <td width="10">:</td>
                  <td class="pedasi-name"><?php echo $request['pedasi_name'] ?></td>
               </tr>
               <tr>
                  <td width="200" class="text-bold text-info">Permohonan Data</td>
                  <td width="10">:</td>
                  <td class="title-name"><?php echo $request['title'] ?></td>
               </tr>
               <tr>
                  <td width="200" class="text-bold text-info">Periode Data</td>
                  <td width="10">:</td>
                  <td class="period-date">
                     <?php echo date('d M Y', strtotime($request['start_date'])).' sd '. 
                     date('d M Y', strtotime($request['end_date']))?> 
                  </td>
               </tr>
               <tr>
                  <td width="200" class="text-bold text-info">Batas Pengumpulan</td>
                  <td width="10">:</td>
                  <td class="limit-date"><?php echo date('d M Y', strtotime($request['limit_date'])) ?></td>
               </tr>
               <tr>
                  <td width="200" class="text-bold text-info">Catatan Disposisi</td>
                  <td width="10">:</td>
                  <td class="status-name"><?php echo $request['disposisi_remark'] ? $request['disposisi_remark'] : '' ?></td>
               </tr>
               <tr>
                  <td width="200" class="text-bold text-info">Status Permohonan Data</td>
                  <td width="10">:</td>
                  <td class="status-name"><?php echo $status_label ?></td>
               </tr>
            </table>
         </div>
         
         <?php
         if($request['status']){
         ?>
         <div class="col-md-12 kompilasi-cont">
            <table class="table table-condensed table-bordered" style="font-size: 13px;">
               <thead>
                  <th class="text-warning">Pengirim</th>
                  <th class="text-warning">Penerima</th>
                  <th class="text-warning">Waktu</th>
               </thead>
               <tbody>
                  <tr>
                     <td class="kompilasi-sender"><?php echo $request['kompilasi_sender'] ?></td>
                     <td class="kompilasi-kasie"><?php echo $request['kompilasi_kasie'] ?></td>
                     <td class="kompilasi-date"><?php echo $request['disposisi_kasie_created'] ?></td>
                  </tr>
                  <?php if($request['status'] == 2 || $request['status'] == 3){ ?>
                  <tr class="status-diserahkan">
                     <td class="kompilasi-pedasi"><?php echo $request['pedasi_name'] ?></td>
                     <td class="kompilasi-kasie"><?php echo $request['kompilasi_kasie'] ?></td>
                     <td class="kompilasi-date-pedasi"><?php echo $request['pedasi_upload'] ?></td>
                  </tr>
                  <?php } ?>
               </tbody>
            </table>
         </div>
         <?php } ?>

      </div>

      <div class="row">

         <?php if($request['description'] || $attachments){ ?>
         <div class="col-md-12" style="border-top:1px solid #d6d6d6;">
            <h4 class="text-info text-bold">Lampiran Format Data</h4>
            <?php 
               echo $request['description'] ? $request['description'] : '';
               if($attachments){
                  echo '<h1>';
                  foreach ($attachments as $v) {
                     echo '<span class="attach-options atc-opt-'.$v['id'].'">';
                        echo '<a href="'.site_url().'attach-request-'.$v['id'].'" data-toggle="tooltip" data-title="'.$v['file_orig_name'].'" style="margin-right:5px;">';
                           echo '<i class="'.$v['icon'].'"></i>';
                        echo '</a>';
                     echo '</span>';
                  }
                  echo '</h1>';
               }
            ?>
         </div>
         <?php } ?>

         <?php
         if($files_request){
            echo '<div class="col-md-12 cont-pedasi-result" style="border-top:1px solid #d6d6d6;">';
            echo '<h4 class="text-success text-bold">Lampiran Kompilasi Data</h4>';
            echo '<table class="table table-condensed" style="font-size:12px;">';
            foreach ($files_request as $v) {
               echo '<tr>';
                  echo '<td>';
                     echo '<i class="'.$v['ext'].'"></i>&nbsp;';
                     echo $v['file_name'];
                  echo '</td>';
                  echo '<td>';
                     echo '<i class="fa fa-clock-o"></i> '.date('d M Y', strtotime($v['created_date']));
                  echo '</td>';
                  echo '<td>';
                     echo '<div class="btn-group">';
                        echo '<a href="'.site_url().'main/download?name='.$v['file_name_orig'].'&dir='.$v['full_path'].'" class="btn btn-default btn-xs" data-toggle="tooltip" data-title="Download">';
                        echo '<i class="fa fa-download"></i>';
                        echo '</a>';
                     echo '</div>';
                  echo '</td>';
               echo '</tr>';
            };
            echo '</table>';
            echo '</div>';
         }
         ?>
      </div>

      <div class="row">
         <div class="col-md-12 text-right" style="border-top:1px solid #d6d6d6;">
            <div class="btn-group" style="margin-top:10px;">
            <?php if($request['status'] == 2){ ?>
               <button type="button" class="btn btn-danger btn-sm btn-revision">Revisi</button>
               <button type="submit" class="btn btn-primary btn-sm btn-submit">Validasi</button>
            <?php } ?>
            <?php if($request['status'] == 3){ ?>
               <button type="button" class="btn btn-danger btn-sm btn-abort">Batalkan Validasi</button>
            <?php } ?>
            </div>
         </div>
      </div>

   </form>

</div>

<script type="text/javascript">

   _uniqid = '<?php echo $_uniqid ?>';

   $(function () {

      $(this).on('submit', '#'+_uniqid+' .form-detail', function(e){
         var form = $(this);
         $(this).ajaxSubmit({
            url : site_url + 'widget_detail/kasie/validasi',
            type : "POST",
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', '<i class="fa fa-send"></i> Kirim');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function(r) {
               session_checked(r._session, r._maintenance);
               if(r.success){
                  $(this).todolist();
                  toastr.success(r.msg);
                  $("#modal-detail").modal("toggle");
               }else{
                  toastr.error(r.msg);
               }
               loading_form(form, 'hide', '<i class="fa fa-send"></i> Kirim');
            },
         });
         e.preventDefault();
      });

      $(this).on('click', '#'+_uniqid+' .btn-abort',function(e){
         var form = $('.form-detail');
         var id = form.find('input[name="id"]').val();

         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'widget_detail/kasie/abort',
            dataType : "JSON",
            data : {
               id : id
            },
            beforeSend: function (xhr) {
               $('#'+_uniqid+' .btn-abort').prop('disabled', true);
               $('#'+_uniqid+' .btn-abort').html('Please wait...');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
            },
            success: function(r){
               session_checked(r._session, r._maintenance);
               if(r.success){
                  $(this).todolist();
                  toastr.success(r.msg);
               }else{
                  toastr.error(r.msg);
               }
            },
            complete: function(){
               $('#'+_uniqid+' .btn-abort').prop('disabled', false);
               $('#'+_uniqid+' .btn-abort').html('Batalkan Validasi');
               $("#modal-detail").modal("toggle");
            }
         });
      });

      $(this).on('click', '#'+_uniqid+' .btn-revision',function(e){
         var form = $('.form-detail');
         var id = form.find('input[name="id"]').val();

         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'widget_detail/kasie/revision',
            dataType : "JSON",
            data : {
               id : id
            },
            beforeSend: function (xhr) {
               $('#'+_uniqid+' .btn-revision').prop('disabled', true);
               $('#'+_uniqid+' .btn-revision').html('Please wait...');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
            },
            success: function(r){
               session_checked(r._session, r._maintenance);
               if(r.success){
                  $(this).todolist();
                  toastr.success(r.msg);
               }else{
                  toastr.error(r.msg);
               }
            },
            complete: function(){
               $('#'+_uniqid+' .btn-revision').prop('disabled', false);
               $('#'+_uniqid+' .btn-revision').html('Batalkan Validasi');
               $("#modal-detail").modal("toggle");
            }
         });
      });

   });
</script>