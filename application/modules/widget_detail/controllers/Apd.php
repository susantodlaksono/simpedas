<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Apd extends MY_Controller {

	public function __construct() {
		parent::__construct();
      $this->load->library('global_mapping');
      $this->load->model('m_request_widget');
      if(in_array(2, $this->_role_id)){
         $this->_path = 'dms/apd/'.$this->_user->id;
      }else if(in_array(3, $this->_role_id)){
         $this->_path = 'dms/kadis/'.$this->_user->id;
      }else if(in_array(5, $this->_role_id)){
         $this->_path = 'dms/pedasi/'.$this->_user->id;
      }else if(in_array(4, $this->_role_id)){         
         $this->_path = 'dms/kasie/'.$this->_user->id;
      }
   }

   public function index(){
      $data['id'] = $this->_get['id'];
      $data['request'] = $this->m_request_widget->request_detail($this->_get['id']);
      $data['attachments'] = $this->global_mapping->get_attachments_request($this->_get['id']);
      $data['status_label'] = $this->global_mapping->request_status($data['request']['status'], 'label-status ');
      $data['files_request'] = $this->m_request_widget->files_request($this->_get['id']);
		$response['html'] = $this->load->view('apd', $data, TRUE);
      $this->json_result($response);
	}

   public function summary_document(){
      $this->load->library('dir_extraction');
      $data['lock'] = $this->m_request_widget->summary_dms_by_id($this->_user->id, 1);
      $data['share'] = $this->m_request_widget->summary_dms_by_id($this->_user->id, 2);
      $data['result'] = $this->dir_extraction->count_dir_ternary_path($this->_path);
      $response['html'] = $this->load->view('apd_summary_document', $data, TRUE);
      $this->json_result($response);
   }

   public function summary_request(){
      $data['total'] = $this->m_request_widget->summary_request_by_id($this->_user->id);
      $data['disposisi'] = $this->m_request_widget->summary_request_by_id($this->_user->id , 'disposisi');
      $data['kompilasi'] = $this->m_request_widget->summary_request_by_id($this->_user->id, 1);
      $data['validasi'] = $this->m_request_widget->summary_request_by_id($this->_user->id, 2);
      $data['diserahkan'] = $this->m_request_widget->summary_request_by_id($this->_user->id, 3);
      $response['html'] = $this->load->view('apd_summary_request', $data, TRUE);
      $this->json_result($response);
   }

   public function update(){
      $response['success'] = FALSE;
      $response['msg'] = 'Function Failed';

      $this->db->trans_start();
      $req['status_pengolahan'] = $this->_post['status_pengolahan'] ? $this->_post['status_pengolahan'] : NULL;
      $req['request_rating'] = $this->_post['request_rating'] ? $this->_post['request_rating'] : NULL;
      $this->db->update('request', $req, array('id' => $this->_post['id']));
      $this->db->trans_complete();
      if($this->db->trans_status()){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['msg'] = 'Status request berhasil diperbaharui';
      }else{
         $this->db->trans_rollback();
      }
      $this->json_result($response);
   }

   public function revision(){
      $response['success'] = FALSE;
      $response['msg'] = 'Function Failed';

      $this->db->trans_start();
      $data['status'] = 1;
      $this->db->update('request', $data, array('id' => $this->_get['id']));
      $this->db->trans_complete();

      if($this->db->trans_status()){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['msg'] = 'Request berhasil direvisi';
      }else{
         $this->db->trans_rollback();
         $response['msg'] = 'Some error occured';
      }
      $this->json_result($response);
   }
	
}