<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Kasie extends MY_Controller {

	public function __construct() {
		parent::__construct();
      $this->load->library('global_mapping');
      $this->load->library('mailing');
      $this->load->model('m_request_widget');
      $this->_path = 'dms/kadis/'.$this->_user->id;
   }

   public function index(){
      $data['id'] = $this->_get['id'];
      $data['kasie'] = $this->m_request_widget->get_kasie($this->_opd->id);
      $data['request'] = $this->m_request_widget->request_detail($this->_get['id']);
      $data['attachments'] = $this->global_mapping->get_attachments_request($this->_get['id']);
      $data['status_label'] = $this->global_mapping->request_status($data['request']['status'], 'label-status ');
      $data['files_request'] = $this->m_request_widget->files_request($this->_get['id']);
		$response['html'] = $this->load->view('kasie', $data, TRUE);
      $this->json_result($response);
	}

   public function summary_document(){
      $this->load->library('dir_extraction');
      $data['lock'] = $this->m_request_widget->summary_dms_by_id($this->_user->id, 1);
      $data['share'] = $this->m_request_widget->summary_dms_by_id($this->_user->id, 2);
      $data['result'] = $this->dir_extraction->count_dir_ternary_path($this->_path);
      $response['html'] = $this->load->view('kasie_summary_document', $data, TRUE);
      $this->json_result($response);
   }

   public function summary_request(){
      $data['total'] = $this->m_request_widget->summary_request_by_opd($this->_opd->id);
      $data['disposisi'] = $this->m_request_widget->summary_request_by_opd($this->_opd->id , 'disposisi');
      $data['kompilasi'] = $this->m_request_widget->summary_request_by_opd($this->_opd->id, 1);
      $data['validasi'] = $this->m_request_widget->summary_request_by_opd($this->_opd->id, 2);
      $data['diserahkan'] = $this->m_request_widget->summary_request_by_opd($this->_opd->id, 3);
      $response['html'] = $this->load->view('kasie_summary_request', $data, TRUE);
      $this->json_result($response);
   }

   public function validasi(){
      $response['success'] = FALSE;
      $response['msg'] = 'Function Failed';

      $this->db->trans_start();
      $data['status'] = 3;
      $data['handover_created'] = date('Y-m-d H:i:s');
      $this->db->update('request', $data, array('id' => $this->_post['id']));
      $this->db->trans_complete();

      if($this->db->trans_status()){

         $mailing = $this->mailing->after_validasi_kasie($this->_post['id']);
         if($mailing['status']){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'Request berhasil di validasi';
         }else{
            $this->db->trans_rollback();   
            $response['msg'] = $mailing['msg'];
         }
      }else{
         $this->db->trans_rollback();
      }
      $this->json_result($response);
   }
	
   public function abort(){
      $response['success'] = FALSE;
      $response['msg'] = 'Function Failed';

      $this->db->trans_start();
      $data['status'] = 2;
      $data['handover_created'] = NULL;
      $this->db->update('request', $data, array('id' => $this->_get['id']));
      $this->db->trans_complete();

      if($this->db->trans_status()){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['msg'] = 'Validasi berhasil dibatalkan';
      }else{
         $this->db->trans_rollback();
         $response['msg'] = 'Some error occured';
      }
      $this->json_result($response);
   }

   public function revision(){
      $response['success'] = FALSE;
      $response['msg'] = 'Function Failed';

      $this->db->trans_start();
      $data['status'] = 1;
      $this->db->update('request', $data, array('id' => $this->_get['id']));
      $this->db->trans_complete();

      if($this->db->trans_status()){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['msg'] = 'Request berhasil direvisi';
      }else{
         $this->db->trans_rollback();
         $response['msg'] = 'Some error occured';
      }
      $this->json_result($response);
   }

}