<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_request_widget extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function request_detail($id){
      $this->db->select('a.*, b.first_name as kompilasi_sender, c.first_name as kompilasi_kasie, d.first_name as pedasi_name');
      $this->db->select('e.name as opd_name');
      $this->db->join('users as b', 'a.disposisi_assignby = b.id', 'left');
      $this->db->join('users as c', 'a.disposisi_kasie = c.id', 'left');
      $this->db->join('users as d', 'a.pedasi_id = d.id', 'left');
      $this->db->join('opd as e', 'a.opd_id = e.id', 'left');
      $this->db->where('a.id', $id);
      return $this->db->get('request as a')->row_array();
   }

   public function files_request($id){
      $this->db->select('a.*');
      $this->db->where('a.req_id', $id);
      $result = $this->db->get('request_result_files as a')->result_array();
      if($result){
         foreach ($result as $v) {
            $data[$v['id']]['id'] = $v['id'];
            $data[$v['id']]['file_name'] = urldecode($v['file_name']);
            $data[$v['id']]['file_name_orig'] = $v['file_name'];
            $data[$v['id']]['created_date'] = $v['created_date'];
            $data[$v['id']]['full_path'] = $v['full_path'];
            $data[$v['id']]['ext'] = $this->global_mapping->mapping_icon_request('.'.$v['extension']);
         }
         return $data;
      }else{
         return FALSE;
      }
   }

   public function summary_dms_by_id($userid, $type){
      $this->db->where('created_by', $userid);
      $this->db->where('type_dms', $type);
      return $this->db->count_all_results('document_tags');
   }

   public function summary_request_by_id($userid, $status = NULL){
      $this->db->where('requested_by', $userid);
      if($status){
         if($status == 'disposisi'){
            $this->db->where('status IS NULL');
         }else{
            $this->db->where('status', $status);
         }
      }
      return $this->db->count_all_results('request');
   }

   public function summary_request_by_opd($opd_id, $status = NULL){
      $this->db->where('opd_id', $opd_id);
      if($status){
         if($status == 'disposisi'){
            $this->db->where('status IS NULL');
         }else{
            $this->db->where('status', $status);
         }
      }
      return $this->db->count_all_results('request');
   }

   public function get_kasie($opdid){
      $this->db->select('a.user_id, b.first_name');
      $this->db->join('users as b', 'a.user_id = b.id', 'left');
      $this->db->where('a.opd_id', $opdid);
      $this->db->where('a.group_id', 4);
      return $this->db->get('mapping_opd as a')->result_array();
   }

}