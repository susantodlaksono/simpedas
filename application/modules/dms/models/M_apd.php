<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_apd extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function get_opd($userid){
      $this->db->select('a.opd_id');
      $this->db->where('a.user_id', $userid);
      $this->db->where('a.group_id', 5);
      $result = $this->db->get('mapping_opd as a')->row_array();
      return $result['opd_id'] ? $result['opd_id'] : NULL;
   }

	public function getting_request($mode, $params, $userid){
      $this->db->select('a.id, a.opd_id, a.title, a.start_date, a.end_date, a.limit_date, a.requested_date, a.status');
      $this->db->select('b.first_name as req_name');
      $this->db->select('c.first_name as pedasi_name');
      $this->db->select('d.total_files');
      $this->db->join('users as b', 'a.requested_by = b.id', 'left');
      $this->db->join('users as c', 'a.pedasi_id = c.id', 'left');
      $this->db->join('(select req_id, count(id) as total_files from request_result_files
      						group by req_id) as d', 
      						'a.id = d.req_id', 'left');
      $this->db->where('a.pedasi_id', $userid);
      if($params['filt_status'] != 'all'){
      	$this->db->where('a.status', $params['filt_status']);
      }else{
      	$this->db->where_in('a.status', array(1,2,3));
      }
      if($params['filt_keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.title', $params['filt_keyword']);
         $this->db->or_like('b.first_name', $params['filt_keyword']);
         $this->db->group_end();
      }
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('request as a', $params['limit'], $params['offset'])->result_array();
         case 'count':
            return $this->db->get('request as a')->num_rows();
      }
   }

   public function shared($reqid, $params){
   	if($params['dir'] && $params['filename']){
   		$this->db->where('req_id', $reqid);
   		$this->db->where('full_path', $params['dir'].'/'.$params['filename']);
   		$total = $this->db->count_all_results('request_result_files');
   		if($total > 0){
   			return TRUE;
   		}else{
   			return FALSE;
   		}
   	}else{
   		return FALSE;
   	}
   }

   public function pedasi_time($req_id){
      if($req_id){
         $tmp['pedasi_upload'] = date('Y-m-d H:i:s');
         $tmp['status'] = 2;
         $this->db->where_in('id', $req_id);
         $this->db->update('request', $tmp);
      }
   }

}