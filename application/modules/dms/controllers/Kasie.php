<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Kasie extends MY_Controller {

  public function __construct() {
    parent::__construct();
      $this->load->model('m_pedasi');
      $this->load->helper('inflector');
      $this->load->library('global_mapping');
      $this->load->library('dir_extraction');
      $this->load->library('date_extraction');
      $this->_path = 'dms/kasie/'.$this->_user->id;
      $this->_lock_dms = $this->get_document_tags(1);
      $this->_share_dms = $this->get_document_tags(2);
   }

   public function index(){
    $this->dir_create($this->_path);

      $data['_css'] = array(
         $this->_path_template.'/plugins/select2/select2.min.css',
         $this->_path_template.'/plugins/krajee-star-rating/star-rating.min.css',
         $this->_path_template.'/plugins/krajee-star-rating/theme.css',
         $this->_path_template.'/plugins/uploader-master/css/jquery.dm-uploader.css'
      ); 
      $data['_js'] = array(
         $this->_path_template.'/plugins/select2/select2.full.min.js',
         $this->_path_template.'/plugins/tinymce/tinymce.min.js',
         $this->_path_template.'/plugins/krajee-star-rating/star-rating.min.js',
         $this->_path_template.'/plugins/uploader-master/js/jquery.dm-uploader.js'
    );  
      $this->render_page($data, 'kasie', 'modular');
   }

   public function read_dir(){
    $response['result'] = $this->list_folder_files($this->_get);
    $response['dir_base'] = $this->_path;
    $response['dir_clean'] = urldecode($this->_get['dir']);

    $nav = '';
    $nav .= '<li><a data-href="" class="breadcrumb-action"><i class="fa fa-home"></i></a></li>';
    $uri_string = explode('/', urldecode($this->_get['dir']));
    $uri = array();
      foreach (array_filter($uri_string) as $rs) {
        $uri[] = urlencode($rs);
        $nav .= '<li><a data-href="'.implode('/', $uri).'" class="breadcrumb-action">'.$rs.'</a></li>';
      }
      $response['nav'] = $nav;
    $this->json_result($response);
   }

   public function upload_file_sample(){
      try {
         if (
            !isset($_FILES['file']['error']) ||
            is_array($_FILES['file']['error'])
         ) {
            throw new RuntimeException('Invalid parameters.');
         }
         switch ($_FILES['file']['error']) {
            case UPLOAD_ERR_OK:
            break;
            case UPLOAD_ERR_NO_FILE:
               throw new RuntimeException('No file sent.');
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
               throw new RuntimeException('Exceeded filesize limit.');
            default:
               throw new RuntimeException('Unknown errors.');
         }

         $config['upload_path']  = $this->_post['dir'] ? $this->_path.'/'.urldecode($this->_post['dir']) : $this->_path;
         $config['allowed_types'] = '*';
         $config['overwrite'] = TRUE;

         $this->load->library('upload', $config);
         if($this->upload->do_upload('file')){
            $uploadresult = $this->upload->data();
            $permission = $uploadresult['full_path']; // get file path
            chmod($permission, 0777); // CHMOD file or any other permission level(s)
            
            $response['path'] = $config['upload_path'];
            $response['status'] = 'ok';
         }else{
            $response['status'] = 'error';
            $response['path'] = $config['upload_path'];
            $response['msg'] = $this->upload->display_errors();
         }
      }catch (RuntimeException $e) {
         http_response_code(400);
         $response['status'] = 'error';
         $response['message'] = $e->getMessage();
      }
      $this->json_result($response);
   }

   public function upload_file(){
    $stat_upload = FALSE;
    $response['success'] = FALSE;
    $response['msg'] = 'Some error occured';

    $config['upload_path']  = $this->_post['dir'] ? $this->_path.'/'.urldecode($this->_post['dir']).'/' : $this->_path;
      $config['allowed_types'] = '*';
      $this->load->library('upload', $config);
      if(isset($_FILES['file_path'])){
        $count_attach = count($_FILES['file_path']['name']);
        if($count_attach > 0){
            for($i = 0; $i < $count_attach; $i++){
               if(!empty($_FILES['file_path']['name'][$i])){
                  $filename = uniqid();
                  $name = $_FILES['file_path']['name'][$i];
                  $ext = pathinfo($name, PATHINFO_EXTENSION);

                  $_FILES['file']['name'] = $_FILES['file_path']['name'][$i];
                  $_FILES['file']['type'] = $_FILES['file_path']['type'][$i];
                  $_FILES['file']['tmp_name'] = $_FILES['file_path']['tmp_name'][$i];
                  $_FILES['file']['error'] = $_FILES['file_path']['error'][$i];
                  $_FILES['file']['size'] = $_FILES['file_path']['size'][$i];
                  
                  if($this->upload->do_upload('file')){
                    $stat_upload = TRUE;
                    $uploadresult = $this->upload->data();
                     $permission = $uploadresult['full_path']; // get file path
                     chmod($permission, 0777); // CHMOD file or any other permission level(s)
                  }else{
                    $response['msg'] = $this->upload->display_errors();
                  }
               }
            }
         }else{
          $response['msg'] = 'Jumlah kosong'; 
         }
    }else{
      $response['msg'] = 'File tidak ada';
    }
    $response['success'] = $stat_upload;
    $response['path'] = $config['upload_path'];
    if($stat_upload){
      $response['msg'] = 'File berhasil diupload';
    }
    $this->json_result($response);
   }

   public function upload_folder(){
    $response['msg'] = 'Some error occured';
    $done_upload = 0;
    $fail_upload = 0;
    $process = FALSE;

    if(isset($this->_post['folder_name']) && count($this->_post['folder_name']) > 0){
      foreach ($this->_post['folder_name'] as $v) {
        if($this->_post['dir']){
          $upload_path = $this->_path.'/'.urldecode($this->_post['dir']).'/'.$v;
        }else{
          $upload_path = $this->_path.'/'.$v;
        }
        if (!file_exists($upload_path)) {
          mkdir($upload_path);
          chmod($upload_path, 0777);
          $done_upload += 1;
        }else{
          $fail_upload += 1;
        }
      }
      $process = TRUE;
    }
      if($process) {
      $response['msg'] = $done_upload.' Folder berhasil dibuat '.$fail_upload.' folder gagal di upload';
      $response['success'] = $process;
    }else{
      $response['msg'] = 'Gagal mengupload folder';
    }
    $this->json_result($response);
   }

   public function assign_flag(){
    $response['success'] = FALSE;
    $response['msg'] = 'Function failed';

    $this->db->trans_start();
      $tmp['file_name'] = $this->_post['name'] ? $this->_post['name'] : NULL;
      $tmp['file_dir'] = $this->_post['dir'] ? urldecode($this->_post['dir']) : NULL;
      if($this->_post['dir'] && $this->_post['name']){
        $tmp['full_dir'] = urldecode($this->_post['dir']).'/'.urldecode($this->_post['name']);
      }
      $tmp['file_extension'] = $this->_post['ext'] ? $this->_post['ext'] : NULL;
      $tmp['file_orig_name'] =  $this->_post['name'] ? $this->_post['name'] : NULL;
      $tmp['type_dms'] = $this->_post['status'];
      $tmp['created_by'] = $this->_user->id;
      $tmp['created_at'] = date('Y-m-d H:i:s');
      $this->db->insert('document_tags', $tmp);
      $this->db->trans_complete();

      if($this->db->trans_status()){
        $this->db->trans_commit();
        $response['success'] = TRUE;
        if($this->_post['status'] == 1){
          $response['msg'] = 'File berhasil di lock';
        }
        if($this->_post['status'] == 2){
          $response['msg'] = 'File berhasil di share';
        }
        if($this->_post['status'] == 3){
          $response['msg'] = 'Favorite berhasil di tambahkan';
        }
      }else{
        $this->db->trans_rollback();
      }
      $this->json_result($response);
   }

   public function remove_flag(){
    $response['success'] = FALSE;
    $response['msg'] = 'Function failed';

    $this->db->trans_start();
    $prm['id'] = $this->_get['id'];
    $prm['type_dms'] = $this->_get['status'];
      $this->db->delete('document_tags', $prm);
      $this->db->trans_complete();

      if($this->db->trans_status()){
        $this->db->trans_commit();
        $response['success'] = TRUE;
        $response['msg'] = 'Tanda berhasil di hapus';
      }else{
        $this->db->trans_rollback();
      }
      $this->json_result($response);
   }


   public function delete_folder(){
    $response['success'] = FALSE;
    $response['msg'] = 'Function failed';

    if($this->_post['dir'] && $this->_post['name']){
      $path = urldecode($this->_post['dir']).'/'.urldecode($this->_post['name']);
      
      $this->db->trans_start();
        $this->delete_folder_files($path);
        $this->db->trans_complete();

      if($this->db->trans_status()){
          $this->db->trans_commit();
          $response['success'] = TRUE;
          $response['msg'] = 'folder berhasil dihapus';
        }else{
          $this->db->trans_rollback();
        }
    }
    $this->json_result($response);
   }

   public function delete_file(){
      $response['success'] = FALSE;
      $response['msg'] = 'Function failed';

      if($this->_post['dir'] && $this->_post['name']){
         $path = urldecode($this->_post['dir']).'/'.urldecode($this->_post['name']);
         
         $this->db->trans_start();
         $tmp['created_by'] = $this->_user->id;
         $tmp['full_dir'] = $path;
         $this->db->delete('document_tags', $tmp);
         unlink($path);
         $this->db->trans_complete();

         if($this->db->trans_status()){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'file berhasil dihapus';
         }else{
            $this->db->trans_rollback();
         }
      }
      $this->json_result($response);
   }

   public function delete_folder_files($dir){
    $files = array_diff(scandir($dir), array('.','..'));
      foreach ($files as $file) {
        if(is_dir("$dir/$file")){
          $this->delete_folder_files("$dir/$file");
      }else{
        $full_dir_lock = $dir.'/'.$file;
        $tmp['created_by'] = $this->_user->id;
        $tmp['full_dir'] = $dir.'/'.$file;
        $this->db->delete('document_tags', $tmp);
        unlink("$dir/$file");

      }
      }
      return rmdir($dir);
  }

   public function getting_request(){
      $data = $this->m_pedasi->getting_request('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'status' => $v['status'],
               'status_label' => $this->global_mapping->request_status($v['status'], 'label-status-table '),
               'opd_id' => $v['opd_id'],
               'pedasi_name' => $v['pedasi_name'],
               'shared' => $this->m_pedasi->shared($v['id'], $this->_get),
               'title' => $v['title'],
               'start_date' => date('d M Y', strtotime($v['start_date'])),
               'end_date' => date('d M Y', strtotime($v['end_date'])),
               'limit_date' => date('d M Y', strtotime($v['limit_date'])),
               'req_name' => $v['req_name'],
               'days_left' => $v['limit_date'] ? $this->date_extraction->left_days_from_now($v['limit_date']) : NULL,
               'requested_date' => date('d M Y', strtotime($v['requested_date'])),
               'requested_time' => date('H:i:s', strtotime($v['requested_date']))
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->m_pedasi->getting_request('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

   public function assign_request_file(){
      $response['success'] = FALSE;
      $response['msg'] = 'Some error occured';
      $this->load->library('active_record_builder');

      if($this->_post['dir'] && $this->_post['filename']){
         $this->db->trans_start();
         foreach ($this->_post['request_id'] as $v) {
            $tmp = array(
               'req_id' => $v,
               'full_path' => $this->_post['dir'].'/'.$this->_post['filename'],
               'extension' => $this->_post['ext'],
               'file_name' => $this->_post['filename'],
               'created_date' => date('Y-m-d H:i:s'),
               'created_by' => $this->_user->id
            );
            $this->active_record_builder->on_duplicate('request_result_files', $tmp);
            $this->m_pedasi->pedasi_time($this->_post['request_id']);
         }
         $this->db->trans_complete();
         if($this->db->trans_status()){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'File berhasil di assign';
         }else{
            $this->db->trans_rollback();
         }
      }else{
         $response['msg'] = 'Entitas direktori tidak ditemukan';
      }
      $this->json_result($response);
   }

   public function dir_create($path){
    if (!file_exists($path)) {
      mkdir($path);
      chmod($path, 0777);
    }
   }

   public function get_document_tags($status){
    $data = array();
    $this->db->where('created_by', $this->_user->id);
    $this->db->where('type_dms', $status);
    $result = $this->db->get('document_tags')->result_array();
    if($result){
      foreach ($result as $v) {
        if($v['file_dir'] && $v['file_name']){
          $data[$v['id']]['dir'] = $v['full_dir'];
          $data[$v['id']]['id'] = $v['id'];
        }
      }
      return $data;
    }else{
      return FALSE;
    }
   }

   public function list_folder_files($params){
    if($params['dir']){
      $dir = $this->_path.'/'.urldecode($params['dir']);
    }else{
      $dir = $this->_path;
    }

      $ffs = scandir($dir);

      unset($ffs[array_search('.', $ffs, true)]);
      unset($ffs[array_search('..', $ffs, true)]);

      // prevent empty ordered elements
      if (count($ffs) < 1)
          return;

      $i = 0;
      $data = array();
      
      $lock_dms = $this->get_document_tags(1);
      $share_dms = $this->get_document_tags(2);
      $favorite_dms = $this->get_document_tags(3);

      foreach($ffs as $ff){
        $time = date('d M Y H:i:s', filectime($dir.'/'.$ff));
        $data[$i]['name'] = $ff;
        if(is_file($dir.'/'.$ff)){
          $ext = pathinfo($dir.'/'.$ff, PATHINFO_EXTENSION);
          $data[$i]['ext_orig'] = $ext;
          $data[$i]['ext'] = $this->global_mapping->mapping_icon_request('.'.$ext.'');
        }else{
          $data[$i]['ext_orig'] = NULL;
          $data[$i]['ext'] = NULL;
        }
        $data[$i]['base_dir'] = $dir;
        $data[$i]['url'] = urlencode($ff);
        $data[$i]['is_file'] = is_file($dir.'/'.$ff);
        $data[$i]['time'] = $this->date_extraction->time_ago($time);
         $data[$i]['clean_time'] = $time;
        $data[$i]['size'] = $this->global_mapping->format_size_units(filesize($dir.'/'.$ff));
        $data[$i]['size_bytes'] = filesize($dir.'/'.$ff);

        if($lock_dms){
          $cond_lock = $this->in_array_r($dir.'/'.$ff, $lock_dms);
          if($cond_lock){
            $data[$i]['lock'] = $cond_lock['id'];
          }else{
            $data[$i]['lock'] = FALSE;
          }
        }

        if($share_dms){
          $cond_share = $this->in_array_r($dir.'/'.$ff, $share_dms);
          if($cond_share){
            $data[$i]['share'] = $cond_share['id'];
          }else{
            $data[$i]['share'] = FALSE;
          }
        }

        if($favorite_dms){
          $cond_favorite = $this->in_array_r($dir.'/'.$ff, $favorite_dms);
          if($cond_favorite){
            $data[$i]['favorites'] = $cond_favorite['id'];
          }else{
            $data[$i]['favorites'] = FALSE;
          }
        }

        $i++;
      }
      if($data){
        if($params['orderby'] == 'desc'){
          usort($data, function($b, $a) use ($params) {
            return $a[$params['order']] <=> $b[$params['order']];
        });
        }
        if($params['orderby'] == 'asc'){
          usort($data, function($a, $b) use ($params) {
            return $a[$params['order']] <=> $b[$params['order']];
        });
        }
      }
      return array(
        'data' => $data,
        'params' => $params['order'],
        'total' => count($data)
      );
  }

  public function download_file(){
      $this->load->helper('download');
      $response['success'] = TRUE;
      force_download($this->_get['filename'], file_get_contents(urldecode($this->_get['trgt'])));
      $this->json_result($response);
   }

  public function scan_dir($path){
      $ite=new RecursiveDirectoryIterator($path);

      $bytestotal=0;
      $nbfiles=0;
      foreach (new RecursiveIteratorIterator($ite) as $filename=>$cur) {
          $filesize=$cur->getSize();
          $bytestotal+=$filesize;
          $nbfiles++;
          if(is_file($filename)){
              $files[] = $filename;
          }
      }

      $bytestotal=number_format($bytestotal);

      return array('total_files'=>$nbfiles,'total_size'=>$bytestotal,'files'=>$files);
  }

  public function in_array_r($needle, $haystack, $strict = false) {
      foreach ($haystack as $item) {
          if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
          return $item;
          }
      }
      return FALSE;
  }

}