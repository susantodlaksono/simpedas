<style type="text/css">
	.breadcrumb{
		background-color: #fbfbfb;
		border: 1px solid #e4e4e4;
	}
	.breadcrumb-action{
		cursor:pointer;font-size: 13px;
	}

	#debug {
		overflow-y: scroll !important;
		height: 180px;	
	}
	.dm-uploader.active {
		border-color: red;
		border-style: solid;
	}
</style>
<div class="row">
	<div class="col-md-3">
		<div class="box box-default" id="cont-dms-menu">
			<div class="box-header with-border">
           <h3 class="box-title">Folders</h3>
           	<div class="box-tools">
             	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
           	</div>
      	</div>
       	<div class="box-body table-responsive no-padding">
       		<ul class="nav nav-pills nav-stacked">
       			<li class="widget-render active" data-wg="dir_dms"><a href=""><i class="fa fa-folder"></i> Semua File</a></li>
       			<li class="widget-render" data-wg="dir_favorites"><a href=""><i class="fa fa-star"></i> Favorit</a></li>
    			</ul>
    		</div>
 		</div>
	</div>
	<div class="col-md-9">
		<div class="box box-default" id="cont-widget-dms">
			<div class="box-header with-border">
        		<h3 class="box-title"><i class="fa fa-folder"></i> Semua File</h3>
     		</div>
     		<div class="box-body" style="max-height: 600px;overflow-y: auto;overflow-x: hidden;min-height:300px;"></div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-3">
		<div class="box box-default" id="cont-dms-menu">
			<div class="box-header with-border">
           <h3 class="box-title">Folders</h3>
           	<div class="box-tools">
             	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
           	</div>
      	</div>
       	<div class="box-body table-responsive no-padding">
       		<ul class="nav nav-pills nav-stacked">
       			<li class="active tab-all-files"><a href=""><i class="fa fa-folder"></i> Semua File</a></li>
       			<li class="tab-favorites"><a href=""><i class="fa fa-star"></i> Favorit</a></li>
       			<!-- <li class=""><a href=""><i class="fa fa-clock-o"></i> Terakhir</a></li> -->
    			</ul>
    		</div>
 		</div>
	</div>
	<div class="col-md-9">
		<div class="box box-default" id="cont-dms-result">
			<div class="box-header with-border">
           <ol class="breadcrumb sect-nav-render" style="width: 885px;"></ol>
           	<div class="box-tools">
           		<button class="btn dropdown-toggle btn-default lg show-options" data-toggle="dropdown" style="margin-top: 7px;">
           			<i class="fa fa-plus"></i>
        			</button>
	          	<ul class="dropdown-menu" style="left:-122px;">
	          		<li><a href="" data-toggle="modal" data-target="#modal-upload-file"><i class="fa fa-file"></i>&nbsp;Upload File</a></li>
	          		<li class="divider"></li>
	          		<li><a class="btn-upload-folder" href="" data-toggle="modal" data-target="#modal-upload-folder"><i class="fa fa-folder"></i>&nbsp;New Folder</a></li>
          		</ul>
	        	</div>
      	</div>
       	<div class="box-body no-padding" id="cont-dms-result" style="font-size: 13px;">
       		<div class="mailbox-messages">
          		<table class="table table-hover table-striped">
          			<thead>
          				<th width="60"></th>
          				<th>Name <a class="change_order" href="#" data-order="name" data-by="desc"><i class="fa fa-sort"></i></th>
          				<th></th>
          				<th>Time <a class="change_order" href="#" data-order="clean_time" data-by="asc"><i class="fa fa-sort-desc"></i></th>
          				<th>Size <a class="change_order" href="#" data-order="size_bytes" data-by="desc"><i class="fa fa-sort"></i></th>
          			</thead>
          			<tbody class="sect-dms-result"></tbody>
       			</table>
    			</div>
    		</div>
    		<div class="box-footer" style="">
           <div class="row">
               <div class="col-md-12"><h5 class="sect-total"></h5></div>
            </div>
         </div>
 		</div>
	</div>
</div>

<div class="modal fade in" id="modal-upload-file">
 	<div class="modal-dialog">
     	<div class="modal-content" style="width: 900px;margin-left: -139px;">
         <form id="form-upload-file">                
         	<div class="modal-body">
         		<div class="row">
         			<div class="col-md-6 col-sm-12">
         				<div id="drag-and-drop-zone" class="dm-uploader text-center" style="border: 0.25rem dashed #A5A5C7;padding: 5rem!important;border: 0.25rem dashed #A5A5C7;">
				            <h4 class="text-muted" style="margin-bottom: 3rem!important;margin-top: 5rem!important;">
				            	Drag &amp; drop files here
				         	</h4>
				            <div class="btn btn-primary btn-block" style="margin-bottom: 3rem!important;">
			                	<span>Open the file Browser</span>
			                	<input type="file" title='Click to add Files' />
				            </div>
			          	</div>
      				</div>
      				<div class="col-md-6 col-sm-12">
      					<div class="box box-default box-solid">
				            <div class="box-header">
				            	<h6 class="box-title" style="font-size: 15px;"><i class="fa fa-upload"></i> Status Upload</h6>
				            	<div class="box-tools">
					           		<button class="btn btn-default clear-list-upload">
					           			<i class="fa fa-trash"></i> Bersihkan List
					        			</button>
				        			</div>
				         	</div>
				         	<div class="box-body">
				         		<ul class="list-unstyled p-2 d-flex flex-column col" id="files" style="overflow:auto;height: 208px;">
				              		<li class="text-muted text-center empty" style="margin-top: 25%">Tidak ada file yang di upload.</li>
				            	</ul>
			         		</div>
			          	</div>
			        	</div>
      			</div>
         		<input type="file" class="form-control input-sm" name="file_path[]" multiple="">
         		<span class="text-muted" style="font-size: 9px;">Bisa lebih dari 1 file</span>
      		</div>
      		<div class="modal-footer">
              	<button type="submit" class="btn btn-primary btn-block"><i class="fa fa-upload"></i> Upload</button>
          	</div>
   		</form>
		</div>
	</div>
</div>

<div class="modal fade in" id="modal-upload-folder">
 	<div class="modal-dialog">
     	<div class="modal-content" style="width: 350px;margin-left: 110px;">
         <form id="form-upload-folder">                
         	<div class="modal-body">
         		<div class="form-group">
         			<input type="text" class="form-control input-sm" name="folder_name[]" placeholder="Nama folder..." required="">
      			</div>
         		<a href="" class="add-more-folder"><h6><i class="fa fa-plus"></i> Tambah lainnya</h6></a>
      		</div>
      		<div class="modal-footer">
              	<button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> Simpan</button>
             </div>
   		</form>
		</div>
	</div>
</div>

<div class="modal fade in" id="modal-assign">
 	<div class="modal-dialog">
     	<div class="modal-content" style="width: 1000px;margin-left: -195px;">
         <div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal">
         		<span aria-hidden="true">×</span>
         	</button>
          	<h4 class="modal-title"><i class="fa fa-share"></i>&nbsp;Share to request</h4>
         </div>
         <form id="form-assign">
         	<input type="hidden" name="dir">
         	<input type="hidden" name="filename">
         	<input type="hidden" name="ext">
         	<div class="modal-body">
         		<div class="row">
         			<div class="col-md-12">
         				<!-- <div class="btn-group" data-toggle="buttons">
								<select type="text" class="form-control input-sm" id="filt_status">
									<option value="all">Semua Status</option>
									<option value="1">Kompilasi Data</option>
									<option value="2">Validasi Kasie</option>
									<option value="3">Diserahkan</option>
								</select>
							</div> -->
		           		<div class="btn-group" data-toggle="buttons">
								<input type="text" class="form-control input-sm" id="filt_keyword" placeholder="Pencarian...">
							</div>
							<div class="btn-group" data-toggle="buttons">
								<button class="btn btn-sm btn-filter"><i class="fa fa-filter"></i> Filter</button>
							</div>
      				</div>
      			</div>
      			<hr>
         		<table class="table table-striped table-condensed" style="font-size:12px;">
						<thead>
							<th></th>
							<th>Tgl <a class="change_order_req" href="#" data-order="a.requested_date" data-by="asc"><i class="fa fa-sort-desc"></i></th>
							<th>Permohonan Data <a class="change_order_req" href="#" data-order="a.title" data-by="asc"><i class="fa fa-sort"></i></th>
							<th>Mulai <a class="change_order_req" href="#" data-order="a.start_date" data-by="asc"><i class="fa fa-sort"></i></th>
							<th>Selesai <a class="change_order_req" href="#" data-order="a.end_date" data-by="asc"><i class="fa fa-sort"></i></th>
							<th>Batas Waktu <a class="change_order_req" href="#" data-order="a.limit_date" data-by="asc"><i class="fa fa-sort"></i></th>
							<th>Status <a class="change_order_req" href="#" data-order="a.status" data-by="asc"><i class="fa fa-sort"></i></th>
							<th>Sharing</th>
						</thead>
	          		<tbody class="sect-data"></tbody>
					</table>
					<div class="row">
	               <div class="col-md-2">
	             		<h6 class="sect-total"></h6>
	               </div>
	               <div class="col-md-10 text-right">
	                  <ul class="pagination sect-pagination pagination-sm no-margin pull-right"></ul>
	               </div>
	            </div>
      		</div>
      		<div class="modal-footer">
              	<button type="submit" class="btn btn-primary"><i class="fa fa-share"></i> Sharing</button>
             </div>
   		</form>
		</div>
	</div>
</div>

<script type="text/html" id="files-template">
   <li class="media">
     <div class="media-body mb-1">
       <h6 style="margin-top: 0;margin-bottom: 5px;">
         <strong>%%filename%%</strong> <span class="text-muted">Waiting</span>
       </h6>
       <div class="progress" style="margin-bottom: 0">
         <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" 
           role="progressbar"
           style="width: 0%" 
           aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
         </div>
       </div>
     </div>
   </li>
 </script>

 <!-- Debug item template -->
 <script type="text/html" id="debug-template">
   <li class="list-group-item text-%%color%%"><strong>%%date%%</strong>: %%message%%</li>
 </script>

<script type="text/javascript">

	// function ui_add_log(message, color){
	//   var d = new Date();

	//   var dateString = (('0' + d.getHours())).slice(-2) + ':' +
	//     (('0' + d.getMinutes())).slice(-2) + ':' +
	//     (('0' + d.getSeconds())).slice(-2);

	//   color = (typeof color === 'undefined' ? 'muted' : color);

	//   var template = $('#debug-template').text();
	//   template = template.replace('%%date%%', dateString);
	//   template = template.replace('%%message%%', message);
	//   template = template.replace('%%color%%', color);
	  
	//   $('#debug').find('li.empty').fadeOut(); // remove the 'no messages yet'
	//   $('#debug').prepend(template);
	// }

	// // Creates a new file and add it to our list
	// function ui_multi_add_file(id, file)
	// {
	//   var template = $('#files-template').text();
	//   template = template.replace('%%filename%%', file.name);

	//   template = $(template);
	//   template.prop('id', 'uploaderFile' + id);
	//   template.data('file-id', id);

	//   $('#files').find('li.empty').fadeOut(); // remove the 'no files yet'
	//   $('#files').prepend(template);
	// }

	// // Changes the status messages on our list
	// function ui_multi_update_file_status(id, status, message)
	// {
	//   $('#uploaderFile' + id).find('span').html(message).prop('class', 'label label-' + status);
	// }

	// // Updates a file progress, depending on the parameters it may animate it or change the color.
	// function ui_multi_update_file_progress(id, percent, color, active)
	// {
	//   color = (typeof color === 'undefined' ? false : color);
	//   active = (typeof active === 'undefined' ? true : active);

	//   var bar = $('#uploaderFile' + id).find('div.progress-bar');

	//   bar.width(percent + '%').attr('aria-valuenow', percent);
	//   bar.toggleClass('progress-bar-striped progress-bar-animated', active);

	//   if (percent === 0){
	//     bar.html('');
	//   } else {
	//     bar.html(percent + '%');
	//   }

	//   if (color !== false){
	//     bar.removeClass('bg-success bg-info bg-warning bg-danger');
	//     bar.addClass('bg-' + color);
	//   }
	// }

	$(function () {

		// _dir = '';
		// _aw = 1;
		// _offset_req = 0;
		// _curpage_req = 1;
		
		Widget.Loader('dir_dms', {
			stop_loader : false
		}, 'cont-widget-dms');

		// $(this).on('click', '.clear-list-upload',function(e){
		// 	e.preventDefault();
		// 	$('#files').html('');
		// });

		$(this).on('click', '.widget-render',function(e){
			e.preventDefault();
			var widget = $(this).data('wg');
			var title = $(this).find('a').html();
			$('#cont-widget-dms').find('.box-title').html(title);
			$('.nav-stacked').find('li').removeClass('active');
			$(this).addClass('active');
			Widget.Loader(widget, {
				stop_loader : false
			}, 'cont-widget-dms');
		});


		// $(this).on('click', '.btn-upload-folder',function(e){
		// 	e.preventDefault();
		// 	$('#modal-upload-folder .modal-body .form-group-added').remove();
		// });

		// $('#drag-and-drop-zone').dmUploader({ //
  //   		url: site_url + 'dms/pedasi/upload_file_sample',
		// 	extFilter: ["jpg", "jpeg", "png", "gif", "pdf", "docx", "xls", "xlsx", "pptx", "ppt", "zip", "rar", "csv", "txt"],
		// 	extraData: function() {
		// 	   return {
		// 	     "dir": _dir
		// 	   };
		// 	},
	 //    	onDragEnter: function(){
		//       this.addClass('active');
		//     },
		//     onDragLeave: function(){
		//       this.removeClass('active');
		//     },
		//     onNewFile: function(id, file){
		//       ui_multi_add_file(id, file);
		//     },
		//     onBeforeUpload: function(id){
		//       ui_multi_update_file_status(id, 'uploading', 'Uploading...');
		//       ui_multi_update_file_progress(id, 0, '', true);
		//     },
		//     onUploadCanceled: function(id) {
		//       ui_multi_update_file_status(id, 'warning', 'Canceled by User');
		//       ui_multi_update_file_progress(id, 0, 'warning', false);
		//     },
		//     onUploadProgress: function(id, percent){
		//       ui_multi_update_file_progress(id, percent);
		//     },
		//     onUploadSuccess: function(id, data){
		//       ui_multi_update_file_status(id, 'success', 'Upload Complete');
		//       ui_multi_update_file_progress(id, 100, 'success', false);
		//     },
		//     onComplete: function(){
		//     	$(this).getting();
		//     },
		//     onUploadError: function(id, xhr, status, message){
		//     	alert(message);
		//       // ui_multi_update_file_status(id, 'danger', message);
		//       // ui_multi_update_file_progress(id, 0, 'danger', false);  
		//     },
	 //     	onFileSizeError: function(file){
	 //      	alert('Ukuran file \'' + file.name + '\' melebihi batas limit');
	 //    	},
	 //    	onFileExtError:function(file){
	 //    		alert('Type pada File \'' + file.name + '\' tidak sesuai');
	 //    	}
	 //  	});

		// $(this).on('click', '.tab-all-files',function(e){
		// 	$('.nav-stacked').find('li').removeClass('active');
		// 	$(this).addClass('active');
		// 	$(this).getting();
		// 	e.preventDefault();
		// });

		// $(this).on('click', '.add-more-folder',function(e){
		// 	e.preventDefault();
		// 	var total = $("#modal-upload-folder .modal-body > .form-group").length;
		// 	if(total < 5){
		// 		t = '';
		// 		t += '<div class="form-group form-group-added">';
		// 		t += '<input type="text" class="form-control input-sm" name="folder_name[]" placeholder="Nama folder..." required="">';
		// 		t += '</div>';
		// 		$('#modal-upload-folder .modal-body').prepend(t);
		// 	}else{
		// 		alert('Maksimal 5 folder setiap submit');
		// 	}
		// });
		
		// $(this).on('click', '.breadcrumb-action',function(e){
		// 	e.preventDefault();
		// 	_dir = $(this).attr('data-href');
		// 	$(this).getting();
		// });

		// $(this).on('submit', '#form-upload-file', function(e){
	 //      var form = $(this);
	 //      $(this).ajaxSubmit({
	 //         url : site_url + 'dms/pedasi/upload_file',
	 //         type : "POST",
	 //         data : {
	 //            dir : _dir
	 //         },
	 //         dataType : "JSON",
	 //         error: function (jqXHR, status, errorThrown) {
	 //            error_handle(jqXHR, status, errorThrown);
	 //            loading_form(form, 'hide', '<i class="fa fa-upload"></i> Upload');
	 //         },
	 //         beforeSend: function (xhr) {
	 //            loading_form(form, 'show', loadingbutton);
	 //         },
	 //         success: function(r) {
	 //         	session_checked(r._session, r._maintenance);
	 //            if(r.success){	               
	 //               $(this).getting();
	 //               toastr.success(r.msg);
	 //               $("#modal-upload-file").modal("toggle");
	 //               form.resetForm();
	 //            }else{
	 //            	toastr.error(r.msg);
	 //            }
	 //            loading_form(form, 'hide', '<i class="fa fa-upload"></i> Upload');
	 //         },
	 //      });
	 //      e.preventDefault();
	 //   });

	   // $(this).on('submit', '#form-upload-folder', function(e){
	   //    var form = $(this);
	   //    $(this).ajaxSubmit({
	   //       url : site_url + 'dms/pedasi/upload_folder',
	   //       type : "POST",
	   //       data : {
	   //          dir : _dir
	   //       },
	   //       dataType : "JSON",
	   //       error: function (jqXHR, status, errorThrown) {
	   //          error_handle(jqXHR, status, errorThrown);
	   //          loading_form(form, 'hide', '<i class="fa fa-upload"></i> Upload');
	   //       },
	   //       beforeSend: function (xhr) {
	   //          loading_form(form, 'show', loadingbutton);
	   //       },
	   //       success: function(r) {
	   //       	session_checked(r._session, r._maintenance);
	   //          if(r.success){	               
	   //             $(this).getting();
	   //             toastr.success(r.msg);
	   //             $("#modal-upload-folder").modal("toggle");
	   //             form.resetForm();
	   //          }else{
	   //          	toastr.error(r.msg);
	   //          }
	   //          loading_form(form, 'hide', '<i class="fa fa-upload"></i> Upload');
	   //       },
	   //    });
	   //    e.preventDefault();
	   // });


		// $.fn.getting = function(option){
	 //      var param = $.extend({
	 //         dir : _dir,
	 //         order : 'clean_time',
	 //         orderby : 'desc'
	 //      }, option);
	      
	 //      ajaxManager.addReq({
	 //         type : "GET",
	 //         url : site_url + 'dms/pedasi/read_dir',
	 //         dataType : "JSON",
	 //         data : {
	 //            dir : param.dir,
	 //            order : param.order,
	 //            orderby : param.orderby
	 //         },
	 //         beforeSend: function (xhr) {
	 //            loading_table('#cont-dms-result', 'show');
	 //         },
	 //         error: function (jqXHR, status, errorThrown) {
	 //            error_handle(jqXHR, status, errorThrown); 
	 //            loading_table('#cont-dms-result', 'hide');
	 //         },
	 //         success : function(r){
	 //            var t = '';
	 //            if(r.result){
	 //               if(r.result.total){
	 //                  var total = r.result.total;
	 //                  $.each(r.result.data, function(k,v){
	 //                  	//init metadata
	 //                  	t += '<input id="name-'+k+'" type="hidden" value="'+k+'">';
  //         					t += '<input id="metaname-'+k+'" type="hidden" value="'+v.name+'">';
  //         					t += '<input id="metanameencode-'+k+'" type="hidden" value="'+v.url+'">';
  //         					t += '<input id="metaext-'+k+'" type="hidden" value="'+(v.ext_orig ? v.ext_orig : '')+'">';
  //         					t += '<input id="metalock-'+k+'" type="hidden" value="'+(v.lock ? v.lock : '')+'">';
  //         					t += '<input id="metashare-'+k+'" type="hidden" value="'+(v.share ? v.share : '')+'">';
  //         					if(param.dir){
  //         						t += '<input id="metadir-'+k+'" type="hidden" value="'+r.dir_base+'/'+param.dir+'">';
  //         					}else{
  //         						t += '<input id="metadir-'+k+'" type="hidden" value="'+r.dir_base+'">';
  //         					}

	 //                  	t += '<tr>';
		//           				t += '<td class="mailbox-star">';
		//           				if(v.favorites){
		//           					t += '&nbsp;<i class="fa fa-star text-yellow" data-toggle="tooltip" data-title="Favorites"></i>';
		//           				}
		//           				if(v.lock){
		//           					t += '&nbsp;<i class="fa fa-lock text-danger" data-toggle="tooltip" data-title="Locked document"></i>';
		//           				}
		//           				if(v.share){
		//           					t += '&nbsp;<i class="fa fa-share-alt text-info" data-toggle="tooltip" data-title="Shared document"></i>';
		//           				}
		//           				t += '</td>';
		//           				t += '<td class="mailbox-name">';
		//           					if(param.dir){
		//           						if(v.is_file){
		//           							t += '<a href="'+base_url+'main/download?name='+v.url+'&dir='+r.dir_base+'/'+param.dir+'/'+v.url+'" data-id="'+k+'" style="cursor:pointer;">';
		//           						}else{
		//           							t += '<a data-href="'+param.dir+'/'+v.url+'" style="cursor:pointer;" class="change-dir">';
		//           						}
		//           					}else{
		//           						if(v.is_file){
		//           							t += '<a href="'+base_url+'main/download?name='+v.name+'&dir='+r.dir_base+'/'+v.url+'" data-id="'+k+'" style="cursor:pointer;">';
		//           						}else{
		//           							t += '<a data-href="'+v.url+'" style="cursor:pointer;" class="change-dir">';
		//           						}
		//           					}
		//           					if(!v.is_file){
		//           						t += '<i class="fa fa-folder"></i>&nbsp;';
		//           					}else{
		//           						if(v.ext){
		//           							t += '<i class="'+v.ext+'"></i>&nbsp;';
		//           						}
		//           					}
		//           					t += v.name
		//           					t += '</a>';
		//           				t += '</td>';
		//           				t += '<td class="mailbox-attachment">';
		//           					t += '<span class="dropdown dropdown-'+k+'">';
		//        				 			t += '<button class="btn dropdown-toggle btn-default btn-xs show-options " data-toggle="dropdown">';
		//        				 				t += '<i class="fa fa-ellipsis-h"></i>';
		//        				 			t += '</button>';
		//        				 			t += '<ul class="dropdown-menu" style="left: -138px;border: 1px solid #cccccc;">';
		//        				 				if(v.is_file){
		//        				 					t += '<li>';
	 //       				 							t += '<a data-id="'+k+'" class="assign-request dropdown-options-dms" data-toggle="modal" data-target="#modal-assign" href="">';
	 //       				 								t += '<i class="fa fa-share"></i>Share to request';
	 //       				 							t += '</a>';
	 //       				 						t += '</li>';
	 //       				 						t += '<li class="divider"></li>';
		//        				 				}
		//        				 				if(!v.favorites){
	 //       				 						t += '<li>';
	 //       				 							t += '<a data-st="3" data-id="'+k+'" class="assign-flag dropdown-options-dms" href="">';
	 //       				 								t += '<i class="fa fa-star"></i>Add to favorit';
	 //       				 							t += '</a>';
	 //       				 						t += '</li>';		       				 			
  //      				 						}else{
  //      				 							t += '<li class="divider"></li>';
  //      				 							t += '<li>';
  //      				 								t += '<a data-st="3" data-id="'+v.favorites+'" class="remove-flag text-danger dropdown-options-dms" href="">';
  //      				 									t += '<i class="fa fa-star"></i><span class="text-danger">Remove favorites</span>';
  //      				 								t += '</a>';
  //      				 							t += '</li>';
  //      				 						}
		//        				 				if(v.is_file){
		//        				 					if(!v.lock){
		//        				 						t += '<li class="divider"></li>';
		//        				 						t += '<li>';
		//        				 							t += '<a data-st="1" data-id="'+k+'" class="assign-flag dropdown-options-dms" href="">';
		//        				 								t += '<i class="fa fa-lock"></i>Lock';
		//        				 							t += '</a>';
		//        				 						t += '</li>';
		//        				 					}else{
		//        				 						t += '<li class="divider"></li>';
		//        				 						t += '<li>';
		//        				 							t += '<a data-st="1" data-id="'+v.lock+'" class="remove-flag text-danger dropdown-options-dms" href="">';
		//        				 								t += '<i class="fa fa-lock"></i><span class="text-danger">Remove lock</span>';
		//        				 							t += '</a>';
		//        				 						t += '</li>';
		//        				 					}

		//        				 					if(!v.share){
		//        				 						t += '<li class="divider"></li>';
		//        				 						t += '<li>';
		//        				 							t += '<a data-st="2" data-id="'+k+'" class="assign-flag dropdown-options-dms" href="">';
		//        				 								t += '<i class="fa fa-share-alt"></i>Share';
		//        				 							t += '</a>';
	 //       				 							t += '</li>';
	 //       				 						}else{
	 //       				 							t += '<li class="divider"></li>';
	 //       				 							t += '<li>';
	 //       				 								t += '<a data-st="2" data-id="'+v.share+'" class="remove-flag text-danger dropdown-options-dms" href="">';
	 //       				 									t += '<i class="fa fa-share-alt"></i><span class="text-danger">Remove share</span>';
	 //       				 								t += '</a>';
	 //       				 							t += '</li>';
	 //       				 						}

	 //       				 						t += '<li class="divider"></li>';
	 //       				 						t += '<li>';
		//        				 						t += '<a data-id="'+k+'" class="delete-file text-danger dropdown-options-dms" href="">';
		//        				 							t += '<i class="fa fa-trash"></i>Delete File';
		//        				 						t += '</a>';
	 //       				 						t += '</li>';
	 //       				 					}else{
	 //       				 						t += '<li class="divider"></li>';
	 //       				 						t += '<li>';
		//        				 						t += '<a data-id="'+k+'" class="delete-folder text-danger dropdown-options-dms" href="">';
		//        				 							t += '<i class="fa fa-trash"></i>Delete Folder';
		//        				 						t += '</a>';
	 //       				 						t += '</li>';
	 //       				 					}
		//        				 			t += '</ul>';
	 //       				 			t += '</span>';
		//        				 	t += '</td>';
		//                     	t += '<td class="mailbox-date">'+v.time+'</td>';
		//                     	t += '<td class="mailbox-size">'+v.size+'</td>';
	 //                    	t += '</tr>';
	 //                  });
	 //               }else{
	 //                  t += '<tr><td class="text-center text-muted" colspan="10">No Result</td></tr>';
	 //               }
  //              }

	 //            $('#cont-dms-result').find('.sect-total').html(''+number_format(total)+' Rows Data');
	 //            $('#cont-dms-result').find('.sect-dms-result').html(t);
	 //            $('#cont-dms-result').find('.sect-nav-render').html('');
	 //            $('#cont-dms-result').find('.sect-nav-render').html(r.nav);
	 //            // $('.request-rating').rating({
	 //            // 	displayOnly: true
	 //            // });
	 //         },
	 //         complete : function(r){
	 //         	loading_table('#cont-dms-result', 'hide');
  //        	}
	 //      });
	 //   }

	 //   $(this).on('click', '.change-dir',function(e){
		// 	e.preventDefault();
		// 	_dir = $(this).attr('data-href');
		// 	$(this).getting();
		// });

	   // $(this).on('click', '.btn-filter',function(e){
	   // 	_offset_req = 0;
	   // 	_curpage_req = 1;
	   // 	var dir = $('#form-assign').find('input[name="dir"]').val();
	   // 	var name = $('#form-assign').find('input[name="filename"]').val();
	   // 	$(this).getting_request({
	   // 		dir : dir,
	   // 		filename : name
	   // 	});
	   // });

		// $.fn.getting_request = function(option){
	 //      var param = $.extend({
	 //         filt_keyword : $('#filt_keyword').val(),
	 //         // filt_status : $('#filt_status').val(),
	 //         dir : null,
	 //         filename : null,
	 //         limit : 10,
	 //         offset : _offset_req, 
	 //         currentPage : _curpage_req,
	 //         order : 'a.id', 
	 //         orderby : 'desc'
	 //      }, option);
	      
	 //      ajaxManager.addReq({
	 //         type : "GET",
	 //         url : site_url + 'dms/pedasi/getting_request',
	 //         dataType : "JSON",
	 //         data : {
	 //            offset : param.offset,
	 //            dir : param.dir,
	 //            filename : param.filename,
	 //            // filt_status : param.filt_status,
	 //            filt_keyword : param.filt_keyword,
	 //            limit : param.limit,
	 //            order : param.order,
	 //            orderby : param.orderby
	 //         },
	 //         beforeSend: function (xhr) {
	 //         },
	 //         error: function (jqXHR, status, errorThrown) {
	 //            error_handle(jqXHR, status, errorThrown); 
	 //         },
	 //         success : function(r){
	 //            var t = '';
  //           	if(r.total > 0){
  //                 var total = r.total;
  //                 $.each(r.result, function(k,v){
  //                 	t += '<tr>';
  //                 		if(v.shared){
  //                 			t += '<td></td>';
  //                 		}else{
  //                 			t += '<td><input type="checkbox" class="option_id" value="'+v.id+'" name="request_id[]"></td>';
  //                 		}
  //                 		t += '<td>';
  //                 			t += v.requested_date+'<br>'+v.requested_time;
  //                 		t += '</td>';
  //                 		t += '<td>'+v.title+'</td>';
  //                 		t += '<td>'+v.start_date+'</td>';
  //                 		t += '<td>'+v.end_date+'</td>';
  //                 		t += '<td>';
  //                 			t += v.limit_date;
  //                 			t += '<br><span style="font-size:10px;font-weight:bold">'+v.days_left+' Days Left</span>';
  //                 		t += '</td>';
  //                 		t += '<td>'+v.status_label+'</td>';
  //                 		t += '<td>'+(v.shared ? '<i class="fa fa-check"></i>' : '')+'</td>';
  //                 	t += '</tr>';
  //                 });
  //              }else{
  //           		t += '<tr><td class="text-center text-muted" colspan="8">Data tidak ditemukan</td></tr>';
  //              }

	 //            $('#modal-assign').find('.sect-total').html(''+number_format(total)+' Rows Data');
	 //            $('#modal-assign').find('.sect-data').html(t);

	 //            $('#modal-assign .sect-pagination').pagination({
		// 	         items: r.total,
		// 	         itemsOnPage: param.limit,
		// 	         edges: 0,
		// 	         hrefTextPrefix: '',
		// 	         displayedPages: 1,
		// 	         currentPage : param.currentPage,
		// 	         prevText : '&laquo;',
		// 	         nextText : '&raquo;',
		// 	         dropdown: true,
		// 	         onPageClick : function(n,e){
		// 	            e.preventDefault();
		// 	            _offset_req = (n-1)*param.limit;
		// 	            _curpage_req = n;
		// 	            var dir = $('#form-assign').find('input[name="dir"]').val();
	 //   					var name = $('#form-assign').find('input[name="filename"]').val();
		// 	            $(this).getting_request();
		// 	         }
		// 	      });
	 //         },
	 //         complete : function(r){
	         	
  //        	}
	 //      });
	 //   }

		// $(this).on('click', '.assign-request',function(e){
	 //      e.preventDefault();
	 //      var id = $(this).data('id');
	 //      name = $('#metanameencode-'+id+'').val();
		// 	dir = $('#metadir-'+id+'').val();
		// 	ext = $('#metaext-'+id+'').val();
		// 	$('#form-assign').find('#filt_keyword').val('');
		// 	$('#form-assign').find('#filt_status').val('all');

		// 	$('#form-assign').find('input[name="dir"]').val(dir);
		// 	$('#form-assign').find('input[name="filename"]').val(name);
		// 	$('#form-assign').find('input[name="ext"]').val(ext);
	 //      $(this).getting_request({
	 //      	dir : dir,
	 //      	filename : name
	 //      });
	 //   });

	   // $(this).on('submit', '#form-assign', function(e){
	   //    var form = $(this);
	   //    var req_id = form.find("input[name='request_id[]']").map(function(){return $(this).val();}).get();
	   //    if(req_id.length > 0){
	   //    	$(this).ajaxSubmit({
		  //        url : site_url + 'dms/pedasi/assign_request_file',
		  //        type : "POST",
		  //        dataType : "JSON",
		  //        error: function (jqXHR, status, errorThrown) {
		  //           error_handle(jqXHR, status, errorThrown);
		  //           loading_form(form, 'hide', '<i class="fa fa-share"></i> Sharing');
		  //        },
		  //        beforeSend: function (xhr) {
		  //           loading_form(form, 'show', loadingbutton);
		  //        },
		  //        success: function(r) {
		  //        	session_checked(r._session, r._maintenance);
		  //           if(r.success){
		  //           	var dir = $('#form-assign').find('input[name="dir"]').val();
	   // 					var name = $('#form-assign').find('input[name="filename"]').val();
		  //              $(this).getting_request({
		  //              	dir : dir,
		  //              	filename : name
		  //              });
		  //              toastr.success(r.msg);
		  //           }else{
		  //           	toastr.error(r.msg);
		  //           }
		  //        },
		  //        complete: function(){
				// 		loading_form(form, 'hide', '<i class="fa fa-share"></i> Sharing');
		  //        }
		  //     });
	   //    }else{
	   //    	alert('tidak ada request yang ditandai');
	   //    }
	   //    e.preventDefault();
	   // });

		// $(this).on('click', '.assign-flag',function(e){
		// 	e.preventDefault();
		// 	id = $(this).attr('data-id');
		// 	stat = $(this).attr('data-st');
		// 	name = $('#metaname-'+id+'').val();
		// 	dir = $('#metadir-'+id+'').val();
		// 	ext = $('#metaext-'+id+'').val();
		//  	ajaxManager.addReq({
	 //         type : "POST",
	 //         url : site_url + 'dms/pedasi/assign_flag',
	 //         dataType : "JSON",
	 //         data : {
	 //            name : name,
	 //            dir : dir,
	 //            status : stat,
	 //            ext : ext
	 //         },
	 //         error: function (jqXHR, status, errorThrown) {
	 //            error_handle(jqXHR, status, errorThrown); 
	 //         },
	 //         success : function(r){
	 //         	session_checked(r._session, r._maintenance);
	 //            if(r.success){
	 //               $(this).getting();
	 //               toastr.success(r.msg);
	 //               $('.dropdown-'+id+'').removeClass('open');
	 //            }else{
	 //            	toastr.error(r.msg);
	 //            }
  //        	}
  //     	});
		// });

		// $(this).on('click', '.remove-flag',function(e){
		// 	e.preventDefault();
		// 	id = $(this).attr('data-id');
		// 	status = $(this).attr('data-st');
		// 	if(id != ''){
		// 	 	ajaxManager.addReq({
		//          type : "GET",
		//          url : site_url + 'dms/pedasi/remove_flag',
		//          dataType : "JSON",
		//          data : {
		//             id : id,
		//             status : status
		//          },
		//          error: function (jqXHR, status, errorThrown) {
		//             error_handle(jqXHR, status, errorThrown); 
		//          },
		//          success : function(r){
		//          	session_checked(r._session, r._maintenance);
		//             if(r.success){
		//                $(this).getting();
		//                toastr.success(r.msg);
		//             }else{
		//             	toastr.error(r.msg);
		//             }
	 //         	}
	 //      	});
  //     	}else{
  //     		alert('ID Not Found');
  //     	}
		// });

		// $(this).on('click', '.delete-folder',function(e){
		// 	e.preventDefault();
		// 	id = $(this).attr('data-id');
		// 	name = $('#metanameencode-'+id+'').val();
		// 	dir = $('#metadir-'+id+'').val();
		//  	ajaxManager.addReq({
	 //         type : "POST",
	 //         url : site_url + 'dms/pedasi/delete_folder',
	 //         dataType : "JSON",
	 //         data : {
	 //            name : name,
	 //            dir : dir,
	 //         },
	 //         error: function (jqXHR, status, errorThrown) {
	 //            error_handle(jqXHR, status, errorThrown); 
	 //         },
	 //         success : function(r){
	 //         	session_checked(r._session, r._maintenance);
	 //            if(r.success){
	 //               $(this).getting();
	 //               toastr.success(r.msg);
	 //            }else{
	 //            	toastr.error(r.msg);
	 //            }
  //        	}
  //     	});
		// });

		// $(this).on('click', '.delete-file',function(e){
		// 	e.preventDefault();
		// 	id = $(this).attr('data-id');
		// 	name = $('#metanameencode-'+id+'').val();
		// 	dir = $('#metadir-'+id+'').val();
		//  	ajaxManager.addReq({
	 //         type : "POST",
	 //         url : site_url + 'dms/pedasi/delete_file',
	 //         dataType : "JSON",
	 //         data : {
	 //            name : name,
	 //            dir : dir
	 //         },
	 //         error: function (jqXHR, status, errorThrown) {
	 //            error_handle(jqXHR, status, errorThrown); 
	 //         },
	 //         success : function(r){
	 //         	session_checked(r._session, r._maintenance);
	 //            if(r.success){
	 //               $(this).getting();
	 //               toastr.success(r.msg);
	 //            }else{
	 //            	toastr.error(r.msg);
	 //            }
  //        	}
  //     	});
		// });

		// $(this).on('click', '.change_order', function(e){
	 //      e.preventDefault();
	 //      $('.change_order').html('<i class="fa fa-sort"></i>');
	 //      $(this).find('i').remove();
	 //      var sent = $(this).data('order');
	 //      var by = $(this).attr('data-by');
	 //      if(by === 'asc'){ 
	 //         $(this).attr('data-by', 'desc');
	 //         $(this).html('<i class="fa fa-sort-asc"></i>');
	 //      }
	 //      else{ 
	 //         $(this).attr('data-by', 'asc');
	 //         $(this).html(' <i class="fa fa-sort-desc"></i>');
	 //      }
	 //     $(this).getting({order:sent,orderby:by});
	 //   });

	   // $(this).on('click', '.change_order_req', function(e){
	   //    e.preventDefault();
	   //    $('.change_order_req').html('<i class="fa fa-sort"></i>');
	   //    $(this).find('i').remove();
	   //    var sent = $(this).data('order');
	   //    var by = $(this).attr('data-by');
	   //    if(by === 'asc'){ 
	   //       $(this).attr('data-by', 'desc');
	   //       $(this).html('<i class="fa fa-sort-asc"></i>');
	   //    }
	   //    else{ 
	   //       $(this).attr('data-by', 'asc');
	   //       $(this).html(' <i class="fa fa-sort-desc"></i>');
	   //    }
	   //   $(this).getting_request({order:sent,orderby:by});
	   // });

	   // $(this).getting();
	});
</script>