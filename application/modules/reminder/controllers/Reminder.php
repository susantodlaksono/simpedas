<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Reminder extends MY_Controller {

	public function __construct() {
		parent::__construct();
      $this->load->library('email');
   }

   public function limit_date_request(){
   	$date = date('Y-m-d', strtotime('+1 day'));
   	$result = $this->db->where('limit_date', $date)->where('status IS NULL')->get('request', 1, 0)->result_array();
   	if($result){
   		foreach ($result as $v) {
   			$pedasi = $this->receiver($v['opd_id'], 5);
   			$kadis = $this->receiver($v['opd_id'], 3);
   			if($v['disposisi_kasie']){
   				$kasie = $this->db->where('id', $v['disposisi_kasie'])->get('users')->row_array();
   			}else{
   				$kasie = NULL;
   			}

   			$body = $this->_body_limit_request($kadis['first_name'], $v['title'], $v['requested_date'], $v['limit_date']);
			 	$this->email->set_mailtype('html');
		      $this->email->from('sc.dwilaksono@gmail.com', 'Simpedas');
		      $this->email->subject('[Diskominfo] INFO | Reminder Permohonan Data | Eksternal');
		      $this->email->to($pedasi['email'].','.$kadis['email']);
		      // $this->email->to($kadis);
		      if($kasie){
		      	$this->email->cc($kasie['email']);
		      }
		      $this->email->message($body);
		      $send = $this->email->send();
		      if($send){
		      	echo 'success';
		      }else{
		      	echo $this->email->print_debugger();
		      }
		   }
   	}else{
   		echo 'none';
   	}
   }

   public function _body_limit_request($kadis, $title, $created, $limit){
   	$t = '';
   	$t .= 'Kepada Bapak/ Ibu '.$kadis.' ysh,<br>';
   	$t .= 'Berdasarkan permohonan <b>'.$title.'</b> yang disampaikan pada tanggal <b>'.date('d M Y', strtotime($created)).'</b>,<br>';
   	$t .= 'kami bermaksud untuk mengingatkan Kembali bahwa batas waktu penyampaian hasil kompilasi data paling lambat tanggal <b>'.date('d M Y', strtotime($limit)).'</b><br>';
   	$t .= 'data tersebut sudah disampaikan pada Dinas Komunikasi dan Informatika Kota Bandung melalui aplikasi Sistem Informasi Manajemen Pedasi yang dapat diakses pada halaman pedasi.xyz<br>';
   	$t .= 'Demikian, atas perhatian dan kerjasamanya kami haturkan terima kasih.';
   	return $t;
   }

   public function receiver($opd_id, $group){
   	$this->db->select('b.first_name, b.email');
   	$this->db->join('users as b', 'a.user_id = b.id', 'left');
   	$this->db->where('a.opd_id', $opd_id);
   	$this->db->where('a.group_id', $group);
   	return $this->db->get('mapping_opd as a')->row_array();

   }

}