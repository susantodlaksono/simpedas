<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Dms_manage extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function delete_by_id($id, $userid){
		$this->db->where('id', $id);
		$this->db->where('created_by', $userid);
		$this->db->delete('document_tags');
	}

	public function delete_by_path($path, $userid){
      $this->db->where('created_by', $userid);
      $this->db->where('full_dir', $path);
      $this->db->delete('document_tags');
   }

   public function get_by_id($status, $userid){
   	$data = array();
   	$this->db->where('created_by', $userid);
   	$this->db->where('type_dms', $status);
   	$result = $this->db->get('document_tags')->result_array();
   	if($result){
   		foreach ($result as $v) {
   			if($v['file_dir'] && $v['file_name']){
   				$data[$v['id']]['dir'] = $v['full_dir'];
   				$data[$v['id']]['id'] = $v['id'];
   			}
   		}
   		return $data;
   	}else{
   		return FALSE;
   	}
   }

   public function get_by_status($mode, $params, $userid, $status){
      $this->db->where('created_by', $userid);
      $this->db->where('type_dms', $status);
      switch ($mode) {
         case 'get':
            return $this->db->get('document_tags')->result_array();
         case 'count':
            return $this->db->get('document_tags')->num_rows();
      }
   }

   public function flag_by_dir($mode, $params, $userid, $path, $status){
      $filedir = $path.'/'.urldecode($params['dir']);
      $this->db->where('file_dir', $filedir);
      $this->db->where('created_by', $userid);
      $this->db->where('type_dms', $status);
      switch ($mode) {
         case 'get':
            return $this->db->get('document_tags')->result_array();
         case 'count':
            return $this->db->get('document_tags')->num_rows();
      }
   }

   public function opd($mode, $params){
      $this->db->select('a.*');
      $this->db->where('a.status', 1);
      if($params['filt_keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.name', $params['filt_keyword']);
         $this->db->or_like('a.phone', $params['filt_keyword']);
         $this->db->or_like('a.description', $params['filt_keyword']);
         $this->db->group_end();
      }
      $this->db->order_by('a.id', 'desc');
      switch ($mode) {
         case 'get':
            return $this->db->get('opd as a', 10, $params['offset'])->result_array();
         case 'count':
            return $this->db->get('opd as a')->num_rows();
      }
   }

   public function getting_request($mode, $params, $userid){
      $this->db->select('a.id, a.opd_id, a.title, a.start_date, a.end_date, a.limit_date, a.requested_date, a.status');
      $this->db->select('b.first_name as req_name');
      $this->db->select('c.first_name as pedasi_name');
      $this->db->select('d.total_files');
      $this->db->join('users as b', 'a.requested_by = b.id', 'left');
      $this->db->join('users as c', 'a.pedasi_id = c.id', 'left');
      $this->db->join('(select req_id, count(id) as total_files from request_result_files
                        group by req_id) as d', 
                        'a.id = d.req_id', 'left');
      $this->db->where('a.pedasi_id', $userid);
      // if($params['filt_status'] != 'all'){
      //    $this->db->where('a.status', $params['filt_status']);
      // }else{
      //    $this->db->where_in('a.status', array(1,2,3));
      // }
      $this->db->where_in('a.status', array(1,2));
      if($params['filt_keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.title', $params['filt_keyword']);
         $this->db->or_like('b.first_name', $params['filt_keyword']);
         $this->db->group_end();
      }
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('request as a', $params['limit'], $params['offset'])->result_array();
         case 'count':
            return $this->db->get('request as a')->num_rows();
      }
   }

   public function request($mode, $params, $userid){
      $this->db->select('a.id, a.opd_id, a.title, a.start_date, a.end_date, a.limit_date, a.requested_date, a.status');
      $this->db->where('a.pedasi_id', $userid);
      $this->db->where_in('a.status', array(1));
      if($params['filt_keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.title', $params['filt_keyword']);
         $this->db->group_end();
      }
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('request as a', $params['limit'], $params['offset'])->result_array();
         case 'count':
            return $this->db->get('request as a')->num_rows();
      }
   }

   public function shared($reqid, $params){
      if($params['dir'] && $params['filename']){
         $this->db->where('req_id', $reqid);
         $this->db->where('full_path', $params['dir'].'/'.$params['filename']);
         $total = $this->db->count_all_results('request_result_files');
         if($total > 0){
            return TRUE;
         }else{
            return FALSE;
         }
      }else{
         return FALSE;
      }
   }

   public function pedasi_time($req_id){
      if($req_id){
         $tmp['pedasi_upload'] = date('Y-m-d H:i:s');
         $tmp['status'] = 2;
         $this->db->where_in('id', $req_id);
         $this->db->update('request', $tmp);
      }
   }
}