<style type="text/css">
	.breadcrumb-action{
		cursor:pointer;font-size: 13px;
	}
</style>

<?php
$_uniqid = uniqid();
?>

<div id="<?php echo $_uniqid ?>">
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb sect-nav-render hidden"></ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="mailbox-messages">
	    		<table class="table table-hover table-striped" style="font-size: 13px;">
	    			<thead>	
	    				<th width="60"></th>
	    				<th>Nama File <a class="change_order co" href="#" data-order="name" data-by="desc"><i class="fa fa-sort"></i></a></th>
	    				<th></th>
	    				<th>Waktu <a class="change_order co" href="#" data-order="clean_time" data-by="asc"><i class="fa fa-sort-desc"></i></a></th>
	    				<th>Ukuran <a class="change_order co" href="#" data-order="size_bytes" data-by="desc"><i class="fa fa-sort"></i></a></th>
	 				</thead>
	    			<tbody class="sect-data"></tbody>
	 			</table>
			</div>
		</div>
	</div>

</div>

<script type="text/javascript">

	_first_load = '<?php echo $first_load ?>';
	_uniqid = '<?php echo $_uniqid ?>';

   $(function () {

		_dir = '';
		_order = 'clean_time';
		_orderby = 'desc';

		$.fn.getting = function(option){
	      var param = $.extend({
	         dir : _dir,
	         order : _order,
	         orderby : _orderby,
	         first_load : false
	      }, option);
	      
	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'document/favorites/getting',
	         dataType : "JSON",
	         data : {
	            dir : param.dir,
	            order : param.order,
	            orderby : param.orderby
	         },
	         beforeSend: function (xhr) {
	         	if(!param.first_load){
	         		spinner('#cont-widget-dms', 'block');
	         	}
	         	$('#'+_uniqid).find('.sect-nav-render').addClass('hidden');
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	         },
	         success : function(r){
	            var t = '';
	         	if(r.total > 0){
	         		$.each(r.result, function(k,v){
	         			t += '<tr>';
	          				t += '<td class="mailbox-star"><i class="fa fa-star text-yellow" data-toggle="tooltip" data-title="Favorites"></i></td>';
	          				t += '<td class="mailbox-name">';
	          				if(v.is_file){
	    							t += '<a href="'+base_url+'main/download?name='+v.encode_file_name+'&dir='+v.encode_full_dir+'" data-id="'+k+'" style="cursor:pointer;">';
	    						}else{
	    							if(v.file_ternary){
	    								t += '<a data-href="'+v.file_ternary+'" style="cursor:pointer;" class="change-dir">';
	    							}else{
	    								t += '<a data-href="'+v.encode_file_name+'" style="cursor:pointer;" class="change-dir">';
	    							}
	    						}
	    						if(!v.is_file){
	       						t += '<i class="fa fa-folder"></i>&nbsp;';
	       					}else{
	       						if(v.file_extension){
	       							t += '<i class="'+v.file_extension+'"></i>&nbsp;';
	       						}
	       					}
	    						t += v.file_name;
	    						t += '</a>';
	       					t += '</td>';
	       					t  += '<td class="mailbox-attachment">';
	          					t += '<span class="dropdown dropdown-'+k+'">';
	       				 			t += '<button class="btn dropdown-toggle btn-default btn-xs show-options " data-toggle="dropdown">';
	       				 				t += '<i class="fa fa-ellipsis-h"></i>';
	       				 			t += '</button>';
	       				 			t += '<ul class="dropdown-menu" style="left: -138px;border: 1px solid #cccccc;">';
	       				 				t += '<li>';
				 								t += '<a data-st="3" data-id="'+v.id+'" class="remove-flag text-danger dropdown-options-dms" href="">';
				 									t += '<i class="fa fa-star"></i><span class="text-danger">Remove favorites</span>';
				 								t += '</a>';
				 							t += '</li>';
	       				 			t += '</ul>';
	    				 			t += '</span>';
	       				 	t += '</td>';
	       				 	t += '<td class="mailbox-date">'+v.created_date+'</td>';
	       				 	t += '<td class="mailbox-size">'+v.size+'</td>';
	       				t += '</tr>';
	      			});
	      		}else{
	      			t += '<tr><td class="text-center text-muted" colspan="10">No Result</td></tr>';
	      		}

	            $('#'+_uniqid).find('.sect-data').html(t);
	         },
	         complete : function(r){
	      		spinner('#cont-widget-dms', 'unblock');
	      		_first_load = false;
      		}
	      });
	   }

	   $.fn.getting_dir = function(option){
	      var param = $.extend({
	         dir : _dir,
	         order : _order,
	         orderby : _orderby
	      }, option);
	      
	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'document/favorites/read_dir',
	         dataType : "JSON",
	         data : {
	            dir : param.dir,
	            order : param.order,
	            orderby : param.orderby
	         },
	         beforeSend: function (xhr) {
         		spinner('#cont-widget-dms', 'block');
	         	$('#'+_uniqid).find('.sect-nav-render').removeClass('hidden');
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	         },
	         success : function(r){
	            var t = '';
	            if(r.result){
                  var total = r.result.total;
                  $.each(r.result.data, function(k,v){
                  	//init metadata
                  	t += '<input id="name-'+k+'" type="hidden" value="'+k+'">';
       					t += '<input id="metaname-'+k+'" type="hidden" value="'+v.name+'">';
       					t += '<input id="metanameencode-'+k+'" type="hidden" value="'+v.url+'">';
       					t += '<input id="metaext-'+k+'" type="hidden" value="'+(v.ext_orig ? v.ext_orig : '')+'">';
       					t += '<input id="metalock-'+k+'" type="hidden" value="'+(v.lock ? v.lock : '')+'">';
       					t += '<input id="metashare-'+k+'" type="hidden" value="'+(v.share ? v.share : '')+'">';
       					if(param.dir){
       						t += '<input id="metadir-'+k+'" type="hidden" value="'+r.dir_base+'/'+param.dir+'">';
       					}else{
       						t += '<input id="metadir-'+k+'" type="hidden" value="'+r.dir_base+'">';
       					}

                  	t += '<tr>';
	          				t += '<td class="mailbox-star">';
	          				if(v.favorites){
	          					t += '&nbsp;<i class="fa fa-star text-yellow" data-toggle="tooltip" data-title="Favorites"></i>';
	          				}
	          				if(v.lock){
	          					t += '&nbsp;<i class="fa fa-lock text-danger" data-toggle="tooltip" data-title="Locked document"></i>';
	          				}
	          				if(v.share){
	          					t += '&nbsp;<i class="fa fa-share-alt text-info" data-toggle="tooltip" data-title="Shared document"></i>';
	          				}
	          				t += '</td>';
	          				t += '<td class="mailbox-name">';
	          					if(param.dir){
	          						if(v.is_file){
	          							t += '<a href="'+base_url+'main/download?name='+v.url+'&dir='+r.dir_base+'/'+param.dir+'/'+v.url+'" data-id="'+k+'" style="cursor:pointer;">';
	          						}else{
	          							t += '<a data-href="'+param.dir+'/'+v.url+'" style="cursor:pointer;" class="change-dir">';
	          						}
	          					}else{
	          						if(v.is_file){
	          							t += '<a href="'+base_url+'main/download?name='+v.name+'&dir='+r.dir_base+'/'+v.url+'" data-id="'+k+'" style="cursor:pointer;">';
	          						}else{
	          							t += '<a data-href="'+v.url+'" style="cursor:pointer;" class="change-dir">';
	          						}
	          					}
	          					if(!v.is_file){
	          						t += '<i class="fa fa-folder"></i>&nbsp;';
	          					}else{
	          						if(v.ext){
	          							t += '<i class="'+v.ext+'"></i>&nbsp;';
	          						}
	          					}
	          					t += v.name
	          					t += '</a>';
	          				t += '</td>';
	          				t += '<td class="mailbox-attachment">';
	          					if(v.favorites){
	          						t += '<span class="dropdown dropdown-'+k+'">';
		       				 			t += '<button class="btn dropdown-toggle btn-default btn-xs show-options " data-toggle="dropdown">';
		       				 				t += '<i class="fa fa-ellipsis-h"></i>';
		       				 			t += '</button>';
		       				 			t += '<ul class="dropdown-menu" style="left: -138px;border: 1px solid #cccccc;">';
	    				 						t += '<li>';
	 				 								t += '<a data-st="3" data-id="'+v.favorites+'" class="remove-flag text-danger dropdown-options-dms" href="">';
	 				 									t += '<i class="fa fa-star"></i><span class="text-danger">Remove favorites</span>';
	 				 								t += '</a>';
	 				 							t += '</li>';	       				 			
		       				 			t += '</ul>';
	       				 			t += '</span>';
	          					}
	       				 	t += '</td>';
	                    	t += '<td class="mailbox-date">'+v.time+'</td>';
	                    	t += '<td class="mailbox-size">'+v.size+'</td>';
                    	t += '</tr>';
                  });
	            }else{
	            	t += '<tr><td class="text-center text-muted" colspan="10">No Result</td></tr>';
	            }

	            $('#'+_uniqid).find('.sect-data').html(t);
	            $('#'+_uniqid).find('.sect-nav-render').html('');
	            $('#'+_uniqid).find('.sect-nav-render').html(r.nav);
	         },
	         complete: function(){
	         	spinner('#cont-widget-dms', 'unblock');
	         }
	      });
	   }

	   $(this).on('click', '#'+_uniqid+' .remove-flag',function(e){
			e.preventDefault();
			id = $(this).attr('data-id');
			status = $(this).attr('data-st');
			if(id != ''){
			 	ajaxManager.addReq({
		         type : "GET",
		         url : site_url + 'document/favorites/remove_flag',
		         dataType : "JSON",
		         data : {
		            id : id,
		            status : status
		         },
		         error: function (jqXHR, status, errorThrown) {
		            error_handle(jqXHR, status, errorThrown); 
		         },
		         success : function(r){
		         	session_checked(r._session, r._maintenance);
		            if(r.success){
		               $(this).getting();
		            }else{
		            	toastr.error(r.msg);
		            }
	         	}
	      	});
	   	}else{
	   		alert('ID Not Found');
	   	}
		});

	   $(this).on('click', '#'+_uniqid+' .change-dir',function(e){
			e.preventDefault();
			_dir = $(this).attr('data-href');
			$('#'+_uniqid+' .co').removeClass('change_order');
			$('#'+_uniqid+' .co').addClass('change_order_dms');
			$(this).getting_dir();
		});

		$(this).on('click', '#'+_uniqid+' .change_order', function(e){
	      e.preventDefault();
	      $('.change_order').html('<i class="fa fa-sort"></i>');
	      $(this).find('i').remove();
	      var sent = $(this).data('order');
	      var by = $(this).attr('data-by');
	      if(by === 'asc'){ 
	         $(this).attr('data-by', 'desc');
	         $(this).html('<i class="fa fa-sort-asc"></i>');
	      }
	      else{ 
	         $(this).attr('data-by', 'asc');
	         $(this).html(' <i class="fa fa-sort-desc"></i>');
	      }
	      _order = sent;
	      _orderby = by;
	     	$(this).getting();
	   });

	   $(this).on('click', '#'+_uniqid+' .breadcrumb-action',function(e){
			e.preventDefault();
			_dir = $(this).attr('data-href');
			$(this).getting_dir();
		});

		$(this).on('click', '#'+_uniqid+' .breadcrumb-action-home', function(e){
	   	e.preventDefault();
	      _dir = '';
	      $('#'+_uniqid+' .co').removeClass('change_order_dms');
			$('#'+_uniqid+' .co').addClass('change_order');
	      $(this).getting();
	   });

		$(this).on('click', '#'+_uniqid+' .change_order_dms', function(e){
	      e.preventDefault();
	      $('.change_order_dms').html('<i class="fa fa-sort"></i>');
	      $(this).find('i').remove();
	      var sent = $(this).data('order');
	      var by = $(this).attr('data-by');
	      if(by === 'asc'){ 
	         $(this).attr('data-by', 'desc');
	         $(this).html('<i class="fa fa-sort-asc"></i>');
	      }
	      else{ 
	         $(this).attr('data-by', 'asc');
	         $(this).html(' <i class="fa fa-sort-desc"></i>');
	      }
	      _order = sent;
	      _orderby = by;
	     	$(this).getting_dir();
	   });

	   $(this).getting();

	});
</script>