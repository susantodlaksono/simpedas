<style type="text/css">
	.breadcrumb-action{
		cursor:pointer;font-size: 13px;
	}
	.dropdown-menu > li > a{
		padding:2px;
		text-align: center;
		font-weight: 600;
		color:#3c8dbc;
		/*line-height: 7px;*/
	}
	.ellipsis-option{
		left: -107px;border: 1px solid #cccccc;width: 120px;min-width: unset;top: 9px;
	}
</style>
<?php $_uniqid = uniqid(); ?>

<div id="<?php echo $_uniqid ?>">
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb sect-nav-render hidden"></ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="mailbox-messages">
	    		<table class="table table-hover table-striped" style="font-size: 13px;">
	    			<thead>	
	    				<th>Nama File <a class="sort-field co" href="#" data-order="name" data-by="desc"><i class="fa fa-sort"></i></a></th>
	    				<th width="10"></th>
	    				<th width="120">Waktu <a class="sort-field co" href="#" data-order="clean_time" data-by="asc"><i class="fa fa-sort-desc"></i></a></th>
	    				<th width="120">Ukuran <a class="sort-field co" href="#" data-order="size_bytes" data-by="desc"><i class="fa fa-sort"></i></a></th>
	 				</thead>
	    			<tbody class="sect-data"></tbody>
	 			</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	_first_load = '<?php echo $first_load ?>';
	_uniqid = '<?php echo $_uniqid ?>';

	$(function () {

		_dir = '';
		_order = 'clean_time';
		_orderby = 'desc'

		$.fn.getting = function(option){
	      var param = $.extend({
	         dir : _dir,
	         first_load : _first_load,
	         order : _order,
	         orderby : _orderby,
	      }, option);
	      
	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'document/sharing/getting',
	         dataType : "JSON",
	         data : {
	            dir : param.dir,
	            order : param.order,
	            orderby : param.orderby
	         },
	         beforeSend: function (xhr) {
	         	if(!param.first_load){
	         		spinner('#cont-widget-dms', 'block');
	         	}
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	         },
	         success : function(r){
      		 	var t = '';
	            if(r.result){
                  var total = r.result.total;
                  $.each(r.result, function(k,v){
                  	t += '<tr>';
                  		t += '<td class="mailbox-name">';
	          				if(v.is_file){
	    							t += '<a href="'+base_url+'main/download?name='+v.encode_file_name+'&dir='+v.encode_full_dir+'" data-id="'+k+'" style="cursor:pointer;">';
	    						}else{
	    							if(v.file_ternary){
	    								t += '<a data-href="'+v.encode_full_dir+'" style="cursor:pointer;" class="change-dir">';
	    							}else{
	    								t += '<a data-href="'+v.encode_file_name+'" style="cursor:pointer;" class="change-dir">';
	    							}
	    						}
	    						if(!v.is_file){
	       						t += '<i class="fa fa-folder"></i>&nbsp;';
	       					}else{
	       						if(v.file_extension){
	       							t += '<i class="'+v.file_extension+'"></i>&nbsp;';
	       						}
	       					}
	    						t += v.file_name;
	    						t += '</a>';
	       					t += '</td>';
	       					t += '<td class="mailbox-attachment">';
		       					t += '<span class="dropdown">';
										t += '<a href="" class="dropdown-toggle" data-toggle="dropdown">';
											t += '<i class="fa fa-ellipsis-h"></i>';
										t += '</a>';
										t += '<ul class="dropdown-menu ellipsis-option">';
											t += '<li>';
												if(v.is_file){
													t += '<a class="dropdown-options-dms" href="'+base_url+'main/download?name='+v.encode_file_name+'&dir='+v.encode_full_dir+'">';
												}else{
													t += '<a class="dropdown-options-dms" href="'+base_url+'main/zipped?name='+v.encode_file_name+'&dir='+v.encode_full_dir+'">';
												}
													t += 'Download';
												t += '</a>';
											t += '</li>';
											t += '<li class="divider"></li>';
										t += '</ul>';
									t += '</span>';
	       					t += '</td>';
	          				t += '<td class="mailbox-date">'+v.time+'</td>';
	                    	t += '<td class="mailbox-size">'+v.size+'</td>';
                  	t += '</tr>';
               	});
            	}else{
            		t += '<tr><td class="text-center text-muted" colspan="10">No Result</td></tr>';
            	}
            	$('#'+_uniqid).find('.sect-data').html(t);
	         },
	         complete : function(r){
	      		spinner('#cont-widget-dms', 'unblock');
	      		_first_load = false;
      		}
      	});
   	}

   	$.fn.dir = function(option){
	      var param = $.extend({
	         dir : _dir,
	         order : _order,
	         orderby : _orderby
	      }, option);
	      
	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'document/sharing/dir',
	         dataType : "JSON",
	         data : {
	            dir : param.dir,
	            order : param.order,
	            orderby : param.orderby
	         },
	         beforeSend: function (xhr) {
         		spinner('#cont-widget-dms', 'block');
	         	$('#'+_uniqid).find('.sect-nav-render').removeClass('hidden');
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	         },
	         success : function(r){
	            var t = '';
	            if(r.result){
                  var total = r.result.total;
                  $.each(r.result.data, function(k,v){
                  	t += '<tr>';
	          				t += '<td class="mailbox-name">';
	          					if(param.dir){
	          						if(v.is_file){
	          							t += '<a href="'+base_url+'main/download?name='+v.url+'&dir='+r.dir_base+'/'+param.dir+'/'+v.url+'" data-id="'+k+'" style="cursor:pointer;">';
	          						}else{
	          							t += '<a data-href="'+param.dir+'/'+v.url+'" style="cursor:pointer;" class="change-dir">';
	          						}
	          					}else{
	          						if(v.is_file){
	          							t += '<a href="'+base_url+'main/download?name='+v.name+'&dir='+r.dir_base+'/'+v.url+'" data-id="'+k+'" style="cursor:pointer;">';
	          						}else{
	          							t += '<a data-href="'+v.url+'" style="cursor:pointer;" class="change-dir">';
	          						}
	          					}
	          					if(!v.is_file){
	          						t += '<i class="fa fa-folder"></i>&nbsp;';
	          					}else{
	          						if(v.ext){
	          							t += '<i class="'+v.ext+'"></i>&nbsp;';
	          						}
	          					}
	          					t += v.name
	          					t += '</a>';
	          				t += '</td>';
	          				t += '<td class="mailbox-attachment">';
		       					t += '<span class="dropdown">';
										t += '<a href="" class="dropdown-toggle" data-toggle="dropdown">';
											t += '<i class="fa fa-ellipsis-h"></i>';
										t += '</a>';
										t += '<ul class="dropdown-menu ellipsis-option">';
											t += '<li>';
												t += '<a class="assign-request-files dropdown-options-dms" data-toggle="modal" data-target="#modal-assign-files" href="">';
													t += 'Download';
												t += '</a>';
											t += '</li>';
											t += '<li class="divider"></li>';
										t += '</ul>';
									t += '</span>';
	       					t += '</td>';
	                    	t += '<td class="mailbox-date">'+v.time+'</td>';
	                    	t += '<td class="mailbox-size">'+v.size+'</td>';
                    	t += '</tr>';
                  });
	            }else{
	            	t += '<tr><td class="text-center text-muted" colspan="10">No Result</td></tr>';
	            }

	            $('#'+_uniqid).find('.sect-data').html(t);
	            $('#'+_uniqid).find('.sect-nav-render').html('');
	            $('#'+_uniqid).find('.sect-nav-render').html(r.nav);
	         },
	         complete: function(){
	         	spinner('#cont-widget-dms', 'unblock');
	         }
	      });
	   }

	   $(this).on('click', '#'+_uniqid+' .breadcrumb-action',function(e){
			e.preventDefault();
			_dir = $(this).attr('data-href');
			$(this).dir();
		});

	   $(this).on('click', '#'+_uniqid+' .breadcrumb-action-home', function(e){
	   	e.preventDefault();
	      _dir = '';
	      $('#'+_uniqid+' .sort-field').removeClass('codms');
			$('#'+_uniqid+' .sort-field').addClass('co');
	      $(this).getting();
	   });

   	$(this).on('click', '#'+_uniqid+' .change-dir',function(e){
			e.preventDefault();
			_dir = $(this).attr('data-href');
			$('#'+_uniqid+' .sort-field').removeClass('co');
			$('#'+_uniqid+' .sort-field').addClass('codms');
			$(this).dir();
		});

   	$(this).on('click', '#'+_uniqid+' .co', function(e){
	      e.preventDefault();
	      $('.co').html('<i class="fa fa-sort"></i>');
	      $(this).find('i').remove();
	      var sent = $(this).data('order');
	      var by = $(this).attr('data-by');
	      if(by === 'asc'){ 
	         $(this).attr('data-by', 'desc');
	         $(this).html('<i class="fa fa-sort-asc"></i>');
	      }
	      else{ 
	         $(this).attr('data-by', 'asc');
	         $(this).html(' <i class="fa fa-sort-desc"></i>');
	      }
	      _order = sent;
	      _orderby = by;
	     	$(this).getting();
	   });

   	$(this).on('click', '#'+_uniqid+' .codms', function(e){
	      e.preventDefault();
	      $('.codms').html('<i class="fa fa-sort"></i>');
	      $(this).find('i').remove();
	      var sent = $(this).data('order');
	      var by = $(this).attr('data-by');
	      if(by === 'asc'){ 
	         $(this).attr('data-by', 'desc');
	         $(this).html('<i class="fa fa-sort-asc"></i>');
	      }
	      else{ 
	         $(this).attr('data-by', 'asc');
	         $(this).html(' <i class="fa fa-sort-desc"></i>');
	      }
	      _order = sent;
	      _orderby = by;
	     	$(this).dir();
	   });

   	$(this).getting();

	});

</script>