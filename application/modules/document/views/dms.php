<style type="text/css">
	.breadcrumb-action{
		cursor:pointer;font-size: 13px;
	}
</style>

<?php
$_uniqid = uniqid();
?>
<style type="text/css">
.act-selected{
 	font-size: 12px;
 	/*color: #303030;*/
 	font-weight: bold;
 	border: 1px solid #e3e3e3;
 	padding: 5px;
 	border-radius: 2px;
}	
.dropdown-menu > li > a{
	padding:2px;
	text-align: center;
	font-weight: 600;
	color:#3c8dbc;
	/*line-height: 7px;*/
}

</style>

<div id="<?php echo $_uniqid ?>">

	<div class="row">
		<div class="col-md-11">
			<ol class="breadcrumb sect-nav-render">
				<li><a data-href="" class="breadcrumb-action"><i class="fa fa-home"></i></a></li>
			</ol>
		</div>
		<div class="col-md-1">
	  		<button class="btn dropdown-toggle btn-default lg show-options" data-toggle="dropdown" style="margin-top: 3px;">
	  			<i class="fa fa-plus"></i>
				</button>
	    	<ul class="dropdown-menu" style="left:-105px;border:1px solid #cccccc;">
	    		<li>
	    			<a href="" data-toggle="modal" data-target="#modal-upload-file" style="font-size: 12px;">
	    				<i class="fa fa-file"></i>&nbsp;Upload File
	    			</a>
	    		</li>
	    		<li class="divider"></li>
	    		<li>
	    			<a class="btn-upload-folder" href="" data-toggle="modal" data-target="#modal-upload-folder" style="font-size: 12px;">
	 					<i class="fa fa-folder"></i>&nbsp;New Folder
	 				</a>
	 			</li>
	 		</ul>
		</div>
	</div>
	<div class="row row-options-selected hidden">
		<div class="col-md-12 text-center">
			<span class="dropdown">
				<a href="" class="act-selected dropdown-toggle" data-toggle="dropdown">
					Beri tindakan pada file terpilih <i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu" style="left: 31px;top: 21px;border: 1px solid #cccccc;min-width: 126px;">
					<li>
						<a class="assign-request-files dropdown-options-dms" data-toggle="modal" data-target="#modal-assign-files" href="">
							Lampirkan
						</a>
					</li>
					<li class="divider"></li>
					<li>
						<a class="assign-flag-bulk dropdown-options-dms" href="" data-st="3">
							Simpan ke favorit
						</a>
					</li>
					<!-- <li class="divider"></li>
					<li>
						<a class="assign-flag-bulk dropdown-options-dms" href="" data-st="1">
							Kunci dokumen
						</a>
					</li> -->
					<li class="divider"></li>
					<li>
						<a class="download-bulk dropdown-options-dms" href="">
							Download
						</a>
					</li>
					<li class="divider"></li>
					<li>
						<a class="share-doc dropdown-options-dms" href="" data-toggle="modal" data-target="#modal-share-doc">
							Share dokumen
						</a>
					</li>
					<li class="divider"></li>
					<li>
						<a class="remove-flag dropdown-options-dms text-red" href="">
							Hapus
						</a>
					</li>
				</ul>
			</span>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="mailbox-messages">
	    		<table class="table table-hover table-striped" style="font-size: 13px;">
	    			<thead>	
	    				<th width="10"><input type="checkbox" class="option_choose"></th>
	    				<th>Nama File <a class="change_order" href="#" data-order="name" data-by="desc"><i class="fa fa-sort"></i></a></th>
	    				<th width="70"></th>
	    				<th width="10"></th>
	    				<th width="120">Waktu <a class="change_order" href="#" data-order="clean_time" data-by="asc"><i class="fa fa-sort-desc"></i></a></th>
	    				<th width="120">Ukuran <a class="change_order" href="#" data-order="size_bytes" data-by="desc"><i class="fa fa-sort"></i></a></th>
	 				</thead>
	    			<tbody class="sect-data"></tbody>
	 			</table>
			</div>
		</div>
	</div>

	<div class="modal fade in" id="modal-assign-files">
	 	<div class="modal-dialog">
	     	<div class="modal-content" style="width: 1000px;margin-left: -195px;">
	         <div class="modal-header">
	         	<button type="button" class="close" data-dismiss="modal">
	         		<span aria-hidden="true">×</span>
	         	</button>
	          	<h4 class="modal-title">Lampirkan File Terpilih</h4>
	         </div>
	         <form class="form-assign-files">
	         	<div class="modal-body">
	         		<div class="row">
	         			<div class="col-md-12">
			           		<div class="btn-group" data-toggle="buttons">
									<input type="text" class="form-control input-sm" id="filt_keyword_files" placeholder="Pencarian...">
								</div>
								<div class="btn-group" data-toggle="buttons">
									<button class="btn btn-sm btn-filter-files"><i class="fa fa-filter"></i> Filter</button>
								</div>
	      				</div>
	      			</div>
	      			<hr>
	      			<table class="table table-striped table-condensed" style="font-size:12px;">
							<thead>
								<th></th>
								<th>Tgl <a class="change_order_req" href="#" data-order="a.requested_date" data-by="asc"><i class="fa fa-sort-desc"></i></th>
								<th>Permohonan Data <a class="change_order_req" href="#" data-order="a.title" data-by="asc"><i class="fa fa-sort"></i></th>
								<th>Mulai <a class="change_order_req" href="#" data-order="a.start_date" data-by="asc"><i class="fa fa-sort"></i></th>
								<th>Selesai <a class="change_order_req" href="#" data-order="a.end_date" data-by="asc"><i class="fa fa-sort"></i></th>
								<th>Batas Waktu <a class="change_order_req" href="#" data-order="a.limit_date" data-by="asc"><i class="fa fa-sort"></i></th>
								<th>Status <a class="change_order_req" href="#" data-order="a.status" data-by="asc"><i class="fa fa-sort"></i></th>
							</thead>
		          		<tbody class="sect-data-request-files"></tbody>
						</table>
						<div class="row">
		               <div class="col-md-2">
		             		<h6 class="sect-total-request-files"></h6>
		               </div>
		               <div class="col-md-10 text-right">
		                  <ul class="pagination sect-pagination-request-files pagination-sm no-margin pull-right"></ul>
		               </div>
		            </div>
	      		</div>
	      		<div class="modal-footer">
	              	<button type="submit" class="btn btn-primary btn-sm">Lampirkan</button>
	             </div>
	   		</form>
			</div>
		</div>
	</div>

	<div class="modal fade in" id="modal-share-doc">
	 	<div class="modal-dialog">
	     	<div class="modal-content" style="width: 1000px;margin-left: -195px;">
	         <div class="modal-header">
	         	<button type="button" class="close" data-dismiss="modal">
	         		<span aria-hidden="true">×</span>
	         	</button>
	          	<h4 class="modal-title">Share File Terpilih</h4>
	         </div>
	         <form class="form-share-doc">
	         	<div class="modal-body">
	         		<div class="row">
	         			<div class="col-md-12">
			           		<div class="btn-group" data-toggle="buttons">
									<input type="text" class="form-control input-sm" id="keyword_opd" placeholder="Pencarian...">
								</div>
								<div class="btn-group" data-toggle="buttons">
									<button class="btn btn-sm btn-filter-opd"><i class="fa fa-filter"></i> Filter</button>
								</div>
	      				</div>
	      			</div>
	      			<hr>
	      			<table class="table table-striped table-condensed" style="font-size:12px;">
							<thead>
								<th></th>
								<th>Name</th>
								<th>No.Telp</th>
								<th>Deskripsi</th>
							</thead>
		          		<tbody class="sect-data-share-opd"></tbody>
						</table>
						<div class="row">
		               <div class="col-md-2">
		             		<h6 class="sect-total-share-opd"></h6>
		               </div>
		               <div class="col-md-10 text-right">
		                  <ul class="pagination sect-pagination-share-opd pagination-sm no-margin pull-right"></ul>
		               </div>
		            </div>
	      		</div>
	      		<div class="modal-footer">
	              	<button type="submit" class="btn btn-primary btn-sm">Share</button>
	             </div>
	   		</form>
			</div>
		</div>
	</div>

	<div class="modal fade in" id="modal-upload-file">
	 	<div class="modal-dialog">
	     	<div class="modal-content" style="width: 900px;margin-left: -139px;">
         	<div class="modal-body">
         		<div class="row">
         			<div class="col-md-6 col-sm-12">
         				<div class="drag-and-drop-zone dm-uploader text-center" style="border: 0.25rem dashed #A5A5C7;padding: 5rem!important;border: 0.25rem dashed #A5A5C7;">
				            <h4 class="text-muted" style="margin-bottom: 3rem!important;margin-top: 5rem!important;">
				            	Drag &amp; drop files here
				         	</h4>
				            <div class="btn btn-primary btn-block" style="margin-bottom: 3rem!important;">
			                	<span>Open the file Browser</span>
			                	<input type="file" title='Click to add Files' />
				            </div>
			          	</div>
      				</div>
      				<div class="col-md-6 col-sm-12">
      					<div class="box box-default box-solid">
				            <div class="box-header">
				            	<h6 class="box-title" style="font-size: 15px;"><i class="fa fa-upload"></i> Status Upload</h6>
				            	<div class="box-tools">
					           		<button class="btn btn-default clear-list-upload">
					           			<i class="fa fa-trash"></i> Bersihkan List
					        			</button>
				        			</div>
				         	</div>
				         	<div class="box-body">
				         		<ul class="list-unstyled p-2 d-flex flex-column col" id="files" style="overflow:auto;height: 208px;">
				              		<li class="text-muted text-center empty" style="margin-top: 25%">Tidak ada file yang di upload.</li>
				            	</ul>
			         		</div>
			          	</div>
			        	</div>
      			</div>
      		</div>
			</div>
		</div>
	</div>

	<div class="modal fade in" id="modal-upload-folder">
	 	<div class="modal-dialog">
	     	<div class="modal-content" style="width: 350px;margin-left: 110px;">
	         <form class="form-upload-folder">                
	         	<div class="modal-body">
	         		<div class="form-group">
	         			<input type="text" class="form-control input-sm" name="folder_name[]" placeholder="Nama folder..." required="">
	      			</div>
	         		<a href="" class="add-more-folder"><h6><i class="fa fa-plus"></i> Tambah lainnya</h6></a>
	      		</div>
	      		<div class="modal-footer">
	              	<button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> Simpan</button>
	             </div>
	   		</form>
			</div>
		</div>
	</div>

	<div class="modal fade in" id="modal-assign">
	 	<div class="modal-dialog">
	     	<div class="modal-content" style="width: 1000px;margin-left: -195px;">
	         <div class="modal-header">
	         	<button type="button" class="close" data-dismiss="modal">
	         		<span aria-hidden="true">×</span>
	         	</button>
	          	<h4 class="modal-title"><i class="fa fa-share"></i>&nbsp;Share to request</h4>
	         </div>
	         <form class="form-assign">
	         	<input type="hidden" name="dir">
	         	<input type="hidden" name="filename">
	         	<input type="hidden" name="ext">
	         	<div class="modal-body">
	         		<div class="row">
	         			<div class="col-md-12">
			           		<div class="btn-group" data-toggle="buttons">
									<input type="text" class="form-control input-sm" id="filt_keyword" placeholder="Pencarian...">
								</div>
								<div class="btn-group" data-toggle="buttons">
									<button class="btn btn-sm btn-filter"><i class="fa fa-filter"></i> Filter</button>
								</div>
	      				</div>
	      			</div>
	      			<hr>
	         		<table class="table table-striped table-condensed" style="font-size:12px;">
							<thead>
								<th></th>
								<th>Tgl <a class="change_order_req" href="#" data-order="a.requested_date" data-by="asc"><i class="fa fa-sort-desc"></i></th>
								<th>Permohonan Data <a class="change_order_req" href="#" data-order="a.title" data-by="asc"><i class="fa fa-sort"></i></th>
								<th>Mulai <a class="change_order_req" href="#" data-order="a.start_date" data-by="asc"><i class="fa fa-sort"></i></th>
								<th>Selesai <a class="change_order_req" href="#" data-order="a.end_date" data-by="asc"><i class="fa fa-sort"></i></th>
								<th>Batas Waktu <a class="change_order_req" href="#" data-order="a.limit_date" data-by="asc"><i class="fa fa-sort"></i></th>
								<th>Status <a class="change_order_req" href="#" data-order="a.status" data-by="asc"><i class="fa fa-sort"></i></th>
								<th>Sharing</th>
							</thead>
		          		<tbody class="sect-data-request"></tbody>
						</table>
						<div class="row">
		               <div class="col-md-2">
		             		<h6 class="sect-total-request"></h6>
		               </div>
		               <div class="col-md-10 text-right">
		                  <ul class="pagination sect-pagination-request pagination-sm no-margin pull-right"></ul>
		               </div>
		            </div>
	      		</div>
	      		<div class="modal-footer">
	              	<button type="submit" class="btn btn-primary"><i class="fa fa-share"></i> Sharing</button>
	             </div>
	   		</form>
			</div>
		</div>
	</div>

	<script type="text/html" id="files-template">
	   <li class="media">
	     <div class="media-body mb-1">
	       <h6 style="margin-top: 0;margin-bottom: 5px;">
	         <strong>%%filename%%</strong> <span class="text-muted">Waiting</span>
	       </h6>
	       <div class="progress" style="margin-bottom: 0">
	         <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" 
	           role="progressbar"
	           style="width: 0%" 
	           aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
	         </div>
	       </div>
	     </div>
	   </li>
	 </script>

</div>

<script type="text/javascript">

	_first_load = '<?php echo $first_load ?>';
	_uniqid = '<?php echo $_uniqid ?>';
	_role_id = '<?php echo $role_id ?>';

   $(function () {

		_dir = '';
		_dir_active_name = '';
		_order = 'clean_time';
		_orderby = 'desc';

		_offset_req = 0;
		_offset_req_files = 0;
		_offset_opd = 0;
		_curpage_req = 1;
		_curpage_req_files = 1;
		_curpage_opd = 1;

		arrfiles = [];
		fav_id = [];

		$.fn.reset_btn_action = function(){
			$('.option_check').each(function(){
	         this.checked = false; 
	      });
	      $('.option_choose').prop('checked', false);
			arrfiles = [];
			$('.row-options-selected').addClass('hidden');
		}

		$.fn.getting = function(option){
	      var param = $.extend({
	         dir : _dir,
	         first_load : _first_load,
	         order : _order,
	         orderby : _orderby,
	      }, option);
	      
	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'document/dms/read_dir',
	         dataType : "JSON",
	         data : {
	            dir : param.dir,
	            order : param.order,
	            orderby : param.orderby
	         },
	         beforeSend: function (xhr) {
	         	$(this).reset_btn_action();
	         	if(!param.first_load){
	         		spinner('#cont-widget-dms', 'block');
	         	}
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	         },
	         success : function(r){
	            var t = '';
               _dir_active_name = r.active_dir_name;
	            if(r.result){
                  var total = r.result.total;
                  $.each(r.result.data, function(k,v){
                  	//init metadata
                  	t += '<input id="name-'+k+'" type="hidden" value="'+k+'">';
       					t += '<input id="metaname-'+k+'" type="hidden" value="'+v.name+'">';
       					t += '<input id="metanameencode-'+k+'" type="hidden" value="'+v.url+'">';
       					if(param.dir){
       						t += '<input id="metaternary-'+k+'" type="hidden" value="'+param.dir+'/'+v.url+'">';
       					}else{
       						t += '<input id="metaternary-'+k+'" type="hidden" value="'+v.url+'">';
       					}
       					t += '<input id="metaext-'+k+'" type="hidden" value="'+(v.ext_orig ? v.ext_orig : '')+'">';
       					t += '<input id="metalock-'+k+'" type="hidden" value="'+(v.lock ? v.lock : '')+'">';
       					t += '<input id="metashare-'+k+'" type="hidden" value="'+(v.share ? v.share : '')+'">';
       					t += '<input id="metafav-'+k+'" type="hidden" value="'+(v.favorites ? v.favorites : '')+'">';

       					fav = v.favorites ? v.favorites : '';
 							share = v.share ? v.share : '';
 							lock = v.lock ? v.lock : '';

       					if(param.dir){
       						t += '<input id="metadir-'+k+'" type="hidden" value="'+r.dir_base+'/'+param.dir+'">';
       					}else{
       						t += '<input id="metadir-'+k+'" type="hidden" value="'+r.dir_base+'">';
       					}

                  	t += '<tr>';
                  		t += '<td><input type="checkbox" class="option_check" value="'+k+'" name="request_id[]"></td>';
                  		t += '<td class="mailbox-name">';
	          					if(param.dir){
	          						if(v.is_file){
	          							t += '<a href="'+base_url+'main/download?name='+v.url+'&dir='+r.dir_base+'/'+param.dir+'/'+v.url+'" data-id="'+k+'" style="cursor:pointer;">';
	          						}else{
	          							t += '<a data-href="'+param.dir+'/'+v.url+'" style="cursor:pointer;" class="change-dir">';
	          						}
	          					}else{
	          						if(v.is_file){
	          							t += '<a href="'+base_url+'main/download?name='+v.name+'&dir='+r.dir_base+'/'+v.url+'" data-id="'+k+'" style="cursor:pointer;">';
	          						}else{
	          							t += '<a data-href="'+v.url+'" style="cursor:pointer;" class="change-dir">';
	          						}
	          					}
	          					if(!v.is_file){
	          						t += '<i class="fa fa-folder"></i>&nbsp;';
	          					}else{
	          						if(v.ext){
	          							t += '<i class="'+v.ext+'" style="font-size:19px"></i>&nbsp;';
	          						}
	          					}
	          					t += v.name
	          					t += '</a>';
	          				t += '</td>';
	          				t += '<td class="mailbox-star">';
	          				if(v.favorites){
	          					t += '&nbsp;<i class="fa fa-star text-yellow" data-toggle="tooltip" data-title="Favorites"></i>';
	          				}
	          				if(v.lock){
	          					t += '&nbsp;<i class="fa fa-lock text-danger" data-toggle="tooltip" data-title="Locked document"></i>';
	          				}
	          				if(v.share){
	          					t += '&nbsp;<i class="fa fa-share-alt text-info" data-toggle="tooltip" data-title="'+v.share+'"></i>';
	          				}
	          				t += '</td>';
	          				t += '<td class="mailbox-attachment">';
	          					t += '<span class="dropdown dropdown-'+k+'">';
	       				 			t += '<a href="" lass="dropdown-toggle show-options " data-toggle="dropdown">';
	       				 				t += '<i class="fa fa-ellipsis-h"></i>';
	       				 			t += '</a>';
	       				 			t += '<ul class="dropdown-menu" style="left: -138px;border: 1px solid #cccccc;">';
	       				 				// if(v.is_file){
	       				 				// 	if(inArray(5, _role_id)){
	       				 				// 		t += '<li>';
	       				 				// 			t += '<a data-id="'+k+'" class="assign-request dropdown-options-dms" data-toggle="modal" data-target="#modal-assign" href="">';
	       				 				// 				t += '<i class="fa fa-share"></i>Share to request';
	       				 				// 			t += '</a>';
	       				 				// 		t += '</li>';
	       				 				// 		t += '<li class="divider"></li>';
	       				 				// 	}
	       				 				// }

	       				 				if(!v.favorites){
       				 						t += '<li>';
       				 							t += '<a data-st="3" data-id="'+k+'" class="assign-flag dropdown-options-dms" href="">';
       				 								t += '<i class="fa fa-star"></i>Add to favorit';
       				 							t += '</a>';
       				 						t += '</li>';		       				 			
    				 						}else{
    				 							t += '<li>';
    				 								t += '<a data-st="3" data-id="'+v.favorites+'" class="remove-flag text-danger dropdown-options-dms" href="">';
    				 									t += '<i class="fa fa-star"></i><span class="text-danger">Remove favorites</span>';
    				 								t += '</a>';
    				 							t += '</li>';
    				 						}
       				 					if(!v.lock){
       				 						t += '<li class="divider"></li>';
       				 						t += '<li>';
       				 							t += '<a data-st="1" data-id="'+k+'" class="assign-flag dropdown-options-dms" href="">';
       				 								t += '<i class="fa fa-lock"></i>Lock';
       				 							t += '</a>';
       				 						t += '</li>';
       				 					}else{
       				 						t += '<li class="divider"></li>';
       				 						t += '<li>';
       				 							t += '<a data-st="1" data-id="'+v.lock+'" class="remove-flag text-danger dropdown-options-dms" href="">';
       				 								t += '<i class="fa fa-lock"></i><span class="text-danger">Remove lock</span>';
       				 							t += '</a>';
       				 						t += '</li>';
       				 					}

       				 					if(!v.share){
       				 						t += '<li class="divider"></li>';
       				 						t += '<li>';
       				 							t += '<a data-st="2" data-id="'+k+'" class="assign-flag dropdown-options-dms" href="">';
       				 								t += '<i class="fa fa-share-alt"></i>Share';
       				 							t += '</a>';
    				 							t += '</li>';
    				 						}else{
    				 							t += '<li class="divider"></li>';
    				 							t += '<li>';
    				 								t += '<a data-st="2" data-id="'+v.share+'" class="remove-flag text-danger dropdown-options-dms" href="">';
    				 									t += '<i class="fa fa-share-alt"></i><span class="text-danger">Remove share</span>';
    				 								t += '</a>';
    				 							t += '</li>';
    				 						}
    				 						if(v.is_file){
       				 						t += '<li class="divider"></li>';
       				 						t += '<li>';
	       				 						t += '<a data-fav="'+fav+'" data-share="'+share+'" data-lock="'+lock+'" data-id="'+k+'" class="delete-file text-danger dropdown-options-dms" href="">';
	       				 							t += '<i class="fa fa-trash"></i>Delete File';
	       				 						t += '</a>';
       				 						t += '</li>';
    				 						}else{
    				 							t += '<li class="divider"></li>';
       				 						t += '<li>';
	       				 						t += '<a data-fav="'+fav+'" data-share="'+share+'" data-lock="'+lock+'" data-id="'+k+'" class="delete-folder text-danger dropdown-options-dms" href="">';
	       				 							t += '<i class="fa fa-trash"></i>Delete Folder';
	       				 						t += '</a>';
       				 						t += '</li>';
    				 						}
	       				 			t += '</ul>';
       				 			t += '</span>';
	       				 	t += '</td>';
	                    	t += '<td class="mailbox-date">'+v.time+'</td>';
	                    	t += '<td class="mailbox-size">'+v.size+'</td>';
                    	t += '</tr>';
                  });
	            }else{
	            	t += '<tr><td class="text-center text-muted" colspan="10">No Result</td></tr>';
	            }
	            $('#'+_uniqid).find('.sect-data').html(t);
	            $('#'+_uniqid).find('.sect-nav-render').html('');
	            $('#'+_uniqid).find('.sect-nav-render').html(r.nav);
	            // $('.request-rating').rating({
	            // 	displayOnly: true
	            // });
	         },
	         complete : function(r){
	      		spinner('#cont-widget-dms', 'unblock');
	      		_first_load = false;
      		}
      	});
   	}

   	$.fn.request = function(option){
	      var param = $.extend({
	         filt_keyword : $('#filt_keyword_files').val(),
	         limit : 10,
	         offset : _offset_req_files, 
	         currentPage : _curpage_req_files,
	         order : 'a.id', 
	         orderby : 'desc'
	      }, option);
	      
	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'document/dms/request',
	         dataType : "JSON",
	         data : {
	            offset : param.offset,
	            filt_keyword : param.filt_keyword,
	            limit : param.limit,
	            order : param.order,
	            orderby : param.orderby
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	         },
	         success : function(r){
	            var t = '';
            	if(r.total > 0){
                  var total = r.total;
                  $.each(r.result, function(k,v){
                  	t += '<tr>';
                  		t += '<td><input type="checkbox" class="request_id_files" value="'+v.id+'" name="request_id_files[]"></td>';
                  		t += '<td>';
                  			t += v.requested_date+'<br>'+v.requested_time;
                  		t += '</td>';
                  		t += '<td>'+v.title+'</td>';
                  		t += '<td>'+v.start_date+'</td>';
                  		t += '<td>'+v.end_date+'</td>';
                  		t += '<td>';
                  			t += v.limit_date;
                  			t += '<br><span style="font-size:10px;font-weight:bold">'+v.days_left+' Days Left</span>';
                  		t += '</td>';
                  		t += '<td>'+v.status_label+'</td>';
                  		t += '<td>'+(v.shared ? '<i class="fa fa-check"></i>' : '')+'</td>';
                  	t += '</tr>';
                  });
               }else{
            		t += '<tr><td class="text-center text-muted" colspan="8">Data tidak ditemukan</td></tr>';
               }

	            $('#'+_uniqid).find('.sect-total-request-files').html(''+number_format(total)+' Rows Data');
	            $('#'+_uniqid).find('.sect-data-request-files').html(t);

	            $('#'+_uniqid+' .sect-pagination-request-files').pagination({
			         items: r.total,
			         itemsOnPage: param.limit,
			         edges: 0,
			         hrefTextPrefix: '',
			         displayedPages: 1,
			         currentPage : param.currentPage,
			         prevText : '&laquo;',
			         nextText : '&raquo;',
			         dropdown: true,
			         onPageClick : function(n,e){
			            e.preventDefault();
			            _offset_req_files = (n-1)*param.limit;
			            _curpage_req_files = n;
			            $(this).request();
			         }
			      });
	         }
	      });
	   }

	   $.fn.opd = function(option){
	      var param = $.extend({
	         filt_keyword : $('#keyword_opd').val(),
	         offset : _offset_opd, 
	         currentPage : _curpage_opd
	      }, option);
	      
	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'document/dms/opd',
	         dataType : "JSON",
	         data : {
	            offset : param.offset,
	            filt_keyword : param.filt_keyword
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	         },
	         success : function(r){
	            var t = '';
            	if(r.total > 0){
                  var total = r.total;
                  $.each(r.result, function(k,v){
                  	t += '<tr>';
                  		t += '<td><input type="checkbox" class="check_opd" value="'+v.id+'" name="opd_id[]"></td>';
                  		t += '<td>'+v.name+'</td>';
                  		t += '<td>'+(v.phone ? v.phone : '')+'</td>';
                  		t += '<td>'+(v.description ? v.description : '')+'</td>';
                  	t += '</tr>';
                  });
               }else{
            		t += '<tr><td class="text-center text-muted" colspan="8">Data tidak ditemukan</td></tr>';
               }

	            $('#'+_uniqid).find('.sect-total-share-opd').html(''+number_format(total)+' Rows Data');
	            $('#'+_uniqid).find('.sect-data-share-opd').html(t);

	            $('#'+_uniqid+' .sect-pagination-share-opd').pagination({
			         items: r.total,
			         itemsOnPage: param.limit,
			         edges: 0,
			         hrefTextPrefix: '',
			         displayedPages: 1,
			         currentPage : param.currentPage,
			         prevText : '&laquo;',
			         nextText : '&raquo;',
			         dropdown: true,
			         onPageClick : function(n,e){
			            e.preventDefault();
			            _offset_opd = (n-1)*param.limit;
			            _curpage_opd = n;
			            $(this).opd();
			         }
			      });
	         }
	      });
	   }


   	$.fn.getting_request = function(option){
	      var param = $.extend({
	         filt_keyword : $('#filt_keyword').val(),
	         // filt_status : $('#filt_status').val(),
	         dir : null,
	         filename : null,
	         limit : 10,
	         offset : _offset_req, 
	         currentPage : _curpage_req,
	         order : 'a.id', 
	         orderby : 'desc'
	      }, option);
	      
	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'document/dms/getting_request',
	         dataType : "JSON",
	         data : {
	            offset : param.offset,
	            dir : param.dir,
	            filename : param.filename,
	            // filt_status : param.filt_status,
	            filt_keyword : param.filt_keyword,
	            limit : param.limit,
	            order : param.order,
	            orderby : param.orderby
	         },
	         beforeSend: function (xhr) {
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	         },
	         success : function(r){
	            var t = '';
            	if(r.total > 0){
                  var total = r.total;
                  $.each(r.result, function(k,v){
                  	t += '<tr>';
                  		if(v.shared){
                  			t += '<td></td>';
                  		}else{
                  			t += '<td><input type="checkbox" class="option_id" value="'+v.id+'" name="request_id[]"></td>';
                  		}
                  		t += '<td>';
                  			t += v.requested_date+'<br>'+v.requested_time;
                  		t += '</td>';
                  		t += '<td>'+v.title+'</td>';
                  		t += '<td>'+v.start_date+'</td>';
                  		t += '<td>'+v.end_date+'</td>';
                  		t += '<td>';
                  			t += v.limit_date;
                  			t += '<br><span style="font-size:10px;font-weight:bold">'+v.days_left+' Days Left</span>';
                  		t += '</td>';
                  		t += '<td>'+v.status_label+'</td>';
                  		t += '<td>'+(v.shared ? '<i class="fa fa-check"></i>' : '')+'</td>';
                  	t += '</tr>';
                  });
               }else{
            		t += '<tr><td class="text-center text-muted" colspan="8">Data tidak ditemukan</td></tr>';
               }

	            $('#'+_uniqid).find('.sect-total-request').html(''+number_format(total)+' Rows Data');
	            $('#'+_uniqid).find('.sect-data-request').html(t);

	            $('#'+_uniqid+' .sect-pagination-request').pagination({
			         items: r.total,
			         itemsOnPage: param.limit,
			         edges: 0,
			         hrefTextPrefix: '',
			         displayedPages: 1,
			         currentPage : param.currentPage,
			         prevText : '&laquo;',
			         nextText : '&raquo;',
			         dropdown: true,
			         onPageClick : function(n,e){
			            e.preventDefault();
			            _offset_req = (n-1)*param.limit;
			            _curpage_req = n;
			            var dir = $('.form-assign').find('input[name="dir"]').val();
	   					var name = $('.form-assign').find('input[name="filename"]').val();
			            $(this).getting_request();
			         }
			      });
	         },
	         complete : function(r){
	         	
         	}
	      });
	   }

	   $(this).on('click', '#'+_uniqid+' .option_check',function(e){
	   	var id = $(this).val();
   	 	name = $('#metanameencode-'+id+'').val();
   	 	namenoencode = $('#metaname-'+id+'').val();
			dir = $('#metadir-'+id+'').val();
			ext = $('#metaext-'+id+'').val();
			ternary = $('#metaternary-'+id+'').val();
			fav = $('#metafav-'+id+'').val();

   		if($(this).is(':checked')){
   			if($('.row-options-selected').hasClass('hidden')){
   				$('.row-options-selected').removeClass('hidden');
   			}
   	  		arrfiles.push({
	            id: id, 
	            name: name, 
	            namenoencode: namenoencode,
	            dir:  dir,
	            ternary:  ternary,
	            ext:  (ext ? ext : 'folder')
        		});

        		if(fav !== ''){
        			fav_id.push({
	            	id: fav 
            	});
        		}

	    	} else {
				arrfiles = arrfiles.filter(function(elem) {  
				  return elem.id !== id; 
				}); 
				fav_id = fav_id.filter(function(elem) {  
				  return elem.id !== id; 
				}); 
	    	}
	    	if(arrfiles.length == 0){
	    		$('.row-options-selected').addClass('hidden');
	    	}
	    	console.log(arrfiles);
	   });

	   $(this).on('click', '#'+_uniqid+' .btn-filter',function(e){
	   	_offset_req = 0;
	   	_curpage_req = 1;
	   	var dir = $('.form-assign').find('input[name="dir"]').val();
	   	var name = $('.form-assign').find('input[name="filename"]').val();
	   	$(this).getting_request({
	   		dir : dir,
	   		filename : name
	   	});
	   });

	   $(this).on('click', '#'+_uniqid+' .btn-filter-files',function(e){
	   	_offset_req_files = 0;
	   	_curpage_req_files = 1;
	   	$(this).request();
	   });

	   $(this).on('click', '#'+_uniqid+' .btn-filter-opd',function(e){
	   	_offset_opd = 0;
	   	_curpage_opd = 1;
	   	$(this).opd();
	   });

    	$(this).on('click', '#'+_uniqid+' .option_choose',function(e){
        	if(this.checked){
        		arrfiles = [];
            $('.option_check').each(function(){
              	var id = $(this).val();
		   	 	name = $('#metanameencode-'+id+'').val();
		   	 	namenoencode = $('#metaname-'+id+'').val();
					dir = $('#metadir-'+id+'').val();
					ext = $('#metaext-'+id+'').val();
					ternary = $('#metaternary-'+id+'').val();

					this.checked = true; 

					arrfiles.push({
		            id: id, 
		            name: name, 
		            namenoencode: namenoencode,
		            dir:  dir,
		            ternary:  ternary,
		            ext:  (ext ? ext : 'folder')
	        		});
            });
            $('.row-options-selected').removeClass('hidden');
        }else{
        		$('.row-options-selected').addClass('hidden');
            $('.option_check').each(function(){
               this.checked = false; 
            });
        }    
    	});

	   $(this).on('submit', '#'+_uniqid+' .form-assign-files', function(e){
	      var form = $(this);
	      var data = [];

	      $('.request_id_files').each(function(){
            if(this.checked){
              	data.push($(this).val());
         	}
        	});
	      if(data.length > 0){
	      	$(this).ajaxSubmit({
		         url : site_url + 'document/dms/assign_request_files',
		         type : "POST",
		         data : {
		         	files : arrfiles
		         },
		         dataType : "JSON",
		         error: function (jqXHR, status, errorThrown) {
		            error_handle(jqXHR, status, errorThrown);
		            loading_form(form, 'hide', 'Lampirkan');
		         },
		         beforeSend: function (xhr) {
		            loading_form(form, 'show', loadingbutton);
		         },
		         success: function(r) {
		         	session_checked(r._session, r._maintenance);
		            if(r.success){
		               $('#modal-assign-files').modal('hide');
		               $(this).reset_btn_action();
		               toastr.success(r.msg);
		            }else{
		            	toastr.error(r.msg);
		            }
		         },
		         complete: function(){
						loading_form(form, 'hide', 'Lampirkan');
		         }
		      });
	      }else{
	      	alert('tidak ada request yang ditandai');
	      }
	      e.preventDefault();
	   });

	   $(this).on('submit', '#'+_uniqid+' .form-share-doc', function(e){
	      var form = $(this);
	      var data = [];

	      $('.check_opd').each(function(){
            if(this.checked){
              	data.push($(this).val());
         	}
        	});
	      if(data.length > 0){
	      	$(this).ajaxSubmit({
		         url : site_url + 'document/dms/share_opd',
		         type : "POST",
		         data : {
		         	files : arrfiles
		         },
		         dataType : "JSON",
		         error: function (jqXHR, status, errorThrown) {
		            error_handle(jqXHR, status, errorThrown);
		            loading_form(form, 'hide', 'Share');
		         },
		         beforeSend: function (xhr) {
		            loading_form(form, 'show', loadingbutton);
		         },
		         success: function(r) {
		         	session_checked(r._session, r._maintenance);
		            if(r.success){
		               $('#modal-share-doc').modal('hide');
		               $(this).getting();
		               $(this).reset_btn_action();
		               toastr.success(r.msg);
		            }else{
		            	toastr.error(r.msg);
		            }
		         },
		         complete: function(){
						loading_form(form, 'hide', 'Share');
		         }
		      });
	      }else{
	      	alert('tidak ada opd yang ditandai');
	      }
	      e.preventDefault();
	   });

	   $(this).on('submit', '#'+_uniqid+' .form-assign', function(e){
	      var form = $(this);
	      var req_id = form.find("input[name='request_id[]']").map(function(){return $(this).val();}).get();
	      if(req_id.length > 0){
	      	$(this).ajaxSubmit({
		         url : site_url + 'document/dms/assign_request_file',
		         type : "POST",
		         dataType : "JSON",
		         error: function (jqXHR, status, errorThrown) {
		            error_handle(jqXHR, status, errorThrown);
		            loading_form(form, 'hide', 'Lampirkan');
		         },
		         beforeSend: function (xhr) {
		         	$(this).reset_btn_action();
		            loading_form(form, 'show', loadingbutton);
		         },
		         success: function(r) {
		         	session_checked(r._session, r._maintenance);
		            if(r.success){
		            	var dir = $('.form-assign').find('input[name="dir"]').val();
	   					var name = $('.form-assign').find('input[name="filename"]').val();
	   					$('.row-options-selected').addClass('hidden');
		               $(this).getting_request({
		               	dir : dir,
		               	filename : name
		               });
		               toastr.success(r.msg);
		            }else{
		            	toastr.error(r.msg);
		            }
		         },
		         complete: function(){
						loading_form(form, 'hide', 'Lampirkan');
		         }
		      });
	      }else{
	      	alert('tidak ada request yang ditandai');
	      }
	      e.preventDefault();
	   });

   	$(this).on('click', '#'+_uniqid+' .assign-request',function(e){
	      e.preventDefault();
	      var id = $(this).data('id');
	      name = $('#metanameencode-'+id+'').val();
			dir = $('#metadir-'+id+'').val();
			ext = $('#metaext-'+id+'').val();
			$('.form-assign').find('#filt_keyword').val('');
			$('.form-assign').find('#filt_status').val('all');

			$('.form-assign').find('input[name="dir"]').val(dir);
			$('.form-assign').find('input[name="filename"]').val(name);
			$('.form-assign').find('input[name="ext"]').val(ext);
	      $(this).getting_request({
	      	dir : dir,
	      	filename : name
	      });
	   });

	   $(this).on('click', '#'+_uniqid+' .assign-request-files',function(e){
	      e.preventDefault();
	      var id = $(this).data('id');
			$('#filt_keyword_files').val('');
	      $('#modal-assign-files').modal('show');
	      $(this).request();
	   });

	   $(this).on('click', '#'+_uniqid+' .share-doc',function(e){
	      e.preventDefault();
	      var id = $(this).data('id');
			$('#keyword_opd').val('');
	      $('#modal-share-doc').modal('show');
	      $(this).opd();
	   });

   	$(this).on('click', '#'+_uniqid+' .change-dir',function(e){
	      e.preventDefault();
	      _dir = $(this).attr('data-href');
	      $(this).getting();
	   });

   	$(this).on('click', '#'+_uniqid+' .breadcrumb-action',function(e){
         e.preventDefault();
         _dir = $(this).attr('data-href');
         $(this).getting();
      });

	   $(this).on('click', '#'+_uniqid+' .breadcrumb-action-home', function(e){
	   	e.preventDefault();
	      _dir = '';
	      $(this).getting();
	   });

	   $(this).on('click', '#'+_uniqid+' .remove-flag',function(e){
			e.preventDefault();
			id = $(this).attr('data-id');
			status = $(this).attr('data-st');
			if(id != ''){
			 	ajaxManager.addReq({
		         type : "GET",
		         url : site_url + 'document/dms/remove_flag',
		         dataType : "JSON",
		         data : {
		            id : id,
		            status : status
		         },
		         error: function (jqXHR, status, errorThrown) {
		            error_handle(jqXHR, status, errorThrown); 
		         },
		         success : function(r){
		         	$(this).reset_btn_action();
		         	session_checked(r._session, r._maintenance);
		            if(r.success){
		               $(this).getting();
		            }else{
		            	toastr.error(r.msg);
		            }
	         	}
	      	});
	   	}else{
	   		alert('ID Not Found');
	   	}
		});

	   $(this).on('click', '#'+_uniqid+' .delete-file',function(e){
			e.preventDefault();
			id = $(this).attr('data-id');
			fav = $(this).attr('data-fav');
			lock = $(this).attr('data-lock');
			share = $(this).attr('data-share');

			name = $('#metanameencode-'+id+'').val();
			dir = $('#metadir-'+id+'').val();
		 	ajaxManager.addReq({
	         type : "POST",
	         url : site_url + 'document/dms/delete_file',
	         dataType : "JSON",
	         data : {
	            name : name,
	            fav : fav,
	            lock : lock,
	            share : share,
	            dir : dir
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	         },
	         success : function(r){
	         	$(this).reset_btn_action();
	         	session_checked(r._session, r._maintenance);
	            if(r.success){
	               $(this).getting();
	               toastr.success(r.msg);
	            }else{
	            	toastr.error(r.msg);
	            }
	      	}
	   	});
		});

	   $(this).on('submit', '#'+_uniqid+' .form-upload-folder', function(e){
	      var form = $(this);
	      $(this).ajaxSubmit({
	         url : site_url + 'document/dms/upload_folder',
	         type : "POST",
	         data : {
	            dir : _dir
	         },
	         dataType : "JSON",
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown);
	            loading_form(form, 'hide', '<i class="fa fa-upload"></i> Upload');
	         },
	         beforeSend: function (xhr) {
	            loading_form(form, 'show', loadingbutton);
	            $(this).reset_btn_action();
	         },
	         success: function(r) {
	            session_checked(r._session, r._maintenance);
	            if(r.success){                
	               $(this).getting();
	               $("#modal-upload-folder").modal("toggle");
	               $('.row-options-selected').addClass('hidden');
	               form.resetForm();
	            }else{
	               toastr.error(r.msg);
	            }
	            loading_form(form, 'hide', '<i class="fa fa-upload"></i> Upload');
	         },
	      });
	      e.preventDefault();
	   });

	   $(this).on('click', '#'+_uniqid+' .add-more-folder',function(e){
	      e.preventDefault();
	      var total = $("#modal-upload-folder .modal-body > .form-group").length;
	      if(total < 5){
	         t = '';
	         t += '<div class="form-group form-group-added">';
	         t += '<input type="text" class="form-control input-sm" name="folder_name[]" placeholder="Nama folder..." required="">';
	         t += '</div>';
	         $('#modal-upload-folder .modal-body').prepend(t);
	      }else{
	         alert('Maksimal 5 folder setiap submit');
	      }
	   });

		$(this).on('click', '#'+_uniqid+' .delete-folder',function(e){
			e.preventDefault();
			id = $(this).attr('data-id');
			fav = $(this).attr('data-fav');
			lock = $(this).attr('data-lock');
			share = $(this).attr('data-share');
			name = $('#metanameencode-'+id+'').val();
			dir = $('#metadir-'+id+'').val();
		 	ajaxManager.addReq({
	         type : "POST",
	         url : site_url + 'document/dms/delete_folder',
	         dataType : "JSON",
	         data : {
	            name : name,
             	fav : fav,
	            lock : lock,
	            share : share,
	            dir : dir
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	         },
	         success : function(r){
	         	$(this).reset_btn_action();
	         	session_checked(r._session, r._maintenance);
	            if(r.success){
	               $(this).getting();
	               toastr.success(r.msg);
	            }else{
	            	toastr.error(r.msg);
	            }
	      	}
	   	});
		});

	   $(this).on('click', '#'+_uniqid+' .assign-flag',function(e){
			e.preventDefault();
			id = $(this).attr('data-id');
			stat = $(this).attr('data-st');
			name = $('#metaname-'+id+'').val();
			ternary = $('#metaternary-'+id+'').val();
			dir = $('#metadir-'+id+'').val();
			ext = $('#metaext-'+id+'').val();
		 	ajaxManager.addReq({
	         type : "POST",
	         url : site_url + 'document/dms/assign_flag',
	         dataType : "JSON",
	         data : {
	            name : name,
	            dir : dir,
	            ternary : ternary,
	            status : stat,
	            ext : ext
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	         },
	         success : function(r){
	         	$(this).reset_btn_action();
	         	session_checked(r._session, r._maintenance);
	            if(r.success){
	               $(this).getting();
	               $('.row-options-selected').addClass('hidden');
	               $('.dropdown-'+id+'').removeClass('open');
	            }else{
	            	toastr.error(r.msg);
	            }
	      	}
	   	});
		});

		$(this).on('click', '#'+_uniqid+' .assign-flag-bulk',function(e){
			e.preventDefault();
			stat = $(this).attr('data-st');
		 	ajaxManager.addReq({
	         type : "POST",
	         url : site_url + 'document/dms/assign_flag_bulk',
	         dataType : "JSON",
	         data : {
	            files : arrfiles,
	            status : stat
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	         },
	         success : function(r){
	         	$(this).reset_btn_action();
	         	session_checked(r._session, r._maintenance);
	            if(r.success){
	               $(this).getting();
	            }else{
	            	toastr.error(r.msg);
	            }
	      	}
	   	});
		});

		$(this).on('click', '#'+_uniqid+' .download-bulk',function(e){
			e.preventDefault();
			stat = $(this).attr('data-st');
		 	ajaxManager.addReq({
	         type : "POST",
	         url : site_url + 'document/dms/zipped_bulk',
	         dataType : "JSON",
	         data : {
	            files : arrfiles,
	            filename : _dir_active_name
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	         },
	         success : function(r){
	         	session_checked(r._session, r._maintenance);
	         	window.location.href = 'document/dms/download_bulk?name='+r.title+'&dir=download/'+r.filename+'.zip';
	      	}
	   	});
		});

	   $(this).on('click', '#'+_uniqid+' .change_order', function(e){
	      e.preventDefault();
	      $('.change_order').html('<i class="fa fa-sort"></i>');
	      $(this).find('i').remove();
	      var sent = $(this).data('order');
	      var by = $(this).attr('data-by');
	      if(by === 'asc'){ 
	         $(this).attr('data-by', 'desc');
	         $(this).html('<i class="fa fa-sort-asc"></i>');
	      }
	      else{ 
	         $(this).attr('data-by', 'asc');
	         $(this).html(' <i class="fa fa-sort-desc"></i>');
	      }
	      _order = sent;
	      _orderby = by;
	     $(this).getting();
	   });

   	$(this).getting();

	});
</script>

<script src="<?php echo $path_scripts ?>/document/dms/getting.js?<?php echo time() ?>"></script>
<script src="<?php echo $path_scripts ?>/document/dms/uploader_file.js?<?php echo time() ?>"></script>
<script src="<?php echo $path_scripts ?>/document/dms/action_selected.js?<?php echo time() ?>"></script>