<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Dms extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
      $this->load->library('global_mapping');
      $this->load->library('date_extraction');
      $this->load->library('dir_extraction');
      $this->load->model('document_tags');
      if(in_array(1, $this->_role_id)){
         $this->_role_id = 1;
         $this->render_page($data, 'superadmin', 'modular');
      }else if(in_array(2, $this->_role_id)){
         $this->_role_id = 2;
         $this->_path = 'dms/apd/'.$this->_user->id;
      }else if(in_array(3, $this->_role_id)){
         $this->_role_id = 3;
         $this->_path = 'dms/kadis/'.$this->_user->id;
      }else if(in_array(5, $this->_role_id)){
         $this->_role_id = 5;
         $this->_path = 'dms/pedasi/'.$this->_user->id;
      }else if(in_array(4, $this->_role_id)){
         $this->_role_id = 4;
         $this->_path = 'dms/kasie/'.$this->_user->id;
      }
	}

	public function index(){
      $data['role_id'] = $this->_role_id;
      $data['path_scripts'] = $this->_path_scripts;
      $data['first_load'] = $this->_get['first_load'] ? $this->_get['first_load'] : FALSE;
		$response['html'] = $this->load->view('dms', $data, TRUE);
      $this->json_result($response);
	}

   public function read_dir(){
      $response['result'] = $this->list_folder_files($this->_get);
      $response['dir_base'] = $this->_path;
      $response['dir_clean'] = urldecode($this->_get['dir']);
      if($response['dir_clean'] != ''){
         $exp = explode('/', $response['dir_clean']);
         $response['active_dir_name'] = end($exp);
      }else{
         $response['active_dir_name'] = date('Y-m-d H:i:s');
      }

      $nav = '';
      $nav .= '<li><a data-href="" class="breadcrumb-action-home" style="cursor:pointer;"><i class="fa fa-home"></i></a></li>';
      $uri_string = explode('/', urldecode($this->_get['dir']));
      $uri = array();
      foreach (array_filter($uri_string) as $rs) {
         $uri[] = urlencode($rs);
         $nav .= '<li><a data-href="'.implode('/', $uri).'" class="breadcrumb-action">'.$rs.'</a></li>';
      }
      $response['nav'] = $nav;
      $this->json_result($response);
   }

   public function request(){
      $data = $this->document_tags->request('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'status' => $v['status'],
               'status_label' => $this->global_mapping->request_status($v['status'], 'label-status-table '),
               'title' => $v['title'],
               'start_date' => date('d M Y', strtotime($v['start_date'])),
               'end_date' => date('d M Y', strtotime($v['end_date'])),
               'limit_date' => date('d M Y', strtotime($v['limit_date'])),
               'days_left' => $v['limit_date'] ? $this->date_extraction->left_days_from_now($v['limit_date']) : NULL,
               'requested_date' => date('d M Y', strtotime($v['requested_date'])),
               'requested_time' => date('H:i:s', strtotime($v['requested_date']))
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->document_tags->request('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

   public function opd(){
      $data = $this->document_tags->opd('get', $this->_get);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'name' => $v['name'],
               'phone' => $v['phone'],
               'description' => $v['description']
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->document_tags->opd('count', $this->_get);
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

   public function getting_request(){
      $data = $this->document_tags->getting_request('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'status' => $v['status'],
               'status_label' => $this->global_mapping->request_status($v['status'], 'label-status-table '),
               'opd_id' => $v['opd_id'],
               'pedasi_name' => $v['pedasi_name'],
               'shared' => NULL,
               'title' => $v['title'],
               'start_date' => date('d M Y', strtotime($v['start_date'])),
               'end_date' => date('d M Y', strtotime($v['end_date'])),
               'limit_date' => date('d M Y', strtotime($v['limit_date'])),
               'req_name' => $v['req_name'],
               'days_left' => $v['limit_date'] ? $this->date_extraction->left_days_from_now($v['limit_date']) : NULL,
               'requested_date' => date('d M Y', strtotime($v['requested_date'])),
               'requested_time' => date('H:i:s', strtotime($v['requested_date']))
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->document_tags->getting_request('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

   public function assign_request_file(){
      $response['success'] = FALSE;
      $response['msg'] = 'Some error occured';
      $this->load->library('active_record_builder');

      if($this->_post['dir'] && $this->_post['filename']){
         $this->db->trans_start();
         foreach ($this->_post['request_id'] as $v) {
            $tmp = array(
               'req_id' => $v,
               'full_path' => $this->_post['dir'].'/'.$this->_post['filename'],
               'extension' => $this->_post['ext'],
               'file_name' => $this->_post['filename'],
               'created_date' => date('Y-m-d H:i:s'),
               'created_by' => $this->_user->id
            );
            $this->active_record_builder->on_duplicate('request_result_files', $tmp);
            $this->document_tags->pedasi_time($this->_post['request_id']);
         }
         $this->db->trans_complete();
         if($this->db->trans_status()){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'File berhasil di assign';
         }else{
            $this->db->trans_rollback();
         }
      }else{
         $response['msg'] = 'Entitas direktori tidak ditemukan';
      }
      $this->json_result($response);
   }

   public function assign_request_files(){
      $response['success'] = FALSE;
      $response['msg'] = 'Some error occured';
      $this->load->library('active_record_builder');
      $folderfound = array_keys(array_combine(array_keys($this->_post['files']), array_column($this->_post['files'], 'ext')),'folder');
      if(!$folderfound){
         $this->db->trans_start();
         foreach ($this->_post['request_id_files'] as $v) {
            foreach ($this->_post['files'] as $vv) {
               $tmp = array(
                  'req_id' => $v,
                  'full_path' => $vv['dir'].'/'.$vv['name'],
                  'extension' => $vv['ext'],
                  'file_name' => $vv['name'],
                  'created_date' => date('Y-m-d H:i:s'),
                  'created_by' => $this->_user->id
               );
               $this->active_record_builder->on_duplicate('request_result_files', $tmp);
            }
         }
         $this->db->trans_complete();
         if($this->db->trans_status()){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'File berhasil di lampirkan pada request terpilih';
         }else{
            $this->db->trans_rollback();
         }
      }else{
         $response['msg'] = 'Folder tidak bisa dilampirkan pada permohonan data';
      }
      $this->json_result($response);
   }

   public function share_opd(){
      $response['success'] = FALSE;
      $response['msg'] = 'Some error occured';
      $this->load->library('active_record_builder');

      $this->db->trans_start();
      foreach ($this->_post['opd_id'] as $v) {
         foreach ($this->_post['files'] as $vv) {
            $tmp['file_name'] = $vv['name'];
            $tmp['file_dir'] = $vv['dir'];
            if($vv['ext'] == 'folder'){
               $explode = explode('/', $vv['ternary']);
               if(count($explode) > 1){
                  $tmp['file_ternary'] = $vv['ternary'];
               }
            }else{
               $tmp['file_ternary'] = NULL;
               $tmp['file_extension'] = $vv['ext'];
            }
            $tmp['full_dir'] = $vv['dir'].'/'.$vv['name'];
            $tmp['opd_id'] = $v;
            $tmp['created_at'] = date('Y-m-d H:i:s');
            $tmp['created_by'] = $this->_user->id;
            $this->active_record_builder->on_duplicate('dms_shared_opd', $tmp);
         }
      }
      $this->db->trans_complete();
      if($this->db->trans_status()){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['msg'] = 'File/folder berhasil di share pada opd terpilih';
      }else{
         $this->db->trans_rollback();
      }
      $this->json_result($response);
   }

   public function list_folder_files($params){
      if($params['dir']){
         $dir = $this->_path.'/'.$params['dir'];
      }else{
         $dir = $this->_path;
      }

      $ffs = scandir(urldecode($dir));

      unset($ffs[array_search('.', $ffs, true)]);
      unset($ffs[array_search('..', $ffs, true)]);

      // prevent empty ordered elements
      if (count($ffs) < 1)
         return;

      $i = 0;
      $data = array();
      
      $lock_dms = $this->get_document_tags(1);
      $share_dms = $this->dms_shared_opd();
      $favorite_dms = $this->get_document_tags(3);

      foreach($ffs as $ff){
         $time = date('d M Y H:i:s', filectime(urldecode($dir).'/'.urldecode($ff)));
         $data[$i]['name'] = $ff;
         if(is_file(urldecode($dir).'/'.urldecode($ff))){
            $ext = pathinfo($dir.'/'.$ff, PATHINFO_EXTENSION);
            $data[$i]['ext_orig'] = $ext;
            $data[$i]['ext'] = $this->global_mapping->mapping_icon('.'.$ext.'', TRUE);
         }else{
            $data[$i]['ext_orig'] = NULL;
            $data[$i]['ext'] = NULL;
         }
         $data[$i]['base_dir'] = $dir;
         $data[$i]['url'] = urlencode($ff);
         $data[$i]['is_file'] = is_file(urldecode($dir).'/'.urldecode($ff));
         $data[$i]['time'] = $this->date_extraction->time_ago($time);
         $data[$i]['clean_time'] = $time;
         $data[$i]['size'] = $this->global_mapping->format_size_units(filesize(urldecode($dir).'/'.urldecode($ff)));
         $data[$i]['size_bytes'] = filesize(urldecode($dir).'/'.urldecode($ff));

         if($lock_dms){
            $cond_lock = $this->dir_extraction->in_array_r($dir.'/'.urlencode($ff), $lock_dms);
            if($cond_lock){
               $data[$i]['lock'] = $cond_lock['id'];
            }else{
               $data[$i]['lock'] = FALSE;
            }
         }

         if($share_dms){
            $cond_share = $this->dir_extraction->in_array_r($dir.'/'.urlencode($ff), $share_dms);
            if($cond_share){
               $data[$i]['share'] = $cond_share['opd_name'];
            }else{
               $data[$i]['share'] = FALSE;
            }
         }

         if($favorite_dms){
            $cond_favorite = $this->dir_extraction->in_array_r($dir.'/'.urlencode($ff), $favorite_dms);
            if($cond_favorite){
               $data[$i]['favorites'] = $cond_favorite['id'];
            }else{
               $data[$i]['favorites'] = FALSE;
            }
         }

         $i++;
      }
      if($data){
         if($params['orderby'] == 'desc'){
            usort($data, function($b, $a) use ($params) {
                return $a[$params['order']] <=> $b[$params['order']];
            });
         }
         if($params['orderby'] == 'asc'){
            usort($data, function($a, $b) use ($params) {
                return $a[$params['order']] <=> $b[$params['order']];
            });
         }
      }
      return array(
         'data' => $data,
         'params' => $params['order'],
         'total' => count($data)
      );
   }

   public function get_document_tags($status){
      $data = array();
      $this->db->where('created_by', $this->_user->id);
      $this->db->where('type_dms', $status);
      $result = $this->db->get('document_tags')->result_array();
      if($result){
         foreach ($result as $v) {
            if($v['file_dir'] && $v['file_name']){
               $data[$v['id']]['dir'] = $v['full_dir'];
               $data[$v['id']]['id'] = $v['id'];
            }
         }
         return $data;
      }else{
         return FALSE;
      }
   }

   public function dms_shared_opd(){
      $data = array();
      $this->db->select('a.*');
      $this->db->select('GROUP_CONCAT(b.name SEPARATOR ",") as opd_name');
      $this->db->join('opd as b', 'a.opd_id = b.id');
      $this->db->where('a.created_by', $this->_user->id);
      $this->db->group_by('a.full_dir');
      $result = $this->db->get('dms_shared_opd as a')->result_array();
      if($result){
         foreach ($result as $v) {
            if($v['file_dir'] && $v['file_name']){
               $data[$v['id']]['dir'] = urldecode($v['full_dir']);
               $data[$v['id']]['id'] = $v['id'];
               $data[$v['id']]['opd_name'] = $v['opd_name'];
            }
         }
         return $data;
      }else{
         return FALSE;
      }
   }

   public function assign_flag(){
      $response['success'] = FALSE;
      $response['msg'] = 'Function failed';

      $this->db->trans_start();
      $tmp['file_name'] = $this->_post['name'] ? $this->_post['name'] : NULL;
      $tmp['file_dir'] = $this->_post['dir'] ? urldecode($this->_post['dir']) : NULL;
      if($this->_post['dir'] && $this->_post['name']){
         $tmp['full_dir'] = urldecode($this->_post['dir']).'/'.urldecode($this->_post['name']);
      }
      if(!$this->_post['ext']){
         $explode = explode('/', $this->_post['ternary']);
         if(count($explode) > 1){
            $tmp['file_ternary'] = urldecode($this->_post['ternary']);   
         }
      }
      $tmp['file_extension'] = $this->_post['ext'] != 'folder' ? $this->_post['ext'] : NULL;
      $tmp['type_dms'] = $this->_post['status'];
      $tmp['created_by'] = $this->_user->id;
      $tmp['created_at'] = date('Y-m-d H:i:s');
      $this->db->insert('document_tags', $tmp);
      $this->db->trans_complete();

      if($this->db->trans_status()){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         if($this->_post['status'] == 1){
            $response['msg'] = 'File berhasil di lock';
         }
         if($this->_post['status'] == 2){
            $response['msg'] = 'File berhasil di share';
         }
         if($this->_post['status'] == 3){
            $response['msg'] = 'Favorite berhasil di tambahkan';
         }
      }else{
         $this->db->trans_rollback();
      }
      $this->json_result($response);
   }

   public function zipped_bulk(){
      $this->load->library('zip');
      $response['success'] = TRUE;
      $filename = uniqid();

      $files = glob('./download/*'); // get all file names
      foreach($files as $file){ // iterate files
        if(is_file($file)) {
          unlink($file); // delete file
        }
      }

      foreach ($this->_post['files'] as $k => $v) {
         if($v['ext'] == 'folder'){
            $this->zip->read_dir(urldecode($v['dir']).'/'.urldecode($v['namenoencode']), FALSE);
         }else{
            $this->zip->read_file(urldecode($v['dir']).'/'.urldecode($v['namenoencode']));
         }
      }
      $this->zip->archive('download/'.$filename.'.zip');
      chmod('download/'.$filename.'.zip', 0777);
      $response['filename'] = $filename;
      $response['title'] = $this->_post['filename'];
      $this->json_result($response);
   }

   public function download_bulk(){
      $this->load->helper('download');
      force_download(urldecode($this->_get['name']).'.zip', file_get_contents(urldecode($this->_get['dir'])), NULL); 
   }


   public function assign_flag_bulk(){
      $response['success'] = FALSE;
      $response['msg'] = 'Function failed';
      $this->load->library('active_record_builder');

      $this->db->trans_start();
      foreach ($this->_post['files'] as $k => $v) {
         $tmp['file_name'] = $v['name'] ? $v['name'] : NULL;
         $tmp['file_dir'] = $v['dir'] ? $v['dir'] : NULL;
         $tmp['file_extension'] = $v['ext'] !== 'folder' ? $v['ext'] : NULL;
         $tmp['type_dms'] = $this->_post['status'];
         $tmp['created_by'] = $this->_user->id;
         $tmp['created_at'] = date('Y-m-d H:i:s');
         
         if($v['dir'] && $v['name']){
            $tmp['full_dir'] = $v['dir'].'/'.$v['name'];
         }
         if($v['ext'] == 'folder'){
            $explode = explode('/', $v['ternary']);
            if(count($explode) > 1){
               $tmp['file_ternary'] = $v['ternary'];   
            }
         }
         $this->active_record_builder->on_duplicate('document_tags', $tmp);
      }
      $this->db->trans_complete();

      if($this->db->trans_status()){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['msg'] = 'Status file berhasil di perbaharui';
      }else{
         $this->db->trans_rollback();
      }
      $this->json_result($response);
   }

   public function upload_file(){
      try {
         if (
            !isset($_FILES['file']['error']) ||
            is_array($_FILES['file']['error'])
         ) {
            throw new RuntimeException('Invalid parameters.');
         }
         switch ($_FILES['file']['error']) {
            case UPLOAD_ERR_OK:
            break;
            case UPLOAD_ERR_NO_FILE:
               throw new RuntimeException('No file sent.');
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
               throw new RuntimeException('Exceeded filesize limit.');
            default:
               throw new RuntimeException('Unknown errors.');
         }

         $config['upload_path']  = $this->_post['dir'] ? $this->_path.'/'.urldecode($this->_post['dir']) : $this->_path;
         $config['allowed_types'] = '*';
         $config['overwrite'] = TRUE;
         $config['remove_spaces'] = FALSE; 


         $this->load->library('upload', $config);
         if($this->upload->do_upload('file')){
            $uploadresult = $this->upload->data();
            chmod($uploadresult['full_path'], 0777);
            $response['status'] = 'ok';
         }else{
            $response['status'] = 'error';
            $response['msg'] = $this->upload->display_errors();
         }
      }catch (RuntimeException $e) {
         http_response_code(400);
         $response['status'] = 'error';
         $response['message'] = $e->getMessage();
      }
      $this->json_result($response);
   }

   public function remove_flag(){
      $response['success'] = FALSE;
      $response['msg'] = 'Function failed';

      $this->db->trans_start();
      $prm['id'] = $this->_get['id'];
      $prm['type_dms'] = $this->_get['status'];
      $this->db->delete('document_tags', $prm);
      $this->db->trans_complete();

      if($this->db->trans_status()){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['msg'] = 'Tanda berhasil di hapus';
      }else{
         $this->db->trans_rollback();
      }
      $this->json_result($response);
   }

   public function upload_folder(){
      $response['msg'] = 'Some error occured';
      $done_upload = 0;
      $fail_upload = 0;
      $process = FALSE;

      if(isset($this->_post['folder_name']) && count($this->_post['folder_name']) > 0){
         foreach ($this->_post['folder_name'] as $v) {
            if($this->_post['dir']){
               $upload_path = $this->_path.'/'.urldecode($this->_post['dir']).'/'.$v;
            }else{
               $upload_path = $this->_path.'/'.$v;
            }
            if (!file_exists($upload_path)) {
               mkdir($upload_path);
               chmod($upload_path, 0777);
               $done_upload += 1;
            }else{
               $fail_upload += 1;
            }
         }
         $process = TRUE;
      }
      if($process) {
         $response['msg'] = $done_upload.' Folder berhasil dibuat '.$fail_upload.' folder gagal di upload';
         $response['success'] = $process;
      }else{
         $response['msg'] = 'Gagal mengupload folder';
      }
      $this->json_result($response);
   }

   public function delete_file(){
      $response['success'] = FALSE;
      $response['msg'] = 'Function failed';

      if($this->_post['dir'] && $this->_post['name']){
         $path = urldecode($this->_post['dir']).'/'.urldecode($this->_post['name']);
         
         $this->db->trans_start();
         if($this->_post['fav'] != ''){
            $this->document_tags->delete_by_id($this->_post['fav'], $this->_user->id);
         }
         if($this->_post['lock'] != ''){
            $this->document_tags->delete_by_id($this->_post['lock'], $this->_user->id);
         }
         if($this->_post['share'] != ''){
            $this->document_tags->delete_by_id($this->_post['share'], $this->_user->id);
         }
         $this->db->trans_complete();
         unlink($path);

         if($this->db->trans_status()){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'file berhasil dihapus';
         }else{
            $this->db->trans_rollback();
         }
      }
      $this->json_result($response);
   }

   public function delete_folder(){
      $response['success'] = FALSE;
      $response['msg'] = 'Function failed';

      if($this->_post['dir'] && $this->_post['name']){
         $path = urldecode($this->_post['dir']).'/'.urldecode($this->_post['name']);
         
         $this->db->trans_start();
         if($this->_post['fav'] != ''){
            $this->document_tags->delete_by_id($this->_post['fav'], $this->_user->id);
         }
         if($this->_post['lock'] != ''){
            $this->document_tags->delete_by_id($this->_post['lock'], $this->_user->id);
         }
         if($this->_post['share'] != ''){
            $this->document_tags->delete_by_id($this->_post['share'], $this->_user->id);
         }
         $this->delete_folder_files($path, $this->_user->id);
         $this->db->trans_complete();

         if($this->db->trans_status()){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'folder berhasil dihapus';
         }else{
            $this->db->trans_rollback();
         }
      }
      $this->json_result($response);
   }

   public function delete_folder_files($dir, $userid){
      $files = array_diff(scandir($dir), array('.','..'));
      foreach ($files as $file) {
         if(is_dir("$dir/$file")){
            $this->delete_folder_files("$dir/$file", $userid);
         }else{
            $this->document_tags->delete_by_path($dir.'/'.$file, $userid);
            unlink("$dir/$file");

         }
      }
      $this->document_tags->delete_by_path($dir, $userid);
      return rmdir($dir);
   }

}
