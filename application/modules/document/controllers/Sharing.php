<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Sharing extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('global_mapping');
      $this->load->library('date_extraction');
      $this->load->library('dir_extraction');
		if(in_array(1, $this->_role_id)){
         $this->_role_id = 1;
         $this->render_page($data, 'superadmin', 'modular');
      }else if(in_array(2, $this->_role_id)){
         $this->_role_id = 2;
         $this->_path = 'dms/apd/'.$this->_user->id;
      }else if(in_array(3, $this->_role_id)){
         $this->_role_id = 3;
         $this->_path = 'dms/kadis/'.$this->_user->id;
      }else if(in_array(5, $this->_role_id)){
         $this->_role_id = 5;
         $this->_path = 'dms/pedasi/'.$this->_user->id;
      }else if(in_array(4, $this->_role_id)){
         $this->_role_id = 4;
         $this->_path = 'dms/kasie/'.$this->_user->id;
      }
	}

	public function index(){
      $data['first_load'] = $this->_get['first_load'] ? $this->_get['first_load'] : FALSE;
		$response['html'] = $this->load->view('sharing', $data, TRUE);
      $this->json_result($response);
	}

	public function getting(){
      $list = array();
      $total = 0;
      $params = $this->_get;
      $data = $this->dms_shared_opd('get', $this->_opd->id);
      if($data){
         $i = 0;
         foreach ($data as $v) {
            $time = date('d M Y H:i:s', filectime(urldecode($v['full_dir'])));
            $is_file = is_file(urldecode($v['full_dir']));

            $list[$i]['file_ternary'] = $v['file_ternary'] ? $v['file_ternary'] : NULL;
            $list[$i]['id'] = $v['id'];
            $list[$i]['is_file'] = $is_file;
            $list[$i]['clean_time'] = $time;
            $list[$i]['size'] = $this->global_mapping->format_size_units(filesize(urldecode($v['full_dir'])));
            $list[$i]['size_bytes'] = filesize(urldecode($v['full_dir']));
            $list[$i]['file_name'] = urldecode($v['file_name']);
            $list[$i]['file_dir'] = urldecode($v['file_dir']);
            $list[$i]['encode_file_dir'] = $v['file_dir'];
            $list[$i]['full_dir'] = urldecode($v['full_dir']);
            $list[$i]['encode_full_dir'] = $v['full_dir'];
            $list[$i]['encode_file_name'] = $v['file_name'];
            $list[$i]['time'] = $this->date_extraction->time_ago($time);
            $list[$i]['file_extension'] = $v['file_extension'] ? $this->global_mapping->mapping_icon_request('.'.$v['file_extension'].'') : NULL;
            $i++;
         }
         if($list){
            if($params['orderby'] == 'desc'){
               usort($list, function($b, $a) use ($params) {
                   return $a[$params['order']] <=> $b[$params['order']];
               });
            }
            if($params['orderby'] == 'asc'){
               usort($list, function($a, $b) use ($params) {
                   return $a[$params['order']] <=> $b[$params['order']];
               });
            }
         }
         $total = $this->dms_shared_opd('count', $this->_opd->id);
      }
      $response['total'] = $total;
      $response['result'] = $list;
      $this->json_result($response);
   }

   public function dir(){
   	$explode_base = explode('/', $this->_get['dir']);
   	$explode_dir = explode('/', $this->_get['dir'], 4);
      $response['result'] = $this->list_folder_files($this->_get);
      $response['dir_base'] = $explode_base[0].'/'.$explode_base[1].'/'.$explode_base[2];
      $response['dir_clean'] = urldecode($explode_dir[3]);

      $nav = '';
      $nav .= '<li><a data-href="" class="breadcrumb-action-home" style="cursor:pointer;"><i class="fa fa-home"></i></a></li>';
      $uri_string = explode('/', urldecode($explode_dir[3]));
      $uri = array();
      foreach (array_filter($uri_string) as $rs) {
         $uri[] = urlencode($rs);
         $check = $this->db->where('file_name', urlencode($rs))->where('opd_id', $this->_opd->id)->count_all_results('dms_shared_opd');
         if($check > 0){
            $nav .= '<li><a data-href="'.$response['dir_base'].'/'.implode('/', $uri).'" class="breadcrumb-action">'.$rs.'</a></li>';
         }
      }
      $response['nav'] = $nav;
      $response['uri'] = $uri;
      $this->json_result($response);
   }

   public function list_folder_files($params){
   	$dir = urldecode($params['dir']);

      $ffs = scandir($dir);

      unset($ffs[array_search('.', $ffs, true)]);
      unset($ffs[array_search('..', $ffs, true)]);

      // prevent empty ordered elements
      if (count($ffs) < 1)
         return;

      $i = 0;
      $data = array();

      foreach($ffs as $ff){
         $time = date('d M Y H:i:s', filectime($dir.'/'.$ff));
         $data[$i]['name'] = $ff;
         if(is_file($dir.'/'.$ff)){
            $ext = pathinfo($dir.'/'.$ff, PATHINFO_EXTENSION);
            $data[$i]['ext_orig'] = $ext;
            $data[$i]['ext'] = $this->global_mapping->mapping_icon_request('.'.$ext.'');
         }else{
            $data[$i]['ext_orig'] = NULL;
            $data[$i]['ext'] = NULL;
         }
         $data[$i]['base_dir'] = $dir;
         $data[$i]['url'] = urlencode($ff);
         $data[$i]['is_file'] = is_file($dir.'/'.$ff);
         $data[$i]['time'] = $this->date_extraction->time_ago($time);
         $data[$i]['clean_time'] = $time;
         $data[$i]['size'] = $this->global_mapping->format_size_units(filesize($dir.'/'.$ff));
         $data[$i]['size_bytes'] = filesize($dir.'/'.$ff);

         $i++;
      }
      if($data){
         if($params['orderby'] == 'desc'){
            usort($data, function($b, $a) use ($params) {
                return $a[$params['order']] <=> $b[$params['order']];
            });
         }
         if($params['orderby'] == 'asc'){
            usort($data, function($a, $b) use ($params) {
                return $a[$params['order']] <=> $b[$params['order']];
            });
         }
      }
      return array(
         'data' => $data,
         'params' => $params['order'],
         'total' => count($data)
      );
   }

   public function dms_shared_opd($mode, $opd){
   	$this->db->where('opd_id', $opd);
      switch ($mode) {
         case 'get':
            return $this->db->get('dms_shared_opd')->result_array();
         case 'count':
            return $this->db->get('dms_shared_opd')->num_rows();
      }
   }

}