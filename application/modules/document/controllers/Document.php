<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Document extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
      if(in_array(1, $this->_role_id)){
         $this->render_page($data, 'superadmin', 'modular');
      }else if(in_array(2, $this->_role_id)){
         $this->_path = 'dms/apd/'.$this->_user->id;
      }else if(in_array(3, $this->_role_id)){
         $this->_path = 'dms/kadis/'.$this->_user->id;
      }else if(in_array(5, $this->_role_id)){
         $this->_path = 'dms/pedasi/'.$this->_user->id;
      }else if(in_array(4, $this->_role_id)){
         $this->_path = 'dms/kasie/'.$this->_user->id;
      }
	}

	public function index(){
      $this->dir_create($this->_path);
		$data['_css'] = array(
         $this->_path_template.'/plugins/select2/select2.min.css',
         $this->_path_template.'/plugins/krajee-star-rating/star-rating.min.css',
         $this->_path_template.'/plugins/krajee-star-rating/theme.css',
         $this->_path_template.'/plugins/uploader-master/css/jquery.dm-uploader.css'
      ); 
      $data['_js'] = array(
         $this->_path_template.'/plugins/select2/select2.full.min.js',
         $this->_path_template.'/plugins/tinymce/tinymce.min.js',
         $this->_path_template.'/plugins/krajee-star-rating/star-rating.min.js',
         $this->_path_template.'/plugins/uploader-master/js/jquery.dm-uploader.js',
		);	
		$this->render_page($data, 'management', 'modular');
	}

   public function dir_create($path){
      if (!file_exists($path)) {
         mkdir($path);
         chmod($path, 0777);
      }
   }
}
