<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Favorites extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
      $this->load->library('global_mapping');
      $this->load->library('date_extraction');
      $this->load->library('dir_extraction');
      $this->load->model('document_tags');
      if(in_array(1, $this->_role_id)){
         $this->render_page($data, 'superadmin', 'modular');
      }else if(in_array(2, $this->_role_id)){
         $this->_path = 'dms/apd/'.$this->_user->id;
      }else if(in_array(3, $this->_role_id)){
         $this->_path = 'dms/kadis/'.$this->_user->id;
      }else if(in_array(5, $this->_role_id)){
         $this->_path = 'dms/pedasi/'.$this->_user->id;
      }else if(in_array(4, $this->_role_id)){
         $this->_path = 'dms/kasie/'.$this->_user->id;
      }
	}

	public function index(){
      $data['path_scripts'] = $this->_path_scripts;
      $data['first_load'] = $this->_get['first_load'] ? $this->_get['first_load'] : FALSE;
		$response['html'] = $this->load->view('favorites', $data, TRUE);
      $this->json_result($response);
	}

   public function getting(){
      $list = array();
      $params = $this->_get;
      $data = $this->document_tags->get_by_status('get', $this->_get, $this->_user->id, 3);
      if($data){
         $i = 0;
         foreach ($data as $v) {
            $time = date('d M Y H:i:s', filectime($v['full_dir']));
            $is_file = is_file($v['full_dir']);

            $list[$i]['file_ternary'] = $v['file_ternary'] ? urlencode($v['file_ternary']) : NULL;
            $list[$i]['id'] = $v['id'];
            $list[$i]['is_file'] = $is_file;
            $list[$i]['clean_time'] = $time;
            $list[$i]['size'] = $this->global_mapping->format_size_units(filesize($v['full_dir']));
            $list[$i]['size_bytes'] = filesize($v['full_dir']);
            $list[$i]['file_name'] = $v['file_name'];
            $list[$i]['name'] = $v['file_name'];
            $list[$i]['file_dir'] = $v['file_dir'];
            $list[$i]['encode_file_dir'] = urlencode($v['file_dir']);
            $list[$i]['full_dir'] = $v['full_dir'];
            $list[$i]['encode_full_dir'] = urlencode($v['full_dir']);
            $list[$i]['encode_file_name'] = urlencode($v['file_name']);
            $list[$i]['created_date'] = $this->date_extraction->time_ago($time);
            $list[$i]['file_extension'] = $v['file_extension'] ? $this->global_mapping->mapping_icon_request('.'.$v['file_extension'].'') : NULL;
            $i++;
         }
         if($list){
            if($params['orderby'] == 'desc'){
               usort($list, function($b, $a) use ($params) {
                   return $a[$params['order']] <=> $b[$params['order']];
               });
            }
            if($params['orderby'] == 'asc'){
               usort($list, function($a, $b) use ($params) {
                   return $a[$params['order']] <=> $b[$params['order']];
               });
            }
         }
         $response['result'] = $list;
         $response['total'] = $this->document_tags->get_by_status('count', $this->_get, $this->_user->id, 3);
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

   public function dir_by_favorites(){
      $list = array();
      $params = $this->_get;
      $data = $this->document_tags->flag_by_dir('get', $this->_get, $this->_user->id, $this->_path, 3);
      if($data){
         $i = 0;
         foreach ($data as $v) {
            $time = date('d M Y H:i:s', filectime($v['full_dir']));
            $is_file = is_file($v['full_dir']);

            $list[$i]['file_ternary'] = $v['file_ternary'] ? urlencode($v['file_ternary']) : NULL;
            $list[$i]['id'] = $v['id'];
            $list[$i]['is_file'] = $is_file;
            $list[$i]['clean_time'] = $time;
            $list[$i]['size'] = $this->global_mapping->format_size_units(filesize($v['full_dir']));
            $list[$i]['size_bytes'] = filesize($v['full_dir']);
            $list[$i]['file_name'] = $v['file_name'];
            $list[$i]['name'] = $v['file_name'];
            $list[$i]['file_dir'] = $v['file_dir'];
            $list[$i]['encode_file_dir'] = urlencode($v['file_dir']);
            $list[$i]['full_dir'] = $v['full_dir'];
            $list[$i]['encode_full_dir'] = urlencode($v['full_dir']);
            $list[$i]['encode_file_name'] = urlencode($v['file_name']);
            $list[$i]['created_date'] = $this->date_extraction->time_ago($time);
            $list[$i]['file_extension'] = $v['file_extension'] ? $this->global_mapping->mapping_icon_request('.'.$v['file_extension'].'') : NULL;
            $i++;
         }
         if($list){
            if($params['orderby'] == 'desc'){
               usort($list, function($b, $a) use ($params) {
                   return $a[$params['order']] <=> $b[$params['order']];
               });
            }
            if($params['orderby'] == 'asc'){
               usort($list, function($a, $b) use ($params) {
                   return $a[$params['order']] <=> $b[$params['order']];
               });
            }
         }
         $response['result'] = $list;
         $response['total'] = $this->document_tags->flag_by_dir('count', $this->_get, $this->_user->id, $this->_path, 3);
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

   public function remove_flag(){
      $response['success'] = FALSE;
      $response['msg'] = 'Function failed';

      $this->db->trans_start();
      $prm['id'] = $this->_get['id'];
      $prm['type_dms'] = $this->_get['status'];
      $this->db->delete('document_tags', $prm);
      $this->db->trans_complete();

      if($this->db->trans_status()){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['msg'] = 'Tanda berhasil di hapus';
      }else{
         $this->db->trans_rollback();
      }
      $this->json_result($response);
   }


   public function read_dir(){
      $response['result'] = $this->list_folder_files($this->_get);
      $response['dir_base'] = $this->_path;
      $response['dir_clean'] = urldecode($this->_get['dir']);

      $nav = '';
      $nav .= '<li><a data-href="" class="breadcrumb-action-home" style="cursor:pointer;"><i class="fa fa-home"></i></a></li>';
      $uri_string = explode('/', urldecode($this->_get['dir']));
      $uri = array();
      foreach (array_filter($uri_string) as $rs) {
         $uri[] = urlencode($rs);
         $check = $this->db->where('file_name', $rs)->where('type_dms', 3)->where('created_by', $this->_user->id)->count_all_results('document_tags');
         // if($check > 0){
            $nav .= '<li><a data-href="'.implode('/', $uri).'" class="breadcrumb-action">'.$rs.'</a></li>';
         // }
      }
      $response['nav'] = $nav;
      $response['uri'] = $uri;
      $this->json_result($response);
   }

   public function list_folder_files($params){
      if($params['dir']){
         $dir = $this->_path.'/'.urldecode($params['dir']);
      }else{
         $dir = $this->_path;
      }

      $ffs = scandir($dir);

      unset($ffs[array_search('.', $ffs, true)]);
      unset($ffs[array_search('..', $ffs, true)]);

      // prevent empty ordered elements
      if (count($ffs) < 1)
         return;

      $i = 0;
      $data = array();
      
      $lock_dms = $this->document_tags->get_by_id(1, $this->_user->id);
      $share_dms = $this->document_tags->get_by_id(2, $this->_user->id);
      $favorite_dms = $this->document_tags->get_by_id(3, $this->_user->id);

      foreach($ffs as $ff){
         $time = date('d M Y H:i:s', filectime($dir.'/'.$ff));
         $data[$i]['name'] = $ff;
         if(is_file($dir.'/'.$ff)){
            $ext = pathinfo($dir.'/'.$ff, PATHINFO_EXTENSION);
            $data[$i]['ext_orig'] = $ext;
            $data[$i]['ext'] = $this->global_mapping->mapping_icon_request('.'.$ext.'');
         }else{
            $data[$i]['ext_orig'] = NULL;
            $data[$i]['ext'] = NULL;
         }
         $data[$i]['base_dir'] = $dir;
         $data[$i]['url'] = urlencode($ff);
         $data[$i]['is_file'] = is_file($dir.'/'.$ff);
         $data[$i]['time'] = $this->date_extraction->time_ago($time);
         $data[$i]['clean_time'] = $time;
         $data[$i]['size'] = $this->global_mapping->format_size_units(filesize($dir.'/'.$ff));
         $data[$i]['size_bytes'] = filesize($dir.'/'.$ff);

         if($lock_dms){
            $cond_lock = $this->dir_extraction->in_array_r($dir.'/'.$ff, $lock_dms);
            if($cond_lock){
               $data[$i]['lock'] = $cond_lock['id'];
            }else{
               $data[$i]['lock'] = FALSE;
            }
         }

         if($share_dms){
            $cond_share = $this->dir_extraction->in_array_r($dir.'/'.$ff, $share_dms);
            if($cond_share){
               $data[$i]['share'] = $cond_share['id'];
            }else{
               $data[$i]['share'] = FALSE;
            }
         }

         if($favorite_dms){
            $cond_favorite = $this->dir_extraction->in_array_r($dir.'/'.$ff, $favorite_dms);
            if($cond_favorite){
               $data[$i]['favorites'] = $cond_favorite['id'];
            }else{
               $data[$i]['favorites'] = FALSE;
            }
         }

         $i++;
      }
      if($data){
         if($params['orderby'] == 'desc'){
            usort($data, function($b, $a) use ($params) {
                return $a[$params['order']] <=> $b[$params['order']];
            });
         }
         if($params['orderby'] == 'asc'){
            usort($data, function($a, $b) use ($params) {
                return $a[$params['order']] <=> $b[$params['order']];
            });
         }
      }
      return array(
         'data' => $data,
         'params' => $params['order'],
         'total' => count($data)
      );
   }
}
