<div class="row" style="margin-bottom: 5px;">
	<div class="col-md-12">
		<div class="btn-toolbar pull-left">
			<div class="btn-group" data-toggle="buttons">
				<input type="text" class="form-control input-sm" id="filt_keyword" placeholder="Pencarian...">
			</div>
			<div class="btn-group" data-toggle="buttons">
				<select type="text" class="form-control input-sm" id="filt_status">
					<option value="all">Semua Status</option>
					<option value="">Proses Disposisi</option>
					<option value="1">Kompilasi Data</option>
					<option value="2">Validasi Kasie</option>
					<option value="3">Diserahkan</option>
				</select>
			</div>
			<div class="btn-group" data-toggle="buttons">
				<button class="btn btn-default btn-sm btn-filter"><i class="fa fa-filter"></i> Filter</button>
			</div>
		</div>
		<div class="btn-toolbar pull-right">
			<div class="btn-group" data-toggle="buttons">
				<button class="btn btn-primary btn-sm btn-create" data-toggle="modal" data-target="#modal-create">
					<i class="fa fa-plus"></i> Tambah Request
				</button>
         </div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="box box-solid" id="cont-data" style="font-size:12px;">
       	<div class="box-body table-responsive">
				<table class="table table-hover table-condensed">
					<thead>
						<th>Tgl Request <a class="change_order" href="#" data-order="a.requested_date" data-by="asc"><i class="fa fa-sort"></i></th>
						<th>Permohonan Data <a class="change_order" href="#" data-order="a.title" data-by="asc"><i class="fa fa-sort"></i></th>
						<th>OPD <a class="change_order" href="#" data-order="a.opd_id" data-by="asc"><i class="fa fa-sort"></i></th>
						<th>Mulai <a class="change_order" href="#" data-order="a.start_date" data-by="asc"><i class="fa fa-sort"></i></th>
						<th>Selesai <a class="change_order" href="#" data-order="a.end_date" data-by="asc"><i class="fa fa-sort"></i></th>
						<th>Batas Waktu <a class="change_order" href="#" data-order="a.limit_date" data-by="asc"><i class="fa fa-sort"></i></th>
						<th>Status Permohonan <a class="change_order" href="#" data-order="a.status" data-by="asc"><i class="fa fa-sort"></i></th>
						<th>Status Pengolahan <a class="change_order" href="#" data-order="a.status_pengolahan" data-by="asc"><i class="fa fa-sort"></i></th>
						<th></th>
					</thead>
          		<tbody class="sect-data"></tbody>
				</table>
    		</div>
    		<div class="box-footer" style="">
           <div class="row">
               <div class="col-md-2">
             		<h5 class="sect-total"></h5>
               </div>
               <div class="col-md-10 text-right">
                  <ul class="pagination sect-pagination pagination-sm no-margin pull-right"></ul>
               </div>
            </div>
         </div>
		</div>
	</div>
</div>

<div class="modal fade in" id="modal-create" style="">
 	<div class="modal-dialog">
     	<div class="modal-content" style="width: 1300px;margin-left: -349px;">
         <div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal">
         		<span aria-hidden="true">×</span>
         	</button>
          	<h4 class="modal-title"><i class="fa fa-plus"></i>&nbsp;Tambah Request</h4>
         </div>
         <form id="form-create">
          	<div class="modal-body">
          		<div class="row">
          			<div class="col-md-6">
          				<div class="form-group">
		                 	<label>Tujuan OPD <span class="text-danger">*</span></label>
		                 	<!-- <input id="input-id" type="text" class="rating" data-size="lg" data-min="0" data-max="5" data-step="1"> -->
		                 	<select class="form-control input-sm choose opd_id" name="opd_id">
		                 		<!-- <option value="" selected="" formrender="#form-create" pedasiid="" pedasiname=""></option> -->
		                 		<?php
		                 		if($mopd){
		                 			foreach ($mopd as $v) {
		                 				echo '<option value="'.$v['id'].'" formrender="#form-create" pedasiid="'.$v['user_id'].'" pedasiname="'.$v['first_name'].'">'.$v['name'].'</option>';
		                 			}
		                 		}
		                 		?>
		                 	</select>
		             	</div>
		             	<div class="form-group">
		                 	<label>Pedasi</label>
		                 	<input type="hidden" name="pedasi_id">
		                 	<input type="text" class="form-control input-sm" name="pedasi_name" disabled="" readonly="">
		             	</div>
		             	<div class="form-group">
		                 	<label>Permohonan Data <span class="text-danger">*</span></label>
		                 	<input type="text" class="form-control input-sm" name="title">
		             	</div>
		             	<div class="row" style="margin-bottom: 15px;">
		          			<div class="col-md-12">
		                 		<label>Periode Data <span class="text-danger">*</span></label>
		       				</div>
		       				<div class="col-md-6">
		       					<input type="hidden" name="start_date" value="">
		       					<div class="input-group">
		       						<span class="btn btn-default input-group-addon">Start</span>
		       						<input type="text" class="form-control input-sm drp" data-trgt="start_date" value="">
		    						</div>
		       				</div>
		       				<div class="col-md-6">
		       					<input type="hidden" name="end_date" value="">
		       					<div class="input-group">
		       						<span class="btn btn-default input-group-addon">End</span>
		       						<input type="text" class="form-control input-sm drp" data-trgt="end_date" value="">
		    						</div>
		       				</div>
		    				</div>
		    				<div class="form-group">
		                 	<label>Batas waktu pengumpulan <span class="text-danger">*</span></label>
		                 	<input type="hidden" name="limit_date" value="">
		                 	<input type="text" class="form-control input-sm drp-limit" data-trgt="limit_date" value="">
		             	</div>
		             	<div class="form-group">
		                 	<label>Lampiran</label>
		                 	<input type="file" class="form-control input-sm" name="file_path[]" multiple="">
		                 	<span class="text-muted" style="font-size: 9px;">Bisa lebih dari 1 file</span>
		             	</div>
       				</div>
       				<div class="col-md-6">
       					<div class="form-group">
		                 	<label>Format Data</label>
		                 	 <textarea class="form-control" name="description" id="description_create"></textarea>
	                 	</div>
       				</div>
       			</div>
          	</div>
          	<div class="modal-footer">
              	<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
             </div>
      	</form>        
     	</div>
 	</div>
</div>

<div class="modal fade in" id="modal-edit" style="">
 	<div class="modal-dialog">
     	<div class="modal-content" style="width: 1300px;margin-left: -349px;">
         <div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal">
         		<span aria-hidden="true">×</span>
         	</button>
          	<h4 class="modal-title"><i class="fa fa-edit"></i>&nbsp;Edit Request</h4>
         </div>
         <form id="form-edit">
         	<input type="hidden" name="id">
          	<div class="modal-body">
          		<div class="row">
          			<div class="col-md-6">
          				<div class="form-group">
		                 	<label>Tujuan OPD <span class="text-danger">*</span></label>
		                 	<!-- <input id="input-id" type="text" class="rating" data-size="lg" data-min="0" data-max="5" data-step="1"> -->
		                 	<select class="form-control input-sm choose opd_id" name="opd_id">
		                 		<!-- <option value="" selected="" formrender="#form-create" pedasiid="" pedasiname=""></option> -->
		                 		<?php
		                 		if($mopd){
		                 			foreach ($mopd as $v) {
		                 				echo '<option value="'.$v['id'].'" formrender="#form-edit" pedasiid="'.$v['user_id'].'" pedasiname="'.$v['first_name'].'">'.$v['name'].'</option>';
		                 			}
		                 		}
		                 		?>
		                 	</select>
		             	</div>
		             	<div class="form-group">
		                 	<label>Pedasi</label>
		                 	<input type="hidden" name="pedasi_id">
		                 	<input type="text" class="form-control input-sm" name="pedasi_name" disabled="" readonly="">
		             	</div>
		             	<div class="form-group">
		                 	<label>Permohonan Data <span class="text-danger">*</span></label>
		                 	<input type="text" class="form-control input-sm" name="title">
		             	</div>
		             	<div class="row" style="margin-bottom: 15px;">
		          			<div class="col-md-12">
		                 		<label>Periode Data <span class="text-danger">*</span></label>
		       				</div>
		       				<div class="col-md-6">
		       					<input type="hidden" name="start_date" value="">
		       					<div class="input-group">
		       						<span class="btn btn-default input-group-addon">Start</span>
		       						<input type="text" class="form-control input-sm drp" data-trgt="start_date" value="">
		    						</div>
		       				</div>
		       				<div class="col-md-6">
		       					<input type="hidden" name="end_date" value="">
		       					<div class="input-group">
		       						<span class="btn btn-default input-group-addon">End</span>
		       						<input type="text" class="form-control input-sm drp" data-trgt="end_date" value="">
		    						</div>
		       				</div>
		    				</div>
		    				<div class="form-group">
		                 	<label>Batas waktu pengumpulan <span class="text-danger">*</span></label>
		                 	<input type="hidden" name="limit_date" value="">
		                 	<input type="text" class="form-control input-sm drp-limit" data-trgt="limit_date" value="">
		             	</div>
		             	<div class="form-group">
		                 	<label>Lampiran</label>
		                 	<input type="file" class="form-control input-sm" name="file_path[]" multiple="">
		                 	<span class="text-muted" style="font-size: 9px;">Bisa lebih dari 1 file</span>
		             	</div>
		             	<div class="form-group list-attachment" style="">
	             		</div>
       				</div>
       				<div class="col-md-6">
       					<div class="form-group">
		                 	<label>Format Data</label>
		                 	 <textarea class="form-control" name="description" id="description_edit"></textarea>
	                 	</div>
       				</div>
       			</div>
          	</div>
          	<div class="modal-footer">
              	<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
             </div>
      	</form>        
     	</div>
 	</div>
</div>

<div class="modal fade in" id="modal-show-files">
 	<div class="modal-dialog">
     	<div class="modal-content" style="width: 700px;margin-left: -50px;">
         <div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal">
         		<span aria-hidden="true">×</span>
         	</button>
          	<h4 class="modal-title"><i class="fa fa-file"></i>&nbsp;File Request</h4>
         </div>
      	<div class="modal-body">
      		<table class="table table-condensed" style="font-size:12px;">
      			<thead>
      				<th>Filename</th>
      				<th>Created</th>
      				<th>PIC</th>
      				<th></th>
      			</thead>
      			<tbody class="sect-data"></tbody>
      		</table>
   		</div>
		</div>
	</div>
</div>

<div class="modal fade in" id="modal-detail">
 	<div class="modal-dialog">
     	<div class="modal-content" style="width: 700px;margin-left: -50px;">
         <div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal">
         		<span aria-hidden="true">×</span>
         	</button>
          	<h4 class="modal-title"><i class="fa fa-file"></i>&nbsp;Detail Permohonan Data</h4>
         </div>
      	<div class="modal-body">
   		</div>
   	</div>
	</div>
</div>

<script type="text/javascript">

	$(function () {

		_offset = 0;
	   _curpage = 1;

	   $('.choose').select2({
			width: '100%'
		});


      $(".request-rating-detail").rating({
         displayOnly: true
      });

	   tinymce.init({
        	selector: '#description_create',
        	plugins: 'link,table',
        	height: 270,	
        	content_style:'p {margin: 0px 0px 15px 0px;}'
    	});

	   $.fn.todolist = function(option){
	      var param = $.extend({
	         filt_keyword : $('#filt_keyword').val(),
	         filt_status : $('#filt_status').val(),
	         limit : 10,
	         offset : _offset, 
	         currentPage : _curpage,
	         order : 'a.id', 
	         orderby : 'desc'
	      }, option);
	      
	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'requestor/monitoring/getting',
	         dataType : "JSON",
	         data : {
	            offset : param.offset,
	            filt_keyword : param.filt_keyword,
	            filt_status : param.filt_status,
	            limit : param.limit,
	            order : param.order,
	            orderby : param.orderby
	         },
	         beforeSend: function (xhr) {
	            loading_table('#cont-data', 'show');
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	            loading_table('#cont-data', 'hide');
	         },
	         success : function(r){
	            var t = '';
               if(r.total){
                  var total = r.total;
                  $.each(r.result, function(k,v){
                     t += '<tr>';
                     	t += '<td>'+v.requested_date+'<br>'+v.requested_time+'</td>';
                     	t += '<td>';
                     		t += v.title;
                     		t += '<input class="request-rating" value="'+(v.request_rating ? v.request_rating : 0)+'" type="text" class="rating" data-size="sm" data-min="0" data-max="10" data-step="2">';
                  		t += '</td>';
                     	t += '<td>'+v.opd+'</td>';
                     	t += '<td>'+v.start_date+'</td>';
                     	t += '<td>'+v.end_date+'</td>';
                     	t += '<td>'+v.limit_date+'<br><span style="font-size:10px;font-weight:bold">'+v.days_left+' Days Left</span></td>';
                     	t += '<td>';
                     		t += v.status_name;
                     	t += '</td>';
                     	t += '<td>';
                     		t += v.status_pengolahan_name;
                     	t += '</td>';
	                     t += '<td>';
	                        t += '<div class="btn-group">';
	                           t += '<button class="btn btn-detail btn-default btn-xs" data-toggle="tooltip" data-title="Detail" data-opd="'+v.opd_id+'" data-status="'+v.status_id+'" data-id="'+v.id+'" data-pedasi="'+v.pedasi+'"><i class="fa fa-file"></i></button>';
	                           if(!v.status_id){
	                           	t += '<button class="btn btn-edit btn-default btn-xs" data-toggle="tooltip" data-title="Edit" data-id="'+v.id+'"><i class="fa fa-edit"></i></button>';
	                           	t += '<button class="btn btn-delete btn-default btn-xs" data-toggle="tooltip" data-title="Delete" data-id="'+v.id+'"><i class="fa fa-trash"></i></button>';
	                           }
	                        t += '</div>';
	                     t += '</td>';
                     t += '</tr>';
                  });
               }else{
                  t += '<tr><td class="text-center text-muted" colspan="10">No Result</td></tr>';
               }

	            $('#cont-data').find('.sect-total').html(''+number_format(total)+' Rows Data');
	            $('#cont-data').find('.sect-data').html(t);
	            $(".request-rating").rating({
	            	displayOnly: true
	            });

	            $('#cont-data .sect-pagination').pagination({
			         items: r.total,
			         itemsOnPage: param.limit,
			         edges: 0,
			         hrefTextPrefix: '',
			         displayedPages: 1,
			         currentPage : param.currentPage,
			         prevText : '&laquo;',
			         nextText : '&raquo;',
			         dropdown: true,
			         onPageClick : function(n,e){
			            e.preventDefault();
			            _offset = (n-1)*param.limit;
			            _curpage = n;
			            $(this).todolist();
			         }
			      });
	         },
	         complete : function(r){
	         	loading_table('#cont-data', 'hide');
         	}
	      });
	   }		

	   $(this).on('click', '.btn-filter',function(e){
         _offset = 0;
         _curpage = 1;
         $(this).todolist();
      });

	   $(this).on('click', '.btn-edit',function(e){
	      var id = $(this).data('id');
	      var form = $('#form-edit');

	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'requestor/monitoring/modify',
	         dataType : "JSON",
	         data : {
	            id : id
	         },
	         beforeSend: function (xhr) {
	            form.resetForm();
             	tinymce.remove('#description_edit');
	            form.find('.list-attachment').hide();
	            loading_button('.btn-edit', id, 'show', '<i class="fa fa-edit"></i>', '');
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown);
	            loading_button('.btn-edit', id, 'hide', '<i class="fa fa-edit"></i>', '');
	         },
	         success: function(r){
	            session_checked(r._session, r._maintenance);
	            form.find('input[name="id"]').val(r.request.id);
	            form.find('input[name="pedasi_id"]').val(r.request.pedasi_id);
	            form.find('input[name="title"]').val(r.request.title);
	            form.find('select[name="opd_id"]').select2('val', r.request.opd_id);
	            form.find('input[name="pedasi_name"]').val(r.request.pedasi_name);

	            tinymce.init({
					  	selector:'#description_edit',
					  	plugins: 'link,table',
			        	height: 270,	
			        	content_style:'p {margin: 0px 0px 15px 0px;}',
					  	init_instance_callback : function(editor) {
				    		editor.setContent(r.request.description);
					  	}
					});

	            // tinymce.get('#description_edit').setContent(r.request.description, {format : 'raw'});

	            if(r.request.start_date){
	               form.find('.drp[data-trgt="start_date"]').val(moment(r.request.start_date).format('DD/MMM/YYYY'));
	               form.find('input[name="start_date"]').val(r.request.start_date);
	            }else{
	               form.find('.drp[data-trgt="start_date"]').val('');
	               form.find('input[name="start_date"]').val('');
	            }

	            if(r.request.end_date){
	               form.find('.drp[data-trgt="end_date"]').val(moment(r.request.end_date).format('DD/MMM/YYYY'));
	               form.find('input[name="end_date"]').val(r.request.end_date);
	            }else{
	               form.find('.drp[data-trgt="end_date"]').val('');
	               form.find('input[name="end_date"]').val('');
	            }

	            if(r.request.limit_date){
	               form.find('.drp[data-trgt="limit_date"]').val(moment(r.request.limit_date).format('DD/MMM/YYYY'));
	               form.find('input[name="limit_date"]').val(r.request.limit_date);
	            }else{
	               form.find('.drp[data-trgt="limit_date"]').val('');
	               form.find('input[name="limit_date"]').val('');
	            }

	            if(r.attachments){
                  t = '';
                  t += '<h4 class="bold"><i class="fa fa-paperclip"></i> Lampiran</h4>';
                  t += '<h1>';
                  $.each(r.attachments, function(kk,vv){
                     t += '<span class="attach-options atc-opt-'+vv.id+'">';
                        t += '<a href="'+site_url+'activity-download-'+vv.id+'" data-toggle="tooltip" data-title="'+vv.file_orig_name+'" style="margin-right:5px;">';
                           t += '<i class="'+vv.icon+'"></i>';
                        t += '</a>';
                        t += '<a class="remove-attach text-danger" data-id="'+vv.id+'" href="" style="font-size: 14px;text-align:center;" data-toggle="tooltip" data-title="Remove">';
                        t += '<i class="fa fa-trash"></i>';
                        t += '</a>';
                     t += '</span>';
                  });
                  t += '</h1>';
                  form.find('.list-attachment').html(t);
                  form.find('.list-attachment').show();
               }


	            $(this).render_picker({
			         modal: '#modal-edit',
			         form: '#form-edit',
			      });
	         },
	         complete: function(){
	            $("#modal-edit").modal("toggle");
	         	loading_button('.btn-edit', id, 'hide', '<i class="fa fa-edit"></i>', '');
	         }
	      });
	      e.preventDefault();
	   });

		$(this).on('click', '.btn-show-files',function(e){
	      var id = $(this).data('id');
	      var form = $('#form-edit');

	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'requestor/monitoring/show_files',
	         dataType : "JSON",
	         data : {
	            id : id
	         },
	         beforeSend: function (xhr) {
             	$('#modal-show-files').find('.sect-data').html('');
	            loading_button('.btn-show-files', id, 'show', '<i class="fa fa-folder"></i>', '');
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown);
	            loading_button('.btn-show-files', id, 'hide', '<i class="fa fa-folder"></i>', '');
	         },
	         success: function(r){
	            session_checked(r._session, r._maintenance);
	            t = '';
	            if(r.result){
	            	$.each(r.result, function(k,v){
	            		t += '<tr>';
		            		t += '<td>';
		            			t += v.file_name;
		            		t += '</td>';
		            		t += '<td>';
		            			t += moment(v.created_date).format('DD MMM YYYY h:mm:ss');
		            		t += '</td>';
		            		t += '<td>';
		            			t += v.pic;
		            		t += '</td>';
		            		t += '<td>';
		            			t += '<div class="btn-group">';
		            				t += '<a href="'+site_url+'main/download?name='+v.file_name+'&dir='+v.full_path+'" class="btn btn-default btn-xs" data-toggle="tooltip" data-title="Download">';
		            				t += '<i class="fa fa-download"></i>';
		            				t += '</a>';
		            			t += '</div>';
		            		t += '</td>';
	            		t += '</tr>';
            		});
	            	$('#modal-show-files').find('.sect-data').html(t);
	            }
	         },
	         complete: function(){
	            $("#modal-show-files").modal("toggle");
	         	loading_button('.btn-show-files', id, 'hide', '<i class="fa fa-folder"></i>', '');
	         }
	      });
	      e.preventDefault();
	   });
		
		$(this).on('click', '.btn-delete', function(e) {
         e.preventDefault();
         var id = $(this).attr('data-id');
         var conf = confirm('Are you sure?');
         if(conf){
            ajaxManager.addReq({
               type : "GET",
               url : site_url + 'requestor/monitoring/delete',
               dataType : "JSON",
               data : {
                  id : id
               },
             	beforeSend: function (xhr) {
		            loading_button('.btn-delete', id, 'show', '<i class="fa fa-trash"></i>', '');
		         },
		         error: function (jqXHR, status, errorThrown) {
		            error_handle(jqXHR, status, errorThrown);
		            loading_button('.btn-delete', id, 'hide', '<i class="fa fa-trash"></i>', '');
		         },
               success: function(r){
                  if(r.success){
                     $(this).todolist();
                     toastr.success(r.msg);
                  }else{
                     toastr.error(r.msg);
                  }  
               },
               complete: function(){
		            loading_button('.btn-delete', id, 'hide', '<i class="fa fa-trash"></i>', '');
		         }
            });
         }else{
            return false;
         }
      });

	   $(this).on('click', '.remove-attach', function(e) {
         e.preventDefault();
         var id = $(this).attr('data-id');
         var conf = confirm('Are you sure ?');
         if(conf){
            ajaxManager.addReq({
               type : "GET",
               url : site_url + 'requestor/monitoring/remove_attach',
               dataType : "JSON",
               data : {
                  id : id
               },
               error: function (jqXHR, status, errorThrown) {
                  error_handle(jqXHR, status, errorThrown);               
               },
               success: function(r){
                  if(r.success){
                     $('.atc-opt-'+id+'').remove();
                  }else{
                     toastr.error(r.msg);
                  }
               }
            });
         }else{
            return false;
         }
      });

	   $(this).on('change', '.opd_id', function(e) {
	   	var pedasi_id = $('option:selected', this).attr('pedasiid');
	  		var pedasi_name = $('option:selected', this).attr('pedasiname');
	  		var form = $('option:selected', this).attr('formrender');
	  		$(form).find('input[name="pedasi_id"]').val(pedasi_id);
	  		$(form).find('input[name="pedasi_name"]').val(pedasi_name);
	   });

	   $(this).on('click', '.btn-detail',function(e){
         var id = $(this).data('id');
         var form = $('#modal-detail');

         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'widget_detail/apd',
            dataType : "JSON",
            data : {
               id : id
            },
            beforeSend: function (xhr) {
               loading_button('.btn-detail', id, 'show', '<i class="fa fa-file"></i>', '');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_button('.btn-detail', id, 'hide', '<i class="fa fa-file"></i>', '');
            },
            success: function(r){
               session_checked(r._session, r._maintenance);
               form.find('.modal-body').html(r.html);
            },
            complete: function(){
               $("#modal-detail").modal("toggle");
               loading_button('.btn-detail', id, 'hide', '<i class="fa fa-file"></i>', '');
            }
         });
      });

    	$(this).on('submit', '#form-create', function(e){
	      var form = $(this);
	      $(this).ajaxSubmit({
	         url : site_url + 'requestor/monitoring/create',
	         type : "POST",
	         dataType : "JSON",
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown);
	            loading_form(form, 'hide', '<i class="fa fa-save"></i> Simpan');
	         },
	         beforeSend: function (xhr) {
	            loading_form(form, 'show', loadingbutton);
	         },
	         success: function(r) {
	         	session_checked(r._session, r._maintenance);
	            if(r.success){
	               _offset = 0;
	               _curpage = 1;
	               $(this).todolist();
	               toastr.success(r.msg);
	               $("#modal-create").modal("toggle");
	               form.resetForm();
	            }else{
	            	toastr.error(r.msg);
	            }
	            loading_form(form, 'hide', '<i class="fa fa-save"></i> Simpan');
	         },
	      });
	      e.preventDefault();
	   });

	   $(this).on('submit', '#form-edit', function(e){
	      var form = $(this);
	      $(this).ajaxSubmit({
	         url : site_url + 'requestor/monitoring/change',
	         type : "POST",
	         dataType : "JSON",
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown);
	            loading_form(form, 'hide', '<i class="fa fa-save"></i> Update');
	         },
	         beforeSend: function (xhr) {
	            loading_form(form, 'show', loadingbutton);
	         },
	         success: function(r) {
	         	session_checked(r._session, r._maintenance);
	            if(r.success){
	               _offset = 0;
	               _curpage = 1;
	               $(this).todolist();
	               toastr.success(r.msg);
	               form.resetForm();
	            }else{
	            	toastr.error(r.msg);
	            }
	         },
	         complete: function(){
	         	$("#modal-edit").modal("toggle");
	         	loading_form(form, 'hide', '<i class="fa fa-save"></i> Update');
	         }
	      });
	      e.preventDefault();
	   });

	   $.fn.render_picker = function (opt) {
	      var s = $.extend({
	         modal: '#modal-create',
	         form: '#form-create',
	      }, opt);

         $('.drp').daterangepicker({
            parentEl : opt.modal,
            autoUpdateInput: false,
            locale: {
               cancelLabel: 'Clear'
            },
            autoApply : true,
            singleDatePicker: true,
            locale: {
               format: 'DD/MMM/YYYY'
            },
            showDropdowns: true
         });

         $('.drp').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MMM/YYYY'));
            var trgt = $(this).data('trgt');
            $(opt.form).find('input[name="'+trgt+'"]').val(picker.startDate.format('YYYY-MM-DD'));
         });

         $('.drp-limit').daterangepicker({
            parentEl : opt.modal,
            autoUpdateInput: false,
            minDate : moment(),
            locale: {
               cancelLabel: 'Clear'
            },
            autoApply : true,
            singleDatePicker: true,
            locale: {
               format: 'DD/MMM/YYYY'
            },
            showDropdowns: true
         });

         $('.drp-limit').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MMM/YYYY'));
            var trgt = $(this).data('trgt');
            $(opt.form).find('input[name="'+trgt+'"]').val(picker.startDate.format('YYYY-MM-DD'));
         });
	   }

	   $(this).on('click', '.btn-create',function(e){
	      $(this).render_picker({
	         modal: '#modal-create',
	         form: '#form-create',
	      });
	      $(".choose").val('').trigger('change');
	   });

	   $(this).on('click', '.change_order', function(e){
	      e.preventDefault();
	      $('.change_order').html('<i class="fa fa-sort"></i>');
	      $(this).find('i').remove();
	      var sent = $(this).data('order');
	      var by = $(this).attr('data-by');
	      if(by === 'asc'){ 
	         $(this).attr('data-by', 'desc');
	         $(this).html('<i class="fa fa-sort-asc"></i>');
	      }
	      else{ 
	         $(this).attr('data-by', 'asc');
	         $(this).html(' <i class="fa fa-sort-desc"></i>');
	      }
	     $(this).getting({order:sent,orderby:by});
	   });

	   $(this).todolist();

   });

</script>