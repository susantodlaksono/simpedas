<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Monitoring extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('m_monitoring');
		$this->load->library('global_mapping');
      $this->load->library('mailing');
		$this->load->library('date_extraction');
   }

   public function index(){
   	$data['mopd'] = $this->m_monitoring->mapping_opd();
    	$data['_css'] = array(
         $this->_path_template.'/plugins/select2/select2.min.css',
         $this->_path_template.'/plugins/krajee-star-rating/star-rating.min.css',
         $this->_path_template.'/plugins/krajee-star-rating/theme.css'
      ); 
      $data['_js'] = array(
         $this->_path_template.'/plugins/select2/select2.full.min.js',
         $this->_path_template.'/plugins/tinymce/tinymce.min.js',
         $this->_path_template.'/plugins/krajee-star-rating/star-rating.min.js',
		);	
      $this->render_page($data, 'monitoring', 'modular');
   }

   public function getting(){
      $list = array();
      $data = $this->m_monitoring->getting('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'requested_date' => date('d M Y', strtotime($v['requested_date'])),
               'requested_time' => date('H:i:s', strtotime($v['requested_date'])),
               'title' => $v['title'],
               'opd' => $v['opd'],
               'opd_id' => $v['opd_id'],
               'total_files' => $v['total_files'],
               'request_rating' => $v['request_rating'],
               'pedasi' => $v['pedasi'],
               'status_id' => $v['status'],
               'status_name' => $this->global_mapping->request_status($v['status'], 'label-status-table '),
               'status_pengolahan_name' => $this->global_mapping->request_status_pengolahan($v['status_pengolahan'], 'label-status-table '),
               'disposisi' => $v['disposisi'],
               'disposisi_created' => $v['disposisi_kasie_created'] ? $this->date_extraction->time_ago($v['disposisi_kasie_created']) : NULL,
               'start_date' => date('d M Y', strtotime($v['start_date'])),
               'end_date' => date('d M Y', strtotime($v['end_date'])),
               'days_left' => $v['limit_date'] ? $this->date_extraction->left_days_from_now($v['limit_date']) : NULL,
               'limit_date' => date('d M Y', strtotime($v['limit_date']))
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->m_monitoring->getting('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

   public function create(){
      $response['success'] = FALSE;
      if($this->_post['end_date'] < $this->_post['start_date']){
         $response['msg'] = 'Tgl akhir periode data tidak boleh kurang dari tgl awal periode';
      }else{
         $this->form_validation->set_rules('title', 'Permohonan data', 'required');
         $this->form_validation->set_rules('start_date', 'Start periode data', 'required');
         $this->form_validation->set_rules('end_date', 'End periode data', 'required');
         $this->form_validation->set_rules('limit_date', 'Batas waktu pengumpulan', 'required');
         if($this->form_validation->run()){
            $result = $this->m_monitoring->create($this->_post, $this->_user->id);
            $response['success'] = $result['success'];
            $response['msg'] = $result['msg'];
         }else{
            $response['msg'] = validation_errors();
         }
      }
      $this->json_result($response);
   }

   public function change(){
      $response['success'] = FALSE;
      $this->form_validation->set_rules('title', 'Permohonan data', 'required');
      $this->form_validation->set_rules('start_date', 'Start periode data', 'required');
      $this->form_validation->set_rules('end_date', 'End periode data', 'required');
      $this->form_validation->set_rules('limit_date', 'Batas waktu pengumpulan', 'required');
      if($this->form_validation->run()){
         $result = $this->m_monitoring->change($this->_post, $this->_user->id);
         $response['success'] = $result['success'];
         $response['msg'] = $result['msg'];
      }else{
         $response['msg'] = validation_errors();
      }
      $this->json_result($response);
   }

   public function detail(){
   	$response['request'] = $this->m_monitoring->request_detail($this->_get['id']);
      $response['attachments'] = $this->global_mapping->get_attachments_request($this->_get['id']);
      $response['status_label'] = $this->global_mapping->request_status($response['request']['status'], 'label-status ');
      $this->json_result($response);
   }

   public function modify(){
      $this->db->select('a.*, b.first_name as pedasi_name');
      $this->db->join('users as b', 'a.pedasi_id = b.id', 'left');
      $this->db->where('a.id', $this->_get['id']);
      $result = $this->db->get('request as a')->row_array();

      $response['request'] = $result;
      $response['attachments'] = $this->global_mapping->get_attachments_request($this->_get['id']);
      $this->json_result($response);
   }

   public function remove_attach(){
      $response['success'] = FALSE;
      if(isset($this->_get['id']) && $this->_get['id'] != ''){
         $attach = $this->db->where('id', $this->_get['id'])->get('request_attach')->row_array();
         if ($this->_file_exists('./'.$attach['file_dir'].$attach['file_name'].'')) {
            $this->load->helper("file");
            unlink('./'.$attach['file_dir'].'/'.$attach['file_name'].'');
            $this->db->delete('request_attach', array('id' => $this->_get['id']));
            $response['success'] = TRUE;
         }else{
            $response['msg'] = 'Check file failed';   
         }
      }else{
         $response['msg'] = 'Function failed';
      }
      $this->json_result($response); 
   }

   public function delete(){
      $response['success'] = FALSE;
      $response['msg'] = 'Some error occured';

      $result = $this->db->where('req_id', $this->_get['id'])->get('request_attach')->result_array();
      $this->db->trans_start();
      if($result){
         foreach ($result as $attach) {
            if ($this->_file_exists('./'.$attach['file_dir'].$attach['file_name'].'')) {
               unlink('./'.$attach['file_dir'].'/'.$attach['file_name'].'');
            }
         }
         $this->db->delete('request', array('id' => $this->_get['id']));
      }else{
         $this->db->delete('request', array('id' => $this->_get['id']));
      }
      $this->db->trans_complete();
      if($this->db->trans_status() === TRUE){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['msg'] = 'Data berhasil di hapus';
      }else{
         $this->db->trans_rollback();
      }
      $this->json_result($response);
   }

   public function show_files(){
      $response['result'] = $this->m_monitoring->files_request($this->_get['id']);
      $this->json_result($response);
   }

   protected function _file_exists($filePath){
      return is_file($filePath) && file_exists($filePath);
   }

}