<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_monitoring extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function getting($mode, $params, $user_id_active){
      $this->db->select('a.*');
      $this->db->select('b.name as opd');
      $this->db->select('c.first_name as pedasi');
      $this->db->select('d.first_name as disposisi');
      $this->db->select('e.total_files');

      $this->db->join('opd as b', 'a.opd_id = b.id', 'left');
      $this->db->join('users as c', 'a.pedasi_id = c.id', 'left');
      $this->db->join('users as d', 'a.disposisi_kasie = d.id', 'left');
      $this->db->join('(select req_id, count(id) as total_files from request_result_files
                        group by req_id) as e', 
                        'a.id = e.req_id', 'left');

      $this->db->where('requested_by', $user_id_active);

      if($params['filt_status'] != 'all'){
         $this->db->where('a.status', $params['filt_status']);
      }

      if($params['filt_keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.title', $params['filt_keyword']);
         $this->db->or_like('b.name', $params['filt_keyword']);
         $this->db->or_like('c.first_name', $params['filt_keyword']);
         $this->db->or_like('d.first_name', $params['filt_keyword']);
         $this->db->group_end();
      }
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('request as a', $params['limit'], $params['offset'])->result_array();
         case 'count':
            return $this->db->get('request as a')->num_rows();
      }
   }
   
	public function mapping_opd(){
		$this->db->select('b.user_id, c.first_name, a.id, a.name');
		$this->db->join('mapping_opd as b', 'a.id = b.opd_id AND group_id = 5', 'left');
		$this->db->join('users as c', 'b.user_id = c.id', 'left');
		return $this->db->get('opd as a')->result_array();
	}

   public function request_detail($id){
      $this->db->select('a.*, b.first_name as kompilasi_sender, c.first_name as kompilasi_kasie, d.first_name as pedasi_name');
      $this->db->select('e.name as opd_name');
      $this->db->join('users as b', 'a.disposisi_assignby = b.id', 'left');
      $this->db->join('users as c', 'a.disposisi_kasie = c.id', 'left');
      $this->db->join('users as d', 'a.pedasi_id = d.id', 'left');
      $this->db->join('opd as e', 'a.opd_id = e.id', 'left');
      $this->db->where('a.id', $id);
      return $this->db->get('request as a')->row_array();
   }

	public function create($params, $uid){
		$st_upload = TRUE;
		$req['title'] = $params['title'];
		$req['opd_id'] = $params['opd_id'];
		$req['pedasi_id'] = $params['pedasi_id'];
		$req['start_date'] = $params['start_date'];
		$req['end_date'] = $params['end_date'];
		$req['limit_date'] = $params['limit_date'];
		$req['description'] = $params['description'];
		$req['requested_by'] = $uid;
		$req['requested_date'] = date('Y-m-d H:i:s');

	 	$this->db->trans_start();
      
      $this->db->insert('request', $req);
      $lastid = $this->db->insert_id();

      $config['upload_path']  = 'files/request/';
      $config['allowed_types'] = '*';
      $this->load->library('upload', $config);
      if(isset($_FILES['file_path'])){
      	$count_attach = count($_FILES['file_path']['name']);
      	if($count_attach > 0){
            for($i = 0; $i < $count_attach; $i++){
               if(!empty($_FILES['file_path']['name'][$i])){
                  $filename = uniqid();
                  $name = $_FILES['file_path']['name'][$i];
                  $ext = pathinfo($name, PATHINFO_EXTENSION);

                  $_FILES['file']['name'] = $filename.'.'.$ext;
                  $_FILES['file']['type'] = $_FILES['file_path']['type'][$i];
                  $_FILES['file']['tmp_name'] = $_FILES['file_path']['tmp_name'][$i];
                  $_FILES['file']['error'] = $_FILES['file_path']['error'][$i];
                  $_FILES['file']['size'] = $_FILES['file_path']['size'][$i];
                  
                  if($this->upload->do_upload('file')){
                     $uploadresult = $this->upload->data();
                     $tmp['req_id'] = $lastid;
                     $tmp['file_name'] = $uploadresult['file_name'];
                     $tmp['file_dir'] = $config['upload_path'];
                     $tmp['file_extension'] = $uploadresult['file_ext'];
                     $tmp['file_orig_name'] =  $_FILES['file_path']['name'][$i];
                     $this->db->insert('request_attach', $tmp);
                     $permission = $uploadresult['full_path']; // get file path
                     chmod($permission, 0777); // CHMOD file or any other permission level(s)
                  }else{
                     $st_upload = FALSE;
                  }
               }
            }
         }
   	}
   	$this->db->trans_complete();
         
      if($this->db->trans_status() === TRUE && $st_upload === TRUE){
         $mailing = $this->mailing->request_data($lastid);
         if($mailing['status']){
            $this->db->trans_commit();
            return array(
               'success' => TRUE,
               'msg' => 'Data berhasil ditambahkan',
            );
         }else{
            $this->db->trans_rollback();   
            return array(
               'success' => FALSE,
               'msg' => $mailing['msg']
            );
         }
      }else{
         $this->db->trans_rollback();
         if($this->db->trans_status() === FALSE){
         	return array(
	            'success' => FALSE,
	            'msg' => 'Function failed',
	         );
         }
         if($st_upload === FALSE){
         	return array(
	            'success' => FALSE,
	            'msg' => 'Tipe file yang tidak sesuai'
	         );
         }
      }

	}

   public function change($params, $uid){
      $st_upload = TRUE;
      $req['title'] = $params['title'];
      $req['opd_id'] = $params['opd_id'];
      $req['pedasi_id'] = $params['pedasi_id'];
      $req['start_date'] = $params['start_date'];
      $req['end_date'] = $params['end_date'];
      $req['limit_date'] = $params['limit_date'];
      $req['description'] = $params['description'];
      $req['requested_by'] = $uid;
      $req['requested_date'] = date('Y-m-d H:i:s');

      $this->db->trans_start();
      
      $this->db->update('request', $req, array('id' => $params['id']));
      $lastid = $this->db->insert_id();

      $config['upload_path']  = 'files/request/';
      $config['allowed_types'] = '*';
      $this->load->library('upload', $config);
      if(isset($_FILES['file_path'])){
         $count_attach = count($_FILES['file_path']['name']);
         if($count_attach > 0){
            for($i = 0; $i < $count_attach; $i++){
               if(!empty($_FILES['file_path']['name'][$i])){
                  $filename = uniqid();
                  $name = $_FILES['file_path']['name'][$i];
                  $ext = pathinfo($name, PATHINFO_EXTENSION);

                  $_FILES['file']['name'] = $filename.'.'.$ext;
                  $_FILES['file']['type'] = $_FILES['file_path']['type'][$i];
                  $_FILES['file']['tmp_name'] = $_FILES['file_path']['tmp_name'][$i];
                  $_FILES['file']['error'] = $_FILES['file_path']['error'][$i];
                  $_FILES['file']['size'] = $_FILES['file_path']['size'][$i];
                  
                  if($this->upload->do_upload('file')){
                     $uploadresult = $this->upload->data();
                     $tmp['req_id'] = $params['id'];
                     $tmp['file_name'] = $uploadresult['file_name'];
                     $tmp['file_dir'] = $config['upload_path'];
                     $tmp['file_extension'] = $uploadresult['file_ext'];
                     $tmp['file_orig_name'] =  $_FILES['file_path']['name'][$i];
                     $this->db->insert('request_attach', $tmp);
                     $permission = $uploadresult['full_path']; // get file path
                     chmod($permission, 0777); // CHMOD file or any other permission level(s)
                  }else{
                     $st_upload = FALSE;
                  }
               }
            }
         }
      }
      $this->db->trans_complete();
         
      if($this->db->trans_status() === TRUE && $st_upload === TRUE){
         $this->db->trans_commit();
         return array(
            'success' => TRUE,
            'msg' => 'Data berhasil diperbaharui',
         );
      }else{
         $this->db->trans_rollback();
         if($this->db->trans_status() === FALSE){
            return array(
               'success' => FALSE,
               'msg' => 'Function failed',
            );
         }
         if($st_upload === FALSE){
            return array(
               'success' => FALSE,
               'msg' => 'Ada tipe file yang tidak sesuai'
            );
         }
      }

   }

   public function files_request($id){
      $this->db->select('a.*, b.first_name as pic');
      $this->db->join('users as b', 'a.created_by = b.id', 'left');
      $this->db->where('req_id', $id);
      return $this->db->get('request_result_files as a')->result_array();
   }

}