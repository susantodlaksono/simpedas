<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Sop extends MY_Controller {

	public function __construct() {
		parent::__construct();
   }

   public function index(){
    	$data = array(
         'link' => 'https://opendatabdg.gitbook.io/pedasi/cara-kerja/bagaimana-pedasi-bekerja/prosedur-operasional'
      );
      $this->render_page($data, 'sop', 'modular');
   }
}