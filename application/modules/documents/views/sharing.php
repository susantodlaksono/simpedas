<style type="text/css">
	.act-selected{
	 	font-size: 13px;
	 	font-weight: bold;
	 	border: 1px solid #e3e3e3;
	 	padding: 5px;
	 	border-radius: 2px;
	}	
	.breadcrumb-action{
		cursor:pointer;font-size: 13px;
	}
	.dropdown-menu > li > a{
		padding:5px;
		text-align: center;
		font-weight: 500;
		color:#7a7a7a;
		font-size: 12px;
	}
	.mailbox-name a{
		color:#333;
		cursor: pointer;
	}
	.dropdown-menu-act{
		left: 0px;top: 19px;border: 1px solid #e3e3e3;min-width: 143px;border-top: 0px;border-radius: unset;background-color: #fbfbfb;
	}
	.popover{
    	top: 17.5px;
    	left: 428.625px;
 		display: block;
    	max-width: 600px;
	}
</style>

<?php
$_uniqid = uniqid();
?>

<div id="<?php echo $_uniqid ?>">

	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb sect-nav-render" style="border-radius: 0">
				<li><a data-href="" class="breadcrumb-action"><i class="fa fa-home"></i></a></li>
			</ol>
		</div>
	</div>

	<div class="row row-options-selected">
		<div class="col-md-12 text-center">
			<span class="dropdown">
				<a href="" class="act-selected dropdown-toggle" data-toggle="dropdown">
					Tindakan file terpilih <i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu dropdown-menu-act">
					<li>
						<a class="download-bulk dropdown-options-dms" href="">
							Download
						</a>
					</li>
				</ul>
			</span>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="mailbox-messages">
	    		<table class="table table-hover table-condensed" style="font-size: 13px;">
	    			<thead>	
	    				<th width="10"><input type="checkbox" class="option_choose"></th>
	    				<th>Nama File <a class="co" href="#" data-order="name" data-by="desc"><i class="fa fa-sort"></i></a></th>
	    				<th width="10"></th>
	    				<th width="120">Waktu <a class="co" href="#" data-order="clean_time" data-by="asc"><i class="fa fa-sort-desc"></i></a></th>
	    				<th width="120">Ukuran <a class="co" href="#" data-order="size_bytes" data-by="desc"><i class="fa fa-sort"></i></a></th>
	    				<th width="120">Dari <a class="co" href="#" data-order="shared_name" data-by="desc"><i class="fa fa-sort"></i></a></th>
	    				<th width="120">OPD <a class="co" href="#" data-order="opd_name" data-by="desc"><i class="fa fa-sort"></i></a></th>
	 				</thead>
	    			<tbody class="sect-data"></tbody>
	 			</table>
			</div>
		</div>
	</div>

	<div class="modal fade in open" id="modal-assign-files">
	 	<div class="modal-dialog">
	     	<div class="modal-content" style="width: 1000px;margin-left: -195px;">
	         <div class="modal-header">
	         	<div class="row">
	         		<div class="col-md-8">
	         			<h4 class="modal-title">Lampirkan File Terpilih</h4>
         			</div>
         			<div class="col-md-4">
         				<div class="box-tools pull-right">
			         		<div class="btn-group" data-toggle="buttons">
									<input type="text" class="form-control input-sm keywordreq" id="keywordreq" placeholder="Pencarian...">
								</div>
		         		</div>
         			</div>
         		</div>
	         </div>
	         <form class="form-assign-files">
         	<div class="modal-body">
      			<table class="table table-striped table-condensed" style="font-size:12px;">
						<thead>
							<th></th>
							<th>Tgl</th>
							<th>Permohonan Data</th>
							<th>Mulai</th>
							<th>Selesai</th>
							<th>Batas Waktu</th>
							<th>Status</th>
							<th></th>
						</thead>
	          		<tbody class="sect-data-request-files"></tbody>
					</table>
					<div class="row">
	               <div class="col-md-2">
	             		<h6 class="sect-total-request-files"></h6>
	               </div>
	               <div class="col-md-10 text-right">
	                  <ul class="pagination sect-pagination-request-files pagination-sm no-margin pull-right"></ul>
	               </div>
	            </div>
      		</div>
      		<div class="modal-footer">
              	<button type="button" class="btn btn-default btn-sm" class="close" data-dismiss="modal">Tutup</button>
              	<button type="submit" class="btn btn-primary btn-sm">Lampirkan</button>
             </div>
	   		</form>
			</div>
		</div>
	</div>

	<div class="modal fade in" id="modal-share-doc">
	 	<div class="modal-dialog">
	     	<div class="modal-content" style="width: 750px;margin-left: -68px;">
	         <div class="modal-header">
	         	<div class="row">
	         		<div class="col-md-8">
	         			<h4 class="modal-title">Share File Terpilih</h4>
         			</div>
         			<div class="col-md-4">
         				<div class="box-tools pull-right">
			         		<div class="btn-group" data-toggle="buttons">
									<input type="text" class="form-control input-sm keywordopd" id="keywordopd" placeholder="Pencarian...">
								</div>
		         		</div>
         			</div>
         		</div>
	         </div>
	         <form class="form-share-doc">
	         	<div class="modal-body">
	      			<table class="table table-striped table-condensed" style="font-size:12px;">
							<thead>
								<th></th>
								<th>Name</th>
								<th>No.Telp</th>
								<th>Deskripsi</th>
							</thead>
		          		<tbody class="sect-data-share-opd"></tbody>
						</table>
						<div class="row">
		               <div class="col-md-2">
		             		<h6 class="sect-total-share-opd"></h6>
		               </div>
		               <div class="col-md-10 text-right">
		                  <ul class="pagination sect-pagination-share-opd pagination-sm no-margin pull-right"></ul>
		               </div>
		            </div>
	      		</div>
	      		<div class="modal-footer">
	      			<button type="button" class="btn btn-default btn-sm" class="close" data-dismiss="modal">Tutup</button>
	              	<button type="submit" class="btn btn-primary btn-sm">Share</button>
	             </div>
	   		</form>
			</div>
		</div>
	</div>

	<div class="modal fade in" id="modal-detail-assign">
	 	<div class="modal-dialog">
	     	<div class="modal-content" style="width: 1000px;margin-left: -195px;">
	         <div class="modal-header">
	         	<button type="button" class="close" data-dismiss="modal">
	         		<span aria-hidden="true">×</span>
	         	</button>
	         	<h4 class="modal-title">Detail Assign Request</h4>
	         </div>
	         <form class="form-detail-assign">
	         	<div class="modal-body">
      			<table class="table table-striped table-condensed" style="font-size:12px;">
						<thead>
							<th></th>
							<th>Tgl</th>
							<th>Permohonan Data</th>
							<th>Mulai</th>
							<th>Selesai</th>
							<th>Batas Waktu</th>
							<th>Status</th>
						</thead>
	          		<tbody class="sect-data-assign-detail"></tbody>
					</table>
      		</div>
      		<div class="modal-footer">
              	<button type="submit" class="btn btn-danger btn-sm">Hapus file dari request yang ditandai</button>
             </div>
	   		</form>
			</div>
		</div>
	</div>

	<div class="modal fade in" id="modal-detail-share">
	 	<div class="modal-dialog">
	     	<div class="modal-content" style="width: 1000px;margin-left: -195px;">
	         <div class="modal-header">
	         	<button type="button" class="close" data-dismiss="modal">
	         		<span aria-hidden="true">×</span>
	         	</button>
	         	<h4 class="modal-title">Detail Share</h4>
	         </div>
	         <form class="form-detail-share">
	         	<div class="modal-body">
      			<table class="table table-striped table-condensed" style="font-size:12px;">
						<thead>
							<th></th>
							<th>Name</th>
							<th>No.Telp</th>
							<th>Deskripsi</th>
						</thead>
	          		<tbody class="sect-data-share-detail"></tbody>
					</table>
      		</div>
      		<div class="modal-footer">
              	<button type="submit" class="btn btn-danger btn-sm">Hapus file dari opd yang ditandai</button>
             </div>
	   		</form>
			</div>
		</div>
	</div>
	
</div>

<script type="text/javascript">

	_first_load = '<?php echo $first_load ?>';
	_uniqid = '<?php echo $_uniqid ?>';

	$(function () {

		arrfiles = [];
		fav_id = [];

		$.fn.reset_breadcrumb = function(){
			t = '<li><a data-href="" class="breadcrumb-action"><i class="fa fa-home"></i></a></li>';
			$('#'+_uniqid+' .sect-nav-render').html(t);
		}
		
		$.fn.bulk_option_reset = function(){
			arrfiles = [];
			fav_id = [];
			$('.option_check').each(function(){
	         this.checked = false; 
	      });
	      $('.option_choose').prop('checked', false);
			$('.row-options-selected').addClass('hidden');
		}

	});
</script>

<script src="<?php echo $path_scripts ?>/documents/sharing/getting.js?<?php echo time() ?>"></script>
<script src="<?php echo $path_scripts ?>/documents/sharing/actions.js?<?php echo time() ?>"></script>
<script src="<?php echo $path_scripts ?>/documents/sharing/form_actions.js?<?php echo time() ?>"></script>