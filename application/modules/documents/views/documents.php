<div class="row">
	<div class="col-md-3">
		<div class="box box-solid" id="cont-dms-menu">
			<div class="box-header">
           <h3 class="box-title">Folders</h3>
           	<div class="box-tools">
             	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
           	</div>
      	</div>
       	<div class="box-body table-responsive no-padding">
       		<ul class="nav nav-pills nav-stacked">
       			<li class="widget-render active" data-load="myfiles"><a href=""><i class="fa fa-folder"></i> Semua File</a></li>
       			<li class="widget-render" data-load="favorites"><a href=""><i class="fa fa-star"></i> Favorit</a></li>
               <li class="widget-render" data-load="sharing"><a href=""><i class="fa fa-share-alt"></i> Sharing</a></li>
    			</ul>
    		</div>
 		</div>
	</div>
	<div class="col-md-9">
		<div class="box box-solid" id="cont-widget-dms">
			<div class="box-header">
        		<h3 class="box-title"><i class="fa fa-folder"></i> Semua File</h3>
     		</div>
     		<div class="box-body cont-result-widget" style="height: 419px;overflow-y: auto;overflow-x: hidden;"></div>
		</div>
	</div>
</div>

<script type="text/javascript">
   $(function () {

      ajaxManager.addReq({
         type : "GET",
         url : site_url + 'documents/myfiles',
         data : {
            first_load : true
         },
         dataType : "JSON",
         beforeSend: function(){
            spinner('#cont-widget-dms', 'block');
         },
         error: function(jqXHR, status, errorThrown){
            error_handle(jqXHR, status, errorThrown); 
         },
         success : function(r){
            $('.cont-result-widget').html(r.html);
         }
      });

      $(this).on('click', '.widget-render',function(e){
         e.preventDefault();
         var load = $(this).data('load');
         var title = $(this).find('a').html();
         $('#cont-widget-dms').find('.box-title').html(title);
         $('.nav-stacked').find('li').removeClass('active');
         $(this).addClass('active');
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'documents/'+load+'',
            data : {
               first_load : true
            },
            dataType : "JSON",
            beforeSend: function(){
               spinner('#cont-widget-dms', 'block');
            },
            error: function(jqXHR, status, errorThrown){
               error_handle(jqXHR, status, errorThrown); 
            },
            success : function(r){
               $('.cont-result-widget').html(r.html);
            }
         });
      });

   });
</script>