<style type="text/css">
	.act-selected{
	 	font-size: 13px;
	 	font-weight: bold;
	 	border: 1px solid #e3e3e3;
	 	padding: 5px;
	 	border-radius: 2px;
	}	
	.breadcrumb-action{
		cursor:pointer;font-size: 13px;
	}
	.dropdown-menu > li > a{
		padding:5px;
		text-align: center;
		font-weight: 500;
		color:#7a7a7a;
		font-size: 12px;
	}
	.mailbox-name a{
		color:#333;
		cursor: pointer;
	}
	.dropdown-menu-act{
		left: 0px;top: 19px;border: 1px solid #e3e3e3;min-width: 143px;border-top: 0px;border-radius: unset;background-color: #fbfbfb;
	}
	.popover{
    	top: 17.5px;
    	left: 428.625px;
 		display: block;
    	max-width: 600px;
	}
</style>

<?php
$_uniqid = uniqid();
?>

<div id="<?php echo $_uniqid ?>">

	<div class="row">
		<div class="col-md-11">
			<ol class="breadcrumb sect-nav-render" style="border-radius: 0">
				<li><a data-href="" class="breadcrumb-action"><i class="fa fa-home"></i></a></li>
			</ol>
		</div>
		<div class="col-md-1">
			<div class="box-tools">
				<div class="btn-group" style="width: 80px;margin-left: -25px;">
					<button class="btn btn-sm" data-toggle="dropdown" style="height: 36px;border-radius: 0;background-color: #f5f5f5;">
			  			<i class="fa fa-plus"></i>
					</button>
			    	<ul class="dropdown-menu" style="left:-82px;border: 1px solid #cccccc;min-width: 120px;">
			    		<li>
			    			<a href="" data-toggle="modal" data-target="#modal-upload-file">
			    				Upload File
			    			</a>
			    		</li>
			    		<li class="divider"></li>
			    		<li>
			    			<a class="btn-upload-folder" href="" data-toggle="modal" data-target="#modal-upload-folder">
			 					Tambah Folder
			 				</a>
			 			</li>
			 		</ul>
			 		<button class="btn btn-sm" data-toggle="modal" data-target="#modal-search-files" style="height: 36px;border-radius: 0;background-color: #f5f5f5;">
			  			<i class="fa fa-search"></i>
					</button>
				</div>
			</div>
		</div>
	</div>

	<div class="row row-options-selected">
		<div class="col-md-12 text-center">
			<span class="dropdown">
				<a href="" class="act-selected dropdown-toggle" data-toggle="dropdown">
					Tindakan file terpilih <i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu dropdown-menu-act">
					<?php
					if(in_array(5, $role_id)){
					?>
					<li>
						<a class="assign-request dropdown-options-dms" data-toggle="modal" data-target="#modal-assign-files" href="">
							Lampirkan
						</a>
					</li>
					<li class="divider"></li>
					<?php
					}
					?>
					<li>
						<a class="assign-flag-bulk dropdown-options-dms" href="" data-st="3">
							Simpan ke favorit
						</a>
					</li>
					<!-- <li class="divider"></li>
					<li>
						<a class="assign-flag-bulk dropdown-options-dms" href="" data-st="1">
							Kunci dokumen
						</a>
					</li> -->
					<li class="divider"></li>
					<li>
						<a class="download-bulk dropdown-options-dms" href="">
							Download
						</a>
					</li>
					<li class="divider"></li>
					<li>
						<a class="share-doc dropdown-options-dms" href="" data-toggle="modal" data-target="#modal-share-doc">
							Share dokumen
						</a>
					</li>
					<li class="divider"></li>
					<li>
						<a class="remove-flag-bulk dropdown-options-dms text-red" href="">
							Hapus
						</a>
					</li>
				</ul>
			</span>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="mailbox-messages">
	    		<table class="table table-hover table-condensed" style="font-size: 13px;">
	    			<thead>	
	    				<th width="10"><input type="checkbox" class="option_choose"></th>
	    				<th>Nama File <a class="co" href="#" data-order="name" data-by="desc"><i class="fa fa-sort"></i></a></th>
	    				<th width="70"></th>
	    				<th width="10"></th>
	    				<th width="120">Waktu <a class="co" href="#" data-order="clean_time" data-by="asc"><i class="fa fa-sort-desc"></i></a></th>
	    				<th width="120">Ukuran <a class="co" href="#" data-order="size_bytes" data-by="desc"><i class="fa fa-sort"></i></a></th>
	    				<th width="10" class="text-center">Share</th>
	    				<?php
						if(in_array(5, $role_id)){
						?>
	    				<th width="10" class="text-center">Assign</th>
	    				<?php
	    				}
	    				?>
	 				</thead>
	    			<tbody class="sect-data"></tbody>
	 			</table>
			</div>
		</div>
	</div>

	<div class="modal fade in" id="modal-upload-folder">
	 	<div class="modal-dialog">
	     	<div class="modal-content" style="width: 350px;margin-left: 110px;">
	     		<div class="modal-header">
	         	<button type="button" class="close" data-dismiss="modal">
	         		<span aria-hidden="true">×</span>
	         	</button>
	         	<h4 class="modal-title">Tambah Folder</h4>
	         </div>
	         <form class="form-upload-folder">                
	         	<div class="modal-body">
	         		<div class="form-group">
	         			<input type="text" class="form-control input-sm" name="folder_name[]" placeholder="Nama folder..." required="">
	      			</div>
	         		<a href="" class="add-more-folder"><h6><i class="fa fa-plus"></i> Tambah lainnya</h6></a>
	      		</div>
	      		<div class="modal-footer">
	              	<button type="submit" class="btn btn-primary btn-block">Simpan</button>
	             </div>
	   		</form>
			</div>
		</div>
	</div>

	<div class="modal fade in" id="modal-rename">
	 	<div class="modal-dialog">
	     	<div class="modal-content" style="width: 350px;margin-left: 110px;">
	     		<div class="modal-header">
	         	<button type="button" class="close" data-dismiss="modal">
	         		<span aria-hidden="true">×</span>
	         	</button>
	         	<h4 class="modal-title modal-title-rename" style="font-size:13px;">s</h4>
	         </div>
	         <form class="form-rename">                
	         	<input type="hidden" name="dir">
	         	<input type="hidden" name="name">
	         	<input type="hidden" name="fav">
	         	<input type="hidden" name="ext">
	         	<div class="modal-body">
	         		<div class="form-group">
	         			<input type="text" class="form-control input-sm" name="rename" required="">
	      			</div>
	      		</div>
	      		<div class="modal-footer">
	              	<button type="submit" class="btn btn-primary btn-block">Rename</button>
	             </div>
	   		</form>
			</div>
		</div>
	</div>

	<div class="modal fade in open" id="modal-search-files">
	 	<div class="modal-dialog">
	     	<div class="modal-content" style="width: 750px;margin-left: -58px;">
	     		<div class="modal-header">
	         	<button type="button" class="close" data-dismiss="modal">
	         		<span aria-hidden="true">×</span>
	         	</button>
	         	<h4 class="modal-title">Pencarian File</h4>
	         </div>
	         <div class="modal-header">
	         	<div class="box-tools">
	         		<div class="btn-group" style="width: 100%;">
	         			<input type="text" class="form-control input-sm search_file" placeholder="Pencarian...">
         			</div>
         		</div>
	         </div>
         	<div class="modal-body sect-data-search">
      		</div>
			</div>
		</div>
	</div>

	<div class="modal fade in" id="modal-upload-file">
	 	<div class="modal-dialog">
	     	<div class="modal-content" style="width: 900px;margin-left: -139px;">
	     		<div class="modal-header">
	         	<button type="button" class="close" data-dismiss="modal">
	         		<span aria-hidden="true">×</span>
	         	</button>
	         	<h4 class="modal-title">Upload File</h4>
	         </div>
         	<div class="modal-body">
         		<div class="row">
         			<div class="col-md-6 col-sm-12">
         				<div class="drag-and-drop-zone dm-uploader text-center" style="border: 0.25rem dashed #A5A5C7;padding: 5rem!important;border: 0.25rem dashed #A5A5C7;">
				            <h4 class="text-muted" style="margin-bottom: 3rem!important;margin-top: 5rem!important;">
				            	Drag &amp; drop files here
				         	</h4>
				            <div class="btn btn-primary btn-block" style="margin-bottom: 3rem!important;">
			                	<span>Open the file Browser</span>
			                	<input type="file" title='Click to add Files' />
				            </div>
			          	</div>
      				</div>
      				<div class="col-md-6 col-sm-12">
      					<div class="box box-default box-solid">
				            <div class="box-header">
				            	<h6 class="box-title" style="font-size: 15px;"><i class="fa fa-upload"></i> Status Upload</h6>
				            	<div class="box-tools">
					           		<button class="btn btn-default clear-list-upload">
					           			<i class="fa fa-trash"></i> Bersihkan List
					        			</button>
				        			</div>
				         	</div>
				         	<div class="box-body">
				         		<ul class="list-unstyled p-2 d-flex flex-column col" id="files" style="overflow:auto;height: 208px;">
				              		<li class="text-muted text-center empty" style="margin-top: 25%">Tidak ada file yang di upload.</li>
				            	</ul>
			         		</div>
			          	</div>
			        	</div>
      			</div>
      		</div>
			</div>
		</div>
	</div>

	<div class="modal fade in open" id="modal-assign-files">
	 	<div class="modal-dialog">
	     	<div class="modal-content" style="width: 1000px;margin-left: -195px;">
	         <div class="modal-header">
	         	<div class="row">
	         		<div class="col-md-8">
	         			<h4 class="modal-title">Lampirkan File Terpilih</h4>
         			</div>
         			<div class="col-md-4">
         				<div class="box-tools pull-right">
			         		<div class="btn-group" data-toggle="buttons">
									<input type="text" class="form-control input-sm keywordreq" id="keywordreq" placeholder="Pencarian...">
								</div>
		         		</div>
         			</div>
         		</div>
	         </div>
	         <form class="form-assign-files">
         	<div class="modal-body">
      			<table class="table table-striped table-condensed" style="font-size:12px;">
						<thead>
							<th></th>
							<th>Tgl</th>
							<th>Permohonan Data</th>
							<th>Mulai</th>
							<th>Selesai</th>
							<th>Batas Waktu</th>
							<th>Status</th>
							<th></th>
						</thead>
	          		<tbody class="sect-data-request-files"></tbody>
					</table>
					<div class="row">
	               <div class="col-md-2">
	             		<h6 class="sect-total-request-files"></h6>
	               </div>
	               <div class="col-md-10 text-right">
	                  <ul class="pagination sect-pagination-request-files pagination-sm no-margin pull-right"></ul>
	               </div>
	            </div>
      		</div>
      		<div class="modal-footer">
              	<button type="button" class="btn btn-default btn-sm" class="close" data-dismiss="modal">Tutup</button>
              	<button type="submit" class="btn btn-primary btn-sm">Lampirkan</button>
             </div>
	   		</form>
			</div>
		</div>
	</div>

	<div class="modal fade in" id="modal-share-doc">
	 	<div class="modal-dialog">
	     	<div class="modal-content" style="width: 750px;margin-left: -68px;">
	         <div class="modal-header">
	         	<div class="row">
	         		<div class="col-md-8">
	         			<h4 class="modal-title">Share File Terpilih</h4>
         			</div>
         			<div class="col-md-4">
         				<div class="box-tools pull-right">
			         		<div class="btn-group" data-toggle="buttons">
									<input type="text" class="form-control input-sm keywordopd" id="keywordopd" placeholder="Pencarian...">
								</div>
		         		</div>
         			</div>
         		</div>
	         </div>
	         <form class="form-share-doc">
	         	<div class="modal-body">
	      			<table class="table table-striped table-condensed" style="font-size:12px;">
							<thead>
								<th></th>
								<th>Name</th>
								<th>No.Telp</th>
								<th>Deskripsi</th>
							</thead>
		          		<tbody class="sect-data-share-opd"></tbody>
						</table>
						<div class="row">
		               <div class="col-md-2">
		             		<h6 class="sect-total-share-opd"></h6>
		               </div>
		               <div class="col-md-10 text-right">
		                  <ul class="pagination sect-pagination-share-opd pagination-sm no-margin pull-right"></ul>
		               </div>
		            </div>
	      		</div>
	      		<div class="modal-footer">
	      			<button type="button" class="btn btn-default btn-sm" class="close" data-dismiss="modal">Tutup</button>
	              	<button type="submit" class="btn btn-primary btn-sm">Share</button>
	             </div>
	   		</form>
			</div>
		</div>
	</div>

	<div class="modal fade in" id="modal-detail-assign">
	 	<div class="modal-dialog">
	     	<div class="modal-content" style="width: 1000px;margin-left: -195px;">
	         <div class="modal-header">
	         	<button type="button" class="close" data-dismiss="modal">
	         		<span aria-hidden="true">×</span>
	         	</button>
	         	<h4 class="modal-title">Detail Assign Request</h4>
	         </div>
	         <form class="form-detail-assign">
	         	<div class="modal-body">
      			<table class="table table-striped table-condensed" style="font-size:12px;">
						<thead>
							<th></th>
							<th>Tgl</th>
							<th>Permohonan Data</th>
							<th>Mulai</th>
							<th>Selesai</th>
							<th>Batas Waktu</th>
							<th>Status</th>
						</thead>
	          		<tbody class="sect-data-assign-detail"></tbody>
					</table>
      		</div>
      		<div class="modal-footer">
              	<button type="submit" class="btn btn-danger btn-sm">Hapus file dari request yang ditandai</button>
             </div>
	   		</form>
			</div>
		</div>
	</div>

	<div class="modal fade in" id="modal-detail-share">
	 	<div class="modal-dialog">
	     	<div class="modal-content" style="width: 1000px;margin-left: -195px;">
	         <div class="modal-header">
	         	<button type="button" class="close" data-dismiss="modal">
	         		<span aria-hidden="true">×</span>
	         	</button>
	         	<h4 class="modal-title">Detail Share</h4>
	         </div>
	         <form class="form-detail-share">
	         	<div class="modal-body">
      			<table class="table table-striped table-condensed" style="font-size:12px;">
						<thead>
							<th></th>
							<th>Name</th>
							<th>No.Telp</th>
							<th>Deskripsi</th>
						</thead>
	          		<tbody class="sect-data-share-detail"></tbody>
					</table>
      		</div>
      		<div class="modal-footer">
              	<button type="submit" class="btn btn-danger btn-sm">Hapus file dari opd yang ditandai</button>
             </div>
	   		</form>
			</div>
		</div>
	</div>

	<script type="text/html" id="files-template">
	   <li class="media">
	     <div class="media-body mb-1">
	       <h6 style="margin-top: 0;margin-bottom: 5px;">
	         <strong>%%filename%%</strong> <span class="text-muted">Waiting</span>
	       </h6>
	       <div class="progress" style="margin-bottom: 0">
	         <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" 
	           role="progressbar"
	           style="width: 0%" 
	           aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
	         </div>
	       </div>
	     </div>
	   </li>
	 </script>

</div>

<script type="text/javascript">

	_first_load = '<?php echo $first_load ?>';
	_uniqid = '<?php echo $_uniqid ?>';

	$(function () {

		arrfiles = [];
		fav_id = [];

		$.fn.bulk_option_reset = function(){
			arrfiles = [];
			fav_id = [];
			$('.option_check').each(function(){
	         this.checked = false; 
	      });
	      $('.option_choose').prop('checked', false);
			$('.row-options-selected').addClass('hidden');
		}

		$(this).on('click', '#'+_uniqid+' .add-more-folder',function(e){
	      e.preventDefault();
	      var total = $("#modal-upload-folder .modal-body > .form-group").length;
	      if(total < 5){
	         t = '';
	         t += '<div class="form-group form-group-added">';
	         t += '<input type="text" class="form-control input-sm" name="folder_name[]" placeholder="Nama folder..." required="">';
	         t += '</div>';
	         $('#modal-upload-folder .modal-body').prepend(t);
	      }else{
	         alert('Maksimal 5 folder setiap submit');
	      }
	   });

	});

</script>

<script src="<?php echo $path_scripts ?>/documents/myfiles/getting.js?<?php echo time() ?>"></script>
<script src="<?php echo $path_scripts ?>/documents/myfiles/form_actions.js?<?php echo time() ?>"></script>
<script src="<?php echo $path_scripts ?>/documents/myfiles/actions.js?<?php echo time() ?>"></script>
<script src="<?php echo $path_scripts ?>/documents/myfiles/uploader_files.js?<?php echo time() ?>"></script>