<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Document_shared extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function get_by_created($userid){
      $data = array();
      $this->db->select('a.*');
      $this->db->select('GROUP_CONCAT(b.name SEPARATOR ",") as opd_name');
      $this->db->join('opd as b', 'a.opd_id = b.id');
      $this->db->where('a.created_by', $userid);
      $this->db->group_by('a.full_dir');
      $result = $this->db->get('document_shared as a')->result_array();
      if($result){
         foreach ($result as $v) {
            if($v['file_dir'] && $v['file_name']){
               $data[$v['id']]['dir'] = $v['full_dir'];
               $data[$v['id']]['id'] = $v['id'];
               $data[$v['id']]['opd_name'] = $v['opd_name'];
            }
         }
         return $data;
      }else{
         return FALSE;
      }
   }

   public function get_by_opd($opd_id){
      $this->db->where('opd_id', $opd_id);
      return $this->db->get('document_shared')->result_array();
   }

   public function map_opd($userid){
      $this->db->select('c.name as opd_name, b.first_name as shared_name');
      $this->db->join('users as b', 'a.user_id = b.id');
      $this->db->join('opd as c', 'a.opd_id = c.id');
      $this->db->where('a.user_id', $userid);
      return $this->db->get('mapping_opd as a')->row_array();
   }

   public function get_by_dir($params, $userid){
      $this->db->select('a.id as share_id, b.*');
      $this->db->join('opd as b', 'a.opd_id = b.id', 'left');
      $this->db->where('a.full_dir', $params['dir'].'/'.$params['filename']);
      $this->db->where('a.created_by', $userid);
      $this->db->order_by('a.id', 'desc');
      return $this->db->get('document_shared as a')->result_array();
   }

   public function rename_by_path($params, $userid){
      $obj['file_name'] = urlencode($params['rename']).'.'.$params['ext'];
      $obj['full_dir'] = $params['dir'].'/'.urlencode($params['rename']).'.'.$params['ext'];
      $where['full_dir'] = $params['dir'].'/'.$params['name'];
      $where['created_by'] = $userid;
      $this->db->update('document_shared', $obj, $where);
   }

}