<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Document_tags extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function get_by_id($status, $userid){
   	$data = array();
   	$this->db->where('created_by', $userid);
   	$this->db->where('type_dms', $status);
   	$result = $this->db->get('document_tags')->result_array();
   	if($result){
   		foreach ($result as $v) {
   			if($v['file_dir'] && $v['file_name']){
   				$data[$v['id']]['dir'] = $v['full_dir'];
   				$data[$v['id']]['id'] = $v['id'];
   			}
   		}
   		return $data;
   	}else{
   		return FALSE;
   	}
   }

   public function get_by_status($mode, $params, $userid, $status){
      $this->db->where('created_by', $userid);
      $this->db->where('type_dms', $status);
      return $this->db->get('document_tags')->result_array();
   }

   public function rename_by_id($params, $userid){
      $obj['file_name'] = urlencode($params['rename']).'.'.$params['ext'];
      $obj['full_dir'] = $params['dir'].'/'.urlencode($params['rename']).'.'.$params['ext'];
      $where['id'] = $params['fav'];
      $this->db->update('document_tags', $obj, $where);
   }

   public function rename_by_path($params, $userid){
      $obj['file_name'] = urlencode($params['rename']).'.'.$params['ext'];
      $obj['full_dir'] = $params['dir'].'/'.urlencode($params['rename']).'.'.$params['ext'];
      $where['full_dir'] = $params['dir'].'/'.$params['name'];
      $where['created_by'] = $userid;
      $this->db->update('document_tags', $obj, $where);
   }

}