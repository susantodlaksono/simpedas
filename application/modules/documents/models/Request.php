<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Request extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function getting($mode, $params, $userid){
      $this->db->select('a.id, a.opd_id, a.title, a.start_date, a.end_date, a.limit_date, a.requested_date, a.status');
      $this->db->select('b.total_files');
      $this->db->join('(select req_id, count(id) as total_files from request_result_files
                        group by req_id) as b', 
                        'a.id = b.req_id', 'left');
      $this->db->where('a.pedasi_id', $userid);
      $this->db->where_in('a.status', array(1,2));
      if($params['keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.title', $params['keyword']);
         $this->db->group_end();
      }
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('request as a', 10, $params['offset'])->result_array();
         case 'count':
            return $this->db->get('request as a')->num_rows();
      }
   }

   public function get_by_dir($params, $userid){
      $this->db->select('a.id as assign_id, b.*');
      $this->db->join('request as b', 'a.req_id = b.id', 'left');
      $this->db->where('a.full_path', $params['dir'].'/'.$params['filename']);
      $this->db->where('a.created_by', $userid);
      $this->db->order_by('a.id', 'desc');
      return $this->db->get('request_result_files as a')->result_array();
   }

   public function get_by_created($userid){
      $data = array();
      $this->db->select('a.*');
      $this->db->select('GROUP_CONCAT(b.title SEPARATOR ",") as title');
      $this->db->join('request as b', 'a.req_id = b.id');
      $this->db->where('a.created_by', $userid);
      $this->db->group_by('a.full_path');
      $result = $this->db->get('request_result_files as a')->result_array();
      if($result){
         foreach ($result as $v) {
            if($v['full_path'] && $v['file_name']){
               $data[$v['id']]['dir'] = $v['full_path'];
               $data[$v['id']]['id'] = $v['id'];
               $data[$v['id']]['title'] = $v['title'];
            }
         }
         return $data;
      }else{
         return FALSE;
      }
   }

   public function rename_by_path($params, $userid){
      $obj['file_name'] = urlencode($params['rename']).'.'.$params['ext'];
      $obj['full_path'] = $params['dir'].'/'.urlencode($params['rename']).'.'.$params['ext'];
      $where['full_path'] = $params['dir'].'/'.$params['name'];
      $where['created_by'] = $userid;
      $this->db->update('request_result_files', $obj, $where);
   }

}
