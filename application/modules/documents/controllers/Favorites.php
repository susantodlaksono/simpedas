<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Favorites extends CoreDoc {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('global_mapping');
		$this->load->library('dir_extraction');
		$this->load->library('date_extraction');
		$this->load->model('document_tags');
		$this->load->model('document_shared');
		$this->load->model('request');
	}

	public function index(){
      $data['path_scripts'] = $this->_path_scripts;
      $data['first_load'] = $this->_get['first_load'] ? $this->_get['first_load'] : FALSE;
		$response['html'] = $this->load->view('favorites', $data, TRUE);
      $this->json_result($response);
	}

	public function getting(){
      $list = array();
      $params = $this->_get;
      $data = $this->document_tags->get_by_status('get', $this->_get, $this->_user->id, 3);
      if($data){
         $i = 0;
         $share = $this->document_shared->get_by_created($this->_user->id);
			$assign = $this->request->get_by_created($this->_user->id);

         foreach ($data as $v) {
            $time = date('d M Y H:i:s', filectime(urldecode($v['full_dir'])));
				$list[$i]['id'] = $v['id'];
				$list[$i]['name'] = $v['file_name'];
				$list[$i]['file_ternary'] = $v['file_ternary'] ? $v['file_ternary'] : NULL;
				$list[$i]['label'] = urldecode($v['file_name']);
				$list[$i]['base_dir'] = $v['full_dir'];
				$list[$i]['file_dir'] = $v['file_dir'];
	         $list[$i]['is_file'] = is_file(urldecode($v['full_dir']));
	         $list[$i]['time'] = $this->date_extraction->time_ago($time);
	         $list[$i]['clean_time'] = $time;
	         $list[$i]['size'] = $this->global_mapping->format_size_units(filesize(urldecode($v['full_dir'])));
	         $list[$i]['size_bytes'] = filesize(urldecode($v['full_dir']));

				if(is_file(urldecode($v['full_dir']))){
	            $ext = pathinfo($v['full_dir'], PATHINFO_EXTENSION);
	            $list[$i]['ext_orig'] = $ext;
	            $list[$i]['ext'] = $this->global_mapping->mapping_icon('.'.$ext.'', TRUE);
	         }else{
	            $list[$i]['ext_orig'] = NULL;
	            $list[$i]['ext'] = NULL;
	         }
	         
	         if($share){
	            $sharestatus = $this->dir_extraction->in_array_r($v['full_dir'], $share);
	            if($sharestatus){
	               $list[$i]['share'] = $sharestatus['opd_name'];
	            }else{
	               $list[$i]['share'] = FALSE;
	            }
	         }

	         if($assign){
	            $assignstatus = $this->dir_extraction->in_array_r($v['full_dir'], $assign);
	            if($assignstatus){
	               $list[$i]['assign'] = $assignstatus['title'];
	            }else{
	               $list[$i]['assign'] = FALSE;
	            }
	         }
            $i++;
         }

         if($list){
            if($params['orderby'] == 'desc'){
               usort($list, function($b, $a) use ($params) {
                   return $a[$params['order']] <=> $b[$params['order']];
               });
            }
            if($params['orderby'] == 'asc'){
               usort($list, function($a, $b) use ($params) {
                   return $a[$params['order']] <=> $b[$params['order']];
               });
            }
         }
      }
      $response['result'] = $list;
      $this->json_result($response);
   }

   public function bulk_remove(){
      $response['success'] = FALSE;
      $response['msg'] = 'Function failed';

      if(isset($this->_post['fav']) && count($this->_post['fav']) > 0){
      	$this->db->trans_start();
	      foreach ($this->_post['fav'] as $k => $v) {
   			$this->db->where('id', $v['id']);
   			$this->db->delete('document_tags');
   		}
	      $this->db->trans_complete();

	      if($this->db->trans_status()){
	         $this->db->trans_commit();
	         $response['success'] = TRUE;
	         $response['msg'] = 'Favorit file berhasil di hapus';
	      }else{
	         $this->db->trans_rollback();
	      }
      }else{
      	$response['msg'] = 'Tidak ada data yang dipilih';
      }
      $this->json_result($response);
   }

}