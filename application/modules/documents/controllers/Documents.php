<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Documents extends CoreDoc {
	
	public function __construct() {
		parent::__construct();
	}

	public function index(){
      $this->dir_create();
		$data['_css'] = array(
         $this->_path_template.'/plugins/select2/select2.min.css',
         $this->_path_template.'/plugins/krajee-star-rating/star-rating.min.css',
         $this->_path_template.'/plugins/krajee-star-rating/theme.css',
         $this->_path_template.'/plugins/uploader-master/css/jquery.dm-uploader.css'
      ); 
      $data['_js'] = array(
         $this->_path_template.'/plugins/select2/select2.full.min.js',
         $this->_path_template.'/plugins/tinymce/tinymce.min.js',
         $this->_path_template.'/plugins/krajee-star-rating/star-rating.min.js',
         $this->_path_template.'/plugins/uploader-master/js/jquery.dm-uploader.js',
		);	
		$this->render_page($data, 'documents', 'modular');
	}
}
