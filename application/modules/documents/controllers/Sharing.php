<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Sharing extends CoreDoc {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('global_mapping');
		$this->load->library('dir_extraction');
		$this->load->library('date_extraction');
		$this->load->model('document_tags');
		$this->load->model('document_shared');
		$this->load->model('request');
	}

	public function index(){
      $data['path_scripts'] = $this->_path_scripts;
      $data['first_load'] = $this->_get['first_load'] ? $this->_get['first_load'] : FALSE;
		$response['html'] = $this->load->view('sharing', $data, TRUE);
      $this->json_result($response);
	}

	public function getting(){
      $list = array();
      $params = $this->_get;
      $data = $this->document_shared->get_by_opd($this->_opd->id);
      if($data){
         $i = 0;
         foreach ($data as $v) {
         	$opd = $this->document_shared->map_opd($v['created_by']);
            $time = date('d M Y H:i:s', filectime(urldecode($v['full_dir'])));
				$list[$i]['id'] = $v['id'];
				$list[$i]['name'] = $v['file_name'];
				$list[$i]['file_ternary'] = $v['file_ternary'] ? $v['file_ternary'] : NULL;
				$list[$i]['label'] = urldecode($v['file_name']);
				$list[$i]['base_dir'] = $v['full_dir'];
				$list[$i]['file_dir'] = $v['file_dir'];
	         $list[$i]['is_file'] = is_file(urldecode($v['full_dir']));
	         $list[$i]['time'] = $this->date_extraction->time_ago($time);
	         $list[$i]['clean_time'] = $time;
	         $list[$i]['shared_name'] = $opd ? $opd['shared_name'] : NULL;
	         $list[$i]['opd_name'] = $opd ? $opd['opd_name'] : NULL;
	         $list[$i]['size'] = $this->global_mapping->format_size_units(filesize(urldecode($v['full_dir'])));
	         $list[$i]['size_bytes'] = filesize(urldecode($v['full_dir']));

				if(is_file(urldecode($v['full_dir']))){
	            $ext = pathinfo($v['full_dir'], PATHINFO_EXTENSION);
	            $list[$i]['ext_orig'] = $ext;
	            $list[$i]['ext'] = $this->global_mapping->mapping_icon('.'.$ext.'', TRUE);
	         }else{
	            $list[$i]['ext_orig'] = NULL;
	            $list[$i]['ext'] = NULL;
	         }
            $i++;
         }

         if($list){
            if($params['orderby'] == 'desc'){
               usort($list, function($b, $a) use ($params) {
                   return $a[$params['order']] <=> $b[$params['order']];
               });
            }
            if($params['orderby'] == 'asc'){
               usort($list, function($a, $b) use ($params) {
                   return $a[$params['order']] <=> $b[$params['order']];
               });
            }
         }
      }
      $response['result'] = $list;
      $this->json_result($response);
   }

   public function dir(){
		$reader = $this->reader($this->_get['dir']);
      $nav = '';
      $nav .= '<li><a data-href="" class="breadcrumb-action-home" style="cursor:pointer;"><i class="fa fa-home"></i></a></li>';
      $uri_string = explode('/', urldecode($this->_get['dir']));

      $privileges = $this->_get['except_ternary'] && $this->_get['act_fldr_name'] != '' ? 
                  $this->privileges_folder_ternary($this->_get['dir'], $this->_get['act_fldr_name']) : NULL;

      $uri = array();
      foreach (array_filter($uri_string) as $rs) {
         $uri[] = urlencode($rs);
         if($privileges){
            if(!in_array(urlencode($rs), $privileges)){
               $nav .= '<li><a data-href="'.implode('/', $uri).'" class="breadcrumb-action">'.$rs.'</a></li>';
            }
         }else{
            $nav .= '<li><a data-href="'.implode('/', $uri).'" class="breadcrumb-action">'.$rs.'</a></li>';
         }
      }

		$response['result'] = $this->result_files($reader, $this->_get);
      $response['nav'] = $nav;
      $response['dir_base'] = $this->_defaultdir;
		$response['dir_clean'] = urldecode($this->_get['dir']);
		if($response['dir_clean'] != ''){
         $exp = explode('/', $response['dir_clean']);
         $response['active_dir_name'] = end($exp);
      }else{
         $response['active_dir_name'] = date('Y-m-d H:i:s');
      }
      $response['role_id'] = $this->_role_id;
		$this->json_result($response);
	}

	public function result_files($reader, $params){
      $data = array();
		$i = 0;
		if($reader['scan']){
			foreach($reader['scan'] as $v){
				$time = date('d M Y H:i:s', filectime(urldecode($reader['dir']).'/'.urldecode($v)));
				$data[$i]['name'] = urlencode($v);
				$data[$i]['label'] = $v;
				$list[$i]['shared_name'] = NULL;
	         $list[$i]['opd_name'] = NULL;
				$data[$i]['base_dir'] = $reader['dir'];
	         $data[$i]['is_file'] = is_file(urldecode($reader['dir']).'/'.urldecode($v));
	         $data[$i]['time'] = $this->date_extraction->time_ago($time);
	         $data[$i]['clean_time'] = $time;
	         $data[$i]['size'] = $this->global_mapping->format_size_units(filesize(urldecode($reader['dir']).'/'.urldecode($v)));
	         $data[$i]['size_bytes'] = filesize(urldecode($reader['dir']).'/'.urldecode($v));

				if(is_file(urldecode($reader['dir']).'/'.urldecode($v))){
	            $ext = pathinfo($reader['dir'].'/'.$v, PATHINFO_EXTENSION);
	            $data[$i]['ext_orig'] = $ext;
	            $data[$i]['ext'] = $this->global_mapping->mapping_icon('.'.$ext.'', TRUE);
	         }else{
	            $data[$i]['ext_orig'] = NULL;
	            $data[$i]['ext'] = NULL;
	         }
	         $i++;
			}
		}

		if($data){
         if($params['orderby'] == 'desc'){
            usort($data, function($b, $a) use ($params) {
             	return $a[$params['order']] <=> $b[$params['order']];
            });
         }
         if($params['orderby'] == 'asc'){
            usort($data, function($a, $b) use ($params) {
             	return $a[$params['order']] <=> $b[$params['order']];
            });
         }
      }
      return array(
         'data' => $data,
         'params' => $params['order'],
         'total' => count($data)
      );
	}

}