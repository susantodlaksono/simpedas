<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Myfiles extends CoreDoc {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('global_mapping');
		$this->load->library('dir_extraction');
		$this->load->library('date_extraction');
		$this->load->model('document_tags');
		$this->load->model('document_shared');
		$this->load->model('request');
	}

	public function index(){
      $data['path_scripts'] = $this->_path_scripts;
      $data['first_load'] = $this->_get['first_load'] ? $this->_get['first_load'] : FALSE;
      $data['role_id'] = $this->_role_id;
		$response['html'] = $this->load->view('myfiles', $data, TRUE);
      $this->json_result($response);
	}

	public function dir(){
		$reader = $this->reader($this->_get['dir']);
      $nav = '';
      $nav .= '<li><a data-href="" class="breadcrumb-action-home" style="cursor:pointer;"><i class="fa fa-home"></i></a></li>';
      $uri_string = explode('/', urldecode($this->_get['dir']));

      $privileges = $this->_get['except_ternary'] && $this->_get['act_fldr_name'] != '' ? 
                  $this->privileges_folder_ternary($this->_get['dir'], $this->_get['act_fldr_name']) : NULL;

      $uri = array();
      foreach (array_filter($uri_string) as $rs) {
         $uri[] = urlencode($rs);
         if($privileges){
            if(!in_array(urlencode($rs), $privileges)){
               $nav .= '<li><a data-href="'.implode('/', $uri).'" class="breadcrumb-action">'.$rs.'</a></li>';
            }
         }else{
            $nav .= '<li><a data-href="'.implode('/', $uri).'" class="breadcrumb-action">'.$rs.'</a></li>';
         }
      }

		$response['result'] = $this->result_files($reader, $this->_get);
      $response['nav'] = $nav;
      $response['dir_base'] = $this->_defaultdir;
		$response['dir_clean'] = urldecode($this->_get['dir']);
		if($response['dir_clean'] != ''){
         $exp = explode('/', $response['dir_clean']);
         $response['active_dir_name'] = end($exp);
      }else{
         $response['active_dir_name'] = date('Y-m-d H:i:s');
      }
      $response['role_id'] = $this->_role_id;
		$this->json_result($response);
	}

	public function result_files($reader, $params){
      $data = array();
		$i = 0;
      if(in_array(5, $this->_role_id)){
         $assign = $this->request->get_by_created($this->_user->id);
      }else{
         $assign = NULL;
      }
		$fav = $this->document_tags->get_by_id(3, $this->_user->id);
		$share = $this->document_shared->get_by_created($this->_user->id);

		if($reader['scan']){
			foreach($reader['scan'] as $v){
				$time = date('d M Y H:i:s', filectime(urldecode($reader['dir']).'/'.urldecode($v)));
				$data[$i]['name'] = urlencode($v);
				$data[$i]['label'] = $v;
				$data[$i]['base_dir'] = $reader['dir'];
	         $data[$i]['is_file'] = is_file(urldecode($reader['dir']).'/'.urldecode($v));
	         $data[$i]['time'] = $this->date_extraction->time_ago($time);
	         $data[$i]['clean_time'] = $time;
	         $data[$i]['size'] = $this->global_mapping->format_size_units(filesize(urldecode($reader['dir']).'/'.urldecode($v)));
	         $data[$i]['size_bytes'] = filesize(urldecode($reader['dir']).'/'.urldecode($v));

				if(is_file(urldecode($reader['dir']).'/'.urldecode($v))){
	            $ext = pathinfo($reader['dir'].'/'.$v, PATHINFO_EXTENSION);
	            $data[$i]['ext_orig'] = $ext;
	            $data[$i]['ext'] = $this->global_mapping->mapping_icon('.'.$ext.'', TRUE);
	         }else{
	            $data[$i]['ext_orig'] = NULL;
	            $data[$i]['ext'] = NULL;
	         }

				if($fav){
	            $favstatus = $this->dir_extraction->in_array_r($reader['dir'].'/'.urlencode($v), $fav);
	            if($favstatus){
	               $data[$i]['fav'] = $favstatus['id'];
	            }else{
	               $data[$i]['fav'] = FALSE;
	            }
	         }
	         
	         if($share){
	            $sharestatus = $this->dir_extraction->in_array_r($reader['dir'].'/'.urlencode($v), $share);
	            if($sharestatus){
	               $data[$i]['share'] = $sharestatus['opd_name'];
	            }else{
	               $data[$i]['share'] = FALSE;
	            }
	         }

	         if($assign){
	            $assignstatus = $this->dir_extraction->in_array_r($reader['dir'].'/'.urlencode($v), $assign);
	            if($assignstatus){
	               $data[$i]['assign'] = $assignstatus['title'];
	            }else{
	               $data[$i]['assign'] = FALSE;
	            }
	         }

	         $i++;
			}
		}

		if($data){
         if($params['orderby'] == 'desc'){
            usort($data, function($b, $a) use ($params) {
             	return $a[$params['order']] <=> $b[$params['order']];
            });
         }
         if($params['orderby'] == 'asc'){
            usort($data, function($a, $b) use ($params) {
             	return $a[$params['order']] <=> $b[$params['order']];
            });
         }
      }
      return array(
         'data' => $data,
         'params' => $params['order'],
         'total' => count($data)
      );
	}

   public function search_keyword(){
      $response['result'] = $this->search_dir_subdirectory($this->_get['keyword']);
      $this->json_result($response);
   }

	public function bulk_flag(){
      $response['success'] = FALSE;
      $response['msg'] = 'Function failed';
      $this->load->library('active_record_builder');

      $this->db->trans_start();
      foreach ($this->_post['files'] as $k => $v) {
         $tmp['file_name'] = $v['name'] ? $v['name'] : NULL;
         $tmp['file_dir'] = $v['dir'] ? $v['dir'] : NULL;
         $tmp['file_extension'] = $v['ext'] !== 'folder' ? $v['ext'] : NULL;
         $tmp['type_dms'] = $this->_post['status'];
         $tmp['created_by'] = $this->_user->id;
         $tmp['created_at'] = date('Y-m-d H:i:s');
         
         if($v['dir'] && $v['name']){
            $tmp['full_dir'] = $v['dir'].'/'.$v['name'];
         }
         if($v['ext'] == 'folder'){
            $explode = explode('/', $v['ternary']);
            if(count($explode) > 1){
               $tmp['file_ternary'] = $v['ternary'];   
            }
         }
         $this->active_record_builder->on_duplicate('document_tags', $tmp);
      }
      $this->db->trans_complete();

      if($this->db->trans_status()){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['msg'] = 'Status file berhasil di perbaharui';
      }else{
         $this->db->trans_rollback();
      }
      $this->json_result($response);
   }

   public function rename(){
      $response['success'] = FALSE;
      $response['msg'] = 'Function failed';
      if($this->_post['dir'] != '' && $this->_post['name'] != ''){

         $this->db->trans_start();
         if($this->_post['ext'] != ''){
            $rename = rename(urldecode($this->_post['dir']).'/'.urldecode($this->_post['name']), 
                           urldecode($this->_post['dir']).'/'.urldecode($this->_post['rename']).'.'.$this->_post['ext']);
            if($rename){
               if($this->_post['fav'] != ''){
                  $this->document_tags->rename_by_id($this->_post, $this->_user->id);
               }else{
                  $this->document_tags->rename_by_path($this->_post, $this->_user->id);
               }
               $this->document_shared->rename_by_path($this->_post, $this->_user->id);
               $this->request->rename_by_path($this->_post, $this->_user->id);
            }
         }else{
            $rename = rename(urldecode($this->_post['dir']).'/'.urldecode($this->_post['name']), 
                           urldecode($this->_post['dir']).'/'.urldecode($this->_post['rename']));
         }
         $this->db->trans_complete();

         if($this->db->trans_status()){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'Nama file berhasil di perbaharui';
         }else{
            $this->db->trans_rollback();
         }
      }else{
         $response['msg'] = 'No parsing parameter';
      }
      $this->json_result($response);
   }

   public function bulk_remove(){
      $response['success'] = FALSE;
      $response['msg'] = 'Function failed';

      foreach ($this->_post['files'] as $k => $v) {
         if($v['ext'] == 'folder'){
         	$this->delete_folder_files(urldecode($v['dir']).'/'.urldecode($v['name']), $v['dir'].'/'.$v['name']);
         }else{
         	$this->delete_full_dir_tags($v['dir'].'/'.$v['name'], $this->_user->id);
		      $this->delete_request_files($v['dir'].'/'.$v['name'], $this->_user->id);
		      $this->delete_full_dir_shared($v['dir'].'/'.$v['name'], $this->_user->id);
         	unlink(urldecode($v['dir']).'/'.urldecode($v['name']));
         }
      }
      $this->db->trans_start();
   	if(isset($this->_post['fav']) && count($this->_post['fav']) > 0){
   		foreach ($this->_post['fav'] as $k => $v) {
   			$this->db->where('id', $v['id']);
   			$this->db->delete('document_tags');
   		}
   	}
      $this->db->trans_complete();

      if($this->db->trans_status()){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['msg'] = 'Status file berhasil di perbaharui';
      }else{
         $this->db->trans_rollback();
      }
      $this->json_result($response);
   }

	public function folder(){
      $response['msg'] = 'Some error occured';
      $done_upload = 0;
      $fail_upload = 0;
      $process = FALSE;

      if(isset($this->_post['folder_name']) && count($this->_post['folder_name']) > 0){
         foreach ($this->_post['folder_name'] as $v) {
            if($this->_post['dir']){
               $upload_path = $this->_defaultdir.'/'.urldecode($this->_post['dir']).'/'.$v;
            }else{
               $upload_path = $this->_defaultdir.'/'.$v;
            }
            if (!file_exists($upload_path)) {
               mkdir($upload_path);
               chmod($upload_path, 0777);
               $done_upload += 1;
            }else{
               $fail_upload += 1;
            }
         }
         $process = TRUE;
      }
      if($process) {
         $response['msg'] = $done_upload.' Folder berhasil dibuat '.$fail_upload.' folder gagal di upload';
         $response['success'] = $process;
      }else{
         $response['msg'] = 'Gagal mengupload folder';
      }
      $this->json_result($response);
   }

   public function request(){
      $data = $this->request->getting('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'status' => $v['status'],
               'status_label' => $this->global_mapping->request_status($v['status'], 'label-status-table '),
               'title' => $v['title'],
               'total_files' => $v['total_files'],
               'start_date' => date('d M Y', strtotime($v['start_date'])),
               'end_date' => date('d M Y', strtotime($v['end_date'])),
               'limit_date' => date('d M Y', strtotime($v['limit_date'])),
               'days_left' => $v['limit_date'] ? $this->date_extraction->left_days_from_now($v['limit_date']) : NULL,
               'requested_date' => date('d M Y', strtotime($v['requested_date'])),
               'requested_time' => date('H:i:s', strtotime($v['requested_date']))
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->request->getting('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

   public function detail_assign(){
   	$list = array();
      $data = $this->request->get_by_dir($this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['assign_id'],
               'status' => $v['status'],
               'status_label' => $this->global_mapping->request_status($v['status'], 'label-status-table '),
               'title' => $v['title'],
               'start_date' => date('d M Y', strtotime($v['start_date'])),
               'end_date' => date('d M Y', strtotime($v['end_date'])),
               'limit_date' => date('d M Y', strtotime($v['limit_date'])),
               'days_left' => $v['limit_date'] ? $this->date_extraction->left_days_from_now($v['limit_date']) : NULL,
               'requested_date' => date('d M Y', strtotime($v['requested_date'])),
               'requested_time' => date('H:i:s', strtotime($v['requested_date']))
            );
         }
      }
      $response['result'] = $list;
      $this->json_result($response);
   }

   public function detail_share(){
      $list = array();
      $data = $this->document_shared->get_by_dir($this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['share_id'],
               'name' => $v['name'],
               'phone' => $v['phone'],
               'description' => $v['description']
            );
         }
      }
      $response['result'] = $list;
      $this->json_result($response);
   }

   public function stream_files(){
   	$result = $this->db->where('req_id', $this->_get['id'])->get('request_result_files')->result_array();
   	foreach ($result as $v) {
   		$explode_dir = explode('/', $v['full_path'], 4);
         $list[] = array(
            'id' => $v['id'],
            'full_path' => urldecode($explode_dir[3]),
            'ext' => $this->global_mapping->mapping_icon('.'.$v['extension'].'', TRUE)
         );
      }
      $response['result'] = $list;
      $this->json_result($response);
   }

   public function delete_assign(){
      $response['success'] = FALSE;
      $response['msg'] = 'Some error occured';

      $this->db->trans_start();
      foreach ($this->_post['assignid'] as $v) {
         $this->db->where('id', $v);
         $this->db->delete('request_result_files');
      }
      $this->db->trans_complete();
      if($this->db->trans_status()){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['msg'] = 'Data file berhasil diperbaharui';
      }else{
         $this->db->trans_rollback();
      }
      $this->json_result($response);
   }

   public function delete_share(){
      $response['success'] = FALSE;
      $response['msg'] = 'Some error occured';

      $this->db->trans_start();
      foreach ($this->_post['shareid'] as $v) {
         $this->db->where('id', $v);
         $this->db->delete('document_shared');
      }
      $this->db->trans_complete();
      if($this->db->trans_status()){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['msg'] = 'Data file berhasil diperbaharui';
      }else{
         $this->db->trans_rollback();
      }
      $this->json_result($response);
   }

   public function assign(){
      $response['success'] = FALSE;
      $response['msg'] = 'Some error occured';
      $this->load->library('active_record_builder');
      $folderfound = array_keys(array_combine(array_keys($this->_post['files']), array_column($this->_post['files'], 'ext')),'folder');
      if(!$folderfound){
         $this->db->trans_start();
         foreach ($this->_post['reqid'] as $v) {
            foreach ($this->_post['files'] as $vv) {
               $tmp = array(
                  'req_id' => $v,
                  'full_path' => $vv['dir'].'/'.$vv['name'],
                  'extension' => $vv['ext'],
                  'file_name' => $vv['name'],
                  'created_date' => date('Y-m-d H:i:s'),
                  'created_by' => $this->_user->id
               );
               $this->active_record_builder->on_duplicate('request_result_files', $tmp);
            }
         }
         $this->db->trans_complete();
         if($this->db->trans_status()){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'File berhasil di lampirkan pada request terpilih';
         }else{
            $this->db->trans_rollback();
         }
      }else{
         $response['msg'] = 'Folder tidak bisa dilampirkan pada permohonan data';
      }
      $this->json_result($response);
   }

   public function share(){
      $response['success'] = FALSE;
      $response['msg'] = 'Some error occured';
      $this->load->library('active_record_builder');

      $this->db->trans_start();
      foreach ($this->_post['opdid'] as $v) {
         foreach ($this->_post['files'] as $vv) {
            $tmp['file_name'] = $vv['name'];
            $tmp['file_dir'] = $vv['dir'];
            if($vv['ext'] == 'folder'){
               $explode = explode('/', $vv['ternary']);
               if(count($explode) > 1){
                  $tmp['file_ternary'] = $vv['ternary'];
               }
            }else{
               $tmp['file_ternary'] = NULL;
               $tmp['file_extension'] = $vv['ext'];
            }
            $tmp['full_dir'] = $vv['dir'].'/'.$vv['name'];
            $tmp['opd_id'] = $v;
            $tmp['created_at'] = date('Y-m-d H:i:s');
            $tmp['created_by'] = $this->_user->id;
            $this->active_record_builder->on_duplicate('document_shared', $tmp);
         }
      }
      $this->db->trans_complete();
      if($this->db->trans_status()){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['msg'] = 'File/folder berhasil di share pada opd terpilih';
      }else{
         $this->db->trans_rollback();
      }
      $this->json_result($response);
   }

	public function upload(){
      try {
         if (
            !isset($_FILES['file']['error']) ||
            is_array($_FILES['file']['error'])
         ) {
            throw new RuntimeException('Invalid parameters.');
         }
         switch ($_FILES['file']['error']) {
            case UPLOAD_ERR_OK:
            break;
            case UPLOAD_ERR_NO_FILE:
               throw new RuntimeException('No file sent.');
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
               throw new RuntimeException('Exceeded filesize limit.');
            default:
               throw new RuntimeException('Unknown errors.');
         }

         $config['upload_path']  = $this->_post['dir'] ? $this->_defaultdir.'/'.urldecode($this->_post['dir']) : $this->_defaultdir;
         $config['allowed_types'] = '*';
         $config['overwrite'] = TRUE;
         $config['remove_spaces'] = FALSE; 

         $this->load->library('upload', $config);
         if($this->upload->do_upload('file')){
            $uploadresult = $this->upload->data();
            chmod($uploadresult['full_path'], 0777);
            $response['status'] = 'ok';
         }else{
            $response['status'] = 'error';
            $response['msg'] = $this->upload->display_errors();
         }
      }catch (RuntimeException $e) {
         http_response_code(400);
         $response['status'] = 'error';
         $response['message'] = $e->getMessage();
      }
      $this->json_result($response);
   }

   public function zipped_bulk(){
      $this->load->library('zip');
      $response['success'] = TRUE;
      $filename = uniqid();

      $files = glob('./download/*');
      foreach($files as $file) {
        	if(is_file($file)) {
       		unlink($file);
        	}
      }

      foreach ($this->_post['files'] as $k => $v) {
         if($v['ext'] == 'folder'){
            $this->zip->read_dir(urldecode($v['dir']).'/'.urldecode($v['name']), FALSE);
         }else{
            $this->zip->read_file(urldecode($v['dir']).'/'.urldecode($v['name']));
         }
      }
      $this->zip->archive('download/'.$filename.'.zip');
      chmod('download/'.$filename.'.zip', 0777);

      $response['filename'] = $filename;
      $response['title'] = $this->_post['filename'];
      $this->json_result($response);
   }

   public function download_bulk(){
      $this->load->helper('download');
      $filename = $this->_get['name'] != '' ? $this->_get['name'] : date('d M Y H.i.s');
      force_download(urldecode($filename).'.zip', file_get_contents(urldecode($this->_get['dir'])), NULL); 
   }

   public function opd(){
      $data = $this->opd_result('get', $this->_get);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'name' => $v['name'],
               'phone' => $v['phone'],
               'description' => $v['description']
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->opd_result('count', $this->_get);
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

   public function opd_result($mode, $params){
      $this->db->select('a.*');
      $this->db->where('a.status', 1);
      if($params['keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.name', $params['keyword']);
         $this->db->or_like('a.phone', $params['keyword']);
         $this->db->or_like('a.description', $params['keyword']);
         $this->db->group_end();
      }
      $this->db->order_by('a.id', 'desc');
      switch ($mode) {
         case 'get':
            return $this->db->get('opd as a', 10, $params['offset'])->result_array();
         case 'count':
            return $this->db->get('opd as a')->num_rows();
      }
   }

}