<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class m_kasie extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function getting($mode, $params, $opdid, $userid){
      $this->db->select('a.*, b.first_name as req_name');
      $this->db->select('c.first_name as pedasi_name');
      $this->db->select('d.total_files');
      $this->db->join('users as b', 'a.requested_by = b.id', 'left');
      $this->db->join('users as c', 'a.pedasi_id = c.id', 'left');
      $this->db->join('(select req_id, count(id) as total_files from request_result_files
                        group by req_id) as d', 
                        'a.id = d.req_id', 'left');
      $this->db->where('a.opd_id', $opdid);
      
      if($params['filt_status'] != 'all'){
         $this->db->where('a.status', $params['filt_status']);
      }else{
      	$this->db->where_in('a.status', array(2,3));
      }
      if($params['filt_keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.title', $params['filt_keyword']);
         $this->db->or_like('b.first_name', $params['filt_keyword']);
         $this->db->or_like('c.first_name', $params['filt_keyword']);
         $this->db->group_end();
      }
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('request as a', $params['limit'], $params['offset'])->result_array();
         case 'count':
            return $this->db->get('request as a')->num_rows();
      }
   }

   public function get_kasie($opdid){
   	$this->db->select('a.user_id, b.first_name');
   	$this->db->join('users as b', 'a.user_id = b.id', 'left');
   	$this->db->where('a.opd_id', $opdid);
   	$this->db->where('a.group_id', 4);
   	return $this->db->get('mapping_opd as a')->result_array();
   }

   public function request_detail($id){
      $this->db->select('a.*, b.first_name as kompilasi_sender, c.first_name as kompilasi_kasie, d.first_name as pedasi_name');
      $this->db->join('users as b', 'a.disposisi_assignby = b.id', 'left');
      $this->db->join('users as c', 'a.disposisi_kasie = c.id', 'left');
      $this->db->join('users as d', 'a.pedasi_id = d.id', 'left');
      $this->db->where('a.id', $id);
      return $this->db->get('request as a')->row_array();
   }

   public function request_files($reqid){
      $this->db->where('req_id', $reqid);
      return $this->db->get('request_result_files')->result_array();
   }

}