<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_pedasi extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function getting($mode, $params, $opdid, $userid){
      $this->db->select('a.*');
      $this->db->select('b.first_name as req_name');
      $this->db->select('c.first_name as pedasi_name');
      $this->db->join('users as b', 'a.requested_by = b.id', 'left');
      $this->db->join('users as c', 'a.pedasi_id = c.id', 'left');
      $this->db->where('a.opd_id', $opdid);
      if($params['filt_status'] != 'all'){
         if($params['filt_status'] == ''){
            $this->db->where_in('a.status', array(1,2,3));
         }else{
            $this->db->where('a.status', $params['filt_status']);
         }
      }else{
         $this->db->where_in('a.status', array(1,2,3));
      }
      if($params['filt_keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.title', $params['filt_keyword']);
         $this->db->or_like('b.first_name', $params['filt_keyword']);
         $this->db->group_end();
      }
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('request as a', $params['limit'], $params['offset'])->result_array();
         case 'count':
            return $this->db->get('request as a')->num_rows();
      }
   }

}