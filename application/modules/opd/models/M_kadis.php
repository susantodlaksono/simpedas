<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_kadis extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function getting($mode, $params, $opdid, $userid){
      $this->db->select('a.*');
      $this->db->select('b.first_name as req_name');
      $this->db->select('c.first_name as pedasi_name');
      $this->db->join('users as b', 'a.requested_by = b.id', 'left');
      $this->db->join('users as c', 'a.pedasi_id = c.id', 'left');
      $this->db->where('a.opd_id', $opdid);
      if($params['filt_status'] != 'all'){
         if($params['filt_status'] == ''){
            $this->db->where('a.status IS NULL');
         }else{
            $this->db->where('a.status', $params['filt_status']);
         }
      }
      if($params['filt_keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.title', $params['filt_keyword']);
         $this->db->or_like('b.first_name', $params['filt_keyword']);
         $this->db->group_end();
      }
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('request as a', $params['limit'], $params['offset'])->result_array();
         case 'count':
            return $this->db->get('request as a')->num_rows();
      }
   }

   public function get_kasie($opdid){
   	$this->db->select('a.user_id, b.first_name');
   	$this->db->join('users as b', 'a.user_id = b.id', 'left');
   	$this->db->where('a.opd_id', $opdid);
   	$this->db->where('a.group_id', 4);
   	return $this->db->get('mapping_opd as a')->result_array();
   }

   public function request_detail($id){
      $this->db->select('a.*, b.first_name as kompilasi_sender, c.first_name as kompilasi_kasie, d.first_name as pedasi_name');
      $this->db->join('users as b', 'a.disposisi_assignby = b.id', 'left');
      $this->db->join('users as c', 'a.disposisi_kasie = c.id', 'left');
      $this->db->join('users as d', 'a.pedasi_id = d.id', 'left');
      $this->db->where('a.id', $id);
      return $this->db->get('request as a')->row_array();
   }

   public function request_files($reqid){
      $this->db->where('req_id', $reqid);
      return $this->db->get('request_result_files')->result_array();
   }

}