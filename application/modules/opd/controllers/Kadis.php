<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Kadis extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('m_kadis');
		$this->load->library('global_mapping');
		$this->load->library('date_extraction');
   }

   public function index(){
    	$data['_css'] = array(
         $this->_path_template.'/plugins/krajee-star-rating/star-rating.min.css',
         $this->_path_template.'/plugins/krajee-star-rating/theme.css'
      ); 
      $data['_js'] = array(
         $this->_path_template.'/plugins/krajee-star-rating/star-rating.min.js',
		);	
      $this->render_page($data, 'kadis', 'modular');
   }

   public function getting(){
      $response['result'] = array();
      $opd = $this->db->where('user_id', $this->_user->id)->get('mapping_opd')->row_array();
      if($opd){
         $data = $this->m_kadis->getting('get', $this->_get, $opd['opd_id'], $this->_user->id);
         if($data){
            foreach ($data as $v) {
               $list[] = array(
                  'id' => $v['id'],
                  'status' => $v['status'],
                  'status_label' => $this->global_mapping->request_status($v['status'], 'label-status-table '),
                  'status_pengolahan_label' => $this->global_mapping->request_status_pengolahan($v['status_pengolahan'], 'label-status-table '),
                  'opd_id' => $v['opd_id'],
                  'pedasi_name' => $v['pedasi_name'],
                  'request_rating' => $v['request_rating'],
                  'title' => $v['title'],
                  'start_date' => date('d M Y', strtotime($v['start_date'])),
                  'end_date' => date('d M Y', strtotime($v['end_date'])),
                  'limit_date' => date('d M Y', strtotime($v['limit_date'])),
                  'req_name' => $v['req_name'],
                  'days_left' => $v['limit_date'] ? $this->date_extraction->left_days_from_now($v['limit_date']) : NULL,
                  'requested_date' => date('d M Y', strtotime($v['requested_date'])),
                  'requested_time' => date('H:i:s', strtotime($v['requested_date']))
               );
            }
            $response['result'] = $list;
            $response['total'] = $this->m_kadis->getting('count', $this->_get, $opd['opd_id'], $this->_user->id);
         }else{
            $response['total'] = 0;
         }
      }
      $this->json_result($response);
   }

   public function detail(){
      $response['request'] = $this->m_kadis->request_detail($this->_get['id']);
      $response['request_files'] = $this->m_kadis->request_files($this->_get['id']);
      $response['attachments'] = $this->global_mapping->get_attachments_request($this->_get['id']);
      if($this->_get['status'] == ''){
         $response['kasie'] = $this->m_kadis->get_kasie($this->_get['opd']);
      }else{
         $response['kasie'] = NULL;
      }
      $response['status_label'] = $this->global_mapping->request_status($response['request']['status'], 'label-status ');
      $this->json_result($response);
   }

   public function abort_disposisi(){
      $response['success'] = FALSE;
      $this->db->trans_start();
      $data['status'] = NULL;
      $data['disposisi_kasie'] = NULL;
      $data['disposisi_kasie_created'] = NULL;
      $data['disposisi_remark'] = NULL;
      $data['disposisi_assignby'] = NULL;
      $this->db->update('request', $data, array('id' => $this->_get['id']));
      $this->db->trans_complete();

      if($this->db->trans_status()){
         $this->db->trans_commit();
         $response['success'] = TRUE;
         $response['msg'] = 'Disposisi berhasil dibatalkan';
      }else{
         $this->db->trans_rollback();
         $response['msg'] = 'Some error occured';
      }
      $this->json_result($response);
   }

}
