<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Pedasi extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('m_pedasi');
		$this->load->library('global_mapping');
		$this->load->library('date_extraction');
   }

   public function index(){
    	$data['_css'] = array(
         $this->_path_template.'/plugins/krajee-star-rating/star-rating.min.css',
         $this->_path_template.'/plugins/krajee-star-rating/theme.css'
      ); 
      $data['_js'] = array(
         $this->_path_template.'/plugins/krajee-star-rating/star-rating.min.js',
		);	
      $this->render_page($data, 'pedasi', 'modular');
   }

   public function getting(){
      $response['result'] = array();
      $opd = $this->db->where('user_id', $this->_user->id)->get('mapping_opd')->row_array();
      if($opd){
         $data = $this->m_pedasi->getting('get', $this->_get, $opd['opd_id'], $this->_user->id);
         if($data){
            foreach ($data as $v) {
               $list[] = array(
                  'id' => $v['id'],
                  'status' => $v['status'],
                  'status_label' => $this->global_mapping->request_status($v['status'], 'label-status-table '),
                  'status_pengolahan_label' => $this->global_mapping->request_status_pengolahan($v['status_pengolahan'], 'label-status-table '),
                  'opd_id' => $v['opd_id'],
                  'pedasi_name' => $v['pedasi_name'],
                  'title' => $v['title'],
                  'start_date' => date('d M Y', strtotime($v['start_date'])),
                  'end_date' => date('d M Y', strtotime($v['end_date'])),
                  'limit_date' => date('d M Y', strtotime($v['limit_date'])),
                  'req_name' => $v['req_name'],
                  'days_left' => $v['limit_date'] ? $this->date_extraction->left_days_from_now($v['limit_date']) : NULL,
                  'requested_date' => date('d M Y', strtotime($v['requested_date'])),
                  'requested_time' => date('H:i:s', strtotime($v['requested_date']))
               );
            }
            $response['result'] = $list;
            $response['total'] = $this->m_pedasi->getting('count', $this->_get, $opd['opd_id'], $this->_user->id);
         }else{
            $response['total'] = 0;
         }
      }
      $this->json_result($response);
   }

}
