<div class="row" style="margin-bottom: 5px;">
	<div class="col-md-12">
		<div class="btn-toolbar pull-left">
			<div class="btn-group" data-toggle="buttons">
				<select type="text" class="form-control input-sm" id="filt_status">
					<option value="all">Semua Status</option>
					<option value="1">Kompilasi Data</option>
					<option value="2">Validasi Kasie</option>
					<option value="3">Diserahkan</option>
				</select>
			</div>
     		<div class="btn-group" data-toggle="buttons">
				<input type="text" class="form-control input-sm" id="filt_keyword" placeholder="Pencarian...">
			</div>
			<div class="btn-group" data-toggle="buttons">
				<button class="btn btn-sm btn-default btn-filter"><i class="fa fa-filter"></i> Filter</button>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="box box-solid" id="cont-monitoring">
       	<div class="box-body">
				<table class="table table-striped table-condensed" style="font-size:12px;">
					<thead>
						<th>Tgl <a class="change_order" href="#" data-order="a.requested_date" data-by="asc"><i class="fa fa-sort-desc"></i></th>
						<th>Permohonan Data <a class="change_order" href="#" data-order="a.title" data-by="asc"><i class="fa fa-sort"></i></th>
						<th>Mulai <a class="change_order" href="#" data-order="a.start_date" data-by="asc"><i class="fa fa-sort"></i></th>
						<th>Selesai <a class="change_order" href="#" data-order="a.end_date" data-by="asc"><i class="fa fa-sort"></i></th>
						<th>Batas Waktu <a class="change_order" href="#" data-order="a.limit_date" data-by="asc"><i class="fa fa-sort"></i></th>
						<th>Status Permohonan <a class="change_order" href="#" data-order="a.status" data-by="asc"><i class="fa fa-sort"></i></th>						
						<th></th>
					</thead>
          		<tbody class="sect-data"></tbody>
				</table>
    		</div>
    		<div class="box-footer">
           <div class="row">
               <div class="col-md-2">
             		<h5 class="sect-total"></h5>
               </div>
               <div class="col-md-10 text-right">
                  <ul class="pagination sect-pagination pagination-sm no-margin pull-right"></ul>
               </div>
            </div>
         </div>
 		</div>
	</div>
</div>

<div class="modal fade in" id="modal-detail">
 	<div class="modal-dialog">
     	<div class="modal-content" style="width: 700px;margin-left: -50px;">
         <div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal">
         		<span aria-hidden="true">×</span>
         	</button>
          	<h4 class="modal-title"><i class="fa fa-file"></i>&nbsp;Detail Permohonan Data</h4>
         </div>
      	<div class="modal-body"></div>
   	</div>
	</div>
</div>

<script type="text/javascript">

	$(function () {

		_offset = 0;
	   _curpage = 1;

	   $.fn.todolist = function(option){
	      var param = $.extend({
	         filt_keyword : $('#filt_keyword').val(),
	         filt_status : $('#filt_status').val(),
	         limit : 10,
	         offset : _offset, 
	         currentPage : _curpage,
	         order : 'a.id', 
	         orderby : 'desc'
	      }, option);
	      
	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'opd/pedasi/getting',
	         dataType : "JSON",
	         data : {
	            offset : param.offset,
	            filt_status : param.filt_status,
	            filt_keyword : param.filt_keyword,
	            limit : param.limit,
	            order : param.order,
	            orderby : param.orderby
	         },
	         beforeSend: function (xhr) {
	            loading_table('#cont-monitoring', 'show');
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	            loading_table('#cont-monitoring', 'hide');
	         },
	         success : function(r){
	            var t = '';
               if(r.result){
               	if(r.total > 0){
	                  var total = r.total;
	                  $.each(r.result, function(k,v){
	                  	t += '<tr>';
	                  		t += '<td>';
	                  			t += v.requested_date+'<br>'+v.requested_time;
	                  		t += '</td>';
	                  		t += '<td>';
	                     		t += v.title;
	                     		t += '<input class="request-rating" value="'+(v.request_rating ? v.request_rating : 0)+'" type="text" class="rating" data-size="sm" data-min="0" data-max="10" data-step="2">';
	                  		t += '</td>';
	                  		t += '<td>'+v.start_date+'</td>';
	                  		t += '<td>'+v.end_date+'</td>';
	                  		t += '<td>';
	                  			t += v.limit_date;
	                  			t += '<br><span style="font-size:10px;font-weight:bold">'+v.days_left+' Days Left</span>';
	                  		t += '</td>';
	                  		t += '<td>'+v.status_label+'</td>';
	                  		// t += '<td>'+v.status_pengolahan_label+'</td>';
	                  		t += '<td>';
	                  			t += '<div class="btn-group">';
		                           t += '<button class="btn btn-detail btn-default btn-xs" data-toggle="tooltip" data-title="Detail" data-opd="'+v.opd_id+'" data-status="'+v.status+'" data-id="'+v.id+'" data-pedasi="'+v.pedasi_name+'"><i class="fa fa-file"></i></button>';
		                        t += '</div>';
	                  		t += '</td>';
	                  	t += '</tr>';
	                  });
                  }else{
               		t += '<tr><td class="text-center text-muted" colspan="7">Data tidak ditemukan</td></tr>';
                  }
               }else{
               	t += '<tr><td class="text-center text-muted" colspan="7">OPD anda tidak ditemukan</td></tr>';
               }

	            $('#cont-monitoring').find('.sect-total').html(''+number_format(total)+' Rows Data');
	            $('#cont-monitoring').find('.sect-data').html(t);
	            $(".request-rating").rating({
	            	displayOnly: true
	            });

	            $('#cont-monitoring .sect-pagination').pagination({
			         items: r.total,
			         itemsOnPage: param.limit,
			         edges: 0,
			         hrefTextPrefix: '',
			         displayedPages: '',
			         currentPage : param.currentPage,
			         prevText : '&laquo;',
			         nextText : '&raquo;',
			         dropdown: true,
			         onPageClick : function(n,e){
			            e.preventDefault();
			            _offset = (n-1)*param.limit;
			            _curpage = n;
			            $(this).todolist();
			         }
			      });
	         },
	         complete : function(r){
	         	loading_table('#cont-monitoring', 'hide');
         	}
	      });
	   }


	   $(this).on('click', '.btn-filter',function(e){
	   	_offset = 0;
	   	_curpage = 1;
	   	$(this).todolist();
	   });

	   $(this).on('click', '.btn-detail',function(e){
         var id = $(this).data('id');
         var form = $('#modal-detail');

         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'widget_detail/pedasi',
            dataType : "JSON",
            data : {
               id : id
            },
            beforeSend: function (xhr) {
               loading_button('.btn-detail', id, 'show', '<i class="fa fa-file"></i>', '');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_button('.btn-detail', id, 'hide', '<i class="fa fa-file"></i>', '');
            },
            success: function(r){
               session_checked(r._session, r._maintenance);
               form.find('.modal-body').html(r.html);
            },
            complete: function(){
               $("#modal-detail").modal("toggle");
               loading_button('.btn-detail', id, 'hide', '<i class="fa fa-file"></i>', '');
            }
         });
      });

	   $(this).on('click', '.change_order', function(e){
	      e.preventDefault();
	      $('.change_order').html('<i class="fa fa-sort"></i>');
	      $(this).find('i').remove();
	      var sent = $(this).data('order');
	      var by = $(this).attr('data-by');
	      if(by === 'asc'){ 
	         $(this).attr('data-by', 'desc');
	         $(this).html('<i class="fa fa-sort-asc"></i>');
	      }
	      else{ 
	         $(this).attr('data-by', 'asc');
	         $(this).html(' <i class="fa fa-sort-desc"></i>');
	      }
	     $(this).getting({order:sent,orderby:by});
	   });

   	$(this).todolist();

   });

</script>