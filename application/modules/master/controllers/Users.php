<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Users extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('m_users');
		$this->load->library('form_validation');
		$this->load->library('image_uploader');
   }

   public function index(){
      $data['pangkat'] = $this->db->where('status', 1)->get('pangkat')->result_array();
      $data['jabatan'] = $this->db->where('status', 1)->get('jabatan')->result_array();
   	$data['m_groups'] = $this->db->get('groups')->result_array();
    	$data['_css'] = array(
         $this->_path_template.'/plugins/fancybox/jquery.fancybox.min.css',
         $this->_path_template.'/plugins/select2/select2.min.css',
      ); 
      $data['_js'] = array(
			$this->_path_template.'/plugins/fancybox/jquery.fancybox.min.js',
         $this->_path_template.'/plugins/select2/select2.full.min.js'
		);	
      $this->render_page($data, 'users', 'modular');
   }

   public function usropd(){
      $data['pangkat'] = $this->db->where('status', 1)->get('pangkat')->result_array();
      $data['jabatan'] = $this->db->where('status', 1)->get('jabatan')->result_array();
      $data['m_groups'] = $this->db->get('groups')->result_array();
      $data['_css'] = array(
         $this->_path_template.'/plugins/fancybox/jquery.fancybox.min.css',
         $this->_path_template.'/plugins/select2/select2.min.css',
      ); 
      $data['_js'] = array(
         $this->_path_template.'/plugins/fancybox/jquery.fancybox.min.js',
         $this->_path_template.'/plugins/select2/select2.full.min.js'
      ); 
      $this->render_page($data, 'usropd', 'modular');
   }

   public function getting(){
      $list = array();
      $data = $this->m_users->getting('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'avatar_path' => $this->image_uploader->fileexistspicture('files/profil_photos/', $v['avatar']),
               'avatar' => $v['avatar'],
               'fullname' => $v['first_name'],
               'dinas' => $v['dinas'],
               'phone' => $v['phone'],
               'username' => $v['username'],
               'pangkat_name' => $v['pangkat_name'],
               'jabatan_name' => $v['jabatan_name'],
               'golongan' => $v['golongan'],
               'pendidikan' => $v['pendidikan'],
               'email' => $v['email'],
               'active' => $v['active'],
               'created_on' => date('d M Y H:i:s', $v['created_on']),
               'role' => $v['role_group']
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->m_users->getting('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

   public function gettingbyopd(){
      $list = array();
      $opd = $this->db->where('user_id', $this->_user->id)->get('mapping_opd')->row_array();
      $data = $this->m_users->getting_by_opd('get', $this->_get, $opd['opd_id'], $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'avatar_path' => $this->image_uploader->fileexistspicture('files/profil_photos/', $v['avatar']),
               'avatar' => $v['avatar'],
               'fullname' => $v['first_name'],
               'dinas' => $v['dinas'],
               'phone' => $v['phone'],
               'username' => $v['username'],
               'pangkat_name' => $v['pangkat_name'],
               'jabatan_name' => $v['jabatan_name'],
               'golongan' => $v['golongan'],
               'pendidikan' => $v['pendidikan'],
               'email' => $v['email'],
               'active' => $v['active'],
               'created_on' => date('d M Y H:i:s', $v['created_on']),
               'role' => $v['role_group']
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->m_users->getting_by_opd('count', $this->_get, $opd['opd_id'], $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

   public function create(){
   	$response['success'] = FALSE;
   	$this->form_validation->set_rules('first_name', 'Nama Lengkap', 'required');
   	$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
   	$this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]');
      $this->form_validation->set_rules('username_telegram', 'Telegram', 'required|is_unique[users.username_telegram]');
   	$this->form_validation->set_rules('password', 'Password', 'required');
      $this->form_validation->set_rules('dinas', 'Dinas', 'required');
      $this->form_validation->set_rules('phone', 'No HP', 'required|numeric|is_unique[users.phone]');
   	if($this->form_validation->run()){
   		$result = $this->m_users->create($this->_post);
   		$response['success'] = $result['success'];
         $response['msg'] = $result['msg'];
   	}else{
   		$response['msg'] = validation_errors();
   	}
   	$this->json_result($response);
   }

   public function delete(){
      $response['success'] = FALSE;
      $result = $this->m_users->delete($this->_post);
      if($result){
         $response['success'] = TRUE;
         $response['msg'] = 'Data berhasil di hapus';
      }else{
         $response['msg'] = 'Function Failed';
      }
      $this->json_result($response);
   }

   public function modify(){
      $this->load->library('image_uploader');
      $response['success'] = FALSE;
      $response['users'] = $this->db->where('id', $this->_get['id'])->get('users')->row_array();
      $response['groups'] = $this->db->where('user_id', $this->_get['id'])->get('users_groups')->row_array();
      $response['avatar'] = $this->image_uploader->fileexistspicture('files/profil_photos/', $response['users']['avatar']);
      $this->json_result($response);
   }

   public function change(){
      $response['success'] = FALSE;
      $avatar_change = FALSE;

      if($this->_post['email'] != $this->_post['before_email']){
         $this->form_validation->set_rules('email', 'Email ', 'required|valid_email|is_unique[users.email]');
      }else{
         $this->form_validation->set_rules('email', 'Email ', 'required|valid_email');
      }
      if($this->_post['username'] != $this->_post['before_username']){
         $this->form_validation->set_rules('username', 'Username ', 'required|is_unique[users.username]');
      }else{
         $this->form_validation->set_rules('username', 'Username ', 'required');
      }
      if($this->_post['username_telegram'] != $this->_post['before_username_telegram']){
         $this->form_validation->set_rules('username_telegram', 'Telegram ', 'required|is_unique[users.username_telegram]');
      }else{
         $this->form_validation->set_rules('username_telegram', 'Telegram ', 'required');
      }
      if($this->_post['phone'] != $this->_post['before_phone']){
         $this->form_validation->set_rules('phone', 'No HP ', 'required|numeric|is_unique[users.phone]');
      }else{
         $this->form_validation->set_rules('phone', 'No HP ', 'required');
      }
      $this->form_validation->set_rules('first_name', 'Nama Lengkap', 'required');

      if($this->form_validation->run()){
         if (!empty($_FILES['photo_path']['name'])){
            $file_path = './files/profil_photos/';
            $upload = $this->image_uploader->upload($_FILES['photo_path']['name'], date('His'), $file_path);
            if($upload['success']){
               $avatar_change = TRUE;
               $user['avatar'] = $upload['success'] ? $upload['file_name'] : NULL;
            }else{
               $response['msg'] = $upload['msg'];
               $this->json_result($response);
            }
         }

         $this->db->trans_start();
         $user['first_name'] = $this->_post['first_name'];
         $user['dinas'] = $this->_post['dinas'];
         $user['pangkat_id'] = $this->_post['pangkat_id'] ? $this->_post['pangkat_id'] : NULL;
         $user['jabatan_id'] = $this->_post['jabatan_id'] ? $this->_post['jabatan_id'] : NULL;
         $user['golongan'] = $this->_post['golongan'] ? $this->_post['golongan'] : NULL;
         $user['pendidikan'] = $this->_post['pendidikan'] ? $this->_post['pendidikan'] : NULL;
         $user['username_telegram'] = $this->_post['username_telegram'] ? $this->_post['username_telegram'] : NULL;
         $user['phone'] = $this->_post['phone'];
         $user['email'] = $this->_post['email'];
         $user['username'] = $this->_post['username'];
         $this->ion_auth->update($this->_post['id'], $user);
         $this->ion_auth->remove_from_group(false, $this->_post['id']);
         $this->ion_auth->add_to_group($this->_post['role'], $this->_post['id']);
         $this->db->trans_complete();

         if($this->db->trans_status()){
            $this->db->trans_commit();
            if($avatar_change){
               if($this->_post['before_avatar'] != ''){
                  if (file_exists('./files/profil_photos/'.$this->_post['before_avatar'].'')) {
                     unlink('./files/profil_photos/'.$this->_post['before_avatar'].'');
                  }
               }
            }
            $response['success'] = TRUE;
            $response['msg'] = 'Data berhasil diperbaharui';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Function failed';
         }
      }else{
         $response['msg'] = validation_errors();
      }
      $this->json_result($response);
   }

   public function change_pwd(){
      $response['success'] = FALSE;
      $this->load->library('ion_auth');
      $this->load->library('validation_users');

      $this->form_validation->set_rules('password', 'Password', 'required');
      $this->form_validation->set_rules('passwordconf', 'Password confirmation', 'required|matches[password]');

      if($this->form_validation->run()){
         $user = array(
            'password' => $this->_post['password'],
            'password_show' => $this->_post['password']
         );
         $this->db->trans_start();
         $this->ion_auth->update($this->_post['id'], $user);
         $this->db->trans_complete();

         if($this->db->trans_status()){
            $this->db->trans_commit();
            $response['success'] = TRUE;
            $response['msg'] = 'Password berhasil diperbaharui';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Function failed';
         }
      }else{
         $response['msg'] = validation_errors();
      }
      $this->json_result($response);
   }

}