<?php
if (!defined('BASEPATH'))
   exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Jabatan extends MY_Controller {

   public function __construct() {
      parent::__construct();
      $this->load->model('m_jabatan');
      $this->load->library('form_validation');
   }   

   public function index(){
      $data['_css'] = array(
         $this->_path_template.'/plugins/select2/select2.min.css',
      ); 
      $data['_js'] = array(
         $this->_path_template.'/plugins/select2/select2.full.min.js'
      ); 
      $this->render_page($data, 'jabatan', 'modular');
   }

   public function getting(){
      $list = array();
      $data = $this->m_jabatan->getting('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'name' => $v['name'],
               'status' => $v['status']
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->m_jabatan->getting('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

   public function create(){
      $response['success'] = FALSE;
      $this->form_validation->set_rules('name', 'Nama Jabatan', 'required');
      if($this->form_validation->run()){
         $result = $this->m_jabatan->create($this->_post);
         $response['success'] = $result['success'];
         $response['msg'] = $result['msg'];
      }else{
         $response['msg'] = validation_errors();
      }
      $this->json_result($response);
   }

   public function change(){
      $response['success'] = FALSE;
      $this->form_validation->set_rules('name', 'Nama Jabatan', 'required');

      if($this->form_validation->run()){
         $result = $this->m_jabatan->change($this->_post);
         $response['success'] = $result['success'];
         $response['msg'] = $result['msg'];
      }else{
         $response['msg'] = validation_errors();
      }
      $this->json_result($response);
   }

   public function delete(){
      $response['success'] = FALSE;
      $result = $this->m_jabatan->delete($this->_post);
      if($result){
         $response['success'] = $result['success'];
         $response['msg'] = $result['msg'];
      }else{
         $response['msg'] = 'Function Failed';
      }
      $this->json_result($response);
   }

   public function modify(){
      $response['jabatan'] = $this->db->where('id', $this->_get['id'])->get('jabatan')->row_array();
      $this->json_result($response);
   }
}