<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Opd extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('m_opd');
      $this->load->library('form_validation');
      $this->load->library('mailing');
   }

   public function get_master_group(){
      $response['data'] = $this->m_opd->master_groups($this->_get['role'], FALSE);
      $this->json_result($response);
   }

   public function index(){
      $data['kadis'] = $this->m_opd->master_groups(3, FALSE);
      $data['kasie'] = $this->m_opd->master_groups(4, FALSE);
      $data['pedasi'] = $this->m_opd->master_groups(5, FALSE);
    	$data['_css'] = array(
         $this->_path_template.'/plugins/select2/select2.min.css',
      ); 
      $data['_js'] = array(
         $this->_path_template.'/plugins/select2/select2.full.min.js'
		);	
      $this->render_page($data, 'opd', 'modular');
   }

   public function getting(){
      $list = array();
      $data = $this->m_opd->getting('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['opd_id'],
               'phone' => $v['phone'],
               'name' => $v['opd_name'],
               'description' => $v['opd_description'],
               'status' => $v['status'],
               'kadis' => $v['kadis_name'],
               'pedasi' => $v['pedasi_name'],
               'kasie' => $v['kasie_name']
            );
         }
         $response['result'] = $list;
         $response['total'] = $this->m_opd->getting('count', $this->_get, $this->_user->id);
      }else{
         $response['total'] = 0;
      }
      $this->json_result($response);
   }

   public function create(){
      $response['success'] = FALSE;
      $this->form_validation->set_rules('name', 'Nama OPD', 'required');
      $this->form_validation->set_rules('kadis', 'Kepala dinas', 'required');
      $this->form_validation->set_rules('pedasi', 'Pedasi', 'required');
      $this->form_validation->set_rules('kasie[]', 'Kepala Seksi', 'required');
      if($this->form_validation->run()){
         $result = $this->m_opd->create($this->_post);
         $response['success'] = $result['success'];
         $response['msg'] = $result['msg'];
         $response['result'] = $result;
      }else{
         $response['msg'] = validation_errors();
      }
      $this->json_result($response);
   }

   public function change(){
      $response['success'] = FALSE;
      $this->form_validation->set_rules('name', 'Nama OPD', 'required');
      $this->form_validation->set_rules('kadis', 'Kepala dinas', 'required');
      $this->form_validation->set_rules('pedasi', 'Pedasi', 'required');
      $this->form_validation->set_rules('kasie[]', 'Kepala Seksi', 'required');

      if($this->form_validation->run()){
         $result = $this->m_opd->change($this->_post);
         $response['success'] = $result['success'];
         $response['msg'] = $result['msg'];
      }else{
         $response['msg'] = validation_errors();
      }
      $this->json_result($response);
   }

   public function delete(){
      $response['success'] = FALSE;
      $result = $this->m_opd->delete($this->_post);
      if($result){
         $response['success'] = $result['success'];
         $response['msg'] = $result['msg'];
      }else{
         $response['msg'] = 'Function Failed';
      }
      $this->json_result($response);
   }

   public function modify(){
      $response['pedasi'] = $this->db->select('user_id')->where('opd_id', $this->_get['id'])->where('group_id', 5)->get('mapping_opd')->row_array();
      $role_pedasi = $response['pedasi'] ? array($response['pedasi']['user_id']) : FALSE;
      $response['mpedasi'] = $this->m_opd->master_groups(5, $role_pedasi);

      $response['kadis'] = $this->db->select('user_id')->where('opd_id', $this->_get['id'])->where('group_id', 3)->get('mapping_opd')->row_array();
      $role_kadis = $response['kadis'] ? array($response['kadis']['user_id']) : FALSE;
      $response['mkadis'] = $this->m_opd->master_groups(3, $role_kadis);

      $kasie = $this->db->select('user_id')->where('opd_id', $this->_get['id'])->where('group_id', 4)->get('mapping_opd')->result_array();
      if($kasie){
         foreach ($kasie as $v) {
            $kasie_list[] = $v['user_id'];
         }
      }else{
         $kasie_list = NULL;
      }
      $response['kasie'] = $kasie_list;
      $response['mkasie'] = $this->m_opd->master_groups(4, $kasie_list);

      $response['opd'] = $this->db->where('id', $this->_get['id'])->get('opd')->row_array();
      $this->json_result($response);
   }

   public function opd_map_edit(){
      $role = $this->db->where('opd_id', $this->_get['id'])->where('group_id', $this->_get['group_id'])->get('mapping_opd')->row_array();
      $role_map = $role ? array($role['user_id']) : FALSE;
      $response['result'] = $this->m_opd->master_groups($this->_get['group_id'], $role_map);
      $response['role_map'] = $role_map;
      $this->json_result($response);
   }
}