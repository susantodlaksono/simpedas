
<div class="row" style="margin-bottom: 5px;">
	<div class="col-md-12">
		<div class="btn-toolbar pull-left">
			<div class="btn-group" data-toggle="buttons">
				<input type="text" class="form-control input-sm" id="filt_keyword" placeholder="Pencarian...">
			</div>
		</div>
		<div class="btn-toolbar pull-right">
			<div class="btn-group" data-toggle="buttons">
				<button class="btn btn-primary btn-sm btn-create" data-toggle="modal" data-target="#modal-create">
					<i class="fa fa-plus"></i> Tambah
				</button>
         </div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="box box-solid" id="cont-data" style="font-size:12px;">
       	<div class="box-body">
				<table class="table table-hover table-condensed">
					<thead>
						<th>Nama <a class="change_order" href="#" data-order="name" data-by="asc"><i class="fa fa-sort"></i></th>
						<th>Active <a class="change_order" href="#" data-order="active" data-by="asc"><i class="fa fa-sort"></i></th>
					</thead>
          		<tbody class="sect-data"></tbody>
				</table>
    		</div>
    		<div class="box-footer" style="">
           <div class="row">
               <div class="col-md-2">
             		<h5 class="sect-total"></h5>
               </div>
               <div class="col-md-10 text-right">
                  <ul class="pagination sect-pagination pagination-sm no-margin pull-right"></ul>
               </div>
            </div>
         </div>
		</div>
	</div>
</div>

<div class="modal fade in" id="modal-create" style="">
 	<div class="modal-dialog">
     	<div class="modal-content">
         <div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal">
         		<span aria-hidden="true">×</span>
         	</button>
          	<h4 class="modal-title"><i class="fa fa-plus"></i>&nbsp;Tambah Pangkat</h4>
         </div>
         <form id="form-create">                
          	<div class="modal-body">
          		<div class="form-group">
                 	<label>Nama Pangkat <span class="text-danger">*</span></label>
                 	<input type="text" class="form-control input-sm" name="name">
             	</div>
             	<div class="form-group">
                 	<label>Status </label>
                 	<select class="form-control input-sm" name="status">
                 		<option value="1">Active</option>
                 		<option value="">Inactive</option>
                 	</select>
             	</div>
          	</div>
          	<div class="modal-footer">
              	<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
             </div>
      	</form>        
     	</div>
 	</div>
</div>

<div class="modal fade in" id="modal-edit" style="">
 	<div class="modal-dialog">
     	<div class="modal-content">
         <div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal">
         		<span aria-hidden="true">×</span>
         	</button>
          	<h4 class="modal-title"><i class="fa fa-edit"></i>&nbsp;Edit Pangkat</h4>
         </div>
         <form id="form-edit">                
         	<input type="hidden" name="id">
          	<div class="modal-body">
          		<div class="form-group">
                 	<label>Nama Pangkat <span class="text-danger">*</span></label>
                 	<input type="text" class="form-control input-sm" name="name">
             	</div>
             	<div class="form-group">
                 	<label>Status </label>
                 	<select class="form-control input-sm" name="status">
                 		<option value="1">Active</option>
                 		<option value="">Inactive</option>
                 	</select>
             	</div>
          	</div>
          	<div class="modal-footer">
              	<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
             </div>
      	</form>        
     	</div>
 	</div>
</div>

<script type="text/javascript">

	$(function () {

		_offset = 0;
	   _curpage = 1;

		$.fn.getting = function(option){
	      var param = $.extend({
	         filt_keyword : $('#filt_keyword').val(),
	         limit : 10,
	         offset : _offset, 
	         currentPage : _curpage,
	         order : 'id', 
	         orderby : 'desc'
	      }, option);
	      
	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'master/pangkat/getting',
	         dataType : "JSON",
	         data : {
	            offset : param.offset,
	            filt_keyword : param.filt_keyword,
	            limit : param.limit,
	            order : param.order,
	            orderby : param.orderby
	         },
	         beforeSend: function (xhr) {
	            loading_table('#cont-data', 'show');
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown); 
	            loading_table('#cont-data', 'hide');
	         },
	         success : function(r){
	            var t = '';
               if(r.total){
                  var total = r.total;
                  $.each(r.result, function(k,v){
                     t += '<tr>';
                     t += '<td>'+v.name+'</td>';
                     if(v.status == 1){
                        t += '<td><span class="label label-success bold" style="padding:5px;">YES</span></td>';
                     }else{
                        t += '<td><span class="label label-danger bold">NO</span></td>';
                     }
                     t += '<td>';
                        t += '<div class="btn-group">';
                           t += '<button class="btn btn-edit btn-default btn-xs" data-toggle="tooltip" data-title="Edit" data-id="'+v.id+'"><i class="fa fa-edit"></i></button>';
                           if(id_role != 2 && id_role != 5 && id_role != 3 && id_role != 4){
                           	t += '<button class="btn btn-delete btn-default btn-xs" data-toggle="tooltip" data-title="Delete" data-id="'+v.id+'"><i class="fa fa-trash"></i></button>';
                           }
                        t += '</div>';
                     t += '</td>';
                     t += '</tr>';
                  });
               }else{
                  t += '<tr><td class="text-center text-muted" colspan="10">No Result</td></tr>';
               }

	            $('#cont-data').find('.sect-total').html(''+number_format(total)+' Rows Data');
	            $('#cont-data').find('.sect-data').html(t);

	            $('#cont-data .sect-pagination').pagination({
			         items: r.total,
			         itemsOnPage: param.limit,
			         edges: 0,
			         hrefTextPrefix: '',
			         displayedPages: 1,
			         currentPage : param.currentPage,
			         prevText : '&laquo;',
			         nextText : '&raquo;',
			         drpangkatown: true,
			         onPageClick : function(n,e){
			            e.preventDefault();
			            _offset = (n-1)*param.limit;
			            _curpage = n;
			            $(this).getting();
			         }
			      });
	         },
	         complete : function(r){
	         	loading_table('#cont-data', 'hide');
         	}
	      });
	   }

	   $(this).on('change', '#filt_keyword', function(e) {
	      e.preventDefault();
	      _offset = 0;
	      _curpage = 1;
	      $(this).getting();
	   });
	   
	   $(this).on('click', '.btn-edit',function(e){
	      var id = $(this).data('id');
	      var form = $('#form-edit');

	      ajaxManager.addReq({
	         type : "GET",
	         url : site_url + 'master/pangkat/modify',
	         dataType : "JSON",
	         data : {
	            id : id
	         },
	         beforeSend: function (xhr) {
	            form.resetForm();
	            loading_button('.btn-edit', id, 'show', '<i class="fa fa-edit"></i>', '');
	         },
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown);
	            loading_button('.btn-edit', id, 'hide', '<i class="fa fa-edit"></i>', '');
	         },
	         success: function(r){
	            session_checked(r._session, r._maintenance);
	            if(r.pangkat){
	            	form.find('input[name="id"]').val(r.pangkat.id);
		            form.find('input[name="name"]').val(r.pangkat.name);
		            form.find('select[name="status"]').val(r.pangkat.status);
	            }

	         },
	         complete: function(){
	         	$("#modal-edit").modal("toggle");
	            loading_button('.btn-edit', id, 'hide', '<i class="fa fa-edit"></i>', '');
	         }
	      });
	      e.preventDefault();
	   });

	   $(this).on('click', '.btn-delete', function(e){
	      var conf = confirm('Are you sure ?');
	      if(conf){
	         var id = $(this).data('id');
	         ajaxManager.addReq({
	            type : "POST",
	            url : site_url + 'master/pangkat/delete',
	            dataType : "JSON",
	            data : {
	               id : id,
	               "flipbooktoken2020" : _csrf_hash
	            },
	            beforeSend: function (xhr) {
	               loading_button('.btn-delete', id, 'show', '<i class="fa fa-trash"></i>', '');
	            },
	            error: function (jqXHR, status, errorThrown) {
	               error_handle(jqXHR, status, errorThrown);
	               loading_button('.btn-delete', id, 'hide', '<i class="fa fa-trash"></i>', '');
	            },
	            success : function(r){
	               session_checked(r._session, r._maintenance);
	               set_csrf(r._token_hash);
	               if(r.success){
	                  $(this).getting();
	               }
	               $.snackbar({
	                	content: r.msg,
	                	timeout: 5000
	               });
	               loading_button('.btn-delete', id, 'hide', '<i class="fa fa-trash"></i>', '');
	            }
	         });
	      }else{
	         return false;
	      }
	      e.preventDefault();
	   });

	   $(this).on('submit', '#form-edit', function(e){
	      var form = $(this);
	      $(this).ajaxSubmit({
	         url : site_url + 'master/pangkat/change',
	         type : "POST",
	         dataType : "JSON",
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown);
	            loading_form(form, 'hide', '<i class="fa fa-save"></i> Update');
	         },
	         beforeSend: function (xhr) {
	            loading_form(form, 'show', loadingbutton);
	         },
	         success: function(r) {
	         	session_checked(r._session, r._maintenance);
	            if(r.success){
	               _offset = 0;
	               _curpage = 1;
	               $(this).getting();
	               toastr.success(r.msg);
	               $("#modal-edit").modal("toggle");
	            }else{
	            	toastr.error(r.msg);
	            }
	            loading_form(form, 'hide', '<i class="fa fa-save"></i> Update');
	         },
	      });
	      e.preventDefault();
	   });

		$(this).on('submit', '#form-create', function(e){
	      var form = $(this);
	      $(this).ajaxSubmit({
	         url : site_url + 'master/pangkat/create',
	         type : "POST",
	         dataType : "JSON",
	         error: function (jqXHR, status, errorThrown) {
	            error_handle(jqXHR, status, errorThrown);
	            loading_form(form, 'hide', '<i class="fa fa-save"></i> Simpan');
	         },
	         beforeSend: function (xhr) {
	            loading_form(form, 'show', loadingbutton);
	         },
	         success: function(r) {
	         	session_checked(r._session, r._maintenance);
	            if(r.success){
	            	form.resetForm();
	               _offset = 0;
	               _curpage = 1;
	               $(this).getting();
	               toastr.success(r.msg);
	               $("#modal-create").modal("toggle");
	            }else{
	            	toastr.error(r.msg);
	            }
	            loading_form(form, 'hide', '<i class="fa fa-save"></i> Simpan');
	         },
	      });
	      e.preventDefault();
	   });

	   $(this).on('click', '.change_order', function(e){
	      e.preventDefault();
	      $('.change_order').html('<i class="fa fa-sort"></i>');
	      $(this).find('i').remove();
	      var sent = $(this).data('order');
	      var by = $(this).attr('data-by');
	      if(by === 'asc'){ 
	         $(this).attr('data-by', 'desc');
	         $(this).html('<i class="fa fa-sort-asc"></i>');
	      }
	      else{ 
	         $(this).attr('data-by', 'asc');
	         $(this).html(' <i class="fa fa-sort-desc"></i>');
	      }
	     $(this).getting({order:sent,orderby:by});
	   });

	   $(this).getting();

	});

</script>