<div class="row" style="margin-bottom: 5px;">
   <div class="col-md-12">
      <div class="btn-toolbar pull-left">
         <div class="btn-group" data-toggle="buttons">
            <input type="text" class="form-control input-sm" id="filt_keyword" placeholder="Pencarian...">
         </div>
      </div>
      <div class="btn-toolbar pull-right">
         <div class="btn-group" data-toggle="buttons">
            <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-create">
               <i class="fa fa-plus"></i> Tambah
            </button>
         </div>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-md-12">
      <div class="box box-solid" id="cont-data" style="font-size:12px;">
         <div class="box-body">
            <table class="table table-hover table-condensed">
               <thead>
                  <th></th>
                  <th>Nama <a class="change_order" href="#" data-order="a.first_name" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Username <a class="change_order" href="#" data-order="a.username" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Pangkat <a class="change_order" href="#" data-order="d.name" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Jabatan <a class="change_order" href="#" data-order="c.name" data-by="asc"><i class="fa fa-sort"></i></th>
                  <th>Golongan <a class="change_order" href="#" data-order="a.golongan" data-by="asc"><i class="fa fa-sort"></i></th>
                  <!-- <th>Pendidikan <a class="change_order" href="#" data-order="a.pendidikan" data-by="asc"><i class="fa fa-sort"></i></th> -->
                  <!-- <th>No.HP <a class="change_order" href="#" data-order="a.phone" data-by="asc"><i class="fa fa-sort"></i></th> -->
                  <th>Role <a class="change_order" href="#" data-order="f.name" data-by="asc"><i class="fa fa-sort"></i></th>
                  <!-- <th>Dinas <a class="change_order" href="#" data-order="a.dinas" data-by="asc"><i class="fa fa-sort"></i></th> -->
                  <!-- <th>Tgl Didaftarkan <a class="change_order" href="#" data-order="a.created_on" data-by="asc"><i class="fa fa-sort"></i></th> -->
                  <th>Active <a class="change_order" href="#" data-order="a.active" data-by="asc"><i class="fa fa-sort"></i></th>
               </thead>
               <tbody class="sect-data"></tbody>
            </table>
         </div>
         <div class="box-footer" style="">
           <div class="row">
               <div class="col-md-2">
                  <h5 class="sect-total"></h5>
               </div>
               <div class="col-md-10 text-right">
                  <ul class="pagination sect-pagination pagination-sm no-margin pull-right"></ul>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade in" id="modal-create" style="">
   <div class="modal-dialog">
      <div class="modal-content modal-lg" style="margin-left: -144px;">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
               <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title"><i class="fa fa-plus"></i>&nbsp;Tambah Pengguna</h4>
         </div>
         <form id="form-create">                
            <div class="modal-body">
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Nama Lengkap <span class="text-danger">*</span></label>
                        <input type="text" class="form-control input-sm" name="first_name">
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Email <span class="text-danger">*</span></label>
                        <input type="email" class="form-control input-sm" name="email">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Username <span class="text-danger">*</span></label>
                        <input type="text" class="form-control input-sm" name="username">
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Password <span class="text-danger">*</span></label>
                        <div class="input-group">
                           <input type="password" class="form-control input-sm" name="password" id="pwdshow">
                              <span class="btn btn-default input-group-addon" style="cursor: pointer;" onclick="showpass('pwdshow')">
                              <i class="fa fa-eye"></i>
                              </span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Pangkat <span class="text-danger">*</span></label>
                        <select class="form-control input-sm choose" name="pangkat_id">
                           <?php
                           if($pangkat){
                              foreach ($pangkat as $v) {
                                 echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                              }
                           }
                           ?>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Jabatan <span class="text-danger">*</span></label>
                        <select class="form-control input-sm choose" name="jabatan_id">
                           <?php
                           if($jabatan){
                              foreach ($jabatan as $v) {
                                 echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                              }
                           }
                           ?>
                        </select>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Golongan <span class="text-danger">*</span></label>
                        <select class="form-control input-sm choose" name="golongan">
                           <option value="II.c">II/a</option>
                           <option value="II.c">II/b</option>
                           <option value="II.c">II/c</option>
                           <option value="II.d">II/d</option>
                           <option value="III.a">III/a</option>
                           <option value="III.b">III/b</option>
                           <option value="III.c">III/c</option>
                           <option value="III.d">III/d</option>
                           <option value="III.d">IV/a</option>
                           <option value="III.d">IV/b</option>
                           <option value="III.d">IV/c</option>
                           <option value="III.d">IV/d</option>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Pendidikan Terakhir <span class="text-danger">*</span></label>
                        <select class="form-control input-sm choose" name="pendidikan">
                           <option value="SMA">SMA</option>
                           <option value="SMK">SMK</option>
                           <option value="Diploma III">Diploma III</option>
                           <option value="S1">S1</option>
                           <option value="S2">S2</option>
                        </select>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Dinas <span class="text-danger">*</span></label>
                        <input type="text" class="form-control input-sm" name="dinas">
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>No HP <span class="text-danger">*</span></label>
                        <input type="number" class="form-control input-sm" name="phone">
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Telegram <span class="text-danger">*</span></label>
                        <div class="input-group">
                           <span class="input-group-addon">@</span>
                           <input type="text" class="form-control input-sm" name="username_telegram">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="form-group form-group-master-group">
                  <label>Role <span class="text-danger">*</span></label>
                  <select class="form-control input-sm" name="role[]">
                     <?php
                     foreach ($m_groups as $v) {
                        echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                     }
                     ?>
                  </select>
               </div>
               <!-- <div class="form-group form-group-master-group">
                  <label>Status Aktif <span class="text-danger">*</span></label>
                  <select class="form-control input-sm" name="status">
                     <option value="1">Ya</option>
                     <option value="0">Tidak</option>
                  </select>
               </div> -->
               <div class="form-group">
                  <label>Foto Profil</label>
                  <input type="file" class="form-control input-sm" name="photo_path">
                  <span class="text-muted" style="font-size: 9px;">(JPG,JPEG,PNG,GIF) Maks. 1000 x 1500</span>
               </div>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
             </div>
         </form>        
      </div>
   </div>
</div>

<div class="modal fade in" id="modal-edit" style="">
   <div class="modal-dialog">
      <div class="modal-content modal-lg" style="margin-left: -144px;">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
               <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title"><i class="fa fa-edit"></i>&nbsp;Edit Pengguna</h4>
         </div>
         <form id="form-edit">       
            <input type="hidden" name="id">
            <input type="hidden" name="before_email">
            <input type="hidden" name="before_username">
            <input type="hidden" name="before_username_telegram">
            <input type="hidden" name="before_avatar">         
            <input type="hidden" name="before_phone">         
            <div class="modal-body">
               <div class="row">
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Nama Lengkap</label>
                        <input type="text" class="form-control input-sm" name="first_name">
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control input-sm" name="email">
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Username</label>
                        <input type="text" class="form-control input-sm" name="username">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Pangkat <span class="text-danger">*</span></label>
                        <select class="form-control input-sm choose" name="pangkat_id">
                           <?php
                           if($pangkat){
                              foreach ($pangkat as $v) {
                                 echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                              }
                           }
                           ?>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Jabatan <span class="text-danger">*</span></label>
                        <select class="form-control input-sm choose" name="jabatan_id">
                           <?php
                           if($jabatan){
                              foreach ($jabatan as $v) {
                                 echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                              }
                           }
                           ?>
                        </select>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Golongan <span class="text-danger">*</span></label>
                        <select class="form-control input-sm choose" name="golongan">
                           <option value="II.c">II/a</option>
                           <option value="II.c">II/b</option>
                           <option value="II.c">II/c</option>
                           <option value="II.d">II/d</option>
                           <option value="III.a">III/a</option>
                           <option value="III.b">III/b</option>
                           <option value="III.c">III/c</option>
                           <option value="III.d">III/d</option>
                           <option value="III.d">IV/a</option>
                           <option value="III.d">IV/b</option>
                           <option value="III.d">IV/c</option>
                           <option value="III.d">IV/d</option>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Pendidikan Terakhir <span class="text-danger">*</span></label>
                        <select class="form-control input-sm choose" name="pendidikan">
                           <option value="SMA">SMA</option>
                           <option value="SMK">SMK</option>
                           <option value="Diploma III">Diploma III</option>
                           <option value="S1">S1</option>
                           <option value="S2">S2</option>
                        </select>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Dinas <span class="text-danger">*</span></label>
                        <input type="text" class="form-control input-sm" name="dinas">
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>No HP <span class="text-danger">*</span></label>
                        <input type="number" class="form-control input-sm" name="phone">
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Telegram <span class="text-danger">*</span></label>
                        <div class="input-group">
                           <span class="input-group-addon">@</span>
                           <input type="text" class="form-control input-sm" name="username_telegram">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="form-group form-group-master-group">
                  <label>Role <span class="text-danger">*</span></label>
                  <select class="form-control input-sm" name="role">
                     <?php
                     foreach ($m_groups as $v) {
                        echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                     }
                     ?>
                  </select>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-md-2 text-center">
                        <img src="" id="img-edit" class="img-rounded" width="75" height="75">
                     </div>
                     <div class="col-md-10">
                        <label>Foto Profil</label>
                        <input type="file" class="form-control input-sm" name="photo_path">
                        <span class="text-muted" style="font-size: 9px;">(JPG,JPEG,PNG,GIF) Maks. 1000 x 1500</span>
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
            </div>
         </form>        
      </div>
   </div>
</div>

<div class="modal fade in" id="modal-password" style="">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
               <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title"><i class="fa fa-key"></i>&nbsp;Edit Password</h4>
         </div>
         <form id="form-password">    
            <input type="hidden" name="id">
            <div class="modal-body">
               <div class="form-group">
                  <label>Password baru</label>
                  <div class="input-group">
                     <input type="password" class="form-control form-control-sm" name="password" id="showpass">
                     <span class="btn btn-default input-group-addon" style="cursor: pointer;" onclick="showpass('showpass')">
                        <i class="fa fa-eye"></i>
                     </span>
                  </div>
               </div>
                <div class="form-group">
                  <label>Konfirmasi password</label>
                  <div class="input-group">
                     <input type="password" class="form-control form-control-sm" name="passwordconf" id="showpassconf">
                     <span class="btn btn-default input-group-addon" style="cursor: pointer;" onclick="showpass('showpassconf')">
                        <i class="fa fa-eye"></i>
                     </span>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
            </div>
         </form>
      </div>
   </div>
</div>

<script type="text/javascript">

   function showpass(trgt) {
      var x = document.getElementById(trgt);
      if (x.type === "password") {
         x.type = "text";
      } else {
         x.type = "password";
      }
   }

   $(function () {
   
      _offset = 0;
      _curpage = 1;

      $('.choose').select2({
         width: '100%'
      });

      $.fn.getting = function(option){
         var param = $.extend({
            filt_keyword : $('#filt_keyword').val(),
            limit : 10,
            offset : _offset, 
            currentPage : _curpage,
            order : 'a.id', 
            orderby : 'desc'
         }, option);
         
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'master/users/gettingbyopd',
            dataType : "JSON",
            data : {
               offset : param.offset,
               filt_keyword : param.filt_keyword,
               limit : param.limit,
               order : param.order,
               orderby : param.orderby
            },
            beforeSend: function (xhr) {
               loading_table('#cont-data', 'show');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
               loading_table('#cont-data', 'hide');
            },
            success : function(r){
               var t = '';
               if(r.total){
                  var total = r.total;
                  $.each(r.result, function(k,v){
                     var users_role = [];
                     t += '<tr>';
                     t += '<td>';
                        t += '<a data-fancybox="" href="'+base_url+v.avatar_path+'">';
                           t += '<img src="'+base_url+v.avatar_path+'" class="img-rounded" width="30" height="30">';
                        t += '</a>';
                     t += '</td>';
                     t += '<td><b>'+v.fullname+'</b><br><span style="font-size:11px;"><i>'+v.email+'</i></span></td>';
                     t += '<td>'+v.username+'</td>';
                     t += '<td>'+(v.pangkat_name ? v.pangkat_name : '')+'</td>';
                     t += '<td>'+(v.jabatan_name ? v.jabatan_name : '')+'</td>';
                     t += '<td>'+(v.golongan ? v.golongan : '')+'</td>';
                     // t += '<td>'+(v.pendidikan ? v.pendidikan : '')+'</td>';
                     // t += '<td>'+(v.phone ? v.phone : '')+'</td>';
                     t += '<td>'+v.role+'</td>';
                     // t += '<td>'+(v.dinas ? v.dinas : '')+'</td>';
                     // t += '<td>'+v.created_on+'</td>';
                     if(v.active == 1){
                        t += '<td><span class="label label-success bold" style="padding:5px;">YES</span></td>';
                     }else{
                        t += '<td><span class="label label-danger bold">NO</span></td>';
                     }
                     t += '<td>';
                        t += '<div class="btn-group">';
                           if(id_role == 1 || id_role == 2){
                              t += '<button class="btn btn-password btn-default btn-xs" data-toggle="tooltip" data-title="Change Password" data-id="'+v.id+'"><i class="fa fa-key"></i></button>';
                              t += '<button class="btn btn-edit btn-default btn-xs" data-toggle="tooltip" data-title="Edit" data-id="'+v.id+'"><i class="fa fa-edit"></i></button>';
                           }
                           if(id_role != 2 && id_role != 5 && id_role != 3 && id_role != 4){
                              t += '<button class="btn btn-delete btn-default btn-xs" data-toggle="tooltip" data-title="Delete" data-id="'+v.id+'"><i class="fa fa-trash"></i></button>';
                           }
                        t += '</div>';
                     t += '</td>';
                     t += '</tr>';
                  });
               }else{
                  t += '<tr><td class="text-center text-muted" colspan="10">No Result</td></tr>';
               }

               $('#cont-data').find('.sect-total').html(''+number_format(total)+' Rows Data');
               $('#cont-data').find('.sect-data').html(t);

               $('#cont-data .sect-pagination').pagination({
                  items: r.total,
                  itemsOnPage: param.limit,
                  edges: 0,
                  hrefTextPrefix: '',
                  displayedPages: 1,
                  currentPage : param.currentPage,
                  prevText : '&laquo;',
                  nextText : '&raquo;',
                  dropdown: true,
                  onPageClick : function(n,e){
                     e.preventDefault();
                     _offset = (n-1)*param.limit;
                     _curpage = n;
                     $(this).getting();
                  }
               });
            },
            complete : function(r){
               loading_table('#cont-data', 'hide');
            }
         });
      }

      $(this).on('submit', '#form-create', function(e){
         var form = $(this);
         $(this).ajaxSubmit({
            url : site_url + 'master/users/create',
            type : "POST",
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', '<i class="fa fa-save"></i> Simpan');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function(r) {
               session_checked(r._session, r._maintenance);
               if(r.success){
                  form.resetForm();
                  _offset = 0;
                  _curpage = 1;
                  $(this).getting();
                  toastr.success(r.msg);
                  $("#modal-create").modal("toggle");
               }else{
                  toastr.error(r.msg);
               }
               loading_form(form, 'hide', '<i class="fa fa-save"></i> Simpan');
            },
         });
         e.preventDefault();
      });

      $(this).on('submit', '#form-edit', function(e){
         var form = $(this);
         $(this).ajaxSubmit({
            url : site_url + 'master/users/change',
            type : "POST",
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', '<i class="fa fa-save"></i> Update');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function(r) {
               session_checked(r._session, r._maintenance);
               if(r.success){
                  _offset = 0;
                  _curpage = 1;
                  $(this).getting();
                  toastr.success(r.msg);
                  $("#modal-edit").modal("toggle");
               }else{
                  toastr.error(r.msg);
               }
               loading_form(form, 'hide', '<i class="fa fa-save"></i> Update');
            },
         });
         e.preventDefault();
      });

      $(this).on('submit', '#form-password', function(e){
         var form = $(this);
         $(this).ajaxSubmit({
            url : site_url + 'master/users/change_pwd',
            type : "POST",
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', '<i class="fa fa-save"></i> Update');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function(r) {
               session_checked(r._session, r._maintenance);
               if(r.success){
                  _offset = 0;
                  _curpage = 1;
                  toastr.success(r.msg);
                  $("#modal-password").modal("toggle");
               }else{
                  toastr.error(r.msg);
               }
               $.snackbar({
                  content: r.msg,
                  timeout: 5000
               });
               loading_form(form, 'hide', '<i class="fa fa-save"></i> Update');
            },
         });
         e.preventDefault();
      });

      $(this).on('click', '.btn-delete', function(e){
         var conf = confirm('Are you sure ?');
         if(conf){
            var id = $(this).data('id');
            ajaxManager.addReq({
               type : "POST",
               url : site_url + 'master/users/delete',
               dataType : "JSON",
               data : {
                  id : id,
                  "flipbooktoken2020" : _csrf_hash
               },
               beforeSend: function (xhr) {
                  loading_button('.btn-delete', id, 'show', '<i class="fa fa-trash"></i>', '');
               },
               error: function (jqXHR, status, errorThrown) {
                  error_handle(jqXHR, status, errorThrown);
                  loading_button('.btn-delete', id, 'hide', '<i class="fa fa-trash"></i>', '');
               },
               success : function(r){
                  session_checked(r._session, r._maintenance);
                  set_csrf(r._token_hash);
                  if(r.success){
                     $(this).getting();
                  }
                  $.snackbar({
                     content: r.msg,
                     timeout: 5000
                  });
                  loading_button('.btn-delete', id, 'hide', '<i class="fa fa-trash"></i>', '');
               }
            });
         }else{
            return false;
         }
         e.preventDefault();
      });

      $(this).on('click', '.btn-edit',function(e){
         var id = $(this).data('id');
         var form = $('#form-edit');

         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'master/users/modify',
            dataType : "JSON",
            data : {
               id : id
            },
            beforeSend: function (xhr) {
               form.resetForm();
               loading_button('.btn-edit', id, 'show', '<i class="fa fa-edit"></i>', '');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_button('.btn-edit', id, 'hide', '<i class="fa fa-edit"></i>', '');
            },
            success: function(r){
               session_checked(r._session, r._maintenance);
               form.find('input[name="id"]').val(r.users.id);
               form.find('input[name="before_username"]').val(r.users.username);
               form.find('input[name="before_email"]').val(r.users.email);
               form.find('input[name="before_avatar"]').val(r.users.avatar);
               form.find('input[name="before_phone"]').val(r.users.phone);
               form.find('input[name="before_username_telegram"]').val(r.users.username_telegram);

               form.find('input[name="username_telegram"]').val(r.users.username_telegram);
               form.find('select[name="pangkat_id"]').val(r.users.pangkat_id).trigger('change');
               form.find('select[name="jabatan_id"]').val(r.users.jabatan_id).trigger('change');
               form.find('select[name="golongan"]').val(r.users.golongan).trigger('change');
               form.find('select[name="pendidikan"]').val(r.users.pendidikan).trigger('change');

               form.find('input[name="phone"]').val(r.users.phone);
               form.find('input[name="dinas"]').val(r.users.dinas);
               form.find('input[name="first_name"]').val(r.users.first_name);
               form.find('input[name="email"]').val(r.users.email);
               form.find('input[name="username"]').val(r.users.username);
               form.find('select[name="role"]').val(r.groups.group_id);

               form.find('#img-edit').attr('src', base_url+r.avatar);

               $("#modal-edit").modal("toggle");
               loading_button('.btn-edit', id, 'hide', '<i class="fa fa-edit"></i>', '');
            }
         });
         e.preventDefault();
      });

      $(this).on('click', '.btn-password', function(e) {
         var id = $(this).data('id');
         var form = $('#form-password');
         form.find('input[name="id"]').val(id);
         $('#modal-password').modal('show');
         e.preventDefault();
      });

      $(this).on('change', '#filt_keyword', function(e) {
         e.preventDefault();
         _offset = 0;
         _curpage = 1;
         $(this).getting();
      });

      $(this).on('click', '.change_order', function(e){
         e.preventDefault();
         $('.change_order').html('<i class="fa fa-sort"></i>');
         $(this).find('i').remove();
         var sent = $(this).data('order');
         var by = $(this).attr('data-by');
         if(by === 'asc'){ 
            $(this).attr('data-by', 'desc');
            $(this).html('<i class="fa fa-sort-asc"></i>');
         }
         else{ 
            $(this).attr('data-by', 'asc');
            $(this).html(' <i class="fa fa-sort-desc"></i>');
         }
        $(this).getting({order:sent,orderby:by});
      });

      $(this).getting();
   });
</script>