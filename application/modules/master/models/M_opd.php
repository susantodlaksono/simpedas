<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_opd extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function getting($mode, $params, $user_id_active){
      $this->db->select('a.id as opd_id, a.name as opd_name, a.description as opd_description, a.status, a.phone');
      $this->db->select('d.first_name as pedasi_name');
      $this->db->select('e.first_name as kadis_name');
      $this->db->select('GROUP_CONCAT(g.first_name SEPARATOR ",") as kasie_name');

      $this->db->join('mapping_opd as b', 'a.id = b.opd_id AND b.group_id = 5', 'left');
      $this->db->join('mapping_opd as c', 'a.id = c.opd_id AND c.group_id = 3', 'left');
      $this->db->join('mapping_opd as f', 'a.id = f.opd_id AND f.group_id = 4', 'left');
      $this->db->join('users as d', 'b.user_id = d.id', 'left');
      $this->db->join('users as e', 'c.user_id = e.id', 'left');
      $this->db->join('users as g', 'f.user_id = g.id', 'left');

      if($params['filt_keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.name', $params['filt_keyword']);
         $this->db->or_like('a.description', $params['filt_keyword']);
         $this->db->or_like('d.first_name', $params['filt_keyword']);
         $this->db->or_like('e.first_name', $params['filt_keyword']);
         $this->db->group_end();
      }
      $this->db->group_by('a.id');
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('opd as a', $params['limit'], $params['offset'])->result_array();
         case 'count':
            return $this->db->get('opd as a')->num_rows();
      }
   }

	public function master_groups($gid, $with_in){
		$registered = $this->registered_group($gid, $with_in);

		$this->db->select('a.user_id, b.first_name');
		$this->db->join('users as b', 'a.user_id = b.id', 'left');
		$this->db->where('a.group_id', $gid);
		if($registered){
			$this->db->where_not_in('a.user_id', $registered);
		}
		return $this->db->get('users_groups as a')->result_array();
	}

	public function registered_group($gid, $with_in){
		$data = array();
		$this->db->select('user_id');
		$this->db->where('group_id', $gid);
		$result = $this->db->get('mapping_opd')->result_array();
		if($result){
			foreach ($result as $v) {
				if($with_in){
					if(!in_array($v['user_id'], $with_in)){
						$data[] = $v['user_id'];
					}
				}else{
					$data[] = $v['user_id'];
				}
			}
			return $data;
		}else{
			return FALSE;
		}
	}

	public function change($params){
		$this->db->trans_start();
		$opd['name'] = $params['name'];
		$opd['phone'] = $params['phone'] ? $params['phone'] : NULL;
		$opd['description'] = $params['description'] ? $params['description'] : NULL;
		$opd['status'] = $params['status'] == 1 ? $params['status'] : NULL;
		$this->db->update('opd', $opd, array('id' => $params['id']));
		$this->db->delete('mapping_opd', array('opd_id' => $params['id']));

		$mapkadis['opd_id'] = $params['id'];
		$mapkadis['user_id'] = $params['kadis'];
		$mapkadis['status'] = 1;
		$mapkadis['group_id'] = 3;
		$mapkadis['created_at'] = date('Y-m-d H:i:s');
		$this->db->insert('mapping_opd', $mapkadis);

		$mappedasi['opd_id'] = $params['id'];
		$mappedasi['user_id'] = $params['pedasi'];
		$mappedasi['status'] = 1;
		$mappedasi['group_id'] = 5;
		$mappedasi['created_at'] = date('Y-m-d H:i:s');
		$this->db->insert('mapping_opd', $mappedasi);

		foreach ($params['kasie'] as $v) {
			$mapkasie['opd_id'] = $params['id'];
			$mapkasie['user_id'] = $v;
			$mapkasie['status'] = 1;
			$mapkasie['group_id'] = 4;
			$mapkasie['created_at'] = date('Y-m-d H:i:s');
			$this->db->insert('mapping_opd', $mapkasie);
		}
		$this->db->trans_complete();

      if($this->db->trans_status()){
         $this->db->trans_commit();
         return array(
            'success' => TRUE,
            'msg' => 'Data berhasil ditambahkan',
         );
      }else{
         $this->db->trans_rollback();
         return array(
            'success' => FALSE,
            'msg' => 'Function failed',
         );
      }
	}

	public function create($params){   
		$this->db->trans_start();
		$opd['name'] = $params['name'];
		$opd['phone'] = $params['phone'] ? $params['phone'] : NULL;
		$opd['description'] = $params['description'] ? $params['description'] : NULL;
		$opd['status'] = $params['status'] == 1 ? $params['status'] : NULL;
		$this->db->insert('opd', $opd);
		$opd_id = $this->db->insert_id();

		$mapkadis['opd_id'] = $opd_id;
		$mapkadis['user_id'] = $params['kadis'];
		$mapkadis['status'] = 1;
		$mapkadis['group_id'] = 3;
		$mapkadis['created_at'] = date('Y-m-d H:i:s');
		$this->db->insert('mapping_opd', $mapkadis);

		$mappedasi['opd_id'] = $opd_id;
		$mappedasi['user_id'] = $params['pedasi'];
		$mappedasi['status'] = 1;
		$mappedasi['group_id'] = 5;
		$mappedasi['created_at'] = date('Y-m-d H:i:s');
		$this->db->insert('mapping_opd', $mappedasi);

		foreach ($params['kasie'] as $v) {
			$mapkasie['opd_id'] = $opd_id;
			$mapkasie['user_id'] = $v;
			$mapkasie['status'] = 1;
			$mapkasie['group_id'] = 4;
			$mapkasie['created_at'] = date('Y-m-d H:i:s');
			$this->db->insert('mapping_opd', $mapkasie);
		}
		$this->db->trans_complete();

      if($this->db->trans_status()){
      	$mailing = $this->mailing->register_opd($params);
        	if($mailing['status']){
           	$this->db->trans_commit();
           	return array(
	            'success' => TRUE,
	            'mailing' => $mailing,
	            'msg' => 'Data berhasil ditambahkans',
	         );
        	}else{
           $this->db->trans_rollback();   
           	return array(
	            'success' => FALSE,
	            'msg' => 'Function failed',
	         );
        	}
      }else{
         $this->db->trans_rollback();
         return array(
            'success' => FALSE,
            'msg' => 'Function failed',
         );
      }
	}

	public function delete($params){
      $this->db->trans_start();
      $this->db->delete('opd', array('id' =>$params['id']));
      $this->db->trans_complete();   

      if($this->db->trans_status()){
         $this->db->trans_commit();
         return array(
            'success' => TRUE,
            'msg' => 'Data berhasil dihapus',
         );
      }else{
         $this->db->trans_rollback();
         return array(
            'success' => FALSE,
            'msg' => 'Function failed',
         );
      }
   }

}