<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_users extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function getting($mode, $params, $user_id_active){
      $this->db->select('a.*');
      $this->db->select('c.name as jabatan_name');
      $this->db->select('d.name as pangkat_name');
      $this->db->select('GROUP_CONCAT(b.group_id SEPARATOR ",") as role_group_id');
      $this->db->select('GROUP_CONCAT(f.name SEPARATOR ",") as role_group');

      $this->db->join('jabatan as c', 'a.jabatan_id = c.id', 'left');
      $this->db->join('pangkat as d', 'a.pangkat_id = d.id', 'left');
      $this->db->join('users_groups as b', 'a.id = b.user_id', 'left');
      $this->db->join('groups as f', 'b.group_id = f.id', 'left');
      $this->db->where('a.id !=', $user_id_active);

      if($params['filt_keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.first_name', $params['filt_keyword']);
         $this->db->or_like('a.username', $params['filt_keyword']);
         $this->db->or_like('a.email', $params['filt_keyword']);
         $this->db->or_like('f.name', $params['filt_keyword']);
         $this->db->or_like('c.name', $params['filt_keyword']);
         $this->db->or_like('d.name', $params['filt_keyword']);
         $this->db->group_end();
      }
      $this->db->group_by('a.id');
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('users as a', $params['limit'], $params['offset'])->result_array();
         case 'count':
            return $this->db->get('users as a')->num_rows();
      }
   }

   public function getting_by_opd($mode, $params, $opd_id, $user_id_active){
      $this->db->select('b.*');
      $this->db->select('c.name as jabatan_name');
      $this->db->select('d.name as pangkat_name');
      $this->db->select('GROUP_CONCAT(e.group_id SEPARATOR ",") as role_group_id');
      $this->db->select('GROUP_CONCAT(f.name SEPARATOR ",") as role_group');

      $this->db->join('users as b', 'a.user_id = b.id', 'left');
      $this->db->join('jabatan as c', 'b.jabatan_id = c.id', 'left');
      $this->db->join('pangkat as d', 'b.pangkat_id = d.id', 'left');
      $this->db->join('users_groups as e', 'a.user_id = e.user_id', 'left');
      $this->db->join('groups as f', 'e.group_id = f.id', 'left');

      $this->db->where('a.user_id !=', $user_id_active);
      $this->db->where('a.opd_id', $opd_id);

      if($params['filt_keyword'] != ""){
         $this->db->group_start();
         $this->db->like('b.first_name', $params['filt_keyword']);
         $this->db->or_like('b.username', $params['filt_keyword']);
         $this->db->or_like('b.email', $params['filt_keyword']);
         $this->db->or_like('f.name', $params['filt_keyword']);
         $this->db->or_like('c.name', $params['filt_keyword']);
         $this->db->or_like('d.name', $params['filt_keyword']);
         $this->db->group_end();
      }
      $this->db->group_by('a.user_id');
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('mapping_opd as a', $params['limit'], $params['offset'])->result_array();
         case 'count':
            return $this->db->get('mapping_opd as a')->num_rows();
      }
   }

   public function create($params){      
      $user['first_name'] = $params['first_name'];
      $user['password_show'] = $params['password'];
      $user['dinas'] = $params['dinas'] ? $params['dinas'] : NULL;
      $user['phone'] = $params['phone'] ? $params['phone'] : NULL;
      $user['pangkat_id'] = $params['pangkat_id'] ? $params['pangkat_id'] : NULL;
      $user['jabatan_id'] = $params['jabatan_id'] ? $params['jabatan_id'] : NULL;
      $user['golongan'] = $params['golongan'] ? $params['golongan'] : NULL;
      $user['pendidikan'] = $params['pendidikan'] ? $params['pendidikan'] : NULL;
      $user['username_telegram'] = $params['username_telegram'] ? $params['username_telegram'] : NULL;

      if (!empty($_FILES['photo_path']['name'])){
         $file_path = './files/profil_photos/';
         $upload = $this->image_uploader->upload($_FILES['photo_path']['name'], date('His'), $file_path);
         if($upload['success']){
            $user['avatar'] = $upload['success'] ? $upload['file_name'] : NULL;
         }else{
            return array(
               'success' => FALSE,
               'msg' => $upload['msg']
            );
         }
      }

      $this->db->trans_start();
      $users = $this->ion_auth->register($params['username'], $params['password'], $params['email'], $user, $params['role']);
      $this->db->trans_complete();

      if($this->db->trans_status()){
         $this->db->trans_commit();
         return array(
            'success' => TRUE,
            'msg' => 'User berhasil ditambahkan',
         );
      }else{
         $this->db->trans_rollback();
         $this->ion_auth->delete_user($users);
         return array(
            'success' => FALSE,
            'msg' => 'Gagal menambahkan user',
         );
      }
   }

   public function delete($params){
      $users = $this->db->select('avatar')->where('id', $params['id'])->get('users')->row_array();
      $this->db->trans_start();
      $this->ion_auth->delete_user($params['id']);
      $this->db->trans_complete();   

      if($this->db->trans_status()){
         $this->db->trans_commit();
         if($users['avatar']){
            if (file_exists('./files/profil_photos/'.$users['avatar'].'')) {
               unlink('./files/profil_photos/'.$users['avatar'].'');
            }
         }
         return TRUE;
      }else{
         return FALSE;  
      }
   }

   public function change($params){
      
   }

}
