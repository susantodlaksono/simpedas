<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_pangkat extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function getting($mode, $params, $user_id_active){
      if($params['filt_keyword'] != ""){
         $this->db->group_start();
         $this->db->like('name', $params['filt_keyword']);
         $this->db->group_end();
      }
      $this->db->order_by($params['order'], $params['orderby']);
      switch ($mode) {
         case 'get':
            return $this->db->get('pangkat', $params['limit'], $params['offset'])->result_array();
         case 'count':
            return $this->db->get('pangkat')->num_rows();
      }
   }

	public function change($params){
		$this->db->trans_start();
		$pangkat['name'] = $params['name'];
		$pangkat['status'] = $params['status'] == 1 ? $params['status'] : NULL;
		$this->db->update('pangkat', $pangkat, array('id' => $params['id']));
		$this->db->trans_complete();

      if($this->db->trans_status()){
         $this->db->trans_commit();
         return array(
            'success' => TRUE,
            'msg' => 'Data berhasil ditambahkan',
         );
      }else{
         $this->db->trans_rollback();
         return array(
            'success' => FALSE,
            'msg' => 'Function failed',
         );
      }
	}

	public function create($params){   
		$this->db->trans_start();
		$pangkat['name'] = $params['name'];
		$pangkat['status'] = $params['status'] == 1 ? $params['status'] : NULL;
		$this->db->insert('pangkat', $pangkat);
		$this->db->trans_complete();

      if($this->db->trans_status()){
         $this->db->trans_commit();
         return array(
            'success' => TRUE,
            'msg' => 'Data berhasil ditambahkan',
         );
      }else{
         $this->db->trans_rollback();
         return array(
            'success' => FALSE,
            'msg' => 'Function failed',
         );
      }
	}

	public function delete($params){
      $this->db->trans_start();
      $this->db->delete('pangkat', array('id' =>$params['id']));
      $this->db->trans_complete();   

      if($this->db->trans_status()){
         $this->db->trans_commit();
         return array(
            'success' => TRUE,
            'msg' => 'Data berhasil dihapus',
         );
      }else{
         $this->db->trans_rollback();
         return array(
            'success' => FALSE,
            'msg' => 'Function failed',
         );
      }
   }

}