<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Main extends MY_Controller {

   public function __construct() {
		parent::__construct();
      $this->load->library('global_mapping');
      $this->load->model('m_main');
   }
   
   public function index(){
      $data['_css'] = array(
         $this->_path_template.'/plugins/select2/select2.min.css',
         $this->_path_template.'/plugins/krajee-star-rating/star-rating.min.css',
         $this->_path_template.'/plugins/krajee-star-rating/theme.css'
      ); 
      $data['_js'] = array(
         $this->_path_template.'/plugins/select2/select2.full.min.js',
         $this->_path_template.'/plugins/tinymce/tinymce.min.js',
         $this->_path_template.'/plugins/krajee-star-rating/star-rating.min.js',
      ); 

      if(in_array(1, $this->_role_id)){
         $this->render_page($data, 'superadmin', 'modular');
      }else if(in_array(2, $this->_role_id)){
         $this->render_page($data, 'adm_apd', 'modular');
      }else if(in_array(3, $this->_role_id)){
         $this->render_page($data, 'adm_kadis', 'modular');
      }else if(in_array(5, $this->_role_id)){
         $this->render_page($data, 'adm_pedasi', 'modular');
      }else if(in_array(4, $this->_role_id)){
         $this->render_page($data, 'adm_kasie', 'modular');
      }
   }

   public function todolistapd(){
      $list = array();
      $data = $this->m_main->todolistapd('get', $this->_get, $this->_user->id);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'requested_date' => date('d M Y', strtotime($v['requested_date'])),
               'requested_time' => date('H:i:s', strtotime($v['requested_date'])),
               'opd' => $v['opd'],
               'title' => $v['title'],
               'request_rating' => $v['request_rating'],
               'status_id' => $v['status'],
               'status_name' => $this->global_mapping->request_status($v['status'], 'label-status-table '),
               'status_pengolahan_name' => $this->global_mapping->request_status_pengolahan($v['status_pengolahan'], 'label-status-table '),
               'start_date' => date('d M Y', strtotime($v['start_date'])),
               'end_date' => date('d M Y', strtotime($v['end_date']))
            );
         }
      }
      $response['result'] = $list;
      $this->json_result($response);
   }

   public function todolistbyopd(){
      $list = array();
      $opd = $this->db->where('user_id', $this->_user->id)->get('mapping_opd')->row_array();
      $data = $this->m_main->todolistbyopd('get', $this->_get, $opd['opd_id']);
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'requested_date' => date('d M Y', strtotime($v['requested_date'])),
               'requested_time' => date('H:i:s', strtotime($v['requested_date'])),
               'opd' => $v['opd'],
               'title' => $v['title'],
               'request_rating' => $v['request_rating'],
               'status_id' => $v['status'],
               'status_name' => $this->global_mapping->request_status($v['status'], 'label-status-table '),
               'start_date' => date('d M Y', strtotime($v['start_date'])),
               'end_date' => date('d M Y', strtotime($v['end_date']))
            );
         }
      }
      $response['result'] = $list;
      $this->json_result($response);
   }

   public function todolistkasie(){
      $list = array();
      $opd = $this->db->where('user_id', $this->_user->id)->get('mapping_opd')->row_array();
      $data = $this->m_main->todolistbyopd('get', $this->_get, $opd['opd_id'], '_kasie');
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'requested_date' => date('d M Y', strtotime($v['requested_date'])),
               'requested_time' => date('H:i:s', strtotime($v['requested_date'])),
               'opd' => $v['opd'],
               'title' => $v['title'],
               'request_rating' => $v['request_rating'],
               'status_id' => $v['status'],
               'status_name' => $this->global_mapping->request_status($v['status'], 'label-status-table '),
               'start_date' => date('d M Y', strtotime($v['start_date'])),
               'end_date' => date('d M Y', strtotime($v['end_date']))
            );
         }
      }
      $response['result'] = $list;
      $this->json_result($response);
   }

   public function todolistpedasi(){
      $list = array();
      $opd = $this->db->where('user_id', $this->_user->id)->get('mapping_opd')->row_array();
      $data = $this->m_main->todolistbyopd('get', $this->_get, $opd['opd_id'], '_pedasi');
      if($data){
         foreach ($data as $v) {
            $list[] = array(
               'id' => $v['id'],
               'requested_date' => date('d M Y', strtotime($v['requested_date'])),
               'requested_time' => date('H:i:s', strtotime($v['requested_date'])),
               'opd' => $v['opd'],
               'title' => $v['title'],
               'request_rating' => $v['request_rating'],
               'status_id' => $v['status'],
               'status_name' => $this->global_mapping->request_status($v['status'], 'label-status-table '),
               'start_date' => date('d M Y', strtotime($v['start_date'])),
               'end_date' => date('d M Y', strtotime($v['end_date']))
            );
         }
      }
      $response['result'] = $list;
      $this->json_result($response);
   }

   public function download(){
      $get = $this->input->get();
      $this->load->helper('download');
      $response['success'] = TRUE;
      force_download(urldecode($get['name']), file_get_contents(urldecode($get['dir'])), NULL);
      // $this->json_result($response);
   }

   public function zipped(){
      $this->load->library('zip');
      $get = $this->input->get();
      $this->zip->read_dir($get['dir'], FALSE);
      $this->zip->download(''.$get['name'].'.zip'); 
   }
   
   public function request_attach_download($id){
      $this->load->helper('download');
      $temp_name = $this->db->select('file_name, file_orig_name, file_dir')->where('id', $id)->get('request_attach')->row_array();
      force_download($temp_name['file_orig_name'], file_get_contents($temp_name['file_dir'].$temp_name['file_name']));
   }
}