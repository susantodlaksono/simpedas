<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Main_superadmin extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function summary_users($type){
		switch ($type) {
			case 'all':
				return $this->db->count_all_results('users');
				break;
			case 'active':
				$this->db->where('active', 1);
				return $this->db->count_all_results('users');
				break;
			case 'inactive':
				$this->db->where('active', 0);
				return $this->db->count_all_results('users');
				break;
		}
	}

	public function summary_master($type){
		switch ($type) {
			case 'division':
				$this->db->where('status', 1);
				return $this->db->count_all_results('master_division');
				break;
			case 'position':
				$this->db->where('status', 1);
				return $this->db->count_all_results('master_position');
				break;
		}
	}

	public function summary_role(){
		$groups = $this->get_groups();
		foreach ($groups as $v) {
			$data[$v['id']]['name'] = $v['name'];
			// $total = $this->db->where('group_id', $v['id'])->get('')
			$data[$v['id']]['total'] = $this->total_by_role($v['id']);
		}
		return $data;
	}

	public function summary_status(){
		$status = $this->get_status();
		foreach ($status as $v) {
			$data[$v['id']]['name'] = $v['name'];
			// $total = $this->db->where('group_id', $v['id'])->get('')
			$data[$v['id']]['total'] = $this->total_by_status($v['id']);
		}
		return $data;
	}

	public function get_status(){
		return $this->db->get('members_status')->result_array();
	}
	
	public function total_by_status($status){	
		$this->db->join('users as b', 'a.user_id = b.id', 'left');
		$this->db->where('a.status_id', $status);
		$this->db->where('b.active', 1);
		return $this->db->count_all_results('members as a');
	}

	public function total_by_role($group_id){	
		$this->db->where('group_id', $group_id);
		return $this->db->count_all_results('users_groups');
	}

	public function get_groups(){
		return $this->db->get('groups')->result_array();
	}
}