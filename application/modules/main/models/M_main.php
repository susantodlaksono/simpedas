<?php
if (!defined('BASEPATH'))
   exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_main extends CI_Model{

   public function __construct() {
      parent::__construct();
   }

   public function todolistapd($mode, $params, $user_id_active){
      $this->db->select('a.*');
      $this->db->select('b.name as opd');
      $this->db->join('opd as b', 'a.opd_id = b.id', 'left');
      $this->db->where('a.requested_by', $user_id_active);

      if($params['filt_status'] != 'all'){
         $this->db->where('a.status', $params['filt_status']);
      }

      if($params['filt_keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.title', $params['filt_keyword']);
         $this->db->or_like('b.name', $params['filt_keyword']);
         $this->db->group_end();
      }
      $this->db->limit($params['limit']);
      $this->db->order_by($params['order'], $params['orderby']);
      return $this->db->get('request as a')->result_array();
   }

   public function todolistbyopd($mode, $params, $opd_id, $role = '_kadis'){
      $this->db->select('a.*');
      $this->db->select('b.name as opd');
      $this->db->join('opd as b', 'a.opd_id = b.id', 'left');
      $this->db->where('a.opd_id', $opd_id);

      if($params['filt_status'] != 'all'){
         $this->db->where('a.status', $params['filt_status']);
      }

      if($role == '_kasie'){
         $this->db->where_in('a.status', array(2,3));
      }

      if($role == '_pedasi'){
         $this->db->where_in('a.status', array(1,2,3));
      }

      if($params['filt_keyword'] != ""){
         $this->db->group_start();
         $this->db->like('a.title', $params['filt_keyword']);
         $this->db->or_like('b.name', $params['filt_keyword']);
         $this->db->group_end();
      }
      $this->db->limit($params['limit']);
      $this->db->order_by($params['order'], $params['orderby']);
      return $this->db->get('request as a')->result_array();
   }

}
