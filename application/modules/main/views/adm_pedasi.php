<div class="row">
   <div class="col-md-6">
      <div class="box box-solid" id="cont-summary-document">
         <div class="box-header">
            <h6 class="box-title" style="font-size: 15px;"><i class="fa fa-tasks"></i> Summary Document</h6>
         </div>
         <div class="box-body"></div>
      </div>
   </div>
   <div class="col-md-6">
      <div class="box box-solid" id="cont-summary-request">
         <div class="box-header">
            <h5 class="box-title" style="font-size: 15px;"><i class="fa fa-clipboard"></i>&nbsp;Summary Request</h5>
         </div>
         <div class="box-body"></div>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-md-7">
      <div class="box box-solid" id="cont-todolist" style="font-size:12px;">
         <div class="box-header">
            <h5 class="box-title" style="font-size: 15px;"><i class="fa fa-edit"></i>&nbsp;To do list</h5>
            <div class="box-tools">
               <div class="btn-toolbar pull-left">
                  <div class="btn-group" data-toggle="buttons">
                     <input type="text" class="form-control input-sm" id="filt_keyword" placeholder="Pencarian...">
                  </div>
                  <div class="btn-group" data-toggle="buttons">
                     <select type="text" class="form-control input-sm" id="filt_status">
                        <option value="all">Semua Status</option>
                        <option value="1">Kompilasi Data</option>
                        <option value="2">Validasi Kasie</option>
                        <option value="3">Diserahkan</option>
                     </select>
                  </div>
                  <div class="btn-group" data-toggle="buttons">
                     <button class="btn btn-primary btn-sm btn-filter"><i class="fa fa-filter"></i> Filter</button>
                  </div>
               </div>
            </div>
         </div>
         <div class="box-body">
            <table class="table table-hover table-condensed">
               <tbody class="sect-data"></tbody>
            </table>
         </div>
      </div>
   </div>
   <div class="col-md-5">
      <div class="box box-solid" id="cont-performance">
         <div class="box-header">
            <h5 class="box-title" style="font-size: 15px;"><i class="fa fa-line-chart"></i>&nbsp;Performance</h5>
         </div>
         <div class="box-body"></div>
      </div>
   </div>
</div>

<div class="modal fade in" id="modal-detail">
   <div class="modal-dialog">
      <div class="modal-content" style="width: 700px;margin-left: -50px;">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
               <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title"><i class="fa fa-file"></i>&nbsp;Detail Permohonan Data</h4>
         </div>
         <div class="modal-body"></div>
      </div>
   </div>
</div>

<script type="text/javascript">

   $(function () {

      $.fn.todolist = function(option){
         var param = $.extend({
            filt_keyword : $('#filt_keyword').val(),
            filt_status : $('#filt_status').val(),
            limit : 10,
            // offset : _offset, 
            // currentPage : _curpage,
            order : 'a.id', 
            orderby : 'desc'
         }, option);
         
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'main/todolistpedasi',
            dataType : "JSON",
            data : {
               // offset : param.offset,
               filt_keyword : param.filt_keyword,
               filt_status : param.filt_status,
               limit : param.limit,
               order : param.order,
               orderby : param.orderby
            },
            beforeSend: function (xhr) {
               loading_table('#cont-todolist', 'show');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
               loading_table('#cont-todolist', 'hide');
            },
            success : function(r){
               var t = '';
               if(r.result.length > 0){
                  $.each(r.result, function(k,v){
                     t += '<tr>';
                        t += '<td width="400">';
                           t += '<b>'+v.title+'</b><br>';
                           t += '<span style="font-size:9px;">';
                           t += '<i class="fa fa-building"></i> '+v.opd+'&nbsp;';
                           t += '<i class="fa fa-calendar"></i> '+v.start_date+' sd '+v.end_date+'&nbsp;';
                           t += '<input class="request-rating" value="'+(v.request_rating ? v.request_rating : 0)+'" type="text" class="rating" data-size="sm" data-min="0" data-max="10" data-step="2">';
                           t += '</span>';
                        t += '</td>';
                        t += '<td width="10" style="vertical-align:middle">';
                           t += v.status_name;
                        t += '</td>';
                        t += '<td width="10" style="vertical-align:middle">';
                           t += '<div class="btn-group">';
                              t += '<button class="btn btn-detail btn-default btn-xs" data-toggle="tooltip" data-title="Edit" data-id="'+v.id+'"><i class="fa fa-file"></i></button>';
                           t += '</div>';
                        t += '</td>';
                     t += '</tr>';
                  });
               }else{
                  t += '<tr><td class="text-center text-muted">No Result</td></tr>';
               }

               $('#cont-todolist').find('.sect-data').html(t);
               $(".request-rating").rating({
                  displayOnly: true
               });
            },
            complete : function(r){
               loading_table('#cont-todolist', 'hide');
            }
         });
      }     

      $.fn.summary_document = function(){
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'widget_detail/pedasi/summary_document',
            dataType : "JSON",
            beforeSend: function (xhr) {
               loading_table('#cont-summary-document', 'show');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
               loading_table('#cont-summary-document', 'hide');
            },
            success : function(r){
               session_checked(r._session, r._maintenance);
               $('#cont-summary-document').find('.box-body').html(r.html);
            },
            complete : function(){
               loading_table('#cont-summary-document', 'hide');
            }
         });
      };

      $.fn.summary_request = function(){
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'widget_detail/pedasi/summary_request',
            dataType : "JSON",
            beforeSend: function (xhr) {
               loading_table('#cont-summary-request', 'show');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown); 
               loading_table('#cont-summary-request', 'hide');
            },
            success : function(r){
               session_checked(r._session, r._maintenance);
               $('#cont-summary-request').find('.box-body').html(r.html);
            },
            complete : function(){
               loading_table('#cont-summary-request', 'hide');
            }
         });
      };

      $(this).on('click', '.btn-detail',function(e){
         var id = $(this).data('id');
         var form = $('#modal-detail');

         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'widget_detail/pedasi',
            dataType : "JSON",
            data : {
               id : id
            },
            beforeSend: function (xhr) {
               loading_button('.btn-detail', id, 'show', '<i class="fa fa-file"></i>', '');
            },
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_button('.btn-detail', id, 'hide', '<i class="fa fa-file"></i>', '');
            },
            success: function(r){
               session_checked(r._session, r._maintenance);
               form.find('.modal-body').html(r.html);
            },
            complete: function(){
               $("#modal-detail").modal("toggle");
               loading_button('.btn-detail', id, 'hide', '<i class="fa fa-file"></i>', '');
            }
         });
      });

      $(this).on('click', '.btn-filter',function(e){
         $(this).todolist();
      });

      $(this).summary_document();
      $(this).summary_request();
      $(this).todolist();

   });

</script>