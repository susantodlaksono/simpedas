<div class="row">
	<div class="col-md-6">
		<div class="panel panel-primary" id="panel-profile">
         <div class="panel-heading">
            <div class="panel-title">Change Password</div>
         </div>
         <div class="panel-body">
         	<form id="form-change-password">
	      		<div class="form-group">
	         		<label>New Password</label>
                  <div class="input-group">
	         		   <input type="password" name="newpass" class="form-control input-sm" id="showpass">
                     <span class="btn btn-default input-group-addon show-pass" style="cursor: pointer;" onclick="showpass()">
                        <i class="fa fa-eye"></i>
                     </span>
                  </div>
	      		</div>
	      		<div class="form-group">
	         		<label>Confirmation New Password</label>
                  <div class="input-group">
	         		   <input type="password" name="confpass" class="form-control input-sm" id="showpassconf">
                     <span class="btn btn-default input-group-addon show-pass" style="cursor: pointer;" onclick="showpassconf()">
                        <i class="fa fa-eye"></i>
                     </span>
                  </div>
	      		</div>
	      		<div class="form-group">
	      			<button class="btn btn-blue btn-block" type="submit">Submit</button>
	   			</div>
      		</form>
      	</div>
      </div>
   </div>
</div>

<script type="text/javascript">

   function showpass() {
      var x = document.getElementById("showpass");
      if (x.type === "password") {
         x.type = "text";
      } else {
         x.type = "password";
      }
   }

   function showpassconf() {
      var x = document.getElementById("showpassconf");
      if (x.type === "password") {
         x.type = "text";
      } else {
         x.type = "password";
      }
   }

	$(function () {

		$(this).on('submit', '#form-change-password', function(e){
			e.preventDefault();
         var form = $(this);
         $(this).ajaxSubmit({
            url  : site_url +'profile/changepass',
            type : "POST",
            data: {
               "flipbooktoken2020" : _csrf_hash
            },
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', 'Submit');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function(r) {
               session_checked(r._session, r._maintenance);
               set_csrf(r._token_hash);
               if(r.success){
               	toastr.success(r.msg);
            	}else{
                  toastr.error(r.msg);
               }
               loading_form(form, 'hide', 'Submit');
            }
         });
      });
	});
</script>