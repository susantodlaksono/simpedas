<div class="row">
   <div class="col-md-6">
      <div class="box box-solid" id="panel-profile">
         <div class="box-header">
            <h6 class="box-title" style="font-size: 15px;"><i class="fa fa-user"></i> Update Profil</h6>
         </div>
         <form id="form-change-profile">
         <input type="hidden" name="id">
         <input type="hidden" name="before_email">
         <input type="hidden" name="before_username">
         <input type="hidden" name="before_username_telegram">
         <input type="hidden" name="before_avatar">         
         <input type="hidden" name="before_phone">       
         <div class="box-body">
            <div class="form-group">
               <label>Nama Lengkap <span class="text-danger">*</span></label>
               <input type="text" class="form-control input-sm" name="first_name">
            </div>
            <div class="form-group">
               <label>Email <span class="text-danger">*</span></label>
               <input type="email" class="form-control input-sm" name="email">
            </div>
            <div class="form-group">
               <label>Username <span class="text-danger">*</span></label>
               <input type="text" class="form-control input-sm" name="username">
            </div>
            <div class="form-group">
               <label>No HP <span class="text-danger">*</span></label>
               <input type="number" class="form-control input-sm" name="phone">
            </div>
            <div class="form-group">
               <label>Telegram <span class="text-danger">*</span></label>
               <div class="input-group">
                  <span class="input-group-addon">@</span>
                  <input type="text" class="form-control input-sm" name="username_telegram">
               </div>
            </div>
            <div class="form-group">
               <div class="row">
                  <div class="col-md-2 text-center">
                     <a data-fancybox="" href="" id="img-edit-href">
                        <img src="" id="img-edit" class="img-rounded" width="75" height="75">
                     </a>
                  </div>
                  <div class="col-md-10">
                     <label>Foto Profil</label>
                     <input type="file" class="form-control input-sm" name="photo_path">
                     <span class="text-muted" style="font-size: 9px;">(JPG,JPEG,PNG,GIF) Maks. 1000 x 1500</span>
                  </div>
               </div>
            </div>
         </div>
         <div class="box-footer">
            <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Update</button>
         </div>
         </form>
      </div>
   </div>
	<div class="col-md-6">
		<div class="box box-solid" id="panel-profile">
         <div class="box-header">
            <h6 class="box-title" style="font-size: 15px;"><i class="fa fa-lock"></i> Update Password</h6>
         </div>
      	<form id="form-change-password">
         <div class="box-body">
	      		<div class="form-group">
	         		<label>Password Baru</label>
                  <div class="input-group">
	         		   <input type="password" name="newpass" class="form-control input-sm" id="showpass">
                     <span class="btn btn-default input-group-addon show-pass" style="cursor: pointer;" onclick="showpass()">
                        <i class="fa fa-eye"></i>
                     </span>
                  </div>
	      		</div>
	      		<div class="form-group">
	         		<label>Konfirmasi Password Baru</label>
                  <div class="input-group">
	         		   <input type="password" name="confpass" class="form-control input-sm" id="showpassconf">
                     <span class="btn btn-default input-group-addon show-pass" style="cursor: pointer;" onclick="showpassconf()">
                        <i class="fa fa-eye"></i>
                     </span>
                  </div>
	      		</div>
      	</div>
         <div class="box-footer">
            <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Update</button>
         </div>
   		</form>
      </div>
   </div>
</div>

<script type="text/javascript">

   function showpass() {
      var x = document.getElementById("showpass");
      if (x.type === "password") {
         x.type = "text";
      } else {
         x.type = "password";
      }
   }

   function showpassconf() {
      var x = document.getElementById("showpassconf");
      if (x.type === "password") {
         x.type = "text";
      } else {
         x.type = "password";
      }
   }

   $(function () {

      $.fn.profile = function(){
         var form = $('#form-change-profile');
         ajaxManager.addReq({
            type : "GET",
            url : site_url + 'profile/detail',
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
            },
            success: function(r){
               session_checked(r._session, r._maintenance);
               form.find('input[name="id"]').val(r.users.id);
               form.find('input[name="before_username"]').val(r.users.username);
               form.find('input[name="before_email"]').val(r.users.email);
               form.find('input[name="before_avatar"]').val(r.users.avatar);
               form.find('input[name="before_phone"]').val(r.users.phone);
               form.find('input[name="before_username_telegram"]').val(r.users.username_telegram);

               form.find('input[name="username_telegram"]').val(r.users.username_telegram);

               form.find('input[name="phone"]').val(r.users.phone);
               form.find('input[name="first_name"]').val(r.users.first_name);
               form.find('input[name="email"]').val(r.users.email);
               form.find('input[name="username"]').val(r.users.username);

               form.find('#img-edit').attr('src', base_url+r.avatar);
               form.find('#img-edit-href').attr('href', base_url+r.avatar);
            }
         });
      }

      $(this).on('submit', '#form-change-profile', function(e){
         var form = $(this);
         $(this).ajaxSubmit({
            url : site_url + 'profile/change_profile',
            type : "POST",
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', '<i class="fa fa-save"></i> Update');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function(r) {
               session_checked(r._session, r._maintenance);
               if(r.success){
                  $(this).profile();
                  toastr.success(r.msg);
               }else{
                  toastr.error(r.msg);
               }
               loading_form(form, 'hide', '<i class="fa fa-save"></i> Update');
            },
         });
         e.preventDefault();
      });

      $(this).on('submit', '#form-change-password', function(e){
         e.preventDefault();
         var form = $(this);
         $(this).ajaxSubmit({
            url  : site_url +'profile/changepass',
            type : "POST",
            data: {
               "flipbooktoken2020" : _csrf_hash
            },
            dataType : "JSON",
            error: function (jqXHR, status, errorThrown) {
               error_handle(jqXHR, status, errorThrown);
               loading_form(form, 'hide', '<i class="fa fa-save"></i> Update');
            },
            beforeSend: function (xhr) {
               loading_form(form, 'show', loadingbutton);
            },
            success: function(r) {
               session_checked(r._session, r._maintenance);
               set_csrf(r._token_hash);
               if(r.success){
                  toastr.success(r.msg);
               }else{
                  toastr.error(r.msg);
               }
               loading_form(form, 'hide', '<i class="fa fa-save"></i> Update');
            }
         });
      });

      $(this).profile();
   });
</script>
