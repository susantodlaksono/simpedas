<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');

/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Profile extends MY_Controller {

   public function __construct() {
		parent::__construct();
      $this->load->library('form_validation');
      $this->load->library('image_uploader');
   }

   public function index(){
      $data['_css'] = array(
         $this->_path_template.'/plugins/fancybox/jquery.fancybox.min.css'
      ); 
      $data['_js'] = array(
         $this->_path_template.'/plugins/fancybox/jquery.fancybox.min.js'
      ); 
      $this->render_page($data, 'profile', 'modular');
   }

   public function detail(){
      $response['users'] = $this->db->where('id', $this->_user->id)->get('users')->row_array();
      $response['avatar'] = $this->image_uploader->fileexistspicture('files/profil_photos/', $response['users']['avatar']);
      $this->json_result($response);
   }

   public function change_profile(){
      $response['success'] = FALSE;
      $avatar_change = FALSE;

      if($this->_post['email'] != $this->_post['before_email']){
         $this->form_validation->set_rules('email', 'Email ', 'required|valid_email|is_unique[users.email]');
      }else{
         $this->form_validation->set_rules('email', 'Email ', 'required|valid_email');
      }
      if($this->_post['username'] != $this->_post['before_username']){
         $this->form_validation->set_rules('username', 'Username ', 'required|is_unique[users.username]');
      }else{
         $this->form_validation->set_rules('username', 'Username ', 'required');
      }
      if($this->_post['username_telegram'] != $this->_post['before_username_telegram']){
         $this->form_validation->set_rules('username_telegram', 'Telegram ', 'required|is_unique[users.username_telegram]');
      }else{
         $this->form_validation->set_rules('username_telegram', 'Telegram ', 'required');
      }
      if($this->_post['phone'] != $this->_post['before_phone']){
         $this->form_validation->set_rules('phone', 'No HP ', 'required|numeric|is_unique[users.phone]');
      }else{
         $this->form_validation->set_rules('phone', 'No HP ', 'required');
      }
      $this->form_validation->set_rules('first_name', 'Nama Lengkap', 'required');

      if($this->form_validation->run()){
         if (!empty($_FILES['photo_path']['name'])){
            $file_path = './files/profil_photos/';
            $upload = $this->image_uploader->upload($_FILES['photo_path']['name'], date('His'), $file_path);
            if($upload['success']){
               $avatar_change = TRUE;
               $user['avatar'] = $upload['success'] ? $upload['file_name'] : NULL;
            }else{
               $response['msg'] = $upload['msg'];
               $this->json_result($response);
            }
         }

         $this->db->trans_start();
         $user['first_name'] = $this->_post['first_name'];
         $user['username_telegram'] = $this->_post['username_telegram'] ? $this->_post['username_telegram'] : NULL;
         $user['phone'] = $this->_post['phone'];
         $user['email'] = $this->_post['email'];
         $user['username'] = $this->_post['username'];
         $this->ion_auth->update($this->_post['id'], $user);
         $this->db->trans_complete();

         if($this->db->trans_status()){
            $this->db->trans_commit();
            if($avatar_change){
               if($this->_post['before_avatar'] != ''){
                  if (file_exists('./files/profil_photos/'.$this->_post['before_avatar'].'')) {
                     unlink('./files/profil_photos/'.$this->_post['before_avatar'].'');
                  }
               }
            }
            $response['success'] = TRUE;
            $response['msg'] = 'Data berhasil diperbaharui';
         }else{
            $this->db->trans_rollback();
            $response['msg'] = 'Function failed';
         }
      }else{
         $response['msg'] = validation_errors();
      }
      $this->json_result($response);
   }

   public function changepass(){
      $r['success'] = FALSE;

      $this->form_validation->set_rules('newpass', 'New Password', 'required');
      $this->form_validation->set_rules('confpass', 'Confirmation New Password', 'required|matches[newpass]');
      if($this->form_validation->run()){
          $data = array(
            'password' => $this->_post['newpass'],
         );
         $result = $this->ion_auth->update($this->_user->id, $data);
         if($result){
            $r['success'] = TRUE;
            $r['msg'] = 'Password berhasil diperbaharui';
         }else{
            $r['msg'] = 'Failed to update data';
         }
      }else{
         $r['msg'] = validation_errors();
      }
      $this->json_result($r);
   }

}