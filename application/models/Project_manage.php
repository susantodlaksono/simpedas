<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Project_manage extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function lead_project($userid){
		$this->db->select('b.code, b.id, b.name, b.project_type');
		$this->db->join('project as b', 'a.project_id = b.id', 'left');
		$this->db->where('a.user_id', $userid);
		$this->db->where_not_in('b.status', array(8));
		return $this->db->get('project_lead as a')->result_array();
	}

	public function project_detail($id){
		$this->db->select('a.*');
		$this->db->select('b.name as project_type_name');
		$this->db->select('c.name as psname');
		$this->db->where('a.id', $id);
		$this->db->join('project_type as b', 'a.project_type = b.id', 'left');
		$this->db->join('project_status as c', 'a.status = c.id', 'left');
		$r = $this->db->get('project as a')->row_array();
		if($r){
			$data['name'] = $r['name'];
			$data['status_id'] = $r['status'];
			$data['code'] = $r['code'];
			$data['start_date'] = $r['start_date'];
			$data['end_date'] = $r['end_date'];
			$data['progress'] = $r['percentage_progress'];
			$data['left_days'] = $r['left_days'] ? $r['left_days'] : 0;
			$data['status'] = $this->mapping_status_project($r['status'], $r['psname'], "status-project");
			return $data;
		}else{
			return FALSE;
		}
	}

	public function status_project($typeid){
		$this->db->where_in('project_type_id', $typeid);
		return $this->db->get('project_status')->result_array();
	}

	public function mapping_status_project($status_id, $status_name, $class = ""){
		if($status_id == '6'){
         return '<span class="label label-info '.$class.'">'.$status_name.'</span>';
      }
      else if($status_id == '7'){
         return '<span class="label label-success '.$class.'">'.$status_name.'</span>';
      }
      else if($status_id == '8'){
         return '<span class="label label-primary '.$class.'">'.$status_name.'</span>';
      }else{
      	return '<span class="label label-default '.$class.'">'.$status_name.'</span>';
      }
   }

    public function task_label($status, $type){
      if($type == 1){
         switch ($status) {
            case '1': return '<span class="label label-danger">Urgent</span>';break;
            case '2': return '<span class="label label-success">Priority</span>';break;
            case '3': return '<span class="label label-default">Routine</span>';break;
         }
      }
      if($type == 2){
         switch ($status) {
            case '1': return '<span class="label label-info">Assigned</span>';break;
            case '2': return '<span class="label label-default">Individual</span>';break;
         }
      }
   }

}