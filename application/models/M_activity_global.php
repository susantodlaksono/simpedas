<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class M_activity_global extends CI_Model{

	public function __construct() {
  		parent::__construct();
	}

	public function approval_result($id){
   	$this->db->select('a.id, a.description, a.date_activity, a.start_time, a.end_time, a.created_date, a.status, b.first_name');
   	$this->db->join('users as b', 'a.created_by = b.id', 'left');
   	$this->db->where('a.id', $id);
   	$result['data'] = $this->db->get('project_task_activity as a')->row_array();
   	$result['attachments'] = $this->get_attachments($id);
   	return $result;
   }

   public function feedback_pic($userid){
   	$this->db->select('first_name');
   	$this->db->where('id', $userid);
   	$result = $this->db->get('users')->row_array();
   	if($result){
   		return $result['first_name'];
   	}else{
   		return '';
   	}
   }

   public function get_attachments($id, $type_attach = NULL){
      $this->db->where('task_activity_id', $id);
      if($type_attach){
      	$this->db->where('type_attach', $type_attach);
      }else{
         $this->db->where('type_attach IS NULL');
      }
      $result = $this->db->get('project_task_activity_doc')->result_array();
      if($result){
      	foreach ($result as $key => $value) {
      		$result[$key]['icon'] = $this->mapping_icon($value['file_extension']);
      	}
      	return $result;
      }else{
      	return FALSE;
      }
   }

   public function get_attachments_leave($id, $type_attach = NULL){
      $this->db->where('leave_id', $id);
      if($type_attach){
         $this->db->where('type_attach', $type_attach);
      }else{
         $this->db->where('type_attach IS NULL');
      }
      $result = $this->db->get('leaves_attachments')->result_array();
      if($result){
         foreach ($result as $key => $value) {
            $result[$key]['icon'] = $this->mapping_icon($value['file_extension']);
         }
         return $result;
      }else{
         return FALSE;
      }
   }

   public function _file_exists($filePath){
      return is_file($filePath) && file_exists($filePath);
   }

 	public function mapping_icon($ext){
		$upper = strtoupper($ext);
   	switch ($upper) {
   		case '.DOCX': return 'fa fa-file-word-o'; break;
   		case '.DOC': return 'fa fa-file-word-o'; break;
   		case '.XLS': return 'fa fa-file-excel-o'; break;
   		case '.XLSX': return 'fa fa-file-excel-o'; break;
   		case '.CSV': return 'fa fa-file-excel-o'; break;
   		case '.PDF': return 'fa fa-file-pdf-o'; break;
   		case '.PPT': return 'fa fa-file-powerpoint-o'; break;
         case '.PPTX': return 'fa fa-file-powerpoint-o'; break;
   		case '.ZIP': return 'fa fa-file-zip-o'; break;
   		case '.RAR': return 'fa fa-file-zip-o'; break;
   		case '.TXT': return 'fa fa-file-text-o'; break;
   		case '.JPG': return 'fa fa-file-image-o'; break;
   		case '.PNG': return 'fa fa-file-image-o'; break;
   		case '.JPEG': return 'fa fa-file-image-o'; break;
   		case '.GIF': return 'fa fa-file-image-o'; break;
   		default : return 'fa fa-question'; break;
   	}
   }

}