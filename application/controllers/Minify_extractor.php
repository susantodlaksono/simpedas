<?php


if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */
require_once APPPATH.'libraries/Minify.php';
require_once APPPATH.'libraries/Minify/JSMin.php';
use MatthiasMullie\Minify;

class Minify_extractor extends MX_Controller {

	public function __construct() {
      parent::__construct();
      $cf = parse_ini_file(CONFIG_FILE, true);
		$this->_apps = $cf['application'];  	
   }

   public function index(){
      $post = $this->input->post();
      if(isset($post['directory']) && $post['directory']){
         $envjsglobal = glob(''.$post['directory'].'*.{js}', GLOB_BRACE);
      }else{
         $envjsglobal = glob('assets/scripts/dev/*.{js}', GLOB_BRACE);
      }
      $i = 0;
      foreach($envjsglobal as $v){
         $globaldevjs[$i]['dev'] = $v;
         $globaldevjs[$i]['prod'] = str_replace('dev', 'prod', $v);
         $i++;
      }
      $data['globaldevjs'] = $globaldevjs;
      $data['template'] = $this->_apps['template'];
		$this->load->view('minify', $data);
   }
   
   public function push(){
      $post = $this->input->post();
      $minifierjs = new Minify\JS();
      $dirreplace = str_replace('dev', 'prod', $post['dir']);
      $minifierjs->add($post['dir']);
      $minifierjs->minify($dirreplace);
      header('Content-Type: application/json');
   	echo json_encode(
         array(
            'success' => TRUE,
            'from' => $post['dir'],
            'to' => $dirreplace,
         )
      );
   }

   // public function test_minify(){
   //    $minifierjs = new Minify\JS();
   //    $minifierjs->add('./assets/scripts/dev/setup.js');
   //    echo $minifierjs->minify('./assets/scripts/prod/setup.js');

   // }

}