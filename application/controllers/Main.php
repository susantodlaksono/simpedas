<?php
if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Main extends MY_Controller {


   public function __construct() {
      parent::__construct();
   }
   
   public function index(){
      $data['_js'] = array(
			'assets/scripts/'.$this->_apps['envjsglobal'].'/pages/main/main.js',
		);		
		foreach ($this->_user_groups as $v) {
         $role_id[] = $v->id; 
      }
		if(in_array(2, $role_id)){
			redirect('news');
		}else{
			$this->render_page($data, 'main');
		}
   }
}