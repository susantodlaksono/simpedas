<?php

class Mv extends MX_Controller {

   public function __construct() {
      parent::__construct();
      // $this->load->helper('url');
      $this->_cf = parse_ini_file(CONFIG_FILE, true);
   }

   public function index(){
      if($this->_cf['application']['maintenance'] == 1) {
         $this->load->view('mv');
      }else{
         redirect('main');
      }
   }

}