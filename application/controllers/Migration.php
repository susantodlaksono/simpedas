<?php


if (!defined('BASEPATH'))
 	exit('No direct script access allowed');


/**
 *
 * @author SUSANTO DWI LAKSONO
 */

use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Writer\Word2007;

class Migration extends MX_Controller {

	public function __construct() {
  		parent::__construct();
	}

	public function read($url = NULL, $filepath = './migrations/', $filename = '_get_file') {
      set_time_limit(0);
      $fullpath = $filepath . $filename;
      if ($url) {
         $file = file_get_contents($url);
         file_put_contents($fullpath, $file);
      }
      $this->load->library('PHPExcel');
      try {
         $result['status'] = TRUE;
         $result['result'] = PHPExcel_IOFactory::load($fullpath);
      }catch (PHPExcel_Reader_Exception $e) {
         $result['status'] = FALSE;
         $result['result'] = 'Error loading file "' . pathinfo($fullpath, PATHINFO_BASENAME) . '": ' . $e->getMessage();
      }
      return $result;
   }

   public function test_email(){
      $this->load->library('email');   
      $this->email->set_mailtype('html');
      $this->email->from('sc.dwilaksono@gmail.com', 'HRIS');
      $this->email->to('susanto@bdg2.ebdesk.com');  
      $this->email->subject('Laporan Kehadiran');
      $this->email->message('test');
      $send = $this->email->send();
      if($send){
         echo 'berhasil';
      }else{
         echo $this->email->print_debugger();
      }
   }

   public function opd_mapping(){
      $kadis = array();
      $pedasi = array();
      $kasie = array();
      $this->db->select('a.group_id, b.email');
      $this->db->join('users as b', 'a.user_id = b.id','left');
      $this->db->where('a.opd_id', 1);
      $this->db->where_in('a.group_id', array(3,4,5));
      $result = $this->db->get('mapping_opd as a')->result_array();
      if($result){
         foreach ($result as $v) {
            if($v['group_id'] == 3){
               $kadis[] = $v['email'];
            }
            if($v['group_id'] == 5){
               $pedasi[] = $v['email'];
            }
            if($v['group_id'] == 4){
               $kasie[] = $v['email'];
            }
         }
         return array(
            'kadis' => $kadis,
            'pedasi' => $pedasi,
            'kasie' => $kasie
         );
      }
   }

   public function coba(){
      $opd = $this->opd_mapping();
      echo implode(',', $opd['kadis']);
      echo implode(',', $opd['pedasi']);
      echo implode(',', $opd['kasie']);
   }

   public function bulk_time(){
      ini_set('memory_limit', '1G');
      $this->load->library('date_extraction');

      $excel_reader = $this->read(NULL, './migration/', 'bulk_time_activity.xlsx');
      if ($excel_reader['status']) {
         $objPHPExcel = $excel_reader['result'];
         $count = 0;
         $i = 0;
         $row = 1;
         $data = array();
         $objPHPExcel->setActiveSheetIndex(0);   
         $worksheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
         foreach ($worksheet as $value) {
            if ($row != 1) {
               $data[$i]['id'] = isset($value['A']) ? $value['A'] : NULL;
               $data[$i]['start_time'] = isset($value['B']) ? $value['B'] : NULL;
               $data[$i]['end_time'] = isset($value['C']) ? $value['C'] : NULL;
            }
            $row++;
            $i++;
         }
         if($data){
            foreach ($data as $v) {
               if($v['id']){
               	$drhours = $this->date_extraction->get_hours_by_time($v['start_time'], $v['end_time']);
               	if($drhours['hours'] >= 9){
		               $drhours['hours'] = ($drhours['hours'] - 1);
		            }
		            $drminutes = $drhours['diff'];
		            if($drminutes == 15){
		               $durationtime = 25;
		            }
		            else if($drminutes == 30){
		               $durationtime = 50;
		            }
		            else if($drminutes == 45){
		               $durationtime = 75;
		            }else{
		               $durationtime = NULL;
		            }
		            $durationhours = $drhours['hours'];
		            if($durationtime){
		               $durationfulltime = $drhours['hours'].'.'. $durationtime;
		            }else{
		               $durationfulltime = $drhours['hours'];
		            }
                	$tmp = array(
                     'duration_hours' => $durationhours,
                     'duration_time' => $durationtime,
                     'duration_fulltime' => $durationfulltime,
                  );
                  $insert = $this->db->update('project_task_activity', $tmp, array('id' => $v['id']));
                  $insert ? $count++ : FALSE;
               }
            }
            echo $count.' Data Updated';
         }else{
         	echo 'No Data Found';
         }
      }else{
         echo json_encode($excel_reader);
      }
   }

   public function validate_qc_status(){
      $activity = $this->get_activity();
      echo json_encode($activity);
   }

   public function get_activity(){
      $this->db->select('id, task_id,qc_approval_activity_id,leader_approved');
      $this->db->where('qc_approval_activity_id IS NULL');
      $this->db->where('leader_approved IS NOT NULL');
      return $this->db->get('project_task_activity')->result_array();
   }

   public function send_telegram(){
      $this->load->library('telegram');
      $body = '';
      $body .= '<b>bold</b>, <strong>bold</strong>';
      $body .= '<i>italic</i>, <em>italic</em>';
      $body .= '<u>underline</u>, <ins>underline</ins>';
      $body .= '<s>strikethrough</s>, <strike>strikethrough</strike>, <del>strikethrough</del>';
      $body .= '<b>bold <i>italic bold <s>italic bold strikethrough</s> <u>underline italic bold</u></i> bold</b>';
      $body .= '<a href="http://www.example.com/">inline URL</a>';
      $body .= '<a href="tg://user?id=123456789">inline mention of a user</a>';
      $body .= '<code>inline fixed-width code</code>';
      $body .= '<pre>pre-formatted fixed-width code block</pre>';
      $body .= '<pre><code class="language-python">pre-formatted fixed-width code block written in the Python programming language</code></pre>';
      $user = array('@susantodwil13');
      foreach ($user as $v) {
         $result = $this->telegram->send($body);
         echo $result;
      }
   }

   public function telegram(){
      $chat_id = '-798940831';
      $token = '2083024698:AAEC5MRTxP3O7GXhi_oMMxcfRHgw4MoKcvo';
      $url = 'https://api.telegram.org/bot'.$token.'/sendMessage';

      $body = '';
      $body .= '<b>bold</b>, <strong>bold</strong>';
      $body .= '<i>italic</i>, <em>italic</em>';
      $body .= '<u>underline</u>, <ins>underline</ins>';
      $body .= '<s>strikethrough</s>, <strike>strikethrough</strike>, <del>strikethrough</del>';
      $body .= '<b>bold <i>italic bold <s>italic bold strikethrough</s> <u>underline italic bold</u></i> bold</b>';
      $body .= '<a href="http://www.example.com/">inline URL</a>';
      $body .= '<a href="tg://user?id=123456789">inline mention of a user</a>';
      $body .= '<code>inline fixed-width code</code>';
      $body .= '<pre>pre-formatted fixed-width code block</pre>';
      $body .= '<pre><code class="language-python">pre-formatted fixed-width code block written in the Python programming language</code></pre>';
      // $body .= '-----------------------------------------------------------'.PHP_EOL;
      // $body .= '```';
      // $body .= '<table>';
      // $body .= '<tr>';
      // $body .= '<td>satu</td>';
      // $body .= '<td>dua</td>';
      // $body .= 'Permohonan : <b>Permohonan 1</b>'.PHP_EOL;
      // $body .= 'Requestor : @susantodwil13';
      // $body .= '</tr>';
      // $body .= '</table>';
      // $body .= '```';
      // $body .= '-----------------------------------------------------------'.PHP_EOL;
      $data = array(
         'chat_id' => $chat_id,
         'text' => $body,
         'parse_mode' => 'HTML'
      );

      $options = array(
         'http' => array(
            'method' =>'POST',
            'header'=> "Content-Type:application/x-www-form-urlencoded\r\n",
            'content'=>http_build_query($data)
         )
      );
      $context = stream_context_create($options);
      $result = file_get_contents($url, false, $context);
      echo print_r($result);
   }

   public function telegram_lib(){
      $this->load->library('telegram');
      $body = '';
      $body .= '<b>PERMOHONAN DATA</b>'.PHP_EOL;
      $body .= '<i>10 Sept 13:00</i>'.PHP_EOL;
      $body .= '----------------------------'.PHP_EOL;
      $body .= '<b>OPD :</b>OPD A '.PHP_EOL;
      $body .= 'OPD A '.PHP_EOL;
      $body .= '<b>Pedasi :</b> <i>@susantodwil13</i> '.PHP_EOL;
      $body .= '<b>Permohonan Data :</b> <i>Permohonan Data Karyawan</i> '.PHP_EOL;
      $body .= '<b>Periode Data :</b> <i>10 Sept sd 11 Sept</i> '.PHP_EOL;
      $body .= '<b>Batas Waktu Pengumpulan :</b> <i>10 Sept</i> '.PHP_EOL;
      $body .= '<b>Status :</b> <i>Proses Disposisi</i> '.PHP_EOL;
      $body .= '--<b><u>10 Sept 13:00</u></b>--'.PHP_EOL;
      $result = $this->telegram->message($body);
      if($result['ok']){
         echo json_encode($result);
      }else{
         echo json_encode($result['description']);
      }
   }

   public function test_encode(){
      $this->_numcode = new Model_utils_numcode("4fFiV8kRTvm5MPNDcyO1dg7lr20Qtn3X6pKLZUqaEsxCwubGYIzhSWJojHeA9B-+/.");
      $num = 'dms/pedasi/42/Folder+1/LICENSE.txt';
      $encoded = base64_encode($num);
      $decoded = base64_decode($encoded);
      echo 'hasil encode : '.$encoded;
      echo '<br>';
      echo 'hasil decode : '.$decoded;
      echo '<br>';
      echo 'total karakter : '.strlen($encoded);
   }

   public function zip_folder(){
      $this->load->library('zip');
      $this->zip->read_dir('dms/pedasi/42/Folder 1/Folder 2', FALSE);
      $this->zip->read_file('dms/pedasi/42/Folder 1/LICENSE.txt');
      $this->zip->read_file('dms/pedasi/42/Folder 1/CodeIgniter-3.1.11-userguide.zip');

      $this->zip->download('Test.zip'); 
   }

   public function read_doc(){
      $filename = 'dms/pedasi/42/Folder 1/wew.docx';
      $filename = realpath($filename);

      $file_extension = strtolower(substr(strrchr($filename,"."),1));

      switch ($file_extension) {
          case "pdf": $ctype="application/pdf"; break;
          case "exe": $ctype="application/octet-stream"; break;
          case "zip": $ctype="application/zip"; break;
          case "doc": $ctype="application/octet-stream"; break;
          case "docx": $ctype="application/octet-stream"; break;
          case "xls": $ctype="application/vnd.ms-excel"; break;
          case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
          case "gif": $ctype="image/gif"; break;
          case "png": $ctype="image/png"; break;
          case "jpe": case "jpeg":
          case "jpg": $ctype="image/jpg"; break;
          default: $ctype="application/force-download";
      }

      if (!file_exists($filename)) {
          die("NO FILE HERE");
      }

      header("Pragma: public");
      header("Expires: 0");
      header('Content-disposition: inline');
      header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
      header("Cache-Control: private",false);
      header("Content-Type: $ctype");
      header("Content-Disposition: attachment; filename=\"".basename($filename)."\";");
      header("Content-Transfer-Encoding: binary");
      header("Content-Length: ".@filesize($filename));
      set_time_limit(0);
      @readfile("$filename") or die("File not found.");
   }

   public function sample_unlink(){
      unlink('./download/6183b947d70c1.zip');
   }

   public function limit_text() {
      $text = 'Permohanan data karyawan PT EBDESK TEKNOLOGI INDONESIA JAKARTA PUSAT MARTAPURA';
    $strings = $text;
      if (strlen($text) > 4) {
          $words = str_word_count($text, 2);
          $pos = array_keys($words);
          if(sizeof($pos) >4)
          {
            $text = substr($text, 0, $pos[4]) . '...';
          }
          // return $text;
      }
      echo $text;
    }

   public function str_sample(){
      $str = 'dms/pedasi/42/Test/List+Media+IMA+20200220.xlsx';
      echo urlencode($str);
   }

   public function reader(){
      

      // $keyword = "png";
      $folder = 'dms/pedasi/42';
      $recursive = new RecursiveDirectoryIterator("$folder");
      $i = 0;
      
      foreach(new RecursiveIteratorIterator($recursive) as $file){
         // if (!((stripos($file, $keyword)) === false) || empty($keyword)){
            $dir = preg_replace("#/#", "/", $file);
            $data[] = $dir;
            // if(is_file($dir)){
            //    $explode = explode('/', $dir);
            //    $exp_limit = explode('/', $dir, 4);
            //    $num = end($explode);

            //    $explode_exp_limit = explode('/', $exp_limit[3]);
            //    foreach (array_keys($explode_exp_limit,  $num, true) as $key) {
            //        unset($explode_exp_limit[$key]);
            //    }
            //    $implode_final = implode('/', $explode_exp_limit);

            //    $result[$i]['directory'] = $dir;
            //    $result[$i]['filename'] = $num;
            //    if($exp_limit[3] == $num){
            //       $result[$i]['path'] = null;
            //    }else{
            //       $result[$i]['path'] = $exp_limit[3];
            //    }
            //    if($implode_final){
            //       $result[$i]['imp_final'] = urlencode($implode_final);
            //    }else{
            //       $result[$i]['imp_final'] = null;
            //    }

            //    $i++;
            // }
         // }
      }
      // sort($result);
      echo json_encode($data);
   }

   public function dir_sim(){
      $str = 'Data+1/Data+2/Data+3/Data+4/Data+5/Data+6';
      $exploded = explode('/', $str);
      $key = array_search('Data+5', $exploded);
      $offset = ($key + 1);
      $implode = implode('/', $exploded);
      $finalexp = explode('/', $implode, $offset);
      $numend = end($finalexp);
      foreach (array_keys($finalexp,  $numend, true) as $k) {
          unset($finalexp[$k]);
      }
      echo json_encode($finalexp);
   }
}